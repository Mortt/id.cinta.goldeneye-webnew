﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class SalesDetail
    {
        public int TransID { get; set; }
        public string TransCode { get; set; }
        public string Sales_Transdate { get; set; }
        public string Sales_Merchant_ID { get; set; }
        public int Sales_Customer_ID { get; set; }
        public int Sales_Type_ID { get; set; }
        public string Sales_Address { get; set; }
        public string Sales_Contact_ID { get; set; }
        public string Sales_Delivery_ID { get; set; }
        public string Sales_Delivery_Contact { get; set; }
        public string Sales_Delivery_Name { get; set; }
        public DateTime Sales_Delivery_InsertOn { get; set; }
        public int Sales_Promo_ID { get; set; }
        public int Sales_Table_No { get; set; }
        public string Sales_Promotion_Amout { get; set; }
        public string Sales_Gross_Amount { get; set; }
        public string Sales_PPN_Amount { get; set; }
        public string Sales_Nett_amount { get; set; }
        public string Sales_Total_Amount { get; set; }
        public string Sales_Total_Qty { get; set; }
        public string Sales_Desc { get; set; }
        public string Sales_isServed { get; set; }
        public DateTime Sales_ServedOn { get; set; }
        public string Sales_ServedBy { get; set; }
        public string Sales_isPaid { get; set; }
        public bool Sales_IsDelete { get; set; }
        public DateTime Sales_InsertOn { get; set; }
        public string Sales_InsertBy { get; set; }
        public DateTime Sales_UpdateOn { get; set; }
        public string Sales_UpdateBy { get; set; }
        public string Insert_Oleh { get; set; }
        public List<_B> B { get; set; }
    }

    public class _B
    {
        public string Sales_No { get; set; }
        public string Sales_Item_ID { get; set; }
        public string Sales_Item_Name { get; set; }
        public string Sales_Qty { get; set; }
        public string Sales_Price { get; set; }
        public string Sales_PriceHPP { get; set; }
        public string Sales_Discount { get; set; }
        public string Sales_LineTotal { get; set; }
        public List<_C> C { get; set; }
    }

    public class _C
    {
        public string Item_Desc { get; set; }
        public string Item_Image1 { get; set; }
        public string Item_Image2 { get; set; }
        public string Item_Name { get; set; }
        public List<_InsertBy> InsertBy { get; set; }
    }

    public class _InsertBy
    {
        public string Insert_By { get; set; }
        public string Insert_Pict { get; set; }
        public DateTime Insert_On { get; set; }
        public List<_UpdateBy> UpdateBy { get; set; }
    }

    public class _UpdateBy
    {
        public string Update_By { get; set; }
        public string Update_Pict { get; set; }
        public DateTime Update_On { get; set; }
    }
}