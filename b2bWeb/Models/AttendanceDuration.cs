﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class JsonAttendanceDuration
    {
        public int total { get; set; }
        public List<rowsAttendanceDuration> rows { get; set; }
    }
    public class rowsAttendanceDuration
    {
        public int Visit_ID { get; set; }
        public int Visit_CompanyID { get; set; }
        public DateTime Visit_InsertOn { get; set; }
        public string Month { get; set; }
        public string Visit_NIK { get; set; }
        public string Employee_Name { get; set; }
        public string Visit_Checkin { get; set; }
        public string Visit_Checkout { get; set; }
        public string Duration { get; set; }
        public string TotalDay { get; set; }
        public string Role_Name { get; set; }
        public string Total_CheckIn { get; set; }
        public string Average { get; set; }
        public string AverageDuration { get; set; }        
        public string TC_Qty { get; set; }
        public string Achievement { get; set; }
    }
}