﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Route
    {        
        public int? Route_ID { get; set; }
        [Required]
        [DisplayName("employee")]
        public string Route_EmployeeNik { get; set; }
        [Required]
        [DisplayName("queue")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public int Route_Queque { get; set; }
        public int? Route_Day { get; set; }
        public DateTime Route_SuggestionTime { get; set; }
        public string Route_Repeat { get; set; }
        public string Repeat_Name { get; set; }
        //[Required]
        //[DisplayName("area")]
        public string Route_AreaID { get; set; }
        public string Nama_SPG { get; set; }
        public string Area_Nama { get; set; }
        public string Insert_Oleh { get; set; }
        public string Update_Oleh { get; set; }        
        public DateTime Route_InsertOn { get; set; }
        public int Route_CompanyID { get; set; }
        [Required]
        [DisplayName("route by")]
        public bool Route_Type { get; set; }
        public string Route_InsertBy { get; set; }
        public DateTime Route_UpdateOn { get; set; }
        public string Route_UpdateBy { get; set; }
        public bool? Route_isActive { get; set; }
        public string jml { get; set; }
    }

    public class JsonRoute
    {
        public int total { get; set; }
        public List<rowsRoute> rows { get; set; }
    }

    public class rowsRoute
    {
        public int Route_ID { get; set; }
        public string Route_EmployeeNik { get; set; }
        public int Route_Queque { get; set; }
        public int Route_Day { get; set; }
        public DateTime Route_SuggestionTime { get; set; }
        public string Route_Repeat { get; set; }
        public string Repeat_Name { get; set; }
        public string Route_AreaID { get; set; }
        public string Area_Nama { get; set; }
        public string Nama_SPG { get; set; }
        public string Insert_Oleh { get; set; }
        public string Update_Oleh { get; set; }
        public DateTime Route_InsertOn { get; set; }
        public int Route_CompanyID { get; set; }
        public bool Route_Type { get; set; }
        public string Route_InsertBy { get; set; }
        public DateTime Route_UpdateOn { get; set; }
        public string Route_UpdateBy { get; set; }
        public bool Route_isActive { get; set; }
    }
}