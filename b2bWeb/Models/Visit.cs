﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Visit
    {
        public double Visit_ID { get; set; }
        public string Visit_Code { get; set; }
        public string Visit_Nik { get; set; }
        public string Visit_AreaID { get; set; }
        public string Visit_Image { get; set; }
        public string Visit_Desc { get; set; }
        public string Visit_RespondID { get; set; }
        public string Visit_Checkin { get; set; }
        public string Visit_Checkout { get; set; }
        public string Visit_Duration { get; set; }
        public double Visit_Longitude { get; set; }
        public double  Visit_Latitude { get; set; }
        public double Visit_Delta { get; set; }
        public DateTime Visit_InsertOn { get; set; }
        public string Visit_InsertBy { get; set; }
        public DateTime Visit_UpdateOn { get; set; }
        public string Visit_UpdateBy { get; set; }
        public bool Visit_Isdelete { get; set; }
        public string Visit_Area_Name { get; set; } 
        public string Visit_Insert_Name { get; set; }
        public string Visit_Update_Name { get; set; }
        public string Visit_Outlet_Code { get; set; }
        public string Visit_Outlet_Name { get; set; }
        public string Visit_Respond_Name { get; set; }
        public int Visit_Durations { get; set; }        
    }

    public class JsonVisit
    {
        public int total { get; set; }
        public List<rowsVisit> rows { get; set; }
    }

    public class rowsVisit
    {
        public int Visit_ID { get; set; }
        public string Visit_Code { get; set; }
        public string Visit_Nik { get; set; }
        public string Visit_AreaID { get; set; }
        public string Visit_Image { get; set; }
        public string Visit_Desc { get; set; }
        public string Visit_RespondID { get; set; }
        public string Visit_Checkin { get; set; }
        public string Visit_Checkout { get; set; }
        public string Visit_Duration { get; set; }
        public double Visit_Longitude { get; set; }
        public double Visit_Latitude { get; set; }
        public double Visit_Delta { get; set; }
        public DateTime Visit_InsertOn { get; set; }
        public string Visit_InsertBy { get; set; }
        public DateTime Visit_UpdateOn { get; set; }
        public string Visit_UpdateBy { get; set; }
        public bool Visit_Isdelete { get; set; }
        public string Visit_Area_Name { get; set; }
        public string Visit_Insert_Name { get; set; }
        public string Visit_Update_Name { get; set; }
        public string Visit_Outlet_Code { get; set; }
        public string Visit_Outlet_Name { get; set; }
        public string Visit_Respond_Name { get; set; }
        public int Visit_Durations { get; set; }
    }
}