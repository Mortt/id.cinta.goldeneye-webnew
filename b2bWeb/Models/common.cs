﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class common
    {
        public string Get_Type { set; get; }
        public string Search { set; get; }
        public DateTime FromDate { set; get; }
        public DateTime ToDate { set; get; }
        public int Company_ID { set; get; }
    }
}