﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace b2bWeb.Models
{
    public class GroupOutletDB
    {        
        //Return list of all Items  
        public List<GroupOutlet> ListAll()
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            List<GroupOutlet> result = null;
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetGroupOutlet_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;
                    Parameter data = new Parameter
                    {
                        Search = "{}",
                        Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                    };

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            result = JsonConvert.DeserializeObject<List<GroupOutlet>>(s, settings);
                            return result.ToList<GroupOutlet>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            return result;
        }

        public List<JsonGroupOutlet> GetDataGroupOutlet(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetGroupOutlet_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonGroupOutlet>>(s, settings);
                            return result.ToList<JsonGroupOutlet>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        //Method for Adding and Update  
        public bool AddUpdate(GroupOutlet itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_GO_Json = JsonConvert.SerializeObject(itm); ;
                SqlCommand com = new SqlCommand("Ins_M_GroupOutlet_BE", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_GroupOutlet", M_GO_Json);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        //Method for Deleting an Visit  
        public bool Delete(GroupOutlet itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            string M_ProfileJson = JsonConvert.SerializeObject(itm);            
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Del_M_GroupOutlet_BE", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_GroupOutlet", M_ProfileJson);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        public int Cek(GroupOutlet data)
        {
            int i;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_GroupOutlet", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("GroupOutlet_Code", data.GroupOutlet_Code);
                    com.Parameters.AddWithValue("Company_ID", Convert.ToInt32(HttpContext.Current.Session["CompanyID"]));
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<GroupOutlet> results = JsonConvert.DeserializeObject<IEnumerable<GroupOutlet>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}