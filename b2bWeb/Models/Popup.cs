﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace b2bWeb.Models
{
    public class Popup
    {
        public int? Pop_ID { set; get; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets allowed")]
        public string Pop_Name { set; get; }
        [Required]
        [DisplayName("module")]
        public string Pop_Module { set; get; }
        public string Pop_Pict { set; get; }
        public string Pop_Text { set; get; }
        public int Pop_Tipe { set; get; }
        public int Pop_CompanyID { set; get; }
        public bool? Pop_IsActive { set; get; }
        public DateTime Pop_InsertOn { set; get; }
        public string Pop_InsertBy { set; get; }
        public string Pop_InsertBy_Name { set; get; }        
        public DateTime Pop_EditOn { set; get; }
        public string Pop_EditBy { set; get; }
        public string Pop_EditBy_Name { set; get; }        
        public bool Pop_Isdelete { set; get; }
        public int Jml { set; get; }
    }

    public class JsonPopup {
        public int total { get; set; }
        public List<rowsPopup> rows { get; set; }
    }

    public class rowsPopup
    {
        public int Pop_ID { set; get; }
        public string Pop_Name { set; get; }
        public string Pop_Module { set; get; }
        public string Pop_Pict { set; get; }
        public string Pop_Text { set; get; }
        public int Pop_Tipe { set; get; }
        public int Pop_CompanyID { set; get; }
        public bool Pop_IsActive { set; get; }
        public DateTime Pop_InsertOn { set; get; }
        public string Pop_InsertBy { set; get; }
        public string Pop_InsertBy_Name { set; get; }
        public DateTime Pop_EditOn { set; get; }
        public string Pop_EditBy { set; get; }
        public string Pop_EditBy_Name { set; get; }
        public bool Pop_Isdelete { set; get; }
    }
}