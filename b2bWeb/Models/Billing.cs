﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Billing
    {
        public string Tahun { get; set; }
        public string Bulan { get; set; }       
        public string Display_InsertBy { get; set; }     
        public string Employee_Name { get; set; }
        public string Employee_LeaderNik { get; set; }
        public string Employee_isB2b { get; set; }        
        public bool Employee_IsActive { get; set; }
    }
}