﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class EmployeeDB
    {
        //declare connection string  
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        //Return list of all Employees  
        public List<Employee> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Employee_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Employee> result = JsonConvert.DeserializeObject<IEnumerable<Employee>>(s, settings);
                        return result.ToList<Employee>();
                    }
                }

            }
            return new List<Employee>();
        }

        public List<JsonEmployee> GetDataEmployee(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_Employee_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonEmployee>>(s, settings);
                            return result.ToList<JsonEmployee>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }        

        public int AddUd(Employee emp)
        {
            emp.Employee_Password = Base64Encode(emp.Employee_Password);
            string M_ProfileJson = JsonConvert.SerializeObject(emp);
            int i;

            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Ins_M_Profile_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_ProfileJson", M_ProfileJson);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Employee  
        public int Delete(Employee emp)
        {
            string M_ProfileJson = JsonConvert.SerializeObject(emp);
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("[Del_M_Employee]", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_ProfileJson", M_ProfileJson);               
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Cek(Employee ID)
        {            
            int i;
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_Employee", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("EmployeeNIK", ID.Employee_NIK);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<Employee> results = JsonConvert.DeserializeObject<IEnumerable<Employee>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}