﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Omset
    {
        public string Outlet_Code { set; get; }
        public string Outlet_Name { set; get; }
        public string Product_Code { set; get; }
        public string Product_Name { set; get; }
        public string Employee_NIK { set; get; }
        public string Employee_Name { set; get; }
        public int Sales_Qty { set; get; }
        public decimal Sales_LineTotal { set; get; }
    }
}