﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class StockReport
    {
        public string Outlet_Code { set; get; }
        public string Outlet_Name { set; get; } 
        public string Product_Code { set; get; }
        public string Product_Name { set; get; }
        public string Employee_NIK { set; get; }
        public string Employee_Name { set; get; }
        public DateTime Sales_TransDate { set; get; }
        public int Opname_stock { set; get; }
        public int Sales_Qty { set; get; }
        public int Sisa_Stock { set; get; }
        public decimal Sales_LineTotal { set; get; }
        public int Opname_Suggestion { set; get; }
        public int Opname_Delta { set; get; }
    }

    public class JsonStockReport
    {
        public int total { get; set; }
        public List<rowsStockReport> rows { get; set; }
    }

    public class rowsStockReport
    {
        public string Outlet_Code { set; get; }
        public string Outlet_Name { set; get; }
        public string Product_Code { set; get; }
        public string Product_Name { set; get; }
        public string Employee_NIK { set; get; }
        public string Employee_Name { set; get; }
        public DateTime Sales_TransDate { set; get; }
        public int Opname_stock { set; get; }
        public int Sales_Qty { set; get; }
        public int Sisa_Stock { set; get; }
        public decimal Sales_LineTotal { set; get; }
        public int Opname_Suggestion { set; get; }
        public int Opname_Delta { set; get; }
    }
}