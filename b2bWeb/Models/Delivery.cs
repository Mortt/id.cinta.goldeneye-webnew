﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Delivery
    {
        public int? TransID { get; set; }
        public string TransCode { get; set; }
        [Required]
        [DisplayName("DO date")]
        public DateTime DO_Transdate { get; set; }
        [Required]
        [DisplayName("outlet")]
        public string DO_OutletCode { get; set; }
        public string Outlet_Name { get; set; }
        [Required]
        [DisplayName("employee")]
        public string DO_PIC { get; set; }
        public string DO_PIC_Name { get; set; }        
        public string DO_Address { get; set; }
        public decimal DO_Total_Amount { get; set; }
        public int DO_Total_Qty { get; set; }
        public string DO_Desc { get; set; }
        public int DO_CompanyID { get; set; }        
        public string DO_Capture_Image { get; set; }
        public string Username { get; set; }    
        public string GetType { get; set; }
        public string DO_Status { get; set; }
        public string Respond_Name { get; set; }        
        public int CompanyID { get; set; }
        public string Search { get; set; }
        public DateTime DO_InsertOn { get; set; }
        public string DO_InsertBy { get; set; }
        public string DO_InsertBy_Name { get; set; }
        public DateTime DO_UpdateOn { get; set; }
        public string DO_UpdateBy { get; set; }
        public string DO_UpdateBy_Name { get; set; }
        [Required]
        [DisplayName("product / item")]
        public string Numpang { get; set; }
        public string QTY { get; set; }
        public int jml { get; set; }
        public List<_Z> Z { get; set; }
    }

    public class JsonDelivery
    {
        public int total { get; set; }
        public List<rowsDelivery> rows { get; set; }
    }

    public class rowsDelivery
    {
        public int TransID { get; set; }
        public string TransCode { get; set; }
        public DateTime DO_Transdate { get; set; }
        public string DO_OutletCode { get; set; }
        public string Outlet_Name { get; set; }
        public string DO_PIC { get; set; }
        public string DO_PIC_Name { get; set; }
        public string DO_Address { get; set; }
        public decimal DO_Total_Amount { get; set; }
        public int DO_Total_Qty { get; set; }
        public string DO_Desc { get; set; }
        public int DO_CompanyID { get; set; }
        public string DO_Capture_Image { get; set; }
        public string Username { get; set; }
        public string GetType { get; set; }
        public string DO_Status { get; set; }
        public bool DO_SendingStatus { get; set; }
        public string Respond_Name { get; set; }
        public int CompanyID { get; set; }
        public string Search { get; set; }
        public DateTime DO_InsertOn { get; set; }
        public string DO_InsertBy { get; set; }
        public string DO_InsertBy_Name { get; set; }
        public DateTime DO_UpdateOn { get; set; }
        public string DO_UpdateBy { get; set; }
        public string DO_UpdateBy_Name { get; set; }
        public string Numpang { get; set; }
        public string QTY { get; set; }
        public List<_Z> Z { get; set; }
    }

    public class _Z
    {        
        public int TransID { get; set; }
        public int idx { get; set; }
        public int DO_No { get; set; }
        public int DO_Item_ID { get; set; }
        public string DO_Item_Name { get; set; }
        public int DO_Qty { get; set; }
        public decimal DO_Price { get; set; }
        public decimal DO_Discount { get; set; }
        public decimal DO_LineTotal { get; set; }
        public List<_P> P { get; set; }
    }

    public class _P
    {
        public string Item_Image1 { get; set; }
        public string Item_Name { get; set; }
    }
}