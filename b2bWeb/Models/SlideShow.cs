﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class SlideShow
    {
        public int? SDS_IDX { set; get; }
        public string SDS_NAME { set; get; }
        public string SDS_URL { set; get; }
        public string SDS_WEBSITE { set; get; }
        public string SDS_DESC { set; get; }
        public bool? SDS_isPUBLISH { set; get; }
        public string SDS_Application { set; get; }
        public int SDS_CompanyID { set; get; }
        public DateTime SDS_INSERTON { set; get; }
        public string SDS_INSERTBY { set; get; }
        public DateTime SDS_UPDATEON { set; get; }
        public string SDS_UPDATEBY { set; get; }
        public bool SDS_ISDELETE { set; get; }
        public string Images_Name { get; set; }
    }

    public class JsonSlideShow
    {
        public int total { get; set; }
        public List<rowsSlideShow> rows { get; set; }
    }
    public class rowsSlideShow
    {
        public int SDS_IDX { set; get; }
        public string SDS_NAME { set; get; }
        public string SDS_URL { set; get; }
        public string SDS_WEBSITE { set; get; }
        public string SDS_DESC { set; get; }
        public bool SDS_isPUBLISH { set; get; }
        public string SDS_Application { set; get; }
        public int SDS_CompanyID { set; get; }
        public DateTime SDS_INSERTON { set; get; }
        public string SDS_INSERTBY { set; get; }
        public DateTime SDS_UPDATEON { set; get; }
        public string SDS_UPDATEBY { set; get; }
        public bool SDS_ISDELETE { set; get; }
        public string AddOrUp { get; set; }
        public string Images_Name { get; set; }
        public string images { get; set; }
    }
}