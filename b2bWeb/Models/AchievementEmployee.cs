﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class AchievementEmployee
    {
        public int TargetByEmployee_ID { set; get; }
        public DateTime TargetByEmployee_Periode { set; get; }
        public string TargetByEmployee_EmployeeNIK { set; get; }
        public decimal TargetByEmployee_Amount { set; get; }
        public int TargetByEmployee_CompanyID { set; get; }
        public string Tahun { set; get; }
        public string Bulan { set; get; }
        public decimal Sales_LineTotal { set; get; }
        public int Sales_QTY { set; get; }
        public string Target_EmployeeName { set; get; }
        public decimal Achievements { set; get; }
        public string Employee_NIK { set; get; }
        public string Employee_Name { set; get; }
    }

    public class JsonAchievementEmployee
    {
        public int total { get; set; }
        public List<rowsAchievementEmployee> rows { get; set; }
    }
    public class rowsAchievementEmployee
    {
        public int TargetByEmployee_ID { set; get; }
        public DateTime TargetByEmployee_Periode { set; get; }
        public string TargetByEmployee_EmployeeNIK { set; get; }
        public decimal TargetByEmployee_Amount { set; get; }
        public int TargetByEmployee_CompanyID { set; get; }
        public string Tahun { set; get; }
        public string Bulan { set; get; }
        public decimal Sales_LineTotal { set; get; }
        public int Sales_QTY { set; get; }
        public string Target_EmployeeName { set; get; }
        public decimal Achievements { set; get; }
        public string Employee_NIK { set; get; }
        public string Employee_Name { set; get; }
    }
}