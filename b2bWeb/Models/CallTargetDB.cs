﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace b2bWeb.Models
{
    public class CallTargetDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Items  
        public List<CallTarget> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetTargetCall_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<CallTarget> result = JsonConvert.DeserializeObject<IEnumerable<CallTarget>>(s, settings);
                        return result.ToList<CallTarget>();
                    }
                }
            }
            return new List<CallTarget>();
        }
        
        public List<JsonCallTarget> GetDataCallTarget(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetTargetCall_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonCallTarget>>(s, settings);
                            return result.ToList<JsonCallTarget>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        //Method for Adding and Update  
        public int AddUpdate(CallTarget itm)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_TargetCall_Json = JsonConvert.SerializeObject(itm);
                SqlCommand com = new SqlCommand("Ins_M_TargetCall_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_TargetCall_Json", M_TargetCall_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting
        public int Delete(CallTarget itm)
        {
            string M_TargetCall_Json = JsonConvert.SerializeObject(itm);
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Del_M_TargetCall_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_TargetCall_Json", M_TargetCall_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Cek(CallTarget data)
        {
            int i;
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_TargetCall", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("Role_ID", data.TC_Role_ID);
                    com.Parameters.AddWithValue("periode", data.TC_Periode);
                    com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<CallTarget> results = JsonConvert.DeserializeObject<IEnumerable<CallTarget>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}