﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class Employee
    {        
        [Required]
        [DisplayName("NIK")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public string Employee_NIK { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Employee_Name { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public string Employee_Phone { get; set; }
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Only Email format allowed")]
        public string Employee_Email { get; set; }
        [Required]
        [DisplayName("password")]
        public string Employee_Password { get; set; }
        public string Employee_Photo { get; set; }
        [Required]
        [DisplayName("leader")]
        public string Employee_LeaderNik { get; set; }
        [Required]
        [DisplayName("role")]
        public string Employee_Role { get; set; }
        [Required]
        [DisplayName("access to website")]
        public bool Employee_isB2b { get; set; }
        public string Role_Name { get; set; }
        public int Employee_PrincipalID { get; set; }
        //public string Leader_NIK { get; set; }
        public DateTime Employee_InsertOn { get; set; }
        public int Employee_CompanyID { get; set; }
        public string Employee_InsertBy { get; set; }        
        public DateTime Employee_UpdateOn { get; set; }
        public string Employee_UpdateBy { get; set; }
        public bool? Employee_IsActive { get; set; }
        public string Insert_Name { get; set; }
        public string Update_Name { get; set; }        
        public string DeCode64Pass { get; set; }
        public string jml { get; set; }
    }

    public class JsonEmployee
    {
        public int total { get; set; }
        public List<rowsEmployee> rows { get; set; }
    }

    public class rowsEmployee
    {
        public string Employee_NIK { get; set; }
        public string Employee_Name { get; set; }
        public string Employee_Phone { get; set; }
        public string Employee_Email { get; set; }
        public string Employee_Password { get; set; }
        public string Employee_Photo { get; set; }
        public string Employee_LeaderNik { get; set; }
        public string Employee_Role { get; set; }
        public bool Employee_isB2b { get; set; }
        public string Role_Name { get; set; }
        public int Employee_PrincipalID { get; set; }
        //public string Leader_NIK { get; set; }
        public DateTime Employee_InsertOn { get; set; }
        public int Employee_CompanyID { get; set; }
        public string Employee_InsertBy { get; set; }
        public DateTime Employee_UpdateOn { get; set; }
        public string Employee_UpdateBy { get; set; }
        public bool Employee_IsActive { get; set; }
        public string Insert_Name { get; set; }
        public string Update_Name { get; set; }
        public string DeCode64Pass { get; set; }
        public string jml { get; set; }
    }
}