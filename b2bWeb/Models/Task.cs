﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Task
    {
        public int Task_ID { get; set; }
        public int Task_HTask_ID { get; set; }
        public string Task_Title { get; set; }
        public DateTime Task_Deadline { get; set; }        
        public string Task_Employee_NIK { get; set; }
        public string Task_Description { get; set; }
        public decimal Task_Longitude { get; set; }
        public decimal Task_Latitude { get; set; }
        public string Task_Image1 { get; set; }
        public string Task_Image2 { get; set; }
        public string Task_Image3 { get; set; }
        public int Task_Company_ID { get; set; }
        public int Task_Respond_ID { get; set; }
        public string Task_InsertBy { get; set; }
        public DateTime Task_InsertOn { get; set; }
        public string Task_UpdateBy { get; set; }
        public DateTime Task_UpdateOn { get; set; }
        public bool Task_Isdelete { get; set; }
        public string Task_Employee_Name { get; set; }
        public string Task_InsertBy_Name { get; set; }
        public string Task_UpdateBy_Name { get; set; }
        public string Respond_Name { get; set; }
    }

    public class JsonTask
    {
        public int total { get; set; }
        public List<rowsTask> rows { get; set; }
    }
    public class rowsTask
    {
        public int Task_ID { get; set; }
        public int Task_HTask_ID { get; set; }
        public string Task_Title { get; set; }
        public DateTime Task_Deadline { get; set; }
        public string Task_Employee_NIK { get; set; }
        public string Task_Description { get; set; }
        public decimal Task_Longitude { get; set; }
        public decimal Task_Latitude { get; set; }
        public string Task_Image1 { get; set; }
        public string Task_Image2 { get; set; }
        public string Task_Image3 { get; set; }
        public int Task_Company_ID { get; set; }
        public int Task_Respond_ID { get; set; }
        public string Task_InsertBy { get; set; }
        public DateTime Task_InsertOn { get; set; }
        public string Task_UpdateBy { get; set; }
        public DateTime Task_UpdateOn { get; set; }
        public bool Task_Isdelete { get; set; }
        public string Task_Employee_Name { get; set; }
        public string Task_InsertBy_Name { get; set; }
        public string Task_UpdateBy_Name { get; set; }
        public string Respond_Name { get; set; }
    }
    public class TaskGet
    {
        public string GetType { get; set; }
        public string Search { get; set; }
        public int CompanyID { get; set; }
        public int SVY_ID { get; set; }
    }
}