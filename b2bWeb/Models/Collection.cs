﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Collection
    {
        public string Outlet_Name { get; set; }
        public string Employee_Name { get; set; }
        public string Respond_Name { get; set; }
        public int? TransID { get; set; }
        [Required]
        [DisplayName("invoice no")]
        public string Inv_No { get; set; }
        [Required]
        [DisplayName("amount")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public decimal Inv_Amount { get; set; }
        public decimal Inv_ActualAmount { get; set; }
        [Required]
        [DisplayName("due date")]
        public DateTime Inv_DueDate { get; set; }
        [Required]
        [DisplayName("outlet")]
        public string Inv_OutletCode { get; set; }
        [Required]
        [DisplayName("employee")]
        public string Inv_PIC { get; set; }
        public string Inv_Desc { get; set; }
        public string Inv_Capture_Image { get; set; }        
        public int Inv_CompanyID { get; set; }
        public DateTime Inv_InsertOn { get; set; }
        public string Inv_InsertBy { get; set; }
        public string Inv_InsertBy_Name { get; set; }
        public DateTime Inv_UpdateOn { get; set; }
        public string Inv_UpdateBy { get; set; }
        public string Inv_UpdateBy_Name { get; set; }
        public bool? Inv_IsActive { get; set; }
        public bool Inv_IsDelete { get; set; }        
        public string Search { get; set; }
        public int CompanyID { get; set; }
        public int jml { get; set; }
    }

    public class JsonCollection
    {
        public int total { get; set; }
        public List<rowsCollection> rows { get; set; }
    }

    public class rowsCollection
    {
        public string Outlet_Name { get; set; }
        public string Employee_Name { get; set; }
        public string Respond_Name { get; set; }
        public int TransID { get; set; }
        public string Inv_No { get; set; }
        public decimal Inv_Amount { get; set; }
        public decimal Inv_ActualAmount { get; set; }
        public DateTime Inv_DueDate { get; set; }
        public string Inv_OutletCode { get; set; }
        public string Inv_PIC { get; set; }
        public string Inv_Desc { get; set; }
        public string Inv_Capture_Image { get; set; }
        public bool Inv_CallStatus { get; set; }        
        public int Inv_CompanyID { get; set; }
        public DateTime Inv_InsertOn { get; set; }
        public string Inv_InsertBy { get; set; }
        public string Inv_InsertBy_Name { get; set; }
        public DateTime Inv_UpdateOn { get; set; }
        public string Inv_UpdateBy { get; set; }
        public string Inv_UpdateBy_Name { get; set; }
        public bool Inv_IsDelete { get; set; }
        public bool Inv_IsActive { get; set; }
        public string Search { get; set; }
        public int CompanyID { get; set; }
    }
}