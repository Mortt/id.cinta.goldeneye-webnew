﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class StockReportDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        
        public List<StockReport> ListAll(DateTime StartDate, DateTime EndDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GET_OPNAME_SELL", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("StartDate", StartDate);
                com.Parameters.AddWithValue("EndDate", EndDate);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<StockReport> result = JsonConvert.DeserializeObject<IEnumerable<StockReport>>(s, settings);
                        return result.ToList<StockReport>();
                    }
                }

            }
            return new List<StockReport>();
        }

        public dynamic StockReport2(DateTime StartDate, DateTime EndDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetStock", con);
                com.Parameters.AddWithValue("StartDate", StartDate);
                com.Parameters.AddWithValue("EndDate", EndDate);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                //dt.Columns.Add("OUTLET CODE", typeof(string));
                //dt.Columns.Add("OUTLET NAME", typeof(string));
                //dt.Columns.Add("ITEM CODE", typeof(string));
                //dt.Columns.Add("ITEM NAME", typeof(string));

                //foreach (DataRow objCon in dt.Rows)
                //{
                //    objCon["OUTLET CODE"] = Convert.ToString(objCon["OPNAME_OUTLETCODE"]);
                //    objCon["OUTLET NAME"] = Convert.ToString(objCon["OUTLET_NAME"]);
                //    objCon["ITEM CODE"] = Convert.ToString(objCon["ITEM_CODE"]);
                //    objCon["ITEM NAME"] = Convert.ToString(objCon["ITEM_NAME"]);
                //}

                return dt;
            }
        }

        public List<JsonStockReport> GetDataStockReport(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GET_OPNAME_SELL_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonStockReport>>(s, settings);
                            return result.ToList<JsonStockReport>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }
    }
}