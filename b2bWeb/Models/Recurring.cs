﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Recurring
    {
        public int? Repeat_IDX { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets allowed")]
        public string Repeat_Name { get; set; }
        [Required]
        [DisplayName("repeat type")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets allowed")]
        public string Repeat_Type { get; set; }
        public int Repeat_Every { get; set; }
        public int? Repeat_Every1 { get; set; }
        public int? Repeat_Every2 { get; set; }
        public int? Repeat_Every3 { get; set; }
        public string Repeat_On { get; set; }
        public string RepeatOn_1 { get; set; }
        public string RepeatOn_2 { get; set; }
        public string RepeatOn_3 { get; set; }
        public string RepeatOn_4 { get; set; }
        public string RepeatOn_5 { get; set; }
        public string RepeatOn_6 { get; set; }
        public string RepeatOn_7 { get; set; }
        public string RepeatOn_8 { get; set; }
        public string RepeatOn_9 { get; set; }
        public string RepeatOn_10 { get; set; }
        public string RepeatOn_11 { get; set; }
        public string RepeatOn_12 { get; set; }
        public string RepeatOn_13 { get; set; }
        public string RepeatOn_14 { get; set; }
        public string RepeatOn_15 { get; set; }
        public string RepeatOn_16 { get; set; }
        public string RepeatOn_17 { get; set; }
        public string RepeatOn_18 { get; set; }
        public string RepeatOn_19 { get; set; }
        public string RepeatOn_20 { get; set; }
        public string RepeatOn_21 { get; set; }
        public string RepeatOn_22 { get; set; }
        public string RepeatOn_23 { get; set; }
        public string RepeatOn_24 { get; set; }
        public string RepeatOn_25 { get; set; }
        public string RepeatOn_26 { get; set; }
        public string RepeatOn_27 { get; set; }
        public string RepeatOn_28 { get; set; }
        public string RepeatOn_29 { get; set; }
        public string RepeatOn_30 { get; set; }
        public string RepeatOn_31 { get; set; }

        public int Repeat_CompanyID { get; set; }
        public bool Repeat_Isdelete { get; set; }
        public DateTime Repeat_insertOn { get; set; }
        public string Repeat_InsertBy { get; set; }
        public string Repeat_InsertBy_Name { get; set; }
        public DateTime Repeat_UpdateOn { get; set; }
        public string Repeat_UpdateBy { get; set; }
        public string Repeat_UpdateBy_Name { get; set; }
    }

    public class JsonRecurring {
        public int total { get; set; }
        public List<rowsRecurring> rows { get; set; }
    }

    public class rowsRecurring
    {
        public int Repeat_IDX { get; set; }
        public string Repeat_Name { get; set; }
        public string Repeat_Type { get; set; }
        public int Repeat_Every { get; set; }
        public string Repeat_On { get; set; }
        public int Repeat_CompanyID { get; set; }
        public bool Repeat_Isdelete { get; set; }
        public DateTime Repeat_insertOn { get; set; }
        public string Repeat_InsertBy { get; set; }
        public string Repeat_InsertBy_Name { get; set; }
        public DateTime Repeat_UpdateOn { get; set; }
        public string Repeat_UpdateBy { get; set; }
        public string Repeat_UpdateBy_Name { get; set; }
    }
}