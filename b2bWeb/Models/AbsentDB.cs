﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class AbsentDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        public dynamic Absent(DateTime StartDate, DateTime EndDate, string Search)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_CheckIn_Diff_Report", con);
                com.CommandTimeout = 250;
                com.Parameters.AddWithValue("StartDate", StartDate);
                com.Parameters.AddWithValue("EndDate", EndDate);                
                com.Parameters.AddWithValue("Search", Search);
                com.Parameters.AddWithValue("CompanyID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                if (dt.Rows.Count > 0)
                {
                    //dt.Columns.Add("Date1", typeof(string));
                    //dt.Columns.Add("Duration1", typeof(TimeSpan));
                    ////TimeSpan ts = t1.Subtract(t2);
                    //for (int R=0; R < dt.Rows.Count; R++)
                    //{
                    //    dt.Rows[R]["Date1"] = Convert.ToDateTime(dt.Rows[R]["Date"]).ToString("dd/MM/yyyy");

                    //    string time1 = Convert.ToString(dt.Rows[R]["CheckIn"]);
                    //    DateTime dateTime1 = DateTime.ParseExact(time1, "HH:mm:ss", CultureInfo.InvariantCulture);

                    //    DateTime dateTime2 = new DateTime();
                        
                    //    if (Convert.ToString(dt.Rows[R]["CheckOut"]) != "")
                    //    {
                    //        string time2 = Convert.ToString(dt.Rows[R]["CheckOut"]);
                    //        dateTime2 = DateTime.ParseExact(time2, "HH:mm:ss", CultureInfo.InvariantCulture);
                    //    }              

                    //    TimeSpan ts = dateTime2.Subtract(dateTime1);
                    //    dt.Rows[R]["Duration1"] = ts;
                    //}

                    //dt.Columns.Remove("Date");
                    //dt.Columns.Add("Date", typeof(string));
                    //dt.Columns.Add("Duration2", typeof(string));

                    //for (int R = 0; R < dt.Rows.Count; R++)
                    //{
                    //    if (Convert.ToString(dt.Rows[R]["CheckOut"]) != "")
                    //    {
                    //        dt.Rows[R]["Duration2"] = Convert.ToString(dt.Rows[R]["Duration1"]);
                    //    }
                    //    else
                    //    {
                    //        dt.Rows[R]["Duration2"] = Convert.ToString(dt.Rows[R]["Duration"]);
                    //    }
                    //    dt.Rows[R]["Date"] = Convert.ToString(dt.Rows[R]["Date1"]);
                    //}

                    //dt.Columns.Remove("Duration");
                    //dt.Columns.Add("Duration", typeof(string));

                    //for (int R = 0; R < dt.Rows.Count; R++)
                    //{
                    //    dt.Rows[R]["Duration"] = Convert.ToString(dt.Rows[R]["Duration2"]);
                    //}

                    //dt.Columns.Remove("Date1");
                    //dt.Columns.Remove("Duration2");
                    //dt.Columns.Remove("Duration1");
                    //dt.Columns["Date"].SetOrdinal(0);
                    return dt;
                }

                return dt;
            }
        }

        public dynamic AbsentDay(DateTime StartDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_CheckIn_Diff_Report", con);
                com.CommandTimeout = 250;
                com.Parameters.AddWithValue("StartDate", StartDate);
                com.Parameters.AddWithValue("EndDate", "");
                com.Parameters.AddWithValue("Search", "Day");
                com.Parameters.AddWithValue("CompanyID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                //if (dt.Rows.Count > 0)
                //{                   
                //    return dt;
                //}

                return dt;
            }
        }
    }
}