﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Survey
    {
        public int SVY_ID { get; set; }
        public string SVY_Code { get; set; }
        public string SVY_Subject { get; set; }
        public string SVY_Desc { get; set; }
        public DateTime SVY_End { get; set; }
        public int SVY_CompanyID { get; set; }
        public DateTime SVY_InsertOn { get; set; }
        public string SVY_InsertBy { get; set; }
        public DateTime SVY_UpdateOn { get; set; }
        public string SVY_UpdateBy { get; set; }
        public bool SVY_IsActive { get; set; }
        public bool SVY_IsDelete { get; set; }
        public string SVY_InsertBy_Name { get; set; }
        public string SVY_UpdateBy_Name { get; set; }
        public string Username { get; set; }
        public string[] Question { get; set; }
        public string[] Answer { get; set; }
        public string[] AnswerType { get; set; }
        public string[] AnswerTypeValue { get; set; }
        public string[] SVY_QuestionID { get; set; }

        public List<SurveyD1> SurveyD1 { get; set; }
    }

    public class JsonSurvey
    {
        public int total { get; set; }
        public List<rowsSurvey> rows { get; set; }
    }

    public class rowsSurvey
    {
        public int SVY_ID { get; set; }
        public string SVY_Code { get; set; }
        public string SVY_Subject { get; set; }
        public string SVY_Desc { get; set; }
        public DateTime SVY_End { get; set; }
        public int SVY_CompanyID { get; set; }
        public DateTime SVY_InsertOn { get; set; }
        public string SVY_InsertBy { get; set; }
        public DateTime SVY_UpdateOn { get; set; }
        public string SVY_UpdateBy { get; set; }
        public bool SVY_IsActive { get; set; }
        public bool SVY_IsDelete { get; set; }
        public string SVY_InsertBy_Name { get; set; }
        public string SVY_UpdateBy_Name { get; set; }
    }

    public class SurveyD1
    {
        public int SVY_ID { get; set; }
        public int SVY_QuestionID { get; set; }
        public string SVY_Question { get; set; }
        public string SVY_Hint { get; set; }
        public string SVY_Type { get; set; }
        public List<SurveyD2> SurveyD2 { get; set; }
    }

    public class SurveyD2
    {
        public int SVY_ID { get; set; }
        public int SVY_QuestionID { get; set; }
        public string SVY_Value { get; set; }
        public string SVY_Desc { get; set; }
        public int SVY_Total { get; set; }
    }

    public class DetailsSurvey
    {
        public int SVY_ID { get; set; }
        //public int SVY_CompanyID { get; set; }
        //public bool SVY_IsActive { get; set; }
        //public DateTime SVY_End { get; set; }
        public string SVY_Desc { get; set; }
        public string SVY_Subject { get; set; }
        public List<DS> DS { get; set; }

    }

    public class DS
    {
        public int Survey_ID { get; set; }
        public string Survey_InsertBy { get; set; }
        public DateTime Survey_InsertOn { get; set; }
        public string Survey_UpdateBy { get; set; }
        public DateTime Survey_UpdateOn { get; set; }
        public List<DS1> DS1 { get; set; }
    }

    public class DS1
    {
        public int Survey_ID { get; set; }
        public string SVY_Question { get; set; }
        public int SVY_QuestionID { get; set; }
        public string SVY_Value { get; set; }
        public string SVY_DescAnswer { get; set; }
        public string SVY_Type { get; set; }
    }
}