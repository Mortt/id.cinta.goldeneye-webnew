﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Sales_Export
    {
        public int TransID { get; set; }
        public string TransCode { get; set; }
        public string Sales_Transdate { get; set; }
        public string Sales_Merchant_ID { get; set; }
        public int Sales_Customer_ID { get; set; }
        public int Sales_Type_ID { get; set; }
        public string Sales_Address { get; set; }
        public string Sales_Contact_ID { get; set; }
        public string Sales_Delivery_ID { get; set; }
        public string Sales_Delivery_Contact { get; set; }
        public string Sales_Delivery_Name { get; set; }
        public DateTime Sales_Delivery_InsertOn { get; set; }
        public int Sales_Promo_ID { get; set; }
        public int Sales_Table_No { get; set; }
        public string Sales_Promotion_Amout { get; set; }
        public string Sales_Gross_Amount { get; set; }
        public string Sales_PPN_Amount { get; set; }
        public string Sales_Nett_amount { get; set; }
        public decimal Sales_Total_Amount { get; set; }
        public int Sales_Total_Qty { get; set; }
        public string Sales_Desc { get; set; }
        public string Sales_isServed { get; set; }
        public DateTime Sales_ServedOn { get; set; }
        public string Sales_ServedBy { get; set; }
        public string Sales_isPaid { get; set; }
        public bool Sales_IsDelete { get; set; }
        public int Sales_CompanyID { get; set; }
        public DateTime Sales_InsertOn { get; set; }
        public string Sales_InsertBy { get; set; }
        public DateTime Sales_UpdateOn { get; set; }
        public string Sales_UpdateBy { get; set; }
        public string Sales_Insert_Name { get; set; }
        public string Sales_Item_ID { set; get; }
        public string Sales_Product_Name { set; get; }
        public int Sales_Qty { set; get; }
        public decimal Sales_Price { set; get; }
        public decimal Sales_LineTotal { set; get; }
        public string Sales_Product_Image { set; get; }
        public string Selling_Outlet { get; set; }
        public string Selling_Outlet_Code { get; set; }
    }


}