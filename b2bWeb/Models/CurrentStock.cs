﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class CurrentStock
    {
        public DateTime Opname_InsertON { set; get; }
        public int Opname_Stock { set; get; }
        public string Opname_ProductCode { set; get; }
        public string Opname_OutletCode { set; get; }
        public int H1_Sales_from_lastOpname { set; get; }
        public string Item_Name { set; get; }
        public string Outlet_Name { set; get; }
        public int Company_ID { set; get; }
        public DateTime STOCK_INSERTON { set; get; }
        public int Current_Stock { set; get; }
    }
    public class JsonCurrentStock
    {
        public int total { get; set; }
        public List<rowsCurrentStock> rows { get; set; }
    }
    public class rowsCurrentStock
    {
        public DateTime Opname_InsertON { set; get; }
        public int Opname_Stock { set; get; }
        public string Opname_ProductCode { set; get; }
        public string Opname_OutletCode { set; get; }
        public int H1_Sales_from_lastOpname { set; get; }
        public string Item_Name { set; get; }
        public string Outlet_Name { set; get; }
        public int Company_ID { set; get; }
        public DateTime STOCK_INSERTON { set; get; }
        public int Current_Stock { set; get; }
    }
}