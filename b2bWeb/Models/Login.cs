﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public partial class Login
    {
        [Required]
        [DisplayName("username")]
        public string Employee_NIK { get; set; }
        public string Employee_Name { get; set; }
        public string Employee_Photo { get; set; }
        public string Employee_Email { get; set; }
        public string Employee_Phone { get; set; }
        public string Employee_LeaderNik { get; set; }
        public string Employee_Role { get; set; }
        public bool Employee_isB2b { get; set; }
        [Required]
        [DisplayName("password")]
        public string Employee_Password { get; set; }
        public int Employee_CompanyID { get; set; }
        public string Company_Logo { get; set; }
        public string Company_Packet_ID { get; set; }
    }

    public partial class LoginPacket
    {        
        public int PacketID { get; set; }
        public string Search { get; set; }
    }
}