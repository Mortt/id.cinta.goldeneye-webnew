﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class OmsetDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        public List<Omset> ListAll(DateTime StartDate, DateTime EndDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GET_OPNAME_SELL_SUMMARY", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("StartDate", StartDate);
                com.Parameters.AddWithValue("EndDate", EndDate);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Omset> result = JsonConvert.DeserializeObject<IEnumerable<Omset>>(s, settings);
                        return result.ToList<Omset>();
                    }
                }

            }
            return new List<Omset>();
        }

        public dynamic DailySell(DateTime StartDate, DateTime EndDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GET_OPNAME_SELL_SUMMARY_COLUMN", con);
                com.Parameters.AddWithValue("StartDate", StartDate);
                com.Parameters.AddWithValue("EndDate", EndDate);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());
                dt.Columns.Add("Sales_LineTotal1", typeof(string));

                CultureInfo cultureInfo = new CultureInfo("id-ID");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["Sales_LineTotal1"] = string.Format(cultureInfo, "{0:C}", dt.Rows[i]["Sales_LineTotal"]);
                }

                dt.Columns.Remove("Sales_LineTotal");
                dt.Columns["Sales_LineTotal1"].ColumnName = "Sales_LineTotal";

                dt.Columns["Sales_LineTotal"].SetOrdinal(6);

                return dt;
            }
        }

    }
}