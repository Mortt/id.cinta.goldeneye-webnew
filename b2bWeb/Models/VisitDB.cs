﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class VisitDB
    {
        //declare connection string  
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Items  
        public List<Visit> ListAll(string Bulan, string Tahun)
        {           
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetVisit_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.AddWithValue("Bulan", Bulan);
                com.Parameters.AddWithValue("Tahun", Tahun);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Visit> result = JsonConvert.DeserializeObject<IEnumerable<Visit>>(s, settings);
                        return result.ToList<Visit>();
                    }
                }

            }
            return new List<Visit>();
        }

        public List<Visit> ListExport(DateTime g, DateTime c)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Export_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("tipe", "VISIT");
                com.Parameters.AddWithValue("startdate", g);
                com.Parameters.AddWithValue("enddate", c);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Visit> result = JsonConvert.DeserializeObject<IEnumerable<Visit>>(s, settings);
                        return result.ToList<Visit>();
                    }
                }

            }
            return new List<Visit>();
        }

        public List<JsonVisit> GetDataVisit(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetVisit_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonVisit>>(s, settings);
                            return result.ToList<JsonVisit>();
                        }
                    }
                }                
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }
    }
}


