﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Mobile_Menu
    {
        public int? MM_ID { get; set; }
        [Required]
        [DisplayName("GoldenEye Menu")]
        public int MM_Menu_GoldenEye_ID { get; set; }
        public string MM_GoldenEye_Name { get; set; }
        [Required]
        [DisplayName("Role")]
        public int MM_Role_ID { get; set; }
        public string MM_Role_Name { get; set; }
        public int MM_Company_ID { get; set; }
        public string MM_InsertBy { get; set; }
        public string MM_InsertBy_Name { get; set; }
        public DateTime MM_InsertOn { get; set; }
        public string MM_UpdateBy { get; set; }
        public string MM_UpdateBy_Name { get; set; }
        public DateTime MM_UpdateOn { get; set; }
        public bool MM_IsDelete { get; set; }
        public bool? MM_IsActive { get; set; }
    }

    public class JsonMobile_Menu
    {
        public int total { get; set; }
        public List<rowsMobile_Menu> rows { get; set; }
    }

    public class rowsMobile_Menu {
        public int MM_ID { get; set; }
        public int MM_Menu_GoldenEye_ID { get; set; }
        public string MM_GoldenEye_Name { get; set; }
        public int MM_Role_ID { get; set; }
        public string MM_Role_Name { get; set; }
        public int MM_Company_ID { get; set; }
        public string MM_InsertBy { get; set; }
        public string MM_InsertBy_Name { get; set; }
        public DateTime MM_InsertOn { get; set; }
        public string MM_UpdateBy { get; set; }
        public string MM_UpdateBy_Name { get; set; }
        public DateTime MM_UpdateOn { get; set; }
        public bool MM_IsDelete { get; set; }
        public bool MM_IsActive { get; set; }
    }
}