﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace b2bWeb.Models
{
    public class InventoryDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        public List<Inventory> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Sp_Get_Inventory_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Inventory> result = JsonConvert.DeserializeObject<IEnumerable<Inventory>>(s, settings);
                        return result.ToList<Inventory>();
                    }
                }
            }
            return new List<Inventory>();
        }

        public List<JsonInventory> GetDataInventory(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Sp_Get_Inventory_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonInventory>>(s, settings);
                            return result.ToList<JsonInventory>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        public int AddUpdDel(Inventory Inv)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_Inv_Json = JsonConvert.SerializeObject(Inv); 
                SqlCommand com = new SqlCommand("Ins_M_Inventory_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Inventory", M_Inv_Json);                
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Delete(Inventory itm)
        {
            string M_ProfileJson = JsonConvert.SerializeObject(itm);
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Del_M_Inventory_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Inventory", M_ProfileJson);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}