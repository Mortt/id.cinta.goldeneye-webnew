﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class BillingDB
    {        
        //Return list of all Items  
        //public List<Billing> ListAll()
        //{
        //    using (SqlConnection con = new SqlConnection(cs))
        //    {
        //        con.Open();
        //        SqlCommand com = new SqlCommand("SP_GET_BILL", con);
        //        com.CommandTimeout = 250;
        //        com.CommandType = CommandType.StoredProcedure;

        //        DateTime date = DateTime.Now;
        //        com.Parameters.Add("@periode", SqlDbType.NVarChar).Value = date;
        //        //com.Parameters.AddWithValue("periode", "");

        //        using (XmlReader reader = com.ExecuteXmlReader())
        //        {
        //            while (reader.Read())
        //            {
        //                //string r = reader.Value.ToString();
        //                string s = General.CleanInvalidXmlChars(reader.Value.ToString());
        //                var settings = new JsonSerializerSettings
        //                {
        //                    NullValueHandling = NullValueHandling.Ignore,
        //                    MissingMemberHandling = MissingMemberHandling.Ignore
        //                };
        //                IEnumerable<Billing> result = JsonConvert.DeserializeObject<IEnumerable<Billing>>(s, settings);
        //                return result.ToList<Billing>();
        //            }
        //        }

        //    }
        //    return new List<Billing>();
        //}

        public static List<Billing> GetData(DateTime date)
        {
            string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            dynamic result = null;
            try
            {
                using (SqlConnection cnn = new SqlConnection(CS))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("SP_GET_BILL", cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };
                    //cmd.Parameters.Add("@periode", SqlDbType.NVarChar).Value = Convert.ToDateTime(date);
                    cmd.Parameters.AddWithValue("@periode", date);
                    cmd.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            result = JsonConvert.DeserializeObject<List<Billing>>(s, settings);
                            return result.ToList<Billing>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return result;
        }
    }
}