﻿using System;
using System.Collections.Generic;

namespace b2bWeb.Models
{
    public class Activities
    {
        public string Employee_NIK { get; set; }
        public string Employee_Name { get; set; }
        public string Employee_Email { get; set; }
        public string Employee_LeaderNik { get; set; }
        public string Employee_Role { get; set; }
        public int Activities_ID { get; set; }
        public int Activities_Company_ID { get; set; }
        public string Activities_NIK { get; set; }
        public string Activities_Outlet_Code { get; set; }
        public string Activities_Respons_ID { get; set; }
        public string Activities_Image { get; set; }
        public string Activities_Desc { get; set; }
        public DateTime Activities_InsertOn { get; set; }
        public string Activities_InsertBy { get; set; }
        public DateTime Activities_UpdateOn { get; set; }
        public string Activities_Updateby { get; set; }
        public bool Activities_IsDelete { get; set; }
        public bool Activities_IsActive { get; set; }
        public string Outlet_Code { get; set; }
        public string Outlet_Name { get; set; }
        public string Respond_Code { get; set; }
        public string Respond_Name { get; set; }
    }

    public class JsonActivities
    {
        public int total { get; set; }
        public List<rowsActivities> rows { get; set; }
    }

    public class rowsActivities
    {
        public string Employee_NIK { get; set; }
        public string Employee_Name { get; set; }
        public string Employee_Email { get; set; }
        public string Employee_LeaderNik { get; set; }
        public string Employee_Role { get; set; }
        public int Activities_ID { get; set; }
        public int Activities_Company_ID { get; set; }
        public string Activities_NIK { get; set; }
        public string Activities_Outlet_Code { get; set; }
        public string Activities_Respons_ID { get; set; }
        public string Activities_Image { get; set; }
        public string Activities_Desc { get; set; }
        public DateTime Activities_InsertOn { get; set; }
        public string Activities_InsertBy { get; set; }
        public DateTime Activities_UpdateOn { get; set; }
        public string Activities_Updateby { get; set; }
        public bool Activities_IsDelete { get; set; }
        public bool Activities_IsActive { get; set; }
        public string Outlet_Code { get; set; }
        public string Outlet_Name { get; set; }
        public string Respond_Code { get; set; }
        public string Respond_Name { get; set; }
    }
}