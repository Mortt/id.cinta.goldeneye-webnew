﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace b2bWeb.Models
{
    public class PaketDB
    {        
        public static List<Paket> ListAll()
        {
            string Cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Paket_BE", con)
                {
                    CommandTimeout = 30,
                    CommandType = CommandType.StoredProcedure
                };
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Paket> result = JsonConvert.DeserializeObject<IEnumerable<Paket>>(s, settings);
                        return result.ToList<Paket>();
                    }
                }

            }
            return new List<Paket>();
        }

        public static int addCompany(Company data)
        {
            string Cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            int i;
            using (SqlConnection con = new SqlConnection(Cs))
            {
                con.Open();
                string M_Company_Json = JsonConvert.SerializeObject(data);
                SqlCommand com = new SqlCommand("Ins_M_Company_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_Company_Json", M_Company_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

    }
}