﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Order
    {
        public int TransID { get; set; }
        public string Order_InsertBy { get; set; }
        public string Order_InsertBy_Name { get; set; }
        public string TransCode { get; set; }
        public DateTime Order_Transdate { get; set; }
        public DateTime Order_InsertOn { get; set; }
        public DateTime Order_UpdateOn { get; set; }
        public decimal Order_Total_Amount { get; set; }
        public int Order_Total_Qty { get; set; }
        public string Order_Desc { get; set; }
        public bool Order_IsDelete { get; set; }
        public string Outlet_Code { get; set; }
        public string Outlet_Name { get; set; }
    }

    public class JsonOrder {
        public int total { get; set; }
        public List<rowsOrder> rows { get; set; }
    }

    public class rowsOrder
    {
        public int TransID { get; set; }
        public string Order_InsertBy { get; set; }
        public string Order_InsertBy_Name { get; set; }
        public string TransCode { get; set; }
        public DateTime Order_Transdate { get; set; }
        public DateTime Order_InsertOn { get; set; }
        public DateTime Order_UpdateOn { get; set; }
        public decimal Order_Total_Amount { get; set; }
        public int Order_Total_Qty { get; set; }
        public string Order_Desc { get; set; }
        public bool Order_IsDelete { get; set; }
        public string Outlet_Code { get; set; }
        public string Outlet_Name { get; set; }
        public List<OrderDetails> Details { get; set; }
    }

    public class OrderDetails
    {
        public int TransID { get; set; }
        public int Order_No { get; set; }
        public int Order_Item_ID { get; set; }
        public int Order_Qty { get; set; }
        public decimal Order_Price { get; set; }
        public decimal Order_LineTotal { get; set; }
        public string Item_Name { get; set; }
        public string Item_Image1 { get; set; }
        public DateTime Order_InsertOn { get; set; }
        public string Order_InsertBy { get; set; }
        public string Employee_Name { get; set; }
        public string TransCode { get; set; }
        public int Order_Total_Qty { get; set; }
        public decimal Order_Total_Amount { get; set; }        
    }
}