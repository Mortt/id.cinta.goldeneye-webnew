﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Inventory
    {
        public int? Inventory_Id { set; get; }
        [Required]
        [DisplayName("item code")]
        public string Inventory_Item_Code { set; get; }
        [Required]
        [DisplayName("quantity")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public int Inventory_Qty { set; get; }
        [Required]
        [DisplayName("unit code")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Inventory_Unit_Code { set; get; }
        public int Inventory_CompanyID { set; get; }
        public DateTime Inventory_InsertOn { set; get; }
        public string Inventory_InsertBy { set; get; }
        public DateTime Inventory_UpdateOn { set; get; }
        public string Inventory_UpdateBy { set; get; }
        public bool? Inventory_IsActive { set; get; }
        public bool Inventory_IsDelete { set; get; }
        public string Product_Name { set; get; }
        public decimal Product_Price { set; get; }
        public string Insert_Name { set; get; }
        public string Update_Name { set; get; }
    }

    public class JsonInventory
    {
        public int total { get; set; }
        public List<rowsInventory> rows { get; set; }
    }

    public class rowsInventory
    {
        public int Inventory_Id { set; get; }        
        public string Inventory_Item_Code { set; get; }        
        public int Inventory_Qty { set; get; }        
        public string Inventory_Unit_Code { set; get; }
        public int Inventory_CompanyID { set; get; }
        public DateTime Inventory_InsertOn { set; get; }
        public string Inventory_InsertBy { set; get; }
        public DateTime Inventory_UpdateOn { set; get; }
        public string Inventory_UpdateBy { set; get; }
        public bool Inventory_IsActive { set; get; }
        public bool Inventory_IsDelete { set; get; }
        public string Product_Name { set; get; }
        public decimal Product_Price { set; get; }
        public string Insert_Name { set; get; }
        public string Update_Name { set; get; }
    }
}