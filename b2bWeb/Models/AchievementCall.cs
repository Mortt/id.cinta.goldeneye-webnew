﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class AchievementCall
    {
        public string TC_Employee_NIK { get; set; }
        public string Employee_Name { get; set; }
        public string Role_Name { get; set; }
        public int TC_Qty { get; set; }
        public DateTime TC_Periode { get; set; }
        public int Actual { get; set; }
    }
    public class JsonAchievementCall
    {
        public int total { get; set; }
        public List<rowsAchievementCall> rows { get; set; }
    }
    public class rowsAchievementCall
    {
        public string TC_Employee_NIK { get; set; }
        public string Employee_Name { get; set; }
        public string Role_Name { get; set; }
        public int TC_Qty { get; set; }
        public DateTime TC_Periode { get; set; }
        public int Actual { get; set; }
    }
}