﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;


namespace b2bWeb.Models
{
    public class AutoCompleteNIKDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Items  
        public List<AutoComplete> ListAll(string keyword)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetAC_NIK", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Search", keyword);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<AutoComplete> result = JsonConvert.DeserializeObject<IEnumerable<AutoComplete>>(s, settings);
                        return result.ToList<AutoComplete>();
                    }
                }

            }
            return new List<AutoComplete>();
        }

    }
}