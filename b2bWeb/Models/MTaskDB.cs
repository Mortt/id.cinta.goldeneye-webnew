﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class MTaskDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        public List<MTask> ListAll(Parameter data)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetTask_NEW_BE1", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                
                string paramJson = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", paramJson);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<MTask> result = JsonConvert.DeserializeObject<IEnumerable<MTask>>(s, settings);
                        return result.ToList<MTask>();
                    }
                }
            }
            return new List<MTask>();
        }

        public List<_DET> ListID(Parameter data)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetTask_NEW_BE1", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;

                string paramJson = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", paramJson);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<_DET> result = JsonConvert.DeserializeObject<IEnumerable<_DET>>(s, settings);
                        return result.ToList<_DET>();
                    }
                }
            }
            return new List<_DET>();
        }

        public List<JsonMTask> GetDataMTask(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetTask_NEW_BE1", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonMTask>>(s, settings);
                            return result.ToList<JsonMTask>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        public static bool AddUpdate(MTask itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_MTaskJson = JsonConvert.SerializeObject(itm);
                SqlCommand com = new SqlCommand("Ins_M_TaskH_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_TaskH_Json", M_MTaskJson);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        //Method for Deleting an Visit  
        public static bool Delete(MTask itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_Task_Json = JsonConvert.SerializeObject(itm); ;
                SqlCommand com = new SqlCommand("Del_M_Task", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_Task_Json", M_Task_Json);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

    }
}