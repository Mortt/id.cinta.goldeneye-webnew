﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Spending
    {
        public int Spending_ID { set; get; }
        public decimal Spending_Amount { set; get; }
        public string Spending_Image { set; get; }
        public string Spending_Desc { set; get; }
        public Int32 Spending_ReasonID { set; get; }
        public int Spending_CompanyID { set; get; }
        public DateTime Spending_InsertOn { set; get; }
        public string Spending_InsertBy { set; get; }
        public string Insert_Spending_Employee_Name { set; get; }
        public DateTime Spending_UpdateOn { set; get; }
        public string Spending_UpdateBy { set; get; }
        public string Update_Spending_Employee_Name { set; get; }
        public bool Spending_Isdelete { set; get; }
        public string Spending_Respond { set; get; }        
    }

    public class JsonSpending
    {
        public int total { get; set; }
        public List<rowsSpending> rows { get; set; }
    }
    public class rowsSpending
    {
        public int Spending_ID { set; get; }
        public decimal Spending_Amount { set; get; }
        public string Spending_Image { set; get; }
        public string Spending_Desc { set; get; }
        public Int32 Spending_ReasonID { set; get; }
        public int Spending_CompanyID { set; get; }
        public DateTime Spending_InsertOn { set; get; }
        public string Spending_InsertBy { set; get; }
        public string Insert_Spending_Employee_Name { set; get; }
        public DateTime Spending_UpdateOn { set; get; }
        public string Spending_UpdateBy { set; get; }
        public string Update_Spending_Employee_Name { set; get; }
        public bool Spending_Isdelete { set; get; }
        public string Spending_Respond { set; get; }
    }
}