﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;


namespace b2bWeb.Models
{
    public class AchievementDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        public List<Achievement> ListAll(DateTime Date)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetAchievement", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Periode", Date);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Achievement> result = JsonConvert.DeserializeObject<IEnumerable<Achievement>>(s, settings);
                        return result.ToList<Achievement>();
                    }
                }

            }
            return new List<Achievement>();
        }

        public List<JsonAchievement> GetDataAchievement(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetAchievement_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonAchievement>>(s, settings);
                            return result.ToList<JsonAchievement>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        public List<Achievement> ListExport(DateTime g)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Export_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("tipe", "ACHIEVEMENT");
                com.Parameters.AddWithValue("startdate", g);
                com.Parameters.AddWithValue("enddate", g);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Achievement> result = JsonConvert.DeserializeObject<IEnumerable<Achievement>>(s, settings);
                        return result.ToList<Achievement>();
                    }
                }
            }
            return new List<Achievement>();
        }
    }
}