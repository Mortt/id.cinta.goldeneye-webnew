﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Type_Outlet
    {
        public int? Type_Outlet_ID { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets allowed")]
        public string Type_Outlet_Name { get; set; }
        [DisplayName("desc")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets allowed")]
        public string Type_Outlet_Desc { get; set; }
        public bool? Type_Outlet_IsActive { get; set; }
        public int Type_Outlet_Company_ID { get; set; }
        public string Type_Outlet_InsertBy { get; set; }
        public string Type_Outlet_InsertBy_Name { get; set; }
        public DateTime Type_Outlet_InsertOn { get; set; }
        public string Type_Outlet_UpdateBy { get; set; }
        public string Type_Outlet_UpdateBy_Name { get; set; }
        public DateTime Type_Outlet_UpdateOn { get; set; }
        public bool Type_Outlet_IsDelete { get; set; }
    }

    public class JsonType_Outlet
    {
        public int total { get; set; }
        public List<rowsType_Outlet> rows { get; set; }
    }

    public class rowsType_Outlet
    {
        public int Type_Outlet_ID { get; set; }
        public string Type_Outlet_Name { get; set; }
        public string Type_Outlet_Desc { get; set; }
        public bool Type_Outlet_IsActive { get; set; }
        public int Type_Outlet_Company_ID { get; set; }
        public string Type_Outlet_InsertBy { get; set; }
        public string Type_Outlet_InsertBy_Name { get; set; }
        public DateTime Type_Outlet_InsertOn { get; set; }
        public string Type_Outlet_UpdateBy { get; set; }
        public string Type_Outlet_UpdateBy_Name { get; set; }
        public DateTime Type_Outlet_UpdateOn { get; set; }
        public bool Type_Outlet_IsDelete { get; set; }
    }
}