﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Outlet
    {
        public int? Outlet_ID { set; get; }
        [Required]
        [DisplayName("code")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Outlet_Code { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Outlet_Name { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Outlet_Desc { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Outlet_Address1 { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Outlet_Address2 { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public string Outlet_Zipcode { get; set; }
        public string Outlet_Type { get; set; }        
        public string Outlet_Picture { get; set; }
        public string Outlet_PICFront { get; set; }
        public string Outlet_PICCashier { get; set; }  
        public string Images_Name { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public string Outlet_Phone { get; set; }
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Only Email format allowed")]
        public string Outlet_Email { get; set; }
        [Required]
        [DisplayName("longitude")]
        public decimal Outlet_Longitude { get; set; }
        [Required]
        [DisplayName("latitude")]
        public decimal Outlet_Latitude { get; set; }
        public DateTime Outlet_InsertOn { get; set; }
        public int Outlet_CompanyID { get; set; }
        public string Outlet_InsertBy { get; set; }
        public string Insert_Name { get; set; }
        [Required]
        [DisplayName("Area")]
        public string Outlet_AreaID { get; set; }
        public string Cari_Area { get; set; }
        public DateTime Outlet_UpdateOn { get; set; }
        public string Outlet_UpdateBy { get; set; }
        public string Update_Name { get; set; }
        public bool? Outlet_Isactive { get; set; }
        public string AddOrUp { get; set; }
        public int jml { get; set; }
        public string GroupOutlet_Name { get; set; }
        public string Outlet_GroupCode { get; set; }
        public int Outlet_Repeat_IDX { get; set; }
        public string Repeat_Name { get; set; }
    }

    public class JsonOutlet
    {
        public int total { get; set; }
        public List<rowsOutlet> rows { get; set; }

    }

    public class rowsOutlet
    {
        public int Outlet_ID { set; get; }
        public string Outlet_Code { get; set; }
        public string Outlet_Name { get; set; }
        public string Outlet_Desc { get; set; }
        public string Outlet_Address1 { get; set; }
        public string Outlet_Address2 { get; set; }
        public string Outlet_Zipcode { get; set; }
        public string Outlet_Type { get; set; }
        public string Outlet_Picture { get; set; }
        public string Outlet_PICFront { get; set; }
        public string Outlet_PICCashier { get; set; }
        public string Images_Name { get; set; }
        public string Outlet_Phone { get; set; }
        public string Outlet_Email { get; set; }
        public decimal Outlet_Longitude { get; set; }
        public decimal Outlet_Latitude { get; set; }
        public DateTime Outlet_InsertOn { get; set; }
        public int Outlet_CompanyID { get; set; }
        public string Outlet_InsertBy { get; set; }
        public string Insert_Name { get; set; }
        public string Outlet_AreaID { get; set; }
        public string Cari_Area { get; set; }
        public DateTime Outlet_UpdateOn { get; set; }
        public string Outlet_UpdateBy { get; set; }
        public string Update_Name { get; set; }
        public bool Outlet_Isactive { get; set; }
        public string GroupOutlet_Name { get; set; }
        public string Outlet_GroupCode { get; set; }
        public int Outlet_Repeat_IDX { get; set; }
        public string Repeat_Name { get; set; }
    }
}