﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class BrandDB
    {//declare connection string  
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Items
        public List<Brand> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetCategory_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Brand> result = JsonConvert.DeserializeObject<IEnumerable<Brand>>(s, settings);
                        return result.ToList<Brand>();
                    }
                }
            }
            return new List<Brand>();
        }

        public List<JsonBrand> GetDataBrand(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetCategory_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonBrand>>(s, settings);
                            return result.ToList<JsonBrand>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        //Method for Adding and Update  
        public int AddUpdate(Brand itm)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_Item_Json = JsonConvert.SerializeObject(itm); 
                SqlCommand com = new SqlCommand("Ins_M_Brand_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_Brand_Json", M_Item_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Visit  
        public int Delete(Brand itm)
        {
            string M_ProfileJson = JsonConvert.SerializeObject(itm);
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();                
                SqlCommand com = new SqlCommand("Del_M_Brand_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_ProfileJson", M_ProfileJson);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Cek(Brand ID)
        {
            int i;
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_Category", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("CategoryCode", ID.Category_Code);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<Brand> results = JsonConvert.DeserializeObject<IEnumerable<Brand>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}



