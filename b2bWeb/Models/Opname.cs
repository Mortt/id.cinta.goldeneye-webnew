﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Opname
    {
        public int Opname_ID { get; set; }
        public string Opname_OutletCode { get; set; }        
        public string Opname_Desc { get; set; }
        public string Opname_Image { get; set; }
        public string Opname_InsertBy { get; set; }
        public DateTime Opname_InsertOn { get; set; }
        public DateTime Opname_UpdateOn { get; set; }        
        public string Opname_UpdateBy { get; set; }        
        public bool Opname_Isdelete { get; set; }
        public string Opname_Employee_Name { get; set; }
        public string Opname_Outlet_Name { get; set; }
        public int Opname_No { get; set; }
        public string Opname_ProductCode { get; set; }
        public string Opname_Product_Name { get; set; }
        public string Opname_Product_Image { get; set; }
        public int Opname_Stock { get; set; }
        public int Opname_Suggestion { get; set; }
    }

    public class JsonOpname
    {
        public int total { get; set; }
        public List<rowsOpname> rows { get; set; }
    }
    public class rowsOpname
    {
        public int Opname_ID { get; set; }
        public string Opname_OutletCode { get; set; }
        public string Opname_Desc { get; set; }
        public string Opname_Image { get; set; }
        public string Opname_InsertBy { get; set; }
        public DateTime Opname_InsertOn { get; set; }
        public DateTime Opname_UpdateOn { get; set; }
        public string Opname_UpdateBy { get; set; }
        public bool Opname_Isdelete { get; set; }
        public string Opname_Employee_Name { get; set; }
        public string Opname_Outlet_Name { get; set; }
        public int Opname_No { get; set; }
        public string Opname_ProductCode { get; set; }
        public string Opname_Product_Name { get; set; }
        public string Opname_Product_Image { get; set; }
        public int Opname_Stock { get; set; }
        public int Opname_Suggestion { get; set; }
    }
}