﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
namespace b2bWeb.Models
{
    public class DashboardDB
    {//declare connection string  
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        //Return list of all Employees  
        public List<Dashboard> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Dashboard", con);
                //SqlCommand com = new SqlCommand("SP_Get_Dashboard_TEST", con);                
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Periode", DateTime.Now);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Dashboard> result = JsonConvert.DeserializeObject<IEnumerable<Dashboard>>(s, settings);
                        return result.ToList<Dashboard>();
                    }
                }

            }
            return new List<Dashboard>();
        }

        public List<DashboardChart> ListSellingChart(string Bulan, string Tahun)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Dashboard_Chart_Sell", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Bulan", Bulan);
                com.Parameters.AddWithValue("Tahun", Tahun);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<DashboardChart> result = JsonConvert.DeserializeObject<IEnumerable<DashboardChart>>(s, settings);
                        return result.ToList<DashboardChart>();
                    }
                }

            }
            return new List<DashboardChart>();
        }

        public List<DashboardAll> ChartAll(string Bulan, string Tahun)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Dashboard_Chart", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Bulan", Bulan);
                com.Parameters.AddWithValue("Tahun", Tahun);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                                
                DataTable dt = new DataTable();                
                dt.Load(com.ExecuteReader());

                DataTable dt2 = new DataTable();

                dt2.Columns.Add("x", typeof(string));
                dt2.Columns.Add("y", typeof(int));
                //int a = dt.Columns.Count;

                for (int a = 1; a < 10; a++)
                {
                    for(int b = 0; b < dt.Rows.Count; b++)
                    {
                        DataRow dr = dt2.NewRow();
                        dr["x"] = dt.Rows[b]["TransName" + a];
                        dr["y"] = dt.Rows[b]["Total" + a];

                        dt2.Rows.Add(dr);
                    }                    
                }

                for (int a = 1; a < 10; a++)
                {
                    dt.Columns.Remove("TransName" + a);
                    dt.Columns.Remove("Total" + a);
                }

                for (int b = 0; b < dt.Rows.Count; b++)
                {
                    DataRow dr = dt2.NewRow();
                    dr["x"] = dt.Rows[b]["TransName"];
                    dr["y"] = dt.Rows[b]["Total"];

                    dt2.Rows.Add(dr);
                }

                dt2.AcceptChanges();

                string JSONresult;
                JSONresult = JsonConvert.SerializeObject(dt2);

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                IEnumerable<DashboardAll> result = JsonConvert.DeserializeObject<IEnumerable<DashboardAll>>(JSONresult, settings);
                return result.ToList<DashboardAll>();
            }
        }

        public List<DashboardProduct> ChartProduct(string Bulan, string Tahun)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Dashboard_Chart_Sell_product", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                com.Parameters.AddWithValue("Bulan", Bulan);
                com.Parameters.AddWithValue("Tahun", Tahun);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<DashboardProduct> result = JsonConvert.DeserializeObject<IEnumerable<DashboardProduct>>(s, settings);
                        return result.ToList<DashboardProduct>();
                    }
                }

            }
            return new List<DashboardProduct>();
        }

        public List<DashboardTrans> ChartTrans(string type, string Bulan, string Tahun)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Dashboard_Transaction", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("type", type);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                com.Parameters.AddWithValue("Bulan", Bulan);
                com.Parameters.AddWithValue("Tahun", Tahun);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<DashboardTrans> result = JsonConvert.DeserializeObject<IEnumerable<DashboardTrans>>(s, settings);
                        return result.ToList<DashboardTrans>();
                    }
                }

            }
            return new List<DashboardTrans>();
        }
    }
}