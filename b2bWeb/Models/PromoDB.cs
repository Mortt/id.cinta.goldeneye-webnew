﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class PromoDB
    {
        //declare connection string  
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        //Return list of all Employees  
        public List<Promo> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetPromo_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Promo> result = JsonConvert.DeserializeObject<IEnumerable<Promo>>(s, settings);
                        return result.ToList<Promo>();
                    }
                }
            }
            return new List<Promo>();
        }

        public List<JsonPromo> GetDataPromo(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetPromo_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonPromo>>(s, settings);
                            return result.ToList<JsonPromo>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        //Method for Deleting an Promo  
        public int Delete(Promo itm)
        {
            string M_ProfileJson = JsonConvert.SerializeObject(itm);
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Del_M_Promo", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_ProfileJson", M_ProfileJson);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int AddUpd(Promo itm)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_Promo_Json = JsonConvert.SerializeObject(itm);
                SqlCommand com = new SqlCommand("Ins_M_Promo_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_Promo_Json", M_Promo_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Cek(Promo ID)
        {
            int i;
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_Promosi", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("Promosi_Code", ID.Promosi_Code);
                    com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<Promo> results = JsonConvert.DeserializeObject<IEnumerable<Promo>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().Jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}