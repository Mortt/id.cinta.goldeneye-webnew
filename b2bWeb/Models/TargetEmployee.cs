﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class TargetEmployee
    {
        public int? TargetByEmployee_ID { get; set; }
        [Required]
        [DisplayName("periode")]
        public DateTime TargetByEmployee_Periode { get; set; }
        [Required]
        [DisplayName("employee")]
        public string TargetByEmployee_EmployeeNIK { get; set; }
        [Required]
        [DisplayName("amount")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public decimal TargetByEmployee_Amount { get; set; }
        public int TargetByEmployee_CompanyID { get; set; }
        public bool? TargetByEmployee_IsActive { get; set; }
        public bool TargetByEmployee_Isdelete { get; set; }
        public string TargetByEmployee_UpdateBy { get; set; }
        public DateTime TargetByEmployee_UpdateOn { get; set; }
        public string TargetByEmployee_InsertBy { get; set; }
        public DateTime TargetByEmployee_InsertOn { get; set; }
        public string TargetByEmployee_EmployeeName { get; set; }
        public int jml { get; set; }
    }

    public class JsonTargetEmployee
    {
        public int total { get; set; }
        public List<rowsTargetEmployee> rows { get; set; }
    }

    public class rowsTargetEmployee
    {
        public int TargetByEmployee_ID { get; set; }
        public DateTime TargetByEmployee_Periode { get; set; }
        public string TargetByEmployee_EmployeeNIK { get; set; }
        public decimal TargetByEmployee_Amount { get; set; }
        public int TargetByEmployee_CompanyID { get; set; }
        public bool TargetByEmployee_IsActive { get; set; }
        public bool TargetByEmployee_Isdelete { get; set; }
        public string TargetByEmployee_UpdateBy { get; set; }
        public DateTime TargetByEmployee_UpdateOn { get; set; }
        public string TargetByEmployee_InsertBy { get; set; }
        public DateTime TargetByEmployee_InsertOn { get; set; }
        public string TargetByEmployee_EmployeeName { get; set; }
    }
}