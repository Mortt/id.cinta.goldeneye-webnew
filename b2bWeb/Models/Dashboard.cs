﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Dashboard
    {
        public string Dashboard_Product { get; set; }
        public string Dashboard_Employee { get; set; }
        public string Dashboard_Brand { get; set; }
        public string Dashboard_Outlet { get; set; }
        public string Dashboard_Route { get; set; }
        public string Dashboard_Area { get; set; }
        public string Dashboard_Respond { get; set; }
        public string Dashboard_Visit { get; set; }
        public string Dashboard_Planogram { get; set; }
        public string Dashboard_Opname { get; set; }
        public string Dashboard_CompAct { get; set; }
        public string Dashboard_Feedback { get; set; }
        public string Dashboard_Sampling { get; set; }
        public string Dashboard_Loyalti { get; set; }
        public string Dashboard_Selling { get; set; }
        public string Dashboard_Coaching { get; set; }
    }

    public class DashboardChart
    {
        public string Sales_Date { get; set; }
        public int Total_QTY { get; set; }
        public decimal Total_AMOUNT { get; set; }
    }

    public class DashboardAll
    {
        public string x { get; set; }
        public int y { get; set; }
    }

    public class DashboardProduct
    {
        public string item_Name { get; set; }
        public int Total_QTY { get; set; }
        public decimal Total_AMOUNT { get; set; }
    }

    public class DashboardTrans
    {
        public string Sampling_Date { get; set; }
        public int Total_QTY { get; set; }
        public string Coaching_Date { get; set; }
        public int Total_Coaching { get; set; }
        public string Employee_Name_Insert { get; set; }
        public string Employee_Name_With { get; set; }
        public string Display_Date { get; set; }
        public int Total_Display { get; set; }
        public string Stock_Date { get; set; }
        public int Total_Stock { get; set; }
        public string Competitor_Date { get; set; }
        public int Total_Competitor { get; set; }
        public string Feedback_Date { get; set; }
        public int Total_Feedback { get; set; }
    }

    //public class DashboardTransSampling
    //{
    //    public string Sampling_Date { get; set; }
    //    public int Total_QTY { get; set; }
    //}

    //public class DashboardTransCoaching
    //{
    //    public string Coaching_Date { get; set; }
    //    public int Total_Coaching { get; set; }
    //    public string Employee_Name_Insert { get; set; }
    //    public string Employee_Name_With { get; set; }
    //}

    //public class DashboardTransDisplay
    //{
    //    public string Display_Date { get; set; }
    //    public int Total_Display { get; set; }
    //}

    //public class DashboardTransStock
    //{
    //    public string Stock_Date { get; set; }
    //    public int Total_Stock { get; set; }
    //}

    //public class DashboardTransCompetitor
    //{
    //    public string Competitor_Date { get; set; }
    //    public int Total_Competitor { get; set; }
    //}
    
    //public class DashboardTransFeedback
    //{
    //    public string Feedback_Date { get; set; }
    //    public int Total_Feedback { get; set; }
    //}
}