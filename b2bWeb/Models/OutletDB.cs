﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class OutletDB
    {
        //declare connection string  
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        //Return list of all Items  
        public List<Outlet> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Outlet_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;

                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Outlet> result = JsonConvert.DeserializeObject<IEnumerable<Outlet>>(s, settings);
                        return result.ToList<Outlet>();
                    }
                }

            }
            return new List<Outlet>();
        }

        public int AddUpdate(Outlet otl)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_OutletJson = JsonConvert.SerializeObject(otl); ;
                SqlCommand com = new SqlCommand("Ins_M_Outlet_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_OutletJson", M_OutletJson);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Visit  
        public int Delete(Outlet ID)
        {
            string M_ProfileJson = JsonConvert.SerializeObject(ID);
            int i;

            using (SqlConnection con = new SqlConnection(cs))
            {
                //string Username = "081315412482";// '; JsonConvert.SerializeObject(ID);
                con.Open();
                SqlCommand com = new SqlCommand("Del_M_Outlet", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_ProfileJson", M_ProfileJson);
                //com.Parameters.AddWithValue("@Outlet_Code", ID);
                //com.Parameters.AddWithValue("@UserName", Username);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Cek(Outlet ID)
        {
            int i;
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_Outlet", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("OutletCode", ID.Outlet_Code);
                    com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<Outlet> results = JsonConvert.DeserializeObject<IEnumerable<Outlet>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }

        public List<JsonOutlet> GetDataOutlet(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_Outlet_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonOutlet>>(s, settings);
                            return result.ToList<JsonOutlet>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }
    }
}



