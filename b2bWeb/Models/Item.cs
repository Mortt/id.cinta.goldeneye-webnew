﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Item
    {
        public int? Item_ID { get; set; }
        [Required]
        [DisplayName("code")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Item_Code { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Item_Name { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Item_Desc { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Item_Desc_Detail { get; set; }
        [Required]
        [DisplayName("brand")]
        public string Item_CategoryCode { get; set; }
        public string Category_Name { get; set; }        
        public string Item_Image1 { get; set; }
        public string Images_Name { get; set; }
        public string Item_Image2 { get; set; }
        [Required]
        [DisplayName("price")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public double Item_Price { get; set; }
        public DateTime Item_InsertOn { get; set; }
        public int Item_CompanyID { get; set; }
        public string Item_InsertBy { get; set; }
        public string Insert_Name { get; set; }
        public DateTime Item_UpdateOn { get; set; }
        public string Item_UpdateBy { get; set; }
        public string Update_Name { get; set; }
        public bool? Item_IsActive { get; set; }
        [DisplayName("sampling")]
        public bool Item_Sampling { get; set; }
        public string jml { get; set; }
    }

    public class JsonItem
    {
        public int total { get; set; }
        public List<rowsItem> rows { get; set; }

    }

    public class rowsItem
    {
        public int Item_ID { get; set; }
        public string Item_Code { get; set; }
        public string Item_Name { get; set; }
        public string Item_Desc { get; set; }
        public string Item_Desc_Detail { get; set; }
        public string Item_CategoryCode { get; set; }
        public string Category_Name { get; set; }
        public string Item_Image1 { get; set; }
        public string Images_Name { get; set; }
        public string Item_Image2 { get; set; }
        public double Item_Price { get; set; }
        public DateTime Item_InsertOn { get; set; }
        public int Item_CompanyID { get; set; }
        public string Item_InsertBy { get; set; }
        public string Insert_Name { get; set; }
        public DateTime Item_UpdateOn { get; set; }
        public string Item_UpdateBy { get; set; }
        public string Update_Name { get; set; }
        public bool Item_IsActive { get; set; }
        public bool Item_Sampling { get; set; }

    }
}