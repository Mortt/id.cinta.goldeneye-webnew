﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Respond
    {
        public int? Respond_ID { get; set; }
        [Required]
        [DisplayName("code")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Respond_Code { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Respond_Name { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Respond_Desc { get; set; }
        [Required]
        [DisplayName("module")]
        public string Respond_Module { get; set; }
        public int Respond_CompanyID { set; get; }
        public DateTime Respond_InsertOn { get; set; }
        public string Respond_InsertBy { get; set; }
        public DateTime Respond_UpdateOn { get; set; }
        public string Respond_UpdateBy { get; set; }
        public bool? Respond_IsActive { get; set; }
        public bool Respond_IsDelete { get; set; }
        public string Insert_Name { get; set; }
        public string Update_Name { get; set; }
        public int Jml { get; set; }
    }

    public class JsonRespond
    {
        public int total { get; set; }
        public List<rowsRespond> rows { get; set; }
    }
    public class rowsRespond
    {
        public string Respond_ID { get; set; }
        public string Respond_Code { get; set; }
        public string Respond_Name { get; set; }
        public string Respond_Desc { get; set; }
        public string Respond_Module { get; set; }
        public int Respond_CompanyID { set; get; }
        public DateTime Respond_InsertOn { get; set; }
        public string Respond_InsertBy { get; set; }
        public DateTime Respond_UpdateOn { get; set; }
        public string Respond_UpdateBy { get; set; }
        public bool Respond_IsActive { get; set; }
        public string Insert_Name { get; set; }
        public string Update_Name { get; set; }
    }
}