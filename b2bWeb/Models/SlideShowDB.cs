﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class SlideShowDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Items  
        public List<SlideShow> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_SlideShow_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<SlideShow> result = JsonConvert.DeserializeObject<IEnumerable<SlideShow>>(s, settings);
                        return result.ToList<SlideShow>();
                    }
                }

            }
            return new List<SlideShow>();
        }

        public List<JsonSlideShow> GetDataSildeShow(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_SlideShow_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonSlideShow>>(s, settings);
                            return result.ToList<JsonSlideShow>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        //Method for Adding and Update  
        public int AddUpdate(SlideShow SA)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_SlideShow_Json = JsonConvert.SerializeObject(SA);
                SqlCommand com = new SqlCommand("Ins_M_Slideshow_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_SlideShow_Json", M_SlideShow_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Delete(SlideShow SA)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_SlideshowJson = JsonConvert.SerializeObject(SA); ;
                SqlCommand com = new SqlCommand("Del_M_SlideShow", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_SlideshowJson", M_SlideshowJson);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}