﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class PlanogramDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Items  
        public List<Planogram> ListAll(string Bulan, string Tahun)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetPlanogram_BE", con);
                com.CommandTimeout = 250; 
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Bulan", Bulan);
                com.Parameters.AddWithValue("Tahun", Tahun);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        //string r = reader.Value.ToString();
                        string s = General.CleanInvalidXmlChars(reader.Value.ToString());
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Planogram> result = JsonConvert.DeserializeObject<IEnumerable<Planogram>>(s, settings);
                        return result.ToList<Planogram>();
                    }
                }

            }
            return new List<Planogram>();
        }
        
        public List<Planogram> ListExport(DateTime g, DateTime c)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Export_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("tipe", "Planogram"); 
                com.Parameters.AddWithValue("startdate", g);
                com.Parameters.AddWithValue("enddate", c);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        //string r = reader.Value.ToString();
                        string s = General.CleanInvalidXmlChars(reader.Value.ToString());
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Planogram> result = JsonConvert.DeserializeObject<IEnumerable<Planogram>>(s, settings);
                        return result.ToList<Planogram>();
                    }
                }

            }
            return new List<Planogram>();
        }

        public List<Planogram> DisplayByID(string id)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();                
                SqlCommand com = new SqlCommand("SP_GetPlanogram_BE_By_ID", con);
                com.CommandTimeout = 250; 
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("id", id);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        //string r = reader.Value.ToString();
                        string s = General.CleanInvalidXmlChars(reader.Value.ToString());
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Planogram> result = JsonConvert.DeserializeObject<IEnumerable<Planogram>>(s, settings);
                        return result.ToList<Planogram>();
                    }
                }

            }
            return new List<Planogram>();
        }

        public List<JsonPlanogram> GetDataPlanogram(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_GetPlanogram_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonPlanogram>>(s, settings);
                            return result.ToList<JsonPlanogram>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }
    }
}
