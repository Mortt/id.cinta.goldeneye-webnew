﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Brand
    {
        [Required]
        [DisplayName("code")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Category_Code { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Category_Name { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Category_Desc { get; set; }
        public string Category_Image { get; set; }
        public string Images_Name { get; set; }
        public int Category_CompanyID { get; set; }
        public string Category_InsertBy { get; set; }
        public DateTime Category_InsertOn { get; set; }
        public string Category_UpdateBy { get; set; }
        public DateTime Category_UpdateON { get; set; }
        public bool? Category_IsActive { get; set; }
        public bool Category_IsDelete { get; set; }
        public string Insert_Name { get; set; }
        public string Update_Name { get; set; }
        public int jml { get; set; }
    }

    public class JsonBrand
    {
        public int total { get; set; }
        public List<rowsBrand> rows { get; set; }

    }

    public class rowsBrand
    {
        public string Category_Code { get; set; }
        public string Category_Name { get; set; }
        public string Category_Desc { get; set; }
        public string Category_Image { get; set; }
        public string Images_Name { get; set; }
        public int Category_CompanyID { get; set; }
        public string Category_InsertBy { get; set; }
        public DateTime Category_InsertOn { get; set; }
        public string Category_UpdateBy { get; set; }
        public DateTime Category_UpdateON { get; set; }
        public bool Category_IsActive { get; set; }
        public bool Category_IsDelete { get; set; }
        public string Insert_Name { get; set; }
        public string Update_Name { get; set; }
    }
}