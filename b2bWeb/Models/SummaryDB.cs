﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class SummaryDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        CultureInfo cultureInfo = new CultureInfo("id-ID");
        public dynamic Employee(DateTime StartDate, DateTime EndDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GET_Summary_By_SPG", con);
                com.CommandTimeout = 250;
                com.Parameters.AddWithValue("FromDate", StartDate);
                com.Parameters.AddWithValue("ToDate", EndDate);
                com.Parameters.AddWithValue("Search", "");                
                com.Parameters.AddWithValue("CompanyID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());
                
                if (dt.Rows.Count > 0)
                {
                    dt.Columns.Remove("Employee_NIK2");

                    int totalColums = dt.Columns.Count;

                    for (int i = 0; i < totalColums; i++)
                    {
                        if (i > 1 && i < totalColums)
                        {
                            dt.Columns.Add(Convert.ToString(dt.Columns[i]) + 'a', typeof(string));                            
                        }
                    }

                    for (int i = 0; i < totalColums; i++)
                    {
                        if (i > 1 && i < totalColums)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {                                
                                dt.Rows[j][Convert.ToString(dt.Columns[i])+'a'] = string.Format(cultureInfo, "{0:C}", dt.Rows[j][dt.Columns[i]]);
                            }                            
                        }
                    }

                    int totalAfter = dt.Columns.Count;
                    var DelTab = new ArrayList();

                    for (int i = 0; i < totalAfter; i++)
                    {
                        if (i > 1 && i < totalAfter)
                        {
                            string substr = Convert.ToString(dt.Columns[i]).Substring(Convert.ToString(dt.Columns[i]).Length - 1);
                            if(substr != "a")
                            {
                                DelTab.Add(Convert.ToString(dt.Columns[i]));
                            }                            
                        }
                    }
                    
                    for(int i=0; i< DelTab.Count; i++)
                    {
                        dt.Columns.Remove(Convert.ToString(DelTab[i]));
                    }
                    
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (Convert.ToString(dt.Columns[i]).Substring(Convert.ToString(dt.Columns[i]).Length - 1) == "a")
                        {
                            string name = Convert.ToString(dt.Columns[i]).Remove(Convert.ToString(dt.Columns[i]).Length - 1);
                            dt.Columns[Convert.ToString(dt.Columns[i])].ColumnName = name;
                        }
                    }                    
                }
                else
                {
                    return dt;
                }
                return dt;
            }
        }

        public dynamic Outlet(DateTime StartDate, DateTime EndDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GET_Summary_By_Outlet", con);
                com.CommandTimeout = 250;
                com.Parameters.AddWithValue("FromDate", StartDate);
                com.Parameters.AddWithValue("ToDate", EndDate);
                com.Parameters.AddWithValue("Search", "");
                com.Parameters.AddWithValue("CompanyID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                if(dt.Rows.Count > 0)
                {
                    dt.Columns.Remove("Outlet_Code2");

                    int totalColums = dt.Columns.Count;

                    for (int i = 0; i < totalColums; i++)
                    {
                        if (i > 1 && i < totalColums)
                        {
                            dt.Columns.Add(Convert.ToString(dt.Columns[i]) + 'a', typeof(string));
                        }
                    }

                    for (int i = 0; i < totalColums; i++)
                    {
                        if (i > 1 && i < totalColums)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                dt.Rows[j][Convert.ToString(dt.Columns[i]) + 'a'] = string.Format(cultureInfo, "{0:C}", dt.Rows[j][dt.Columns[i]]);
                            }
                        }
                    }

                    int totalAfter = dt.Columns.Count;
                    var DelTab = new ArrayList();

                    for (int i = 0; i < totalAfter; i++)
                    {
                        if (i > 1 && i < totalAfter)
                        {
                            string substr = Convert.ToString(dt.Columns[i]).Substring(Convert.ToString(dt.Columns[i]).Length - 1);
                            if (substr != "a")
                            {
                                DelTab.Add(Convert.ToString(dt.Columns[i]));
                            }
                        }
                    }

                    for (int i = 0; i < DelTab.Count; i++)
                    {
                        dt.Columns.Remove(Convert.ToString(DelTab[i]));
                    }

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (Convert.ToString(dt.Columns[i]).Substring(Convert.ToString(dt.Columns[i]).Length - 1) == "a")
                        {
                            string name = Convert.ToString(dt.Columns[i]).Remove(Convert.ToString(dt.Columns[i]).Length - 1);
                            dt.Columns[Convert.ToString(dt.Columns[i])].ColumnName = name;
                        }
                    }
                }
                else
                {

                    return dt;
                }


                return dt;
            }
        }

        public dynamic Product(DateTime StartDate, DateTime EndDate)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GET_Summary_By_Item", con);
                com.CommandTimeout = 250;
                com.Parameters.AddWithValue("FromDate", StartDate);
                com.Parameters.AddWithValue("ToDate", EndDate);
                com.Parameters.AddWithValue("Search", "");
                com.Parameters.AddWithValue("CompanyID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                if (dt.Rows.Count > 0)
                {
                    dt.Columns.Remove("Product_Code@CompanyID");
                    int totalColums = dt.Columns.Count;

                    for (int i = 0; i < totalColums; i++)
                    {
                        if (i > 1 && i < totalColums)
                        {
                            dt.Columns.Add(Convert.ToString(dt.Columns[i]) + 'a', typeof(string));
                        }
                    }

                    for (int i = 0; i < totalColums; i++)
                    {
                        if (i > 1 && i < totalColums)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                dt.Rows[j][Convert.ToString(dt.Columns[i]) + 'a'] = string.Format(cultureInfo, "{0:C}", dt.Rows[j][dt.Columns[i]]);
                            }
                        }
                    }

                    int totalAfter = dt.Columns.Count;
                    var DelTab = new ArrayList();

                    for (int i = 0; i < totalAfter; i++)
                    {
                        if (i > 1 && i < totalAfter)
                        {
                            string substr = Convert.ToString(dt.Columns[i]).Substring(Convert.ToString(dt.Columns[i]).Length - 1);
                            if (substr != "a")
                            {
                                DelTab.Add(Convert.ToString(dt.Columns[i]));
                            }
                        }
                    }

                    for (int i = 0; i < DelTab.Count; i++)
                    {
                        dt.Columns.Remove(Convert.ToString(DelTab[i]));
                    }

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (Convert.ToString(dt.Columns[i]).Substring(Convert.ToString(dt.Columns[i]).Length - 1) == "a")
                        {
                            string name = Convert.ToString(dt.Columns[i]).Remove(Convert.ToString(dt.Columns[i]).Length - 1);
                            dt.Columns[Convert.ToString(dt.Columns[i])].ColumnName = name;
                        }
                    }
                }
                else
                {
                    return dt;
                }               

                return dt;
            }
        }
    }
}