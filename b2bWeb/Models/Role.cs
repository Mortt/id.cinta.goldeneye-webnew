﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Role
    {
        public int? Role_ID { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets allowed")]
        public string Role_Name { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Role_Desc { get; set; }
        [Required]
        [DisplayName("type")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Role_Type { get; set; }
        public DateTime Role_InsertOn { get; set; }
        public int Role_CompanyID { get; set; }
        public string Role_InsertBy { get; set; }
        public string Insert_Name { get; set; }
        public DateTime Role_UpdateOn { get; set; }
        public string Role_UpdateBy { get; set; }
        public string Update_Name { get; set; }
        public bool? Role_IsActive { get; set; }
        public string jml { get; set; }
    }

    public class JsonRole
    {
        public int total { get; set; }
        public List<rowsRole> rows { get; set; }
    }

    public class rowsRole
    {
        public int Role_ID { get; set; }
        public string Role_Name { get; set; }
        public string Role_Desc { get; set; }
        public string Role_Type { get; set; }
        public DateTime Role_InsertOn { get; set; }
        public int Role_CompanyID { get; set; }
        public string Role_InsertBy { get; set; }
        public string Insert_Name { get; set; }
        public DateTime Role_UpdateOn { get; set; }
        public string Role_UpdateBy { get; set; }
        public string Update_Name { get; set; }
        public bool Role_IsActive { get; set; }
    }
}