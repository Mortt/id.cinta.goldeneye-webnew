﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class RecieveDetails
    {
        public string Recieve_ID { get; set; }
        public string Recieve_OutletCode { get; set; }
        public string Recieve_Image { get; set; }
        public string Recieve_Desc { get; set; }
        public string Recieve_Amount { get; set; }
        public string Recieve_Image2 { get; set; }
        public string Recieve_ImageTTD { get; set; }
        public string Recieve_isClosed { get; set; }
        public DateTime Recieve_ClosedOn { get; set; }
        public string Recieve_ClosedBy { get; set; }
        public string Recieve_CompanyID { get; set; }
        public DateTime Recieve_InsertOn { get; set; }
        public DateTime Recieve_UpdateOn { get; set; }
        public string Recieve_InsertBy { get; set; }
        public string Recieve_UpdateBy { get; set; }
        public string Recieve_Isdelete { get; set; }
        public List<_E> E { get; set; }
    }
    
    public class _E
    {
        public string Recieve_Delta { get; set; }
        public string Recieve_No { get; set; }
        public string Recieve_ProductCode { get; set; }
        public string Recieve_Stock { get; set; }
        public string Recieve_Suggestion { get; set; }
        public List<_F> F { get; set; }
    }

    public class _F
    {
        public string Outlet_Name { get; set; }
        public List<_R> R { get; set; }
    }

    public class _R
    {
        public string Item_Name { get; set; }
        public string Item_Image1 { get; set; }
        public List<_D> D { get; set; }
    }

    public class _D
    {        
        public string Employee_Name { get; set; }
    }
}
        
