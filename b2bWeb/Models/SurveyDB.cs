﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class SurveyDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        public List<Survey> ListAll(TaskGet me)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Survey_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;                

                string M_Survey_Json = JsonConvert.SerializeObject(me);
                com.Parameters.AddWithValue("@paramJson", M_Survey_Json);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Survey> result = JsonConvert.DeserializeObject<IEnumerable<Survey>>(s, settings);
                        return result.ToList<Survey>();
                    }
                }

            }
            return new List<Survey>();
        }

        public List<JsonSurvey> GetDataSurvey(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_Survey_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonSurvey>>(s, settings);
                            return result.ToList<JsonSurvey>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }


        //Method for Deleting an Visit  
        public static bool Delete(Survey itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_Survey_Json = JsonConvert.SerializeObject(itm); ;
                SqlCommand com = new SqlCommand("Del_M_Survey", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_Survey_Json", M_Survey_Json);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        public static bool AddUpdate(Survey itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_SurveyH_Json = JsonConvert.SerializeObject(itm);
                SqlCommand com = new SqlCommand("Ins_M_SurveyH_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_SurveyH_Json", M_SurveyH_Json);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        public static bool AddUpdateQuest(string itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                //string M_SurveyD1_Json = JsonConvert.SerializeObject(itm);
                SqlCommand com = new SqlCommand("Ins_M_SurveyD1_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_SurveyD1_Json", itm);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        public List<DetailsSurvey> ListDetails(TaskGet me)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Survey_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;

                string M_Survey_Json = JsonConvert.SerializeObject(me);
                com.Parameters.AddWithValue("@paramJson", M_Survey_Json);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<DetailsSurvey> result = JsonConvert.DeserializeObject<IEnumerable<DetailsSurvey>>(s, settings);
                        return result.ToList<DetailsSurvey>();
                    }
                }

            }
            return new List<DetailsSurvey>();
        }

        public dynamic ToExcel(int ID)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Survey_BE_ToExcel", con);
                com.CommandTimeout = 250;
                com.Parameters.AddWithValue("SVY_ID", ID);
                com.Parameters.AddWithValue("CompanyID", HttpContext.Current.Session["CompanyID"]);
                com.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                dt.Columns["Survey_ID"].ColumnName = "Narasumber";

                if (dt.Rows.Count > 0)
                {
                    for(int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["Narasumber"] = i+1;
                    }                                        
                    return dt;
                }
                else
                {
                    return dt;
                }
            }
        }
    }
}