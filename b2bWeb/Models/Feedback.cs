﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Feedback
    {
        public int Feedback_ID { get; set; }
        public string Feedback_ProductCode { get; set; }        
        public string Feedback_OutletCode { get; set; }        
        public string Feedback_Image { get; set; }
        public string Feedback_Desc { get; set; }
        public int Feedback_RespondID { get; set; }        
        public string Feedback_InsertBy { get; set; }
        public DateTime Feedback_InsertOn { get; set; }
        public string Feedback_UpdateBy { get; set; }
        public DateTime Feedback_UpdateOn { get; set; }        
        public bool Feedback_Isdelete { get; set; }
        public string Feedback_Employee_Name { get; set; }
        public string Feedback_Product_Name { get; set; }
        public string Feedback_Respond_Name { get; set; }
    }

    public class JsonFeedback
    {
        public int total { get; set; }
        public List<rowsFeedback> rows { get; set;}
    }
    public class rowsFeedback
    {
        public int Feedback_ID { get; set; }
        public string Feedback_ProductCode { get; set; }
        public string Feedback_OutletCode { get; set; }
        public string Feedback_Image { get; set; }
        public string Feedback_Desc { get; set; }
        public int Feedback_RespondID { get; set; }
        public string Feedback_InsertBy { get; set; }
        public DateTime Feedback_InsertOn { get; set; }
        public string Feedback_UpdateBy { get; set; }
        public DateTime Feedback_UpdateOn { get; set; }
        public bool Feedback_Isdelete { get; set; }
        public string Feedback_Employee_Name { get; set; }
        public string Feedback_Product_Name { get; set; }
        public string Feedback_Respond_Name { get; set; }
    }
}