﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class DeliveryDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        public List<Delivery> ListAll()
        {            
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Tr_DeliveryOrder", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;

                Delivery itm = new Delivery();
                itm.Search = "";
                itm.CompanyID = Convert.ToInt16(HttpContext.Current.Session["CompanyID"]);

                string paramJson = JsonConvert.SerializeObject(itm);
                com.Parameters.AddWithValue("@paramJson", paramJson);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Delivery> result = JsonConvert.DeserializeObject<IEnumerable<Delivery>>(s, settings);
                        return result.ToList<Delivery>();
                    }
                }
            }
            return new List<Delivery>();
        }

        public List<JsonDelivery> GetDataDelivery(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_Tr_DeliveryOrder_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;
                    string Parameter = "";                   

                    Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonDelivery>>(s, settings);
                            return result.ToList<JsonDelivery>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        public List<_Z> GetDataDeliveryDetails(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_Tr_DeliveryOrder_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;
                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<_Z>>(s, settings);
                            return result.ToList<_Z>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }
        public static bool AddUpdate(Delivery itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string Tr_DOHJson = JsonConvert.SerializeObject(itm);
                SqlCommand com = new SqlCommand("Ins_DeliveryOrder_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Tr_DOHJson", Tr_DOHJson);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        public static bool Delete(Delivery itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            string Tr_Deli = JsonConvert.SerializeObject(itm);
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Del_Tr_Delivery", con);
                com.CommandType = CommandType.StoredProcedure;  
                com.Parameters.AddWithValue("@Tr_DeliveryJson", Tr_Deli);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        public dynamic ListExport(DateTime g, DateTime c)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Export_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("tipe", "DELIVERY");
                com.Parameters.AddWithValue("startdate", g);
                com.Parameters.AddWithValue("enddate", c);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                return dt;
            }
        }

        public static int Cek(Delivery data)
        {
            int i;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_Delivery", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("DO_Transdate", data.DO_Transdate);
                    com.Parameters.AddWithValue("DO_CompanyID", HttpContext.Current.Session["CompanyID"]);
                    com.Parameters.AddWithValue("DO_OutletCode", data.DO_OutletCode);
                    com.Parameters.AddWithValue("DO_PIC", data.DO_PIC);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<Delivery> results = JsonConvert.DeserializeObject<IEnumerable<Delivery>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}