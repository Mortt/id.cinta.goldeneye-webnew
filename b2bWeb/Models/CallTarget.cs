﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace b2bWeb.Models
{
    public class CallTarget
    {
        public int? TC_id { get; set; }
        //[Required]
        //[DisplayName("employee")]        
        //public string TC_Employee_NIK { get; set; }
        [Required]
        [DisplayName("role")]
        public int TC_Role_ID { get; set; }        
        [Required]
        [DisplayName("periode")]
        public DateTime TC_Periode { get; set; }
        [Required]
        [DisplayName("quantity")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public int TC_Qty { get; set; }
        public int TC_CompanyID { get; set; }
        public bool? TC_IsActive { get; set; }
        public bool TC_IsDelete { get; set; }
        public string TC_UpdateBy { get; set; }
        public DateTime TC_UpdateOn { get; set; }
        public string TC_InsertBy { get; set; }
        public DateTime TC_InsertOn { get; set; }
        public string TC_EmployeeName { get; set; }
        public string TC_RoleName { get; set; }
        public int jml { get; set; }
    }

    public class JsonCallTarget
    {
        public int total { get; set; }
        public List<rowsCallTarget> rows { get; set; }
    }

    public class rowsCallTarget
    {
        public int TC_id { get; set; }
        //public string TC_Employee_NIK { get; set; }
        public int TC_Role_ID { get; set; }
        public DateTime TC_Periode { get; set; }
        public int TC_Qty { get; set; }
        public int TC_CompanyID { get; set; }
        public bool TC_IsActive { get; set; }
        public bool TC_IsDelete { get; set; }
        public string TC_UpdateBy { get; set; }
        public DateTime TC_UpdateOn { get; set; }
        public string TC_InsertBy { get; set; }
        public DateTime TC_InsertOn { get; set; }
        public string TC_EmployeeName { get; set; }
        public string TC_RoleName { get; set; }
    }
}