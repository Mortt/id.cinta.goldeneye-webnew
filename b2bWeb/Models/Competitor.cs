﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Competitor
    {
        public int CompActivity_ID { get; set; }
        public string CompActivity_ProductCode { get; set; }
        public string CompActivity_OutletCode { get; set; }
        public string CompActivity_Image { get; set; }
        public string CompActivity_Desc { get; set; }
        public string CompActivity_InsertBy { get; set; }
        public DateTime CompActivity_InsertOn { get; set; }
        public string CompActivity_UpdateBy { get; set; }
        public DateTime CompActivity_UpdateOn { get; set; }
        public bool CompActivity_isActive { get; set; }
        public bool CompActivity_Isdelete { get; set; }
        public string Nama_SPG { get; set; }
        public string Item_Name { get; set; }
    }

    public class JsonCompetitor
    {
        public int total { get; set; }
        public List<rowsCompetitor> rows { get; set; }
    }
    public class rowsCompetitor
    {
        public int CompActivity_ID { get; set; }
        public string CompActivity_ProductCode { get; set; }
        public string CompActivity_OutletCode { get; set; }
        public string CompActivity_Image { get; set; }
        public string CompActivity_Desc { get; set; }
        public string CompActivity_InsertBy { get; set; }
        public DateTime CompActivity_InsertOn { get; set; }
        public string CompActivity_UpdateBy { get; set; }
        public DateTime CompActivity_UpdateOn { get; set; }
        public bool CompActivity_isActive { get; set; }
        public bool CompActivity_Isdelete { get; set; }
        public string Nama_SPG { get; set; }
        public string Item_Name { get; set; }
    }
}