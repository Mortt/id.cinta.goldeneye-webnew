﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace b2bWeb.Models
{
    public class MenuDB
    {
        public static List<Menu> GetMenu()
        {
            string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            dynamic result = null;
            try
            {
                using (SqlConnection cnn = new SqlConnection(CS))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("SP_Get_Menu", cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };
                   
                    cmd.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            result = JsonConvert.DeserializeObject<List<Menu>>(s, settings);
                            return result.ToList<Menu>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return result;
        }

        public static List<HeaderMenu> ListMenu(LoginPacket Menu)
        {
            string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_M_Menu", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;

                string paramJson = JsonConvert.SerializeObject(Menu);
                com.Parameters.AddWithValue("@paramJson", paramJson);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<HeaderMenu> result = JsonConvert.DeserializeObject<IEnumerable<HeaderMenu>>(s, settings);
                        return result.ToList<HeaderMenu>();
                    }
                }
            }
            return new List<HeaderMenu>();
        }
    }
}