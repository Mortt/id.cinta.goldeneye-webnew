﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Achievement
    {
        public decimal Achievements { set; get; }
        public string Tahun { set; get; }
        public string Bulan { set; get; }
        public decimal Sales_LineTotal { set; get; }
        public int Sales_QTY { set; get; }
        public DateTime Target_Periode { set; get; }
        public string Target_OutletCode { set; get; }
        public string Target_OutletName { set; get; }
        public decimal Target_Amount { set; get; }
        public string Employee_NIK { set; get; }
        public string Employee_Name { set; get; }
    }

    public class JsonAchievement
    {
        public int total { get; set; }
        public List<rowsAchievement> rows { get; set; }
    }

    public class rowsAchievement
    {
        public decimal Achievements { set; get; }
        public string Tahun { set; get; }
        public string Bulan { set; get; }
        public decimal Sales_LineTotal { set; get; }
        public int Sales_QTY { set; get; }
        public DateTime Target_Periode { set; get; }
        public string Target_OutletCode { set; get; }
        public string Target_OutletName { set; get; }
        public decimal Target_Amount { set; get; }
        public string Employee_NIK { set; get; }
        public string Employee_Name { set; get; }
    }
}