﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Planogram
    {
        public int Planogram_ID { get; set; }
        public string Planogram_ProductCode { get; set; }
        public string Employee_Name { get; set; }
        public string Area_Name { get; set; }
        public string Planogram_OutletCode { get; set; }
        public string Outlet_Name { get; set; }
        public string Planogram_Image { get; set; }
        public string Planogram_Image2 { get; set; }
        public string Planogram_Image3 { get; set; }
        public string Planogram_Image4 { get; set; }
        public string Planogram_Image5 { get; set; }
        public int Planogram_Tier { get; set; }
        public string Planogram_Desc { get; set; }
        public string Respond_Module { get; set; }        
        public double Planogram_ReasonID { get; set; }
        //public string Planogram_ReasonName { get; set; }
        public string Planogram_InsertBy { get; set; }
        public DateTime Planogram_InsertOn { get; set; }
        public string Planogram_UpdateBy { get; set; }
        public DateTime Planogram_UpdateOn { get; set; }        
        public bool Planogram_Isdelete { get; set; }
    }

    public class JsonPlanogram
    {
        public int total { get; set; }
        public List<rowsPlanogram> rows { get; set; }
    }
    public class rowsPlanogram
    {
        public int Planogram_ID { get; set; }
        public string Planogram_ProductCode { get; set; }
        public string Employee_Name { get; set; }
        public string Area_Name { get; set; }
        public string Planogram_OutletCode { get; set; }
        public string Outlet_Name { get; set; }
        public string Planogram_Image { get; set; }
        public string Planogram_Image2 { get; set; }
        public string Planogram_Image3 { get; set; }
        public string Planogram_Image4 { get; set; }
        public string Planogram_Image5 { get; set; }
        public int Planogram_Tier { get; set; }
        public string Planogram_Desc { get; set; }
        public string Respond_Module { get; set; }
        public double Planogram_ReasonID { get; set; }
        //public string Planogram_ReasonName { get; set; }
        public string Planogram_InsertBy { get; set; }
        public DateTime Planogram_InsertOn { get; set; }
        public string Planogram_UpdateBy { get; set; }
        public DateTime Planogram_UpdateOn { get; set; }
        public bool Planogram_Isdelete { get; set; }
    }
}