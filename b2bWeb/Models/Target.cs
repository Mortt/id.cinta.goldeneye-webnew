﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Target
    {
        public int? Target_ID { set; get; }
        [Required]
        [DisplayName("periode")]
        public DateTime Target_Periode { set; get; }
        [Required]
        [DisplayName("outlet code")]
        [RegularExpression("^[a-zA-Z 0-9 +-]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Target_OutletCode { set; get; }
        public string Target_OutletName { set; get; }
        [Required]
        [DisplayName("amount")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public decimal Target_Amount { set; get; }
        public int Target_CompanyID { set; get; }
        public string Target_InsertBy { set; get; }
        public DateTime Target_InsertOn { set; get; }
        public string Target_UpdateBy { set; get; }
        public DateTime Target_UpdateOn { set; get; }
        public bool? Target_IsActive { set; get; }
        public int jml { set; get; }        
    }

    public class JsonTarget
    {
        public int total { get; set; }
        public List<rowsTarget> rows { get; set; }
    }

    public class rowsTarget
    {
        public int Target_ID { set; get; }
        public DateTime Target_Periode { set; get; }
        public string Target_OutletCode { set; get; }
        public string Target_OutletName { set; get; }
        public decimal Target_Amount { set; get; }
        public int Target_CompanyID { set; get; }
        public string Target_InsertBy { set; get; }
        public DateTime Target_InsertOn { set; get; }
        public string Target_UpdateBy { set; get; }
        public DateTime Target_UpdateOn { set; get; }
        public bool Target_IsActive { set; get; }
    }
}