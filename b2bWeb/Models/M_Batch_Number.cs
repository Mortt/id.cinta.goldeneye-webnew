﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class M_Batch_Number
    {
        public int? BN_id { set; get; }
        [Required]
        [DisplayName("product")]
        public string BN_Item_Code { set; get; }
        [Required]
        [DisplayName("batch number")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string BN_BatchNumber { set; get; }
        [Required]
        [DisplayName("exp date")]
        public DateTime BN_Exp_Date { set; get; }
        public int BN_CompanyID { set; get; }
        public DateTime BN_InsertOn { set; get; }
        public string BN_InsertBy { set; get; }
        public DateTime BN_UpdateOn { set; get; }
        public string BN_UpdateBy { set; get; }
        public bool? BN_IsActive { set; get; }
        public bool BN_IsDelete { set; get; }
        public string Product_Name { set; get; }
        public string Insert_Name { set; get; }
        public string Category_Name { set; get; }
        public string Update_Name { set; get; }       
    }
    public class JsonM_Batch_Number
    {
        public int total { get; set; }
        public List<rowsM_Batch_Number> rows { get; set; }
    }

    public class rowsM_Batch_Number
    {
        public int BN_id { set; get; }
        public string BN_Item_Code { set; get; }
        public string BN_BatchNumber { set; get; }
        public DateTime BN_Exp_Date { set; get; }
        public int BN_CompanyID { set; get; }
        public DateTime BN_InsertOn { set; get; }
        public string BN_InsertBy { set; get; }
        public DateTime BN_UpdateOn { set; get; }
        public string BN_UpdateBy { set; get; }
        public bool BN_IsActive { set; get; }
        public bool BN_IsDelete { set; get; }
        public string Product_Name { set; get; }
        public string Insert_Name { set; get; }
        public string Category_Name { set; get; }
        public string Update_Name { set; get; }
    }
}