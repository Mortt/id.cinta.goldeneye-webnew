﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Sampling
    {
        public Int32 Sampling_ID { get; set; }
        public string Sampling_ProductCode { get; set; }
        public string Sampling_CategoryCode { get; set; }
        public string Sampling_OutletCode { get; set; }
        public string Sampling_Image { get; set; }
        public int Sampling_QTY { get; set; }
        public string Sampling_Desc { get; set; }
        public Int32 Sampling_RespondID { get; set; }
        public DateTime Sampling_InsertOn { get; set; }
        public DateTime Sampling_UpdateOn { get; set; }
        public string Sampling_InsertBy { get; set; }
        public string Sampling_UpdateBy { get; set; }
        public bool Sampling_Isdelete { get; set; }
        public string Nama_Item { get; set; }
        public string Nama_Category { get; set; }
        public string Nama_SPG { get; set; }
        public string Nama_Outlet { get; set; }
        public string Nama_Respond { get; set; }

    }

    public class JsonSampling
    {
        public int total { get; set; }
        public List<rowsSampling> rows { get; set; }

    }
    public class rowsSampling
    {
        public Int32 Sampling_ID { get; set; }
        public string Sampling_ProductCode { get; set; }
        public string Sampling_CategoryCode { get; set; }
        public string Sampling_OutletCode { get; set; }
        public string Sampling_Image { get; set; }
        public int Sampling_QTY { get; set; }
        public string Sampling_Desc { get; set; }
        public Int32 Sampling_RespondID { get; set; }
        public DateTime Sampling_InsertOn { get; set; }
        public DateTime Sampling_UpdateOn { get; set; }
        public string Sampling_InsertBy { get; set; }
        public string Sampling_UpdateBy { get; set; }
        public bool Sampling_Isdelete { get; set; }
        public string Nama_Item { get; set; }
        public string Nama_Category { get; set; }
        public string Nama_SPG { get; set; }
        public string Nama_Outlet { get; set; }
        public string Nama_Respond { get; set; }

    }
}