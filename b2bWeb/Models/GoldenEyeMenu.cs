﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class GoldenEyeMenu
    {
        public int MMG_ID { get; set; }
        public string MMG_Name { get; set; }
        public string MMG_Icon { get; set; }
        public bool MMG_IsActive { get; set; }
        public bool MMG_IsDelete { get; set; }
        public DateTime MMG_InsertOn { get; set; }
        public string MMG_InsertBy { get; set; }
        public DateTime MMG_UpdateOn { get; set; }
        public string MMG_UpdateBy { get; set; }
    }
}