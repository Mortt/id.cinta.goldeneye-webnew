﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Company
    {
        public int Company_ID { set; get; }

        [Required(ErrorMessage = "Nama Perusahaan Required")]
        public string Company_Name { get; set; }
        public string Company_Desc { get; set; }
        public decimal Company_Distance { set; get; }
        public string Company_Distance1 { set; get; }
        public decimal Company_DistanceOutlet { set; get; }
        public string Company_DistanceOutlet1 { set; get; }
        public bool Company_FaceRecognation { set; get; }
        public int Company_RouteOptimize { set; get; }        
        public string Company_Connect { set; get; }
        public DateTime Company_InsertOn { set; get; }
        public string Company_Insertby { set; get; }
        public DateTime Company_UpdateOn { set; get; }
        public string Company_Updateby { set; get; }
        public bool Company_IsActive { set; get; }

        [Required(ErrorMessage = "Email Required")]
        public string Company_Email { set; get; }

        [Required(ErrorMessage = "NPWP Required")]
        public string Company_NPWP { set; get; }

        [Required(ErrorMessage = "Address Required")]
        [DataType(DataType.MultilineText)]
        public string Company_Address { set; get; }

        [Required(ErrorMessage = "No Tlp Required")]
        public string Company_No_Telp { set; get; }

        [Required(ErrorMessage = "WhatsApp Required")]
        public string Company_Wa { set; get; }
        public string Company_Logo { set; get; }
        public string Images { get; set; }
        public string Images_Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Packet Required")]        
        public int Company_Packet_ID { get; set; }
    }
}