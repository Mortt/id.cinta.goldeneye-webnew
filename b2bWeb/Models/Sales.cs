﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Sales
    {
        public int TransID { get; set; }
        public string TransCode { get; set; }
        public string Sales_Transdate { get; set; }
        public string Sales_Merchant_ID { get; set; }
        public int Sales_Customer_ID { get; set; }
        public int Sales_Type_ID { get; set; }
        public string Sales_Address { get; set; }
        public string Sales_Contact_ID { get; set; }
        public string Sales_Delivery_ID { get; set; }
        public string Sales_Delivery_Contact { get; set; }
        public string Sales_Delivery_Name { get; set; }
        public DateTime Sales_Delivery_InsertOn { get; set; }
        public int Sales_Promo_ID { get; set; }
        public int Sales_Table_No { get; set; }
        public string Sales_Promotion_Amout { get; set; }
        public string Sales_Gross_Amount { get; set; }
        public string Sales_PPN_Amount { get; set; }
        public string Sales_Nett_amount { get; set; }
        public double Sales_Total_Amount { get; set; }
        public string Sales_Total_Qty { get; set; }
        public string Sales_Desc { get; set; }
        public string Sales_isServed { get; set; }
        public DateTime Sales_ServedOn { get; set; }
        public string Sales_ServedBy { get; set; }
        public string Sales_isPaid { get; set; }
        public bool Sales_IsDelete { get; set; }
        public DateTime Sales_InsertOn { get; set; }
        public string Sales_InsertBy { get; set; }
        public DateTime Sales_UpdateOn { get; set; }
        public string Sales_UpdateBy { get; set; }
        public string Insert_Oleh { get; set; }
        public string Selling_Outlet { get; set; }
        public string Selling_Outlet_Code { get; set; }        
    }

    public class JsonSales
    {
        public int total { get; set; }
        public List<rowsSales> rows { get; set; }
    }

    public class rowsSales
    {
        public int TransID { get; set; }
        public string TransCode { get; set; }
        public string Sales_Transdate { get; set; }
        public string Sales_Merchant_ID { get; set; }
        public int Sales_Customer_ID { get; set; }
        public int Sales_Type_ID { get; set; }
        public string Sales_Address { get; set; }
        public string Sales_Contact_ID { get; set; }
        public string Sales_Delivery_ID { get; set; }
        public string Sales_Delivery_Contact { get; set; }
        public string Sales_Delivery_Name { get; set; }
        public DateTime Sales_Delivery_InsertOn { get; set; }
        public int Sales_Promo_ID { get; set; }
        public int Sales_Table_No { get; set; }
        public string Sales_Promotion_Amout { get; set; }
        public string Sales_Gross_Amount { get; set; }
        public string Sales_PPN_Amount { get; set; }
        public string Sales_Nett_amount { get; set; }
        public double Sales_Total_Amount { get; set; }
        public string Sales_Total_Qty { get; set; }
        public string Sales_Desc { get; set; }
        public string Sales_isServed { get; set; }
        public DateTime Sales_ServedOn { get; set; }
        public string Sales_ServedBy { get; set; }
        public string Sales_isPaid { get; set; }
        public bool Sales_IsDelete { get; set; }
        public DateTime Sales_InsertOn { get; set; }
        public string Sales_InsertBy { get; set; }
        public DateTime Sales_UpdateOn { get; set; }
        public string Sales_UpdateBy { get; set; }
        public string Insert_Oleh { get; set; }
        public string Selling_Outlet { get; set; }
        public string Selling_Outlet_Code { get; set; }
    }

}