﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Pay
    {
        public int Paid_ID { get; set; }
        public string Paid_Code { get; set; }
        public DateTime Paid_Transdate { get; set; }
        public DateTime Paid_Periode { get; set; }
        public int Paid_CompanyID { get; set; }
        public string Paid_Type { get; set; }
        public DateTime Paid_InsertOn { get; set; }
        public string Paid_InsertBy { get; set; }
        public DateTime Paid_UpdateOn { get; set; }
        public string Paid_UpdateBy { get; set; }
        public bool Paid_Isdelete { get; set; }
        public string Pay_InsertBy_Name { get; set; }
        public string Pay_UpdateBy_Name { get; set; }
    }

    public class JsonPay
    {
        public int total { get; set; }
        public List<rowsPay> rows { get; set; }
    }
    public class rowsPay
    {
        public int Paid_ID { get; set; }
        public string Paid_Code { get; set; }
        public DateTime Paid_Transdate { get; set; }
        public DateTime Paid_Periode { get; set; }
        public int Paid_CompanyID { get; set; }
        public string Paid_Type { get; set; }
        public DateTime Paid_InsertOn { get; set; }
        public string Paid_InsertBy { get; set; }
        public DateTime Paid_UpdateOn { get; set; }
        public string Paid_UpdateBy { get; set; }
        public bool Paid_Isdelete { get; set; }
        public string Pay_InsertBy_Name { get; set; }
        public string Pay_UpdateBy_Name { get; set; }
    }
}