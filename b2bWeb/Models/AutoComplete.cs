﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class AutoComplete
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string NIK { get; set; }
        public string Name_NIK { get; set; }
        public string Regen_Dis { get; set; }

    }
}