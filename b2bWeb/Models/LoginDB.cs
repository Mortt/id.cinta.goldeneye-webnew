﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web.Mvc;
namespace b2bWeb.Models
{
    public class LoginDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Visits  
        public List<Login> ListAll(string username, string password)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Logins", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("username", username);
                com.Parameters.AddWithValue("password", password);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    IEnumerable<Login> result = JsonConvert.DeserializeObject<IEnumerable<Login>>(rdr.GetString(0).ToString(), settings);

                    if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["Username"] == null)
                    {
                        HttpContext.Current.Session["UserID"] = result.ToList<Login>()[1].ToString();
                        HttpContext.Current.Session["FirstName"] = result.ToList<Login>()[2].ToString();
                        HttpContext.Current.Session["CompanyID"] = result.ToList<Login>()[10].ToString();

                    }


                    return result.ToList<Login>();
                }
                return new List<Login>();
            }
        }


    }
}