﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace b2bWeb.Models
{
    public class CompanyDB
    {
        public static List<Company> ListAll()
        {
            string Cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GET_COMPANY", con)
                {
                    CommandTimeout = 30,
                    CommandType = CommandType.StoredProcedure
                };
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Company> result = JsonConvert.DeserializeObject<IEnumerable<Company>>(s, settings);
                        return result.ToList<Company>();
                    }
                }

            }
            return new List<Company>();
        }

        public int UpdateCompany(Company model)
        {
            string Cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            int i;
            using (SqlConnection con = new SqlConnection(Cs))
            {
                con.Open();
                string M_Company_Json = JsonConvert.SerializeObject(model);
                SqlCommand com = new SqlCommand("Ins_M_Company_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@paramJson", M_Company_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}