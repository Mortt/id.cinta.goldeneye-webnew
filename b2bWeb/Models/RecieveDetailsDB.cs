﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace b2bWeb.Models
{
    public class RecieveDetailsDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //Return list of all Items  
        public List<RecieveDetails> ListAll(int value)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_GetRecieve_Details", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("ID", value);
                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<RecieveDetails> result = JsonConvert.DeserializeObject<IEnumerable<RecieveDetails>>(s, settings);
                        return result.ToList<RecieveDetails>();
                    }
                }

            }
            return new List<RecieveDetails>();
        }

    }
}