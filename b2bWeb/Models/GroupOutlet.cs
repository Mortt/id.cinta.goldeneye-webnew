﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class GroupOutlet
    {
        public int? GroupOutlet_ID { get; set; }
        [Required]
        [DisplayName("code")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string GroupOutlet_Code { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string GroupOutlet_Name { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string GroupOutlet_Desc { get; set; }
        public string GroupOutlet_CompanyID { get; set; }
        public bool? GroupOutlet_isActive { get; set; }
        public bool GroupOutlet_isDelete { get; set; }
        public DateTime GroupOutlet_InsertOn { get; set; }
        public string GroupOutlet_InsertBy { get; set; }
        public string GroupOutlet_InsertBy_Name { get; set; }
        public DateTime GroupOutlet_UpdateOn { get; set; }
        public string GroupOutlet_UpdateBy { get; set; }
        public string GroupOutlet_UpdateBy_Name { get; set; }
        public int jml { get; set; }
    }

    public class JsonGroupOutlet
    {
        public int total { get; set; }
        public List<rowsGroupOutlet> rows { get; set; }
    }

    public class rowsGroupOutlet
    {
        public int GroupOutlet_ID { get; set; }
        public string GroupOutlet_Code { get; set; }
        public string GroupOutlet_Name { get; set; }
        public string GroupOutlet_Desc { get; set; }
        public string GroupOutlet_CompanyID { get; set; }
        public bool GroupOutlet_isActive { get; set; }
        public bool GroupOutlet_isDelete { get; set; }
        public DateTime GroupOutlet_InsertOn { get; set; }
        public string GroupOutlet_InsertBy { get; set; }
        public string GroupOutlet_InsertBy_Name { get; set; }
        public DateTime GroupOutlet_UpdateOn { get; set; }
        public string GroupOutlet_UpdateBy { get; set; }
        public string GroupOutlet_UpdateBy_Name { get; set; }
    }
}