﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;
using Newtonsoft.Json;

namespace b2bWeb.Models
{
    public class M_Batch_NumberDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        public List<M_Batch_Number> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("S_Get_BatchNumber_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<M_Batch_Number> result = JsonConvert.DeserializeObject<IEnumerable<M_Batch_Number>>(s, settings);
                        return result.ToList<M_Batch_Number>();
                    }
                }

            }
            return new List<M_Batch_Number>();
        }        

        public int AddUd(M_Batch_Number Inv)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string M_Inv_Json = JsonConvert.SerializeObject(Inv);
                SqlCommand com = new SqlCommand("SP_Ins_BatchNumber_BE", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BatchNumber", M_Inv_Json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Delete(M_Batch_Number itm)
        {
            string M_ProfileJson = JsonConvert.SerializeObject(itm);
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Del_BatchNumber_BE", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BatchNumber", M_ProfileJson);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public List<JsonM_Batch_Number> GetDataBatch_Number(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("S_Get_BatchNumber_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonM_Batch_Number>>(s, settings);
                            return result.ToList<JsonM_Batch_Number>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }
    }
}