﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Promo
    {
        public int? Promosi_ID { set; get; }
        [Required]
        [DisplayName("code")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Promosi_Code { set; get; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Promosi_Name { set; get; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Promosi_Desc { set; get; }
        [Required]
        [DisplayName("category/brand")]
        //ini diisi dengan code product karna ada perubahan
        public string Promosi_CategoryCode { set; get; }
        public string Promosi_File1 { set; get; }
        public string Promosi_File11 { set; get; }        
        public string Promosi_File2 { set; get; }
        public string Promosi_File22 { set; get; }
        public int Promosi_Type { set; get; }
        public int Promosi_CompanyID { set; get; }
        public DateTime Promosi_InsertOn { set; get; }
        public string Promosi_InsertBy { set; get; }
        public DateTime Promosi_UpdateOn { set; get; }
        public string Promosi_UpdateBy { set; get; }
        public bool? Promosi_IsActive { set; get; }
        public bool Promosi_IsDelete { set; get; }
        public string Promosi_Category_Name { set; get; }
        public string Promosi_InsertBy_Name { set; get; }
        public string Promosi_UpdateBy_Name { set; get; }
        public int Radios { set; get; }
        public int Jml { set; get; }
    }

    public class JsonPromo
    {
        public int total { get; set; }
        public List<rowsPromo> rows { get; set; }
    }
    public class rowsPromo
    {
        public int Promosi_ID { set; get; }
        public string Promosi_Code { set; get; }
        public string Promosi_Name { set; get; }
        public string Promosi_Desc { set; get; }
        public string Promosi_CategoryCode { set; get; }
        public string Promosi_File1 { set; get; }
        public string Promosi_File11 { set; get; }
        public string Promosi_File2 { set; get; }
        public string Promosi_File22 { set; get; }
        public int Promosi_Type { set; get; }
        public int Promosi_CompanyID { set; get; }
        public DateTime Promosi_InsertOn { set; get; }
        public string Promosi_InsertBy { set; get; }
        public DateTime Promosi_UpdateOn { set; get; }
        public string Promosi_UpdateBy { set; get; }
        public bool Promosi_IsActive { set; get; }
        public bool Promosi_IsDelete { set; get; }
        public string Promosi_Category_Name { set; get; }
        public string Promosi_InsertBy_Name { set; get; }
        public string Promosi_UpdateBy_Name { set; get; }
        public int Radios { set; get; }
    }
}