﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class ExpDate
    {
        public int ExpDate_ID { get; set; }
        public string ExpDate_Date { get; set; }
        public string ExpDate_Image { get; set; }
        public string ExpDate_Image2 { get; set; }
        public string ExpDate_Image3 { get; set; }
        public string ExpDate_Image4 { get; set; }
        public string ExpDate_Image5 { get; set; }
        public string ExpDate_InsertBy { get; set; }
        public DateTime ExpDate_InsertOn { get; set; }
        public bool ExpDate_Isdelete { get; set; }
        public string ExpDate_OutletCode { get; set; }
        public string ExpDate_ProductCode { get; set; }
        public string Item_Name { get; set; }
        public string ExpDate_Qty { get; set; }
        public string ExpDate_ReasonID { get; set; }
        public string ExpDate_UpdateBy { get; set; }
        public DateTime ExpDate_UpdateOn { get; set; }
        public string Outlet_Name { get; set; }
        public string Employee_Name { get; set; }
        public string Area_Name { get; set; }
        public string Respond_Name { get; set; }
        public string ExpDate_BatchNo { get; set; }
        public string Item_Image1 { get; set; }
    }

    public class JsonExpDate
    {
        public int total { get; set; }
        public List<rowsExpDate> rows { get; set; }
    }
    public class rowsExpDate
    {
        public int ExpDate_ID { get; set; }
        public string ExpDate_Date { get; set; }
        public string ExpDate_Image { get; set; }
        public string ExpDate_Image2 { get; set; }
        public string ExpDate_Image3 { get; set; }
        public string ExpDate_Image4 { get; set; }
        public string ExpDate_Image5 { get; set; }
        public string ExpDate_InsertBy { get; set; }
        public DateTime ExpDate_InsertOn { get; set; }
        public bool ExpDate_Isdelete { get; set; }
        public string ExpDate_OutletCode { get; set; }
        public string ExpDate_ProductCode { get; set; }
        public string Item_Name { get; set; }
        public string ExpDate_Qty { get; set; }
        public string ExpDate_ReasonID { get; set; }
        public string ExpDate_UpdateBy { get; set; }
        public DateTime ExpDate_UpdateOn { get; set; }
        public string Outlet_Name { get; set; }
        public string Employee_Name { get; set; }
        public string Area_Name { get; set; }
        public string Respond_Name { get; set; }
        public string ExpDate_BatchNo { get; set; }
        public string Item_Image1 { get; set; }
    }
}