﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Coaching
    {
        public int Coaching_ID { set; get; }
        public string CoachingWith { set; get; }
        public string Coaching_With { set; get; }
        public string Coaching_ReffID { set; get; }
        public string Coaching_Desc { set; get; }
        public DateTime Coaching_Start { set; get; }
        public DateTime Coaching_End { set; get; }
        public string Coaching_ReasonID { set; get; }
        public int Coaching_CompanyID { set; get; }
        public DateTime Coaching_InsertOn { set; get; }
        public string CoachingInsertBy { set; get; }
        public DateTime Coaching_UpdateOn { set; get; }
        public string Coaching_UpdateBy { set; get; }
        public bool Coaching_Isdelete { set; get; }
    }

    public class JsonCoaching
    {
        public int total { get; set; }
        public List<rowsCoaching> rows { get; set; }
    }

    public class rowsCoaching
    {
        public int Coaching_ID { set; get; }
        public string CoachingWith { set; get; }
        public string Coaching_With { set; get; }
        public string Coaching_ReffID { set; get; }
        public string Coaching_Desc { set; get; }
        public DateTime Coaching_Start { set; get; }
        public DateTime Coaching_End { set; get; }
        public string Coaching_ReasonID { set; get; }
        public int Coaching_CompanyID { set; get; }
        public DateTime Coaching_InsertOn { set; get; }
        public string CoachingInsertBy { set; get; }
        public DateTime Coaching_UpdateOn { set; get; }
        public string Coaching_UpdateBy { set; get; }
        public bool Coaching_Isdelete { set; get; }
    }
}