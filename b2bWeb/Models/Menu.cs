﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Menu
    {
        //public int Menu_ID { set; get; }
        //public int Master_Menu_ID { set; get; }
        //public int Sub_Menu_ID { set; get; }
        //public int Sub_Menu_2ID { set; get; }
        //public string Menu_URL { set; get; }
        //public string Menu_Controller { set; get; }
        //public string Menu_Controller_Name { set; get; }
        //public string Menu_Name { set; get; }
        //public string Menu_Description { set; get; }
        //public string Menu_Icon { set; get; }
        //public int Menu_Company_ID { set; get; }
        //public DateTime Menu_InsertOn { set; get; }
        //public int Menu_InsertBy { set; get; }
        //public DateTime Menu_UpdateOn { set; get; }
        //public int Menu_UpdateBy { set; get; }

        public int Menu_Analytics_Company_ID { set; get; }
        public string Menu_Analytics_Name { set; get; }
        public string Menu_Analytics_Url { set; get; }
        public string Menu_Analytics_Controller { set; get; }
    }

    public class HeaderMenu
    {
        public int MHeader_ID { get; set; }
        public string MHeader_Name { get; set; }
        public string MHeader_Icon { get; set; }
        public List<_Menu> Menu { get; set; }
    }

    public class _Menu
    {
        public string Menu_Controller { set; get; }
        public string Menu_Controller_Name { set; get; }
        public string Menu_Icon { set; get; }
        public int Menu_ID { set; get; }
        public int Menu_MHeader_ID { set; get; }
        public string Menu_Name { set; get; }
    }
}   