﻿using System.Collections.Generic;

namespace b2bWeb.Models
{
    public class Map
    {
        public string title { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string Employee_Name { get; set; }
        public string Date { get; set; }
        public string Visit_Checkin { get; set; }
        public string Visit_Checkout { get; set; }
        public string Outlet_Name { get; set; }
        public string number { get; set; }
        public string Visit_Image { get; set; }
        public string Visit_Nik { get; set; }
        public List<_LatLng> LatLng { get; set; }
    }

    public class _LatLng
    {
        public string lat { get; set; }
        public string lng { get; set; }
        public string Employee_Name { get; set; }
        public string Date { get; set; }
        public string Outlet_Name { get; set; }
        public string Visit_Checkin { get; set; }
        public string Visit_Checkout { get; set; }
        public string number { get; set; }
        public string Visit_Image { get; set; }
    }

}