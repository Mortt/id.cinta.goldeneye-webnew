﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class CollectionDB
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        public List<Collection> ListAll()
        {            
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Tr_Collection", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;

                Collection itm = new Collection();
                itm.Search = "";
                itm.CompanyID = Convert.ToInt16(HttpContext.Current.Session["CompanyID"]);

                string paramJson = JsonConvert.SerializeObject(itm);
                com.Parameters.AddWithValue("@paramJson", paramJson);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Collection> result = JsonConvert.DeserializeObject<IEnumerable<Collection>>(s, settings);
                        return result.ToList<Collection>();
                    }
                }

            }
            return new List<Collection>();
        }

        public List<JsonCollection> GetDataCollection(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_Tr_Collection_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonCollection>>(s, settings);
                            return result.ToList<JsonCollection>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        //Method for Adding and Update  
        public static bool AddUpdate(Collection itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string Tr_CollectionJson = JsonConvert.SerializeObject(itm); ;
                SqlCommand com = new SqlCommand("Ins_Tr_Collection_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Tr_CollectionJson", Tr_CollectionJson);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        //Method for Deleting an Visit  
        public static bool Delete(Collection itm)
        {
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            string Tr_Collection = JsonConvert.SerializeObject(itm);
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Del_Tr_Collection_BE", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Tr_Collection", Tr_Collection);
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    reader.Close();
                }
            }
            return false;
        }

        public dynamic ListExport(DateTime g, DateTime c)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Export_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("tipe", "COLLECTION");
                com.Parameters.AddWithValue("startdate", g);
                com.Parameters.AddWithValue("enddate", c);
                com.Parameters.AddWithValue("Company_ID", HttpContext.Current.Session["CompanyID"]);

                DataTable dt = new DataTable();
                dt.Load(com.ExecuteReader());

                return dt;
            }
        }

        public static int Cek(Collection data)
        {
            int i;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_Collection", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("Inv_DueDate", data.Inv_DueDate);
                    com.Parameters.AddWithValue("DO_CompanyID", HttpContext.Current.Session["CompanyID"]);
                    com.Parameters.AddWithValue("Inv_OutletCode", data.Inv_OutletCode);
                    com.Parameters.AddWithValue("Inv_PIC", data.Inv_PIC);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<Collection> results = JsonConvert.DeserializeObject<IEnumerable<Collection>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}