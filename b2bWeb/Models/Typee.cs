﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Typee
    {
        public int Type_ID { set; get; }
        public string Type_Code { set; get; }
        public string Type_Name { set; get; }
        public string Type_Desc { set; get; }
        public string Type_Module { set; get; }
        public int Type_CompanyID { set; get; }
        public DateTime Type_InsertOn { set; get; }
        public string Type_InsertBy { set; get; }
        public DateTime Type_UpdateOn { set; get; }
        public string Type_UpdateBy { set; get; }
        public bool Type_IsActive { set; get; }
        public bool Type_IsDelete { set; get; }
    }
}