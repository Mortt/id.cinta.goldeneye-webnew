﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Area
    {
        public int? Area_ID { get; set; }
        [Required]
        [DisplayName("name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets allowed")]
        public string Area_Name { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Area_Desc { get; set; }
        public string Area_Image { get; set; }
        [Required]
        [DisplayName("district")]
        public string Area_DistrictID { get; set; }
        [Required]
        [DisplayName("longitude")]
        public decimal Area_Long { get; set; }
        [Required]
        [DisplayName("latitude")]
        public decimal Area_Lat { get; set; }
        public int Area_CompanyID { get; set; }
        public int Area_PrincipalID { get; set; }
        public string Area_InsertBy { get; set; }
        public DateTime Area_InsertOn { get; set; }
        public string Area_UpdateBy { get; set; }
        public DateTime Area_UpdateOn { get; set; }
        public bool? Area_isActive { get; set; }
        public string District_Name { get; set; }
        public string Insert_Oleh { get; set; }
        public string Update_Oleh { get; set; }
        public string jml { get; set; }
    }

    public class JsonArea
    {
        public int total { get; set; }
        public List<rowsArea> rows { get; set; }

    }

    public class rowsArea
    {
        public int Area_ID { get; set; }
        public string Area_Name { get; set; }
        public string Area_Desc { get; set; }
        public string Area_Image { get; set; }
        public string Area_DistrictID { get; set; }
        public decimal Area_Long { get; set; }
        public decimal Area_Lat { get; set; }
        public int Area_CompanyID { get; set; }
        public int Area_PrincipalID { get; set; }
        public string Area_InsertBy { get; set; }
        public DateTime Area_InsertOn { get; set; }
        public string Area_UpdateBy { get; set; }
        public DateTime Area_UpdateOn { get; set; }
        public bool Area_isActive { get; set; }
        public string District_Name { get; set; }
        public string Insert_Oleh { get; set; }
        public string Update_Oleh { get; set; }
    }
}