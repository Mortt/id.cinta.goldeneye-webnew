﻿using System;
using System.ComponentModel;

namespace b2bWeb.Models
{
    public class Category
    {
        public string Category_Code { get; set; }
        public string Category_Name { get; set; }
        public string Category_Desc { get; set; }
    }
}