﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Parameter
    {
        public int PageNumber { get; set; }
        public int RowspPage { get; set; }
        public int Company_ID { get; set; }
        public string Search { get; set; }
        public string Get_Type { get; set; }
        public string Filter { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}