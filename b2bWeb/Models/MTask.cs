﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class MTask
    {
        public int? Task_ID { get; set; }
        [Required]
        [DisplayName("title")]
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Task_Title { get; set; }
        public string Task_Image_Example { get; set; }
        [RegularExpression("^[a-zA-Z 0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed")]
        public string Task_Desc { get; set; }
        public DateTime Task_Deadline { get; set; }
        public int Task_CompanyID { get; set; }
        public bool Task_IsPublish { get; set; }        
        public DateTime Task_InsertOn { get; set; }
        public string Task_InsertBy { get; set; }
        public DateTime Task_UpdateOn { get; set; }
        public string Task_UpdateBy { get; set; }
        public bool? Task_IsActive { get; set; }
        public bool Task_IsDelete { get; set; }
        public string Task_Insert_Name { get; set; }
        public string Task_Update_Name { get; set; }
        public string Search { get; set; }
        public int CompanyID { get; set; }
        public string GetType { get; set; }
        public string Username { get; set; }
        [Required]
        [DisplayName("employee")]
        public string[] Numpang { get; set; }
        public string Images { get; set; }
        public string Employee_NIK { get; set; }
        public string Employee_Name { get; set; }
        public List<_DET> DET { get; set; }
    }

    public class JsonMTask
    {
        public int total { get; set; }
        public List<rowsMTask> rows { get; set; }
    }
    public class rowsMTask
    {
        public int Task_ID { get; set; }
        public string Task_Title { get; set; }
        public string Task_Image_Example { get; set; }
        public string Task_Desc { get; set; }
        public DateTime Task_Deadline { get; set; }
        public int Task_CompanyID { get; set; }
        public bool Task_IsPublish { get; set; }
        public DateTime Task_InsertOn { get; set; }
        public string Task_InsertBy { get; set; }
        public DateTime Task_UpdateOn { get; set; }
        public string Task_UpdateBy { get; set; }
        public bool Task_IsActive { get; set; }
        public bool Task_IsDelete { get; set; }
        public string Task_Insert_Name { get; set; }
        public string Task_Update_Name { get; set; }
        public List<_DET> DET { get; set; }
    }

    public class _DET
    {       
        public string Employee_NIK { get; set; }
        public List<_EMP> EMP { get; set; }
    }

    public class _EMP
    {
        public string Employee_Name { get; set; }
    }
}