﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Stock
    {
        public int? Stock_ID { get; set; }
        public string Stock_OutletCode { get; set; }        
        public string Stock_Outlet_Name { get; set; }     
        [Required]
        [DisplayName("item")]
        public string Stock_ItemCode { get; set; }
        public string Stock_Item_Name { get; set; }
        [Required]
        [DisplayName("suggestion")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers allowed")]
        public int Stock_Suggestion { get; set; }
        public string Stock_InsertBy { get; set; }
        public DateTime Stock_InsertOn { get; set; }
        public int Stock_CompanyID { set; get; }
        public string Stock_UpdateBy { get; set; }
        public DateTime Stock_UpdateOn { get; set; }
        public bool? Stock_IsActive { get; set; }
        public bool Stock_IsDelete { get; set; }        
        public string Stock_Insert_Name { get; set; }
        public string Stock_Update_Name { get; set; }
        public int Jml { get; set; }  
    }

    public class JsonStock
    {
        public int total { get; set; }
        public List<rowsStock> rows { get; set; }
    }

    public class rowsStock
    {
        public int Stock_ID { get; set; }
        public string Stock_OutletCode { get; set; }
        public string Stock_Outlet_Name { get; set; }
        public string Stock_ItemCode { get; set; }
        public string Stock_Item_Name { get; set; }
        public int Stock_Suggestion { get; set; }
        public string Stock_InsertBy { get; set; }
        public DateTime Stock_InsertOn { get; set; }
        public int Stock_CompanyID { set; get; }
        public string Stock_UpdateBy { get; set; }
        public DateTime Stock_UpdateOn { get; set; }
        public bool Stock_IsActive { get; set; }
        public bool Stock_IsDelete { get; set; }
        public string Stock_Insert_Name { get; set; }
        public string Stock_Update_Name { get; set; }
    }
}
