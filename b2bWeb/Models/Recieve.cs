﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Recieve
    {
        public string Recieve_ID { get; set; }
        public string Recieve_OutletCode { get; set; }
        public string Recieve_Image { get; set; }
        public string Recieve_Desc { get; set; }
        public double Recieve_Amount { get; set; }
        public string Recieve_Image2 { get; set; }
        public string Recieve_ImageTTD { get; set; }
        public string Recieve_isClosed { get; set; }
        public DateTime Recieve_ClosedOn { get; set; }
        public string Recieve_ClosedBy { get; set; }
        public string Recieve_CompanyID { get; set; }
        public DateTime Recieve_InsertOn { get; set; }
        public DateTime Recieve_UpdateOn { get; set; }
        public string Recieve_InsertBy { get; set; }
        public string Recieve_UpdateBy { get; set; }
        public bool Recieve_Isdelete { get; set; }
        public string Recieve_Delta { get; set; }
        public string Recieve_No { get; set; }
        public string Recieve_ProductCode { get; set; }
        public string Recieve_Stock { get; set; }
        public string Recieve_Suggestion { get; set; }
        public string Recieve_Outlet_Name { get; set; }
        public string Recieve_Product_Name { get; set; }
        public string Recieve_Product_Image { get; set; }
        public string Recieve_Employee_Name { get; set; }
    }

    public class JsonRecieve
    {
        public int total { get; set; }
        public List<rowsRecieve> rows { get; set; }
    }

    public class rowsRecieve
    {
        public string Recieve_ID { get; set; }
        public string Recieve_OutletCode { get; set; }
        public string Recieve_Image { get; set; }
        public string Recieve_Desc { get; set; }
        public double Recieve_Amount { get; set; }
        public string Recieve_Image2 { get; set; }
        public string Recieve_ImageTTD { get; set; }
        public string Recieve_isClosed { get; set; }
        public DateTime Recieve_ClosedOn { get; set; }
        public string Recieve_ClosedBy { get; set; }
        public string Recieve_CompanyID { get; set; }
        public DateTime Recieve_InsertOn { get; set; }
        public DateTime Recieve_UpdateOn { get; set; }
        public string Recieve_InsertBy { get; set; }
        public string Recieve_UpdateBy { get; set; }
        public bool Recieve_Isdelete { get; set; }
        public string Recieve_Delta { get; set; }
        public string Recieve_No { get; set; }
        public string Recieve_ProductCode { get; set; }
        public string Recieve_Stock { get; set; }
        public string Recieve_Suggestion { get; set; }
        public string Recieve_Outlet_Name { get; set; }
        public string Recieve_Product_Name { get; set; }
        public string Recieve_Product_Image { get; set; }
        public string Recieve_Employee_Name { get; set; }
    }
}