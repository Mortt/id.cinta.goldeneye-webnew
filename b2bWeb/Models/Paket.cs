﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b2bWeb.Models
{
    public class Paket
    {
        public int Packet_ID { get; set; }
        public string Packet_Name { get; set; }
        public string Packet_Desc { get; set; }
        public string Packet_Icon { get; set; }
        public DateTime Packet_InsertOn { get; set; }
        public DateTime Packet_UpdateOn { get; set; }
        public bool Packet_IsActive { get; set; }
        public bool Packet_IsDelete { get; set; }
        public string Packet_InsertBy { get; set; }
        public string Packet_UpdateBy { get; set; }
    }
}