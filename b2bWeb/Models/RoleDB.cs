﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace b2bWeb.Models
{
    public class RoleDB
    {//declare connection string  
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        //Return list of all Employees  
        public List<Role> ListAll()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_Get_Role_NEW_BE", con);
                com.CommandTimeout = 250;
                com.CommandType = CommandType.StoredProcedure;
                Parameter data = new Parameter
                {
                    Search = "{}",
                    Company_ID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"]),
                };

                string Parameter = JsonConvert.SerializeObject(data);
                com.Parameters.AddWithValue("@parameter", Parameter);

                using (XmlReader reader = com.ExecuteXmlReader())
                {
                    while (reader.Read())
                    {
                        string s = reader.Value.ToString();
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        IEnumerable<Role> result = JsonConvert.DeserializeObject<IEnumerable<Role>>(s, settings);
                        return result.ToList<Role>();
                    }
                }
            }
            return new List<Role>();
        }

        public List<JsonRole> GetDataRole(Parameter data)
        {
            dynamic result = null;
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("SP_Get_Role_NEW_BE", con);
                    com.CommandTimeout = 250;
                    com.CommandType = CommandType.StoredProcedure;

                    string Parameter = JsonConvert.SerializeObject(data);
                    com.Parameters.AddWithValue("@parameter", Parameter);

                    using (XmlReader reader = com.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            result = JsonConvert.DeserializeObject<List<JsonRole>>(s, settings);
                            return result.ToList<JsonRole>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return result;
        }

        //Method for Updating Employee record  
        public int AddUpd(Role emp)
        {
            string json = JsonConvert.SerializeObject(emp);
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Ins_M_Role_Json", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_Role_Json", json);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Employee  
        public int Delete(Role data)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                string Del_M_Role = JsonConvert.SerializeObject(data);
                con.Open();
                SqlCommand com = new SqlCommand("Del_M_Role", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@M_Role_Json", Del_M_Role);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Cek(Role data)
        {
            int i;
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Cek_Role", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("Role_Name", data.Insert_Name);
                    com.Parameters.AddWithValue("CompanyID", HttpContext.Current.Session["CompanyID"]);
                    SqlDataReader rdrCompany = com.ExecuteReader();
                    while (rdrCompany.Read())
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        IEnumerable<Role> results = JsonConvert.DeserializeObject<IEnumerable<Role>>(rdrCompany.GetString(0).ToString(), settings);
                        i = Convert.ToInt16(results.First().jml.ToString());
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return i = 0;
            }
            i = 0;
            return i;
        }
    }
}