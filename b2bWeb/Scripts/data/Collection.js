﻿
// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        cleanValue();
        if (row.Inv_CallStatus == true) {
            alert("collection is done...");
        } else {
            $(".modal-title").text('Edit ' + ViewBag);
            $("#ModalAU").modal('show');

            $('.errorOutlet').addClass('d-none');
            $('.errorEmployee').addClass('d-none');
            $('.errorDueDate').addClass('d-none');
            $('.errorAmount').addClass('d-none');
            $('.errorInvNo').addClass('d-none');   

            $("#active").removeClass("d-none");

            $("#id").val(row.TransID);
            $('#Inv_No').val(row.Inv_No);
            $('#Inv_OutletCode').selectpicker('val', row.Inv_OutletCode).prop("disabled", true);
            $("#commonDate").val(ToJavaScriptDateA(row.Inv_DueDate)).prop("disabled", true);
            $('#Inv_PIC').selectpicker('val', row.Inv_PIC).prop("disabled", true);
            $('#Inv_Amount').val(row.Inv_Amount);
            $('#Inv_IsActive').selectpicker('val', row.Inv_IsActive.toString());
        }
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Outlet_Name);
        $("#idDel").val(row.TransID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");

    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'TransID': $("#id").val(),
            'Inv_No': $("#Inv_No").val(),
            'Inv_OutletCode': $("#Inv_OutletCode").val(),
            'Inv_PIC': $("#Inv_PIC").val(),
            'Inv_Amount': $("#Inv_Amount").val().split(",").join(""),
            'Inv_DueDate': $("#commonDate").val(),
            'Inv_IsActive': $("#Inv_IsActive").val()
        },
        success: function (data) {
            $('.errorOutlet').addClass('d-none');
            $('.errorEmployee').addClass('d-none');
            $('.errorDueDate').addClass('d-none');
            $('.errorAmount').addClass('d-none');
            $('.errorInvNo').addClass('d-none');   

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");
                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Inv_No") {
                            $('.errorInvNo').removeClass('d-none');
                            $('.errorInvNo').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Inv_Amount") {
                            $('.errorAmount').removeClass('d-none');
                            $('.errorAmount').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Inv_DueDate") {
                            $('.errorDueDate').removeClass('d-none');
                            $('.errorDueDate').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Inv_OutletCode") {
                            $('.errorOutlet').removeClass('d-none');
                            $('.errorOutlet').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Inv_PIC") {
                            $('.errorEmployee').removeClass('d-none');
                            $('.errorEmployee').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'TransID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#id").val("");
    $("#Inv_No").val("");
    $("#Inv_OutletCode").val("").prop("disabled", false);
    $("#Inv_PIC").val("").prop("disabled", false);
    $("#Inv_Amount").val("");
    $("#commonDate").val("").prop("disabled", false);
    $("#active").addClass("d-none");
    $('.selectpicker').selectpicker('val', 0);
    
    $('.errorOutlet').addClass('d-none');
    $('.errorEmployee').addClass('d-none');
    $('.errorDueDate').addClass('d-none');
    $('.errorAmount').addClass('d-none');
    $('.errorInvNo').addClass('d-none');   

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});


$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '<td>' + k[g][4] + '</td>' +
                    '<td>' + k[g][5] + '</td>' +
                    '<td>' + k[g][6] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});