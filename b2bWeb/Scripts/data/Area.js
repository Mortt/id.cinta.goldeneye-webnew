﻿
// create event
$('.create').click(function () {   
    $('.create').addClass("d-none");
    $('.createL').removeClass("d-none");
    var url = $('#ModalAU').data('url');
    url += '?Lat=' + "-6.165087" + '&Long=' + "106.914355";
    $('#viewAddUp').empty();
    $.get(url, function (data) {
        $('.create').removeClass("d-none");
        $('.createL').addClass("d-none");
        $('#viewAddUp').html(data);
        $(".modal-title").text('Add ' + ViewBag);
        cleanValue();        
        $("#ModalAU").modal('show');
    });
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $('#like'+row.Area_ID).addClass("d-none");
        $('#likeL'+row.Area_ID).removeClass("d-none");
        var url = $('#ModalAU').data('url');
        url += '?Lat=' + row.Area_Lat + '&Long=' + row.Area_Long;
        $('#viewAddUp').empty();
        $.get(url, function (data) {
            $('#like' + row.Area_ID).removeClass("d-none");
            $('#likeL' + row.Area_ID).addClass("d-none");
            $('#viewAddUp').html(data);

            $(".modal-title").text('Edit ' + ViewBag);
            $("#ModalAU").modal('show');

            $('.errorName').addClass('d-none');
            $('.errorDistrict').addClass('d-none');
            $('.errorLongitude').addClass('d-none');
            $('.errorLatitude').addClass('d-none');
            $('.errorDesc').addClass('d-none');

            $("#active").removeClass('d-none');
            $("#id").val(row.Area_ID);
            $("#Area_Name").val(row.Area_Name);
            $("#Area_Desc").val(row.Area_Desc);
            $("#searchInput").val("");
            $('.selectpicker').selectpicker('val', row.Area_DistrictID);
            //window.onload = initialize(row.Area_Lat, row.Area_Long);
            //new google.maps.event.addDomListener(window, 'load', initialize(row.Area_Lat, row.Area_Long));
            $("#Long").val(row.Area_Long);
            $("#Lat").val(row.Area_Lat);
            $("#Area_isActive").selectpicker('val', row.Area_isActive.toString());
            $("#active").removeClass("d-none");
        });
        
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Area_Name);
        $("#idDel").val(row.Area_ID);
        $("#ModalDelete").modal('show');
    }
}

function cleanValue() {
    $("#id").val("");
    $("#Area_Name").val("");
    $("#Area_Desc").val("");
    $('.selectpicker').selectpicker('val', 0);
    $("#Area_DistrictID").val("");
    $("#searchInput").val("");
    //window.onload =
    //initialize(parseFloat(-6.165087), parseFloat(106.914355));
    //new google.maps.event.addDomListener(window, 'load', initialize(parseFloat(-6.165087), parseFloat(106.914355)));
    $("#Long").val("");
    $("#Lat").val("");
    $("#Area_isActive").val("");
    $("#active").addClass('d-none');

    $('.errorName').addClass('d-none');
    $('.errorDistrict').addClass('d-none');
    $('.errorLongitude').addClass('d-none');
    $('.errorLatitude').addClass('d-none');
    $('.errorDesc').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});

$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '<td>' + k[g][4] + '</td>' +
                    '<td>' + k[g][5] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag +" successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag +" failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});