﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorCode').addClass('d-none');
        $('.errorName').addClass('d-none');
        $('.errorDesc').addClass('d-none');
        

        $("#Category_Code").val(row.Category_Code);
        $("#Category_Code").attr("disabled", true);
        if (row.Category_Image == null) {
            $("#ImagesCategory_Image").attr("src", "/Content/images/default-image.png");
            $(".cr-image").attr("alt", "");
        } else {
            $("#ImagesCategory_Image").attr("src", row.Category_Image); 
            $(".cr-image").attr("src", row.Category_Image);
        }        
        
        $("#Category_Image").val(row.Category_Image);
        $("#Category_Name").val(row.Category_Name);
        $("#Category_Desc").val(row.Category_Desc);
        $("#Category_IsActive").selectpicker ("val", row.Category_IsActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Category_Name);
        $("#idDel").val(row.Category_Code);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Category_Code': $("#Category_Code").val(),
            'Category_Name': $("#Category_Name").val(),
            'Category_Desc': $("#Category_Desc").val(),
            'Category_Image': $("#Category_Image").val(),
            'Category_IsActive': $("#Category_IsActive").val()
        },
        success: function (data) {
            $('.errorCode').addClass('d-none');
            $('.errorName').addClass('d-none');
            $('.errorDesc').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data with that code already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");

                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Category_Code") {
                            $('.errorCode').removeClass('d-none');
                            $('.errorCode').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Category_Name") {
                            $('.errorName').removeClass('d-none');
                            $('.errorName').text(data.errorModel[i]["errors"]);
                        }       

                        if (data.errorModel[i]["key"] == "Category_Desc") {
                            $('.errorDesc').removeClass('d-none');
                            $('.errorDesc').text(data.errorModel[i]["errors"]);
                        }                        
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Category_Code': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#ImagesCategory_Image").attr("src", "/Content/images/default-image.png");
    $(".cr-image").attr("src", "/Content/images/default-image.png");
    $("#Category_Image").val("");
    $("#uploadCategory_Image").val("");
    $("#id").val("");
    $("#Category_Code").val("");
    $("#Category_Code").attr("disabled", false);
    $("#Category_Name").val("");
    $("#Category_Desc").val("");
    $("#Category_IsActive").val("");
    $("#active").addClass('d-none');

    $('.errorCode').addClass('d-none');
    $('.errorName').addClass('d-none');
    $('.errorDesc').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});

$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});

//image
$uploadCategory_Image = $('#upload-Category_Image').croppie({
    enableExif: true,
    viewport: {
        width: 250,
        height: 250
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#uploadCategory_Image').on('change', function () {
    var reader1 = new FileReader();
    reader1.onload = function (e) {
        $uploadCategory_Image.croppie('bind', {
            url: e.target.result
        }).then(function () {
            console.log('jQuery bind complete');
        });

    }
    reader1.readAsDataURL(this.files[0]);
});

$('.upload-Category_Image').on('click', function (ev) {
    $uploadCategory_Image.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (img) {
        //$("#Images1TYPE_ICON").attr("src", img);
        $("#ImagesCategory_Image").attr("src", img);
        $("#ChooseImageCategory_Image").modal("hide");
        $("#AddModal").modal("show");
        $("#Category_Image").val(img);
    });
});