﻿
// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        cleanValue();
        if (row.DO_SendingStatus == true) {
            alert("delivery is done...");
        } else {
            $(".modal-title").text('Edit ' + ViewBag);
            $("#ModalAU").modal('show');

            $("#anyProduct").removeClass("d-none");
            $('.errorOutlet').addClass('d-none');
            $('.errorEmployee').addClass('d-none');
            $('.errorDoDate').addClass('d-none');
            $('.errorItem').addClass('d-none');

            $("#id").val(row.TransID);
            $('#DO_OutletCode').selectpicker('val', row.DO_OutletCode).prop("disabled", true);
            $('#DO_PIC').selectpicker('val', row.DO_PIC).prop("disabled", true);
            $("#commonDate").val(ToJavaScriptDateA(row.DO_Transdate)).prop("disabled", true);

            $.ajax({
                type: 'POST',
                url: "Delivery/GetbyID",
                data: {
                    'TransID': row.TransID,
                },
                success: function (data) {
                    //var arrayItem = [];
                    //for (var q = 0; q < data.length; q++) {
                    //    //arrayItem.push(data[q].DO_Item_ID + "|" + data[q].P[0].Item_Name + "|" + data[q].DO_Price);
                    //}
                    //console.log(arrayItem);
                    //$('#Item').selectpicker('text', arrayItem);

                    for (var i = 0; i < data.length; i++) {
                        $("#PilihProduct").append('<div class="form-group row" id="PilihProductEdit_' + data[i].DO_Item_ID + '">' +
                            '<label for="" class="col-sm-6 col-form-label">' + data[i].P[0].Item_Name + '</label>' +
                            '<div class="col-sm-6">' +
                            '<input type="number" class="form-control Quantity" name="QTY" id="QTY" placeholder="Quantity" value="' + data[i].DO_Qty + '" autocomplete="off"/>' +
                            '<input type="hidden" class="form-control Nump" name="Numpang" id="Numpang" value="' + data[i].DO_Item_ID + '|' + data[i].DO_Price + '" />' +
                            '</div>' +
                            '</div>');                  
                    }
                },
            });
        }
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.DO_PIC_Name);
        $("#idDel").val(row.TransID);
        $("#ModalDelete").modal('show');
    },
    'click .details': function (e, value, row, index) {
        cleanValue();
        $.ajax({
            type: 'POST',
            url: "Delivery/GetbyID",
            data: {
                'TransID': row.TransID,
            },
            success: function (data) {
                $("#IsiDetails").empty();
                $("#Details").modal('show');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].P[0].Item_Image1 == null) {
                        var image = "/Content/images/default-image.png";
                    } else {
                        var image = data[i].P[0].Item_Image1;
                    }
                    $("#IsiDetails").append('<div class="card text-center">' +
                        '<br/>' +
                        '<p class="text-center"><img src="' + image +'" id="details_image" class="card-img-top" alt="..." style="width:80px;height:100px;"></p>' +
                        '<div class="card-body">' +
                        '<u><h6 class="card-title" id="Details_Name">Name = ' + data[i].P[0].Item_Name +'</h6></u>' +
                        '<span class="card-text" id="Details_Price">Price = ' + convertToRupiah(data[i].DO_Price) + '</span><br />' +
                        '<span class="card-text" id="Details_Quantity">Quantity = ' + data[i].DO_Qty + '</span><br />' +
                        '<span class="card-text" id="Details_Amount">Amount = ' + convertToRupiah(data[i].DO_LineTotal) + '</span><br />' +
                        '</div>' +
                        '</div>');
                }
            },
        });
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");

    var Qty = $('.Quantity').serialize();
    var Nump = $('.Nump').serialize();
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'TransID': $("#id").val(),
            'DO_Transdate': $("#commonDate").val(),
            'DO_OutletCode': $("#DO_OutletCode").val(),
            'DO_PIC': $("#DO_PIC").val(),
            'Numpang': Nump,
            'QTY': Qty
        },
        success: function (data) {
            $('.errorOutlet').addClass('d-none');
            $('.errorDoDate').addClass('d-none');
            $('.errorEmployee').addClass('d-none');
            $('.errorItem').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");
                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "DO_OutletCode") {
                            $('.errorOutlet').removeClass('d-none');
                            $('.errorOutlet').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "DO_Transdate") {
                            $('.errorDoDate').removeClass('d-none');
                            $('.errorDoDate').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "DO_PIC") {
                            $('.errorEmployee').removeClass('d-none');
                            $('.errorEmployee').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Numpang") {
                            $('.errorItem').removeClass('d-none');
                            $('.errorItem').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'TransID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#id").val("");
    $("#DO_OutletCode").val("").prop("disabled", false);
    $("#DO_PIC").val("").prop("disabled", false);
    $('.selectpicker').selectpicker('val', 0);
    $("#commonDate").val("").prop("disabled", false);
    $("#Item").val("");
    $("#PilihProduct").empty();    

    $("#anyProduct").addClass("d-none");
    $('.errorOutlet').addClass('d-none');
    $('.errorEmployee').addClass('d-none');
    $('.errorDoDate').addClass('d-none');
    $('.errorItem').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

$('#Item').change(function () {
    var val = $("#Item option:selected").val();
    var a = val.split("|");

    var myEle = document.getElementById("PilihProduct" + a[0]);
    if (myEle) {
        alert("Product sudah dipilih");
    } else {
        $("#anyProduct").removeClass("d-none");
        $("#PilihProduct").append(            
            '<div class="form-group row" id="PilihProduct' + a[0] + '">' +
            '<label for="" class="col-sm-6 col-form-label">' + a[1] + '</label>' +
            '<div class="col-sm-6">' +
            '<input type="number" class="form-control Quantity" name="QTY" id="QTY" placeholder="Quantity" autocomplete="off" required/>' +
            '<input type="hidden" class="form-control Nump" name="Numpang" id="Numpang" value="' + a[0] + '|' + a[2] + '"/>' +
            '</div>' +
            '</div>');
    }
});

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});


$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '<td>' + k[g][4] + '</td>' +
                    '<td>' + k[g][5] + '</td>' +
                    '<td>' + k[g][6] + '</td>' +
                    '<td>' + k[g][7] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});
