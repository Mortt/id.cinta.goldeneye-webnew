﻿// create event
$('.create').click(function () {
    $('.create').addClass("d-none");
    $('.createL').removeClass("d-none");
    var url = $('#ModalAU').data('url');
    url += '?Lat=' + "-6.165087" + '&Long=' + "106.914355";
    $('#viewAddUp').empty();
    $.get(url, function (data) {
        $('.create').removeClass("d-none");
        $('.createL').addClass("d-none");
        $('#viewAddUp').html(data);
        $(".modal-title").text('Add ' + ViewBag);
        cleanValue();
        $("#ModalAU").modal('show');
    });
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $('#like' + row.Outlet_ID).addClass("d-none");
        $('#likeL' + row.Outlet_ID).removeClass("d-none");
        var url = $('#ModalAU').data('url');
        url += '?Lat=' + row.Outlet_Latitude + '&Long=' + row.Outlet_Longitude;
        $('#viewAddUp').empty();
        $.get(url, function (data) {
            $('#like' + row.Outlet_ID).removeClass("d-none");
            $('#likeL' + row.Outlet_ID).addClass("d-none");
            $('#viewAddUp').html(data);

            $(".modal-title").text('Edit ' + ViewBag);
            $("#ModalAU").modal('show');

            $('.errorCode').addClass('d-none');
            $('.errorName').addClass('d-none');
            $('.errorArea').addClass('d-none');
            $('.errorLongitude').addClass('d-none');
            $('.errorLatitude').addClass('d-none');
            $('.errorDesc').addClass('d-none');
            $('.errorAddress1').addClass('d-none');
            $('.errorAddress2').addClass('d-none');
            $('.errorPhone').addClass('d-none');
            $('.errorPos').addClass('d-none');
            $('.errorEmail').addClass('d-none');

            $("#id").val(row.Outlet_ID);
            if (row.Outlet_Picture == null) {
                $("#ImagesOutlet_Image").attr("src", "/Content/images/default-image.png");
                $(".cr-image").attr("alt", "");
            } else if (row.Outlet_Picture == "tidak ada foto") {
                $("#ImagesOutlet_Image").attr("src", "/Content/images/default-image.png");
                $(".cr-image").attr("alt", "");
            } else {
                $("#ImagesOutlet_Image").attr("src", row.Outlet_Picture);
                $(".cr-image").attr("src", row.Outlet_Picture);
            }
            $("#Outlet_Image").val(row.Outlet_Picture);
            $("#Outlet_Code").val(row.Outlet_Code).prop("disabled", true);
            $("#Outlet_Name").val(row.Outlet_Name);
            $("#Outlet_Email").val(row.Outlet_Email);
            $('#Outlet_GroupCode').selectpicker('val', row.Outlet_GroupCode);
            $('#Outlet_Type').selectpicker('val', row.Outlet_Type);
            $("#Outlet_Desc").val(row.Outlet_Desc);
            $('#Outlet_AreaID').selectpicker('val', row.Outlet_AreaID);
            $('#Outlet_Repeat_IDX').selectpicker('val', row.Outlet_Repeat_IDX);
            $("#Outlet_Zipcode").val(row.Outlet_Zipcode);
            $("#Outlet_Phone").val(row.Outlet_Phone);
            $("#Long").val(row.Outlet_Longitude);
            $("#Lat").val(row.Outlet_Latitude);
            $("#Outlet_Address1").val(row.Outlet_Address1);
            $("#Outlet_Address2").val(row.Outlet_Address2);
            $("#Outlet_Isactive").selectpicker('val', row.Outlet_Isactive.toString());
            $("#active").removeClass("d-none");
        });
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Outlet_Name);
        $("#idDel").val(row.Outlet_ID);
        $("#ModalDelete").modal('show');
    }
}

function cleanValue() {
    $("#ImagesOutlet_Image").attr("src", "/Content/images/default-image.png");
    $(".cr-image").attr("src", "/Content/images/default-image.png");
    $("#Outlet_Image").val("");
    $("#uploadOutlet_Image").val("");    
    $("#id").val("");
    $("#Outlet_Code").val("").prop("disabled", false);
    $("#Outlet_Name").val("");
    $("#Outlet_Email").val("");
    $('#Outlet_GroupCode').val(""); 
    $("#Outlet_Type").val("");
    $("#Outlet_Desc").val("");
    $("#Outlet_AreaID").val("");
    $("#Outlet_Zipcode").val("");
    $("#Outlet_Phone").val("");
    $("#searchInput").val("");
    $("#Long").val("");
    $("#Lat").val("");
    $("#Outlet_Address1").val("");
    $("#Outlet_Address2").val("");
    $('.selectpicker').selectpicker('val', 0);
    $("#Outlet_Isactive").val("");
    $("#active").addClass('d-none');

    $('.errorCode').addClass('d-none');
    $('.errorName').addClass('d-none');
    $('.errorArea').addClass('d-none');
    $('.errorLongitude').addClass('d-none');
    $('.errorLatitude').addClass('d-none');
    $('.errorDesc').addClass('d-none');
    $('.errorAddress1').addClass('d-none');
    $('.errorAddress2').addClass('d-none');
    $('.errorPhone').addClass('d-none');
    $('.errorPos').addClass('d-none');
    $('.errorEmail').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});

$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '<td>' + k[g][4] + '</td>' +
                    '<td>' + k[g][5] + '</td>' +
                    '<td>' + k[g][6] + '</td>' +
                    '<td>' + k[g][7] + '</td>' +
                    '<td>' + k[g][8] + '</td>' +
                    '<td>' + k[g][9] + '</td>' +
                    '<td>' + k[g][10] + '</td>' +
                    '<td>' + k[g][11] + '</td>' +
                    '<td>' + k[g][12] + '</td>' +
                    '<td>' + k[g][13] + '</td>' +
                    '<td>' + k[g][14] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag +" successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag +" failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});

//icon
$uploadOutlet_Image = $('#upload-Outlet_Image').croppie({
    enableExif: true,
    viewport: {
        width: 250,
        height: 250
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#uploadOutlet_Image').on('change', function () {
    var reader1 = new FileReader();
    reader1.onload = function (e) {
        $uploadOutlet_Image.croppie('bind', {
            url: e.target.result
        }).then(function () {
            console.log('jQuery bind complete');
        });

    }
    reader1.readAsDataURL(this.files[0]);
});

$('.upload-Outlet_Image').on('click', function (ev) {
    $uploadOutlet_Image.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (img) {
        //$("#Images1TYPE_ICON").attr("src", img);
        $("#ImagesOutlet_Image").attr("src", img);
        $("#ChooseImageOutlet_Image").modal("hide");
        $("#AddModal").modal("show");
        $("#Outlet_Image").val(img);
    });
});