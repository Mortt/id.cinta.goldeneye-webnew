﻿
// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorEmployee').addClass('d-none');
        $('.errorPeriode').addClass('d-none');
        $('.errorAmount').addClass('d-none');

        $("#active").removeClass('d-none');
        $("#id").val(row.TargetByEmployee_ID);
        $("#TargetByEmployee_EmployeeNIK").selectpicker("val", row.TargetByEmployee_EmployeeNIK);
        $("#TargetByEmployee_Amount").val(row.TargetByEmployee_Amount);
        $("#dateMonth").val(ToMonthYear(row.TargetByEmployee_Periode)).attr("disabled", true);
        $("#TargetByEmployee_IsActive").selectpicker('val', row.TargetByEmployee_IsActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.TargetByEmployee_EmployeeName + ", Periode : " + ToMonthNameAndYear(row.TargetByEmployee_Periode));
        $("#idDel").val(row.TargetByEmployee_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'TargetByEmployee_ID': $("#id").val(),
            'TargetByEmployee_Periode': $("#dateMonth").val(),
            'TargetByEmployee_EmployeeNIK': $("#TargetByEmployee_EmployeeNIK").val(),
            'TargetByEmployee_Amount': $("#TargetByEmployee_Amount").val().split(",").join(""),
            'TargetByEmployee_IsActive': $("#TargetByEmployee_IsActive").val()
        },
        success: function (data) {
            $('.errorEmployee').addClass('d-none');
            $('.errorPeriode').addClass('d-none');
            $('.errorAmount').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");
                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "TargetByEmployee_EmployeeNIK") {
                            $('.errorEmployee').removeClass('d-none');
                            $('.errorEmployee').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "TargetByEmployee_Periode") {
                            $('.errorPeriode').removeClass('d-none');
                            $('.errorPeriode').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "TargetByEmployee_Amount") {
                            $('.errorAmount').removeClass('d-none');
                            $('.errorAmount').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'TargetByEmployee_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#id").val("");
    $(".selectpicker").selectpicker("val", 0);
    $("#TargetByEmployee_ID").val("");
    $("#TargetByEmployee_EmployeeNIK").val("");
    $("#TargetByEmployee_Amount").val("");
    $("#dateMonth").val("").attr("disabled", false);
    $("#TargetByEmployee_IsActive").val("");
    $("#active").addClass('d-none');

    $('.errorEmployee').addClass('d-none');
    $('.errorPeriode').addClass('d-none');
    $('.errorAmount').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});

$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});