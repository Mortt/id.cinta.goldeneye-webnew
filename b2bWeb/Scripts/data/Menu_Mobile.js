﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorRole').addClass('d-none');
        $('.errorMenu').addClass('d-none');

        $("#id").val(row.MM_ID);
        $("#MM_Role_ID").selectpicker('val', row.MM_Role_ID).prop("disabled", true);
        $("#MM_Menu_GoldenEye_ID").selectpicker('val', row.MM_Menu_GoldenEye_ID);
        $("#MM_IsActive").selectpicker("val", row.MM_IsActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.MM_GoldenEye_Name);
        $("#idDel").val(row.MM_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'MM_ID': $("#id").val(),
            'MM_Role_ID': $("#MM_Role_ID").val(),
            'MM_Menu_GoldenEye_ID': $("#MM_Menu_GoldenEye_ID").val(),
            'MM_IsActive': $("#MM_IsActive").val()
        },
        success: function (data) {
            $('.errorRole').addClass('d-none');
            $('.errorMenu').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");

                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "MM_Role_ID") {
                            $('.errorRole').removeClass('d-none');
                            $('.errorRole').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "MM_Menu_GoldenEye_ID") {
                            $('.errorMenu').removeClass('d-none');
                            $('.errorMenu').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'MM_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {   
    $("#id").val("");
    $(".selectpicker").selectpicker("val", 0);
    $("#MM_Role_ID").val("").prop("disabled", false);
    $("#MM_Menu_GoldenEye_ID").val("");
    $("#MM_IsActive").val("");
    $("#active").addClass('d-none');

    $('.errorRole').addClass('d-none');
    $('.errorMenu').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}
