﻿
// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorCode').addClass('d-none');
        $('.errorName').addClass('d-none');
        $('.errorBrand').addClass('d-none');
        $('.errorPrice').addClass('d-none');
        $('.errorSampling').addClass('d-none');
        $('.errorDesc').addClass('d-none');
        $('.errorDescDetail').addClass('d-none');

        if (row.Item_Image1 == null) {
            $("#ImagesItem_Image1").attr("src", "/Content/images/default-image.png");
            $(".cr-image").attr("alt", "");
        } else {
            $("#ImagesItem_Image1").attr("src", row.Item_Image1);
            $(".cr-image").attr("src", row.Item_Image1);
        }

        $("#Item_Image1").val(row.Item_Image1);

        $("#id").val(row.Item_ID);
        $("#Item_Code").val(row.Item_Code).prop("disabled", true);
        $("#Item_Name").val(row.Item_Name);
        $("#Item_Desc").val(row.Item_Desc);
        $("#Item_Desc_Detail").val(row.Item_Desc_Detail);
        $("#Item_CategoryCode").selectpicker('val', row.Item_CategoryCode);
        $("#Item_Sampling").selectpicker('val', row.Item_Sampling.toString());
        $("#Item_Price").val(row.Item_Price);        
        $("#Item_IsActive").selectpicker('val', row.Item_IsActive.toString());
        $("#active").removeClass('d-none');
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Item_Name);
        $("#idDel").val(row.Item_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Item_ID': $("#id").val(),
            'Item_Image1': $("#Item_Image1").val(),
            'Item_Code': $("#Item_Code").val(),
            'Item_Name': $("#Item_Name").val(),
            'Item_Desc': $("#Item_Desc").val(),
            'Item_Desc_Detail': $("#Item_Desc_Detail").val(),
            'Item_CategoryCode': $("#Item_CategoryCode").val(),
            'Item_Price': $("#Item_Price").val().split(",").join(""),
            'Item_Sampling': $("#Item_Sampling").val(),
            'Item_IsActive': $("#Item_IsActive").val()
        },
        success: function (data) {
            $('.errorCode').addClass('d-none');
            $('.errorName').addClass('d-none');
            $('.errorBrand').addClass('d-none');
            $('.errorPrice').addClass('d-none');
            $('.errorSampling').addClass('d-none');
            $('.errorDesc').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data with that code already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");
                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Item_Code") {
                            $('.errorCode').removeClass('d-none');
                            $('.errorCode').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Item_Name") {
                            $('.errorName').removeClass('d-none');
                            $('.errorName').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Item_CategoryCode") {
                            $('.errorBrand').removeClass('d-none');
                            $('.errorBrand').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Item_Price") {
                            $('.errorPrice').removeClass('d-none');
                            $('.errorPrice').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Item_Sampling") {
                            $('.errorSampling').removeClass('d-none');
                            $('.errorSampling').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Item_Desc") {
                            $('.errorDesc').removeClass('d-none');
                            $('.errorDesc').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Item_Desc_Detail") {
                            $('.errorDescDetail').removeClass('d-none');
                            $('.errorDescDetail').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Item_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#ImagesItem_Image1").attr("src", "/Content/images/default-image.png");
    $(".cr-image").attr("src", "/Content/images/default-image.png");
    $("#Item_Image1").val("");
    $("#uploadItem_Image1").val("");
    $("#id").val("");
    $("#Item_Code").val("").prop("disabled", false);
    $("#Item_Name").val("");
    $("#Item_Desc").val("");
    $("#Item_Desc_Detail").val("");
    $('.selectpicker').selectpicker('val', 0);
    $("#Item_CategoryCode").val("");
    $("#Item_Price").val("");
    $("#Item_Sampling").val("");
    $("#Item_IsActive").val("");
    $("#active").addClass('d-none');

    $('.errorCode').addClass('d-none');
    $('.errorName').addClass('d-none');
    $('.errorBrand').addClass('d-none');
    $('.errorPrice').addClass('d-none');
    $('.errorSampling').addClass('d-none');
    $('.errorDesc').addClass('d-none');
    $('.errorDescDetail').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});

$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '<td>' + k[g][4] + '</td>' +
                    '<td>' + k[g][5] + '</td>' +
                    '<td>' + k[g][6] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});

//image
$uploadItem_Image1 = $('#upload-Item_Image1').croppie({
    enableExif: true,
    viewport: {
        width: 250,
        height: 250
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#uploadItem_Image1').on('change', function () {
    var reader1 = new FileReader();
    reader1.onload = function (e) {
        $uploadItem_Image1.croppie('bind', {
            url: e.target.result
        }).then(function () {
            console.log('jQuery bind complete');
        });

    }
    reader1.readAsDataURL(this.files[0]);
});

$('.upload-Item_Image1').on('click', function (ev) {
    $uploadItem_Image1.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (img) {
        //$("#Images1TYPE_ICON").attr("src", img);
        $("#ImagesItem_Image1").attr("src", img);
        $("#ChooseImageItem_Image1").modal("hide");
        $("#AddModal").modal("show");
        $("#Item_Image1").val(img);
    });
});

//var rupiah = document.getElementById('Item_Price');
//rupiah.addEventListener('keyup', function (e) {
//    // tambahkan 'Rp.' pada saat form di ketik
//    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
//    rupiah.value = formatRupiah(this.value, 'Rp. ');
//});

///* Fungsi formatRupiah */
//function formatRupiah(angka, prefix) {
//    var number_string = angka.replace(/[^,\d]/g, '').toString(),
//        split = number_string.split(','),
//        sisa = split[0].length % 3,
//        rupiah = split[0].substr(0, sisa),
//        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

//    // tambahkan titik jika yang di input sudah menjadi angka ribuan
//    if (ribuan) {
//        separator = sisa ? '.' : '';
//        rupiah += separator + ribuan.join('.');
//    }

//    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
//    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
//}