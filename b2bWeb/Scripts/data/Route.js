﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorEmployee').addClass('d-none');
        $('.errorQueue').addClass('d-none');
        $('.errorArea').addClass('d-none');
        $('.errorType').addClass('d-none');

        $("#id").val(row.Route_ID);     
        $('#Route_EmployeeNik').selectpicker('val', row.Route_EmployeeNik);
        $("#Route_Queque").val(row.Route_Queque);
        $('#Route_Day').selectpicker('val', row.Route_Day);
        $("#Route_SuggestionTime").val(ToTimeOnly(row.Route_SuggestionTime));
        $('#Route_AreaID').selectpicker('val', row.Route_AreaID);
        $("#Route_Type").selectpicker('val', row.Route_Type.toString());
        $("#Route_isActive").selectpicker('val', row.Route_isActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Nama_SPG);
        $("#idDel").val(row.Route_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Route_ID': $("#id").val(),
            'Route_EmployeeNik': $("#Route_EmployeeNik").val(),
            'Route_Queque': $("#Route_Queque").val(),
            'Route_Day': $("#Route_Day").val(),
            'Route_SuggestionTime': $("#Route_SuggestionTime").val(),
            'Route_AreaID': $("#Route_AreaID").val(),
            'Route_isActive': $("#Route_isActive").val(),
            'Route_Type': $("#Route_Type").val()
        },
        success: function (data) {
            $('.errorEmployee').addClass('d-none');
            $('.errorQueue').addClass('d-none');
            //$('.errorArea').addClass('d-none');
            $('.errorType').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag +" successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag +" successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    console.log(data);
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");

                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Route_EmployeeNik") {
                            $('.errorEmployee').removeClass('d-none');
                            $('.errorEmployee').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Route_Queque") {
                            $('.errorQueue').removeClass('d-none');
                            $('.errorQueue').text(data.errorModel[i]["errors"]);
                        }

                        //if (data.errorModel[i]["key"] == "Route_AreaID") {
                        //    $('.errorArea').removeClass('d-none');
                        //    $('.errorArea').text(data.errorModel[i]["errors"]);
                        //}

                        if (data.errorModel[i]["key"] == "Route_Type") {
                            $('.errorType').removeClass('d-none');
                            $('.errorType').text(data.errorModel[i]["errors"]);
                        }                        
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Route_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#Route_EmployeeNik").val("");
    $("#id").val("");
    $("#Route_Queque").val("");
    $("#Route_Day").val("");
    $("#Route_SuggestionTime").val("");
    $("#Route_AreaID").val("");
    $("#Route_Type").val("");
    $("#Route_Role").val("");
    $('.selectpicker').selectpicker('val', 0);
    $("#Route_isActive").val("");
    $("#active").addClass('d-none');

    $('.errorEmployee').addClass('d-none');
    $('.errorQueue').addClass('d-none');
    $('.errorArea').addClass('d-none');
    $('.errorType').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});

$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '<td>' + k[g][4] + '</td>' +
                    '<td>' + k[g][5] + '</td>' +
                    '<td>' + k[g][6] + '</td>' +
                    '<td>' + k[g][7] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});