﻿
// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        if (row.SDS_URL == null) {
            $("#ImagesSDS_URL").attr("src", "/Content/images/default-image.png");
            $("#item_Image1").val("");
            $("#SDS_URL").val("");

            //$(".cr-image").attr("alt", "");
        } else {
            $("#ImagesSDS_URL").attr("src", row.SDS_URL);
            $("#item_Image1").val("");
            $("#SDS_URL").val(row.SDS_URL);
            //$(".cr-image").attr("src", row.SDS_URL);
        }

        $("#item_Image1").val("");
        $("#SDS_URL").val(row.SDS_URL);

        $("#id").val(row.SDS_IDX);
        $("#SDS_isPUBLISH").selectpicker('val', row.SDS_isPUBLISH.toString());
        $("#active").removeClass('d-none');
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        //$("#nameDel").text(row.Item_Name);
        $("#idDel").val(row.SDS_IDX);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'SDS_IDX': $("#id").val(),
            'SDS_URL': $("#SDS_URL").val(),
            'SDS_isPUBLISH': $("#SDS_isPUBLISH").val()
        },
        success: function (data) {           

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });

            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'SDS_IDX': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#ImagesSDS_URL").attr("src", "/Content/images/default-image.png");
    //$(".cr-image").attr("src", "/Content/images/default-image.png");
    $("#SDS_URL").val("");
    $("#item_Image1").val("");    
    $("#uploadSDS_URL").val("");
    $("#id").val("");
    $('.selectpicker').selectpicker('val', 0);
    $("#SDS_isPUBLISH").val("");
    $("#active").addClass('d-none');
    
    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

////image
//$uploadSDS_URL = $('#upload-SDS_URL').croppie({
//    enableExif: true,
//    viewport: {
//        width: 250,
//        height: 250
//    },
//    boundary: {
//        width: 300,
//        height: 300
//    }
//});

//$('#uploadSDS_URL').on('change', function () {
//    var reader1 = new FileReader();
//    reader1.onload = function (e) {
//        $uploadSDS_URL.croppie('bind', {
//            url: e.target.result
//        }).then(function () {
//            console.log('jQuery bind complete');
//        });

//    }
//    reader1.readAsDataURL(this.files[0]);
//});

//$('.upload-SDS_URL').on('click', function (ev) {
//    $uploadSDS_URL.croppie('result', {
//        type: 'canvas',
//        size: 'viewport'
//    }).then(function (img) {
//        //$("#Images1TYPE_ICON").attr("src", img);
//        $("#ImagesSDS_URL").attr("src", img);
//        $("#ChooseImageSDS_URL").modal("hide");
//        $("#AddModal").modal("show");
//        $("#SDS_URL").val(img);
//    });
//});

function readURLSS(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#ImagesSDS_URL').attr('src', e.target.result).width(200).height(200);
            $('#SDS_URL').val(e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
