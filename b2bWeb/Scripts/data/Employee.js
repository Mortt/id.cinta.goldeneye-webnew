﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorNIK').addClass('d-none');
        $('.errorName').addClass('d-none');
        $('.errorPassword').addClass('d-none');
        $('.errorPhone').addClass('d-none');
        $('.errorRole').addClass('d-none');
        $('.errorB2B').addClass('d-none');
        $('.errorLeader').addClass('d-none');
        $('.errorEmail').addClass('d-none');

        $("#Employee_NIK").attr("disabled", true);
        $("#Employee_NIK").val(row.Employee_NIK);
        if (row.Employee_Photo == null) {
            $("#ImagesEmployee_Photo").attr("src", "/Content/images/default-image.png");
            $(".cr-image").attr("alt", "");
        } else if (row.Employee_Photo == "tidak ada foto") {
            $("#ImagesEmployee_Photo").attr("src", "/Content/images/default-image.png");
            $(".cr-image").attr("alt", "");
        } else {
            $("#ImagesEmployee_Photo").attr("src", row.Employee_Photo);
            $(".cr-image").attr("src", row.Employee_Photo);
        }
        $("#Employee_Photo").val(row.Employee_Photo);
        $("#Employee_Name").val(row.Employee_Name);
        $("#Employee_Phone").val(row.Employee_Phone);
        $("#Employee_Email").val(row.Employee_Email);
        $('#Employee_LeaderNik').selectpicker('val', row.Employee_LeaderNik);
        $('#Employee_Role').selectpicker('val', row.Employee_Role);
        $("#Employee_Password").val(row.Employee_Password);
        $("#Employee_isB2b").selectpicker('val', row.Employee_isB2b.toString());
        $("#Employee_IsActive").selectpicker('val', row.Employee_IsActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Employee_Name);
        $("#idDel").val(row.Employee_NIK);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {            
            'Employee_Photo': $("#Employee_Photo").val(),
            'Employee_NIK': $("#Employee_NIK").val(),
            'Employee_Name': $("#Employee_Name").val(),
            'Employee_Phone': $("#Employee_Phone").val(),
            'Employee_Email': $("#Employee_Email").val(),
            'Employee_Password': $("#Employee_Password").val(),
            'Employee_LeaderNik': $("#Employee_LeaderNik").val(),
            'Employee_Role': $("#Employee_Role").val(),
            'Employee_isB2b': $("#Employee_isB2b").val(),
            'Employee_IsActive': $("#Employee_IsActive").val()
        },
        success: function (data) {
            $('.errorNIK').addClass('d-none');
            $('.errorName').addClass('d-none');
            $('.errorPassword').addClass('d-none');
            $('.errorPhone').addClass('d-none');
            $('.errorRole').addClass('d-none');
            $('.errorB2B').addClass('d-none');
            $('.errorLeader').addClass('d-none');
            $('.errorEmail').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    console.log(data);
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");

                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Employee_NIK") {
                            $('.errorNIK').removeClass('d-none');
                            $('.errorNIK').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Employee_Name") {
                            $('.errorName').removeClass('d-none');
                            $('.errorName').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Employee_Password") {
                            $('.errorPassword').removeClass('d-none');
                            $('.errorPassword').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Employee_LeaderNik") {
                            $('.errorLeader').removeClass('d-none');
                            $('.errorLeader').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Employee_Role") {
                            $('.errorRole').removeClass('d-none');
                            $('.errorRole').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Employee_isB2b") {
                            $('.errorB2B').removeClass('d-none');
                            $('.errorB2B').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Employee_Phone") {
                            $('.errorPhone').removeClass('d-none');
                            $('.errorPhone').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Employee_Email") {
                            $('.errorEmail').removeClass('d-none');
                            $('.errorEmail').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Employee_NIK': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#ImagesEmployee_Photo").attr("src", "/Content/images/default-image.png");
    $(".cr-image").attr("src", "/Content/images/default-image.png");
    $("#Employee_Photo").val("");
    $("#uploadEmployee_Photo").val("");
    $("#Employee_NIK").val("");
    $("#Employee_Name").val("");
    $("#Employee_Password").val("");
    $("#Employee_Phone").val("");
    $("#Employee_Email").val("");    
    $("#Employee_Role").val("");
    $("#Employee_isB2b").val("");
    $("#Employee_LeaderNik").val("");
    $('.selectpicker').selectpicker('val', 0);
    $("#Employee_IsActive").val("");
    $("#active").addClass('d-none');
        
    $('.errorNIK').addClass('d-none');
    $('.errorName').addClass('d-none');
    $('.errorPassword').addClass('d-none');
    $('.errorPhone').addClass('d-none');
    $('.errorRole').addClass('d-none');
    $('.errorB2B').addClass('d-none');
    $('.errorLeader').addClass('d-none');
    $('.errorEmail').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//import
$('.import').click(function () {
    $(".modal-title").text('Import ' + ViewBag);
    $("#ModalImport").modal('show');
    $("#input-excel").val("");
    $("#result-table").addClass("d-none");
    $(".Import").removeClass("d-none");
    $(".ImportL").addClass("d-none");
    $("#qw").empty();
});

$(document).ready(function () {
    var input = document.getElementById('input-excel')
    input.addEventListener('change', function () {
        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
            $('.errorImport').addClass('d-none');
            $("#result-table").removeClass("d-none");
            var json_object = JSON.stringify(data);
            var k = JSON.parse(json_object);
            for (g = 1; g < k.length; g++) {
                $('#qw').append(
                    '<tr>' +
                    '<td>' + k[g][0] + '</td>' +
                    '<td>' + k[g][1] + '</td>' +
                    '<td>' + k[g][2] + '</td>' +
                    '<td>' + k[g][3] + '</td>' +
                    '<td>' + k[g][4] + '</td>' +
                    '<td>' + k[g][5] + '</td>' +
                    '<td>' + k[g][6] + '</td>' +
                    '<td>' + k[g][7] + '</td>' +
                    '<td>' + k[g][8] + '</td>' +
                    '</tr>'
                );
            }

            //$('#table_import').DataTable({
            //    //responsive: true
            //});

        }, (error) => {
            console.error(error)
            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
        })
    })
});

$('.Import').on('click', function () {
    if (document.getElementById("input-excel").value.length == 0) {
        $('.errorImport').removeClass('d-none');
    } else {
        $(".ImportL").removeClass("d-none");
        $(".Import").addClass("d-none");
        var fileInput = document.getElementById('input-excel');
        var formdata = new FormData();

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#ModalImport").modal('hide');
                if (data == "success") {
                    document.getElementsByName("refresh")[0].click();
                    swal("Success", "import " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                }
            }
        });
    }
});

//icon
$uploadEmployee_Photo = $('#upload-Employee_Photo').croppie({
    enableExif: true,
    viewport: {
        width: 250,
        height: 250
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#uploadEmployee_Photo').on('change', function () {
    var reader1 = new FileReader();
    reader1.onload = function (e) {
        $uploadEmployee_Photo.croppie('bind', {
            url: e.target.result
        }).then(function () {
            console.log('jQuery bind complete');
        });

    }
    reader1.readAsDataURL(this.files[0]);
});

$('.upload-Employee_Photo').on('click', function (ev) {
    $uploadEmployee_Photo.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (img) {
        //$("#Images1TYPE_ICON").attr("src", img);
        $("#ImagesEmployee_Photo").attr("src", img);
        $("#ChooseImageEmployee_Photo").modal("hide");
        $("#AddModal").modal("show");
        $("#Employee_Photo").val(img);
    });
});