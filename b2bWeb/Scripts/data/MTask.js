﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {  
        cleanValue();
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorTitle').addClass('d-none');
        $('.errorDesc').addClass('d-none');
        $('.errorDateline').addClass('d-none');
        $('.errorNik').addClass('d-none');

        $("#id").val(row.Task_ID);
        $("#Task_Title").val(row.Task_Title);
        $("#Task_Employee_NIK").val(row.Task_Employee_NIK);

        $.ajax({
            type: 'POST',
            url: 'MTask/GetbyID',
            data: {
                'Task_ID': row.Task_ID,
            },
            success: function (data) {
                var D = [];
                for (A = 0; A < data.length; A++) {
                    D.push(data[A].Employee_NIK);
                }
                $('#Numpang').selectpicker('val', D);
            },
        });        

        if (row.Task_Image_Example == null) {
            $("#ImagesTask_Image_Example").attr("src", "/Content/images/default-image.png");
            $(".cr-image").attr("alt", "");
        } else {
            $("#ImagesTask_Image_Example").attr("src", row.Task_Image_Example);
            $(".cr-image").attr("src", row.Task_Image_Example);
        }

        $("#Task_Image_Example").val(row.Task_Image_Example);
        $("#Task_Desc").val(row.Task_Desc);
        $("#commonDate").val(ToJavaScriptDateA(row.Task_Deadline));
        $("#Task_IsActive").selectpicker('val', row.Task_IsActive.toString());
        $("#Task_IsPublish").selectpicker('val', row.Task_IsPublish.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Task_Title);
        $("#idDel").val(row.Task_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Task_ID': $("#id").val(),
            'Task_Title': $("#Task_Title").val(),
            'Task_Image_Example': $("#Task_Image_Example").val(),
            'Task_Desc': $("#Task_Desc").val(),
            'Task_Deadline': $("#commonDate").val(),
            'Numpang': $("#Numpang").val(),
            'Task_IsPublish': $("#Task_IsPublish").val(),
            'Task_IsActive': $("#Task_IsActive").val()
        },
        success: function (data) {
            $('.errorTitle').addClass('d-none');
            $('.errorDesc').addClass('d-none');
            $('.errorDateline').addClass('d-none');
            $('.errorNIK').addClass('d-none');
            $('.errorPublish').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    console.log(data);
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");

                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Numpang") {
                            $('.errorNIK').removeClass('d-none');
                            $('.errorNIK').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Task_Title") {
                            $('.errorTitle').removeClass('d-none');
                            $('.errorTitle').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Task_Desc") {
                            $('.errorDesc').removeClass('d-none');
                            $('.errorDesc').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Task_Deadline") {
                            $('.errorDeadline').removeClass('d-none');
                            $('.errorDeadline').text(data.errorModel[i]["errors"]);
                        }      

                        if (data.errorModel[i]["key"] == "Task_IsPublish") {
                            $('.errorPublish').removeClass('d-none');
                            $('.errorPublish').text(data.errorModel[i]["errors"]);
                        }   
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Task_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#ImagesTask_Image_Example").attr("src", "/Content/images/default-image.png");
    $(".cr-image").attr("src", "/Content/images/default-image.png");
    $("#Task_Image_Example").val("");
    $("#uploadTask_Image_Example").val("");
    $("#id").val("");
    $("#Task_Title").val("");
    $("#Task_Desc").val("");
    $("#commonDate").val("");
    $("#Task_IsPublish").val("");
    $('.selectpicker').selectpicker('val', 0);
    $("#Task_IsActive").val("");
    $("#active").addClass('d-none');

    $('.errorTitle').addClass('d-none');
    $('.errorDesc').addClass('d-none');
    $('.errorDateline').addClass('d-none');
    $('.errorNik').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

//icon
$uploadTask_Image_Example = $('#upload-Task_Image_Example').croppie({
    enableExif: true,
    viewport: {
        width: 250,
        height: 250
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#uploadTask_Image_Example').on('change', function () {
    var reader1 = new FileReader();
    reader1.onload = function (e) {
        $uploadTask_Image_Example.croppie('bind', {
            url: e.target.result
        }).then(function () {
            console.log('jQuery bind complete');
        });

    }
    reader1.readAsDataURL(this.files[0]);
});

$('.upload-Task_Image_Example').on('click', function (ev) {
    $uploadTask_Image_Example.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (img) {
        //$("#Images1TYPE_ICON").attr("src", img);
        $("#ImagesTask_Image_Example").attr("src", img);
        $("#ChooseImageTask_Image_Example").modal("hide");
        $("#AddModal").modal("show");
        $("#Task_Image_Example").val(img);
    });
});