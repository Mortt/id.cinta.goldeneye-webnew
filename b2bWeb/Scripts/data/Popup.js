﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        console.log(row);
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorName').addClass('d-none');
        $('.errorModule').addClass('d-none');
                
        $("#id").val(row.Pop_ID);
        $("#Pop_Name").val(row.Pop_Name);
        $("#Pop_Module").selectpicker("val", row.Pop_Module.toString());
        
        if (row.Pop_Tipe != 1) {
            $("#radio1").removeClass("Radios").prop("checked", false);
            $("#radio2").addClass("Radios").prop("checked", true);
            $("#collapse1").collapse("hide");
            $("#collapse2").collapse("show");
            $("#ImagesPop_Pict").attr("src", "/Content/images/default-image.png");
            $(".cr-image")[0].setAttribute("alt", "");
            $('#Pop_Text').val(row.Pop_Text);
            $("#Pop_Pict").val("");
        } else {
            $("#radio1").addClass("Radios").prop("checked", true);
            $("#radio2").removeClass("Radios").prop("checked", false);
            $("#collapse1").collapse("show");
            $("#collapse2").collapse("hide");
            $("#ImagesPop_Pict").attr("src", row.Pop_Pict);
            $(".cr-image")[0].setAttribute("src", row.Pop_Pict);
            $("#Pop_Pict").val(row.Pop_Pict);
            $('#Pop_Text').val("");
        }

        $("#Pop_IsActive").selectpicker("val", row.Pop_IsActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Pop_Name);
        $("#idDel").val(row.Pop_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");

    var Rd = $('.Radios').val();
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Pop_ID': $("#id").val(),
            'Pop_Name': $("#Pop_Name").val(),
            'Pop_Module': $("#Pop_Module").val(),
            'Pop_Pict': $("#Pop_Pict").val(),
            'Pop_Tipe': Rd,
            'Pop_Text': $("#Pop_Text").val(),
            'Pop_IsActive': $("#Pop_IsActive").val()
        },
        success: function (data) {
            $('.errorName').addClass('d-none');
            $('.errorModule').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data with that code already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");

                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Pop_Module") {
                            $('.errorModule').removeClass('d-none');
                            $('.errorModule').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Pop_Name") {
                            $('.errorName').removeClass('d-none');
                            $('.errorName').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Pop_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#ImagesPop_Pict").attr("src", "/Content/images/default-image.png");
    $(".cr-image").attr("src", "/Content/images/default-image.png");
    $("#Pop_Pict").val("");
    
    $("#id").val("");
    $("#Pop_Name").val("");
    $("#Pop_Module").val("");
    $("#Pop_Text").val("");
    $("#Pop_Module").selectpicker("val", 0);
    $("#Pop_IsActive").val("");
    $("#active").addClass('d-none');

    $("#radio1").prop("checked", false);
    $("#radio2").prop("checked", false);
    $("#collapse1").removeClass("show");
    $("#collapse2").removeClass("show");
        
    $('.errorName').addClass('d-none');
    $('.errorModule').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

////import
//$('.import').click(function () {
//    $(".modal-title").text('Import ' + ViewBag);
//    $("#ModalImport").modal('show');
//    $("#input-excel").val("");
//    $("#result-table").addClass("d-none");
//    $(".Import").removeClass("d-none");
//    $(".ImportL").addClass("d-none");
//    $("#qw").empty();
//});

//$(document).ready(function () {
//    var input = document.getElementById('input-excel')
//    input.addEventListener('change', function () {
//        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
//            $('.errorImport').addClass('d-none');
//            $("#result-table").removeClass("d-none");
//            var json_object = JSON.stringify(data);
//            var k = JSON.parse(json_object);
//            for (g = 1; g < k.length; g++) {
//                $('#qw').append(
//                    '<tr>' +
//                    '<td>' + k[g][0] + '</td>' +
//                    '<td>' + k[g][1] + '</td>' +
//                    '<td>' + k[g][2] + '</td>' +
//                    '</tr>'
//                );
//            }

//            //$('#table_import').DataTable({
//            //    //responsive: true
//            //});

//        }, (error) => {
//            console.error(error)
//            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
//        })
//    })
//});

//$('.Import').on('click', function () {
//    if (document.getElementById("input-excel").value.length == 0) {
//        $('.errorImport').removeClass('d-none');
//    } else {
//        $(".ImportL").removeClass("d-none");
//        $(".Import").addClass("d-none");
//        var fileInput = document.getElementById('input-excel');
//        var formdata = new FormData();

//        for (i = 0; i < fileInput.files.length; i++) {
//            formdata.append(fileInput.files[i].name, fileInput.files[i]);
//        }

//        $.ajax({
//            url: $(this).data('request-url'),
//            type: 'POST',
//            data: formdata,
//            cache: false,
//            contentType: false,
//            processData: false,
//            success: function (data) {
//                $("#ModalImport").modal('hide');
//                if (data == "success") {
//                    document.getElementsByName("refresh")[0].click();
//                    swal("Success", "import " + ViewBag + " successfully", {
//                        icon: "success",
//                        buttons: false,
//                        timer: 2000,
//                    });
//                } else {
//                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
//                        icon: "error",
//                        buttons: false,
//                        timer: 2000,
//                    });
//                }
//            }
//        });
//    }
//});

//image
$uploadPop_Pict = $('#upload-Pop_Pict').croppie({
    enableExif: true,
    viewport: {
        width: 250,
        height: 250
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#uploadPop_Pict').on('change', function () {
    var reader1 = new FileReader();
    reader1.onload = function (e) {
        $uploadPop_Pict.croppie('bind', {
            url: e.target.result
        }).then(function () {
            console.log('jQuery bind complete');
        });

    }
    reader1.readAsDataURL(this.files[0]);
});

$('.upload-Pop_Pict').on('click', function (ev) {
    $uploadPop_Pict.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (img) {
        //$("#Images1TYPE_ICON").attr("src", img);
        $("#ImagesPop_Pict").attr("src", img);
        $("#ChooseImagePop_Pict").modal("hide");
        $("#AddModal").modal("show");
        $("#Pop_Pict").val(img);
    });
});


function image() {
    $("#radio1").addClass("Radios").prop("checked", true);
    $("#radio2").removeClass("Radios").prop("checked", false);
    $("#collapse1").collapse("show");
    $("#collapse2").collapse("hide");
}

function text() {
    $("#radio1").removeClass("Radios").prop("checked", false);
    $("#radio2").addClass("Radios").prop("checked", true);
    $("#collapse1").collapse("hide");
    $("#collapse2").collapse("show");
}