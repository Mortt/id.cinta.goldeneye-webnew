﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        cleanValue();
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorCode').addClass('d-none');
        $('.errorName').addClass('d-none');
        $('.errorBrand').addClass('d-none');
        $('.errorDesc').addClass('d-none');

        $("#id").val(row.Promosi_ID);
        $("#Promosi_Code").val(row.Promosi_Code).prop("disabled", true);
        $("#Promosi_Name").val(row.Promosi_Name);
        $("#Promosi_Desc").val(row.Promosi_Desc);
        $("#Promosi_CategoryCode").selectpicker("val", row.Promosi_CategoryCode);

        if (row.Promosi_Type == 1) {
            $("#radio1").prop("checked", true).addClass("Radios");
            $("#radio2").prop("checked", false).removeClass("Radios");
            $("#radio3").prop("checked", false).removeClass("Radios");
            $("#collapse1").collapse("show");
            $("#collapse2").collapse("hide");
            $("#collapse3").collapse("hide");
            $('#Promosi_Vidio_File1').removeAttr('name');
            $('#Promosi_Vidio_File2').removeAttr('name');
            $('#Promosi_Link_File1').removeAttr('name');
            $('#Promosi_Link_File2').removeAttr('name');
            $('#Images1').attr('src', row.Promosi_File1);
            $('#Images2').attr('src', row.Promosi_File2);
            $('#Promosi_Image_File11').val(row.Promosi_File1);
            $('#Promosi_Image_File22').val(row.Promosi_File2);
            $('#Promosi_Vidio_File11').removeAttr('name');
            $('#Promosi_Vidio_File22').removeAttr('name');

            if (row.Promosi_File1 != null) {
                $("#ImagesPromosi_File1").attr("src", row.Promosi_File1);
                //$(".cr-image")[0].setAttribute("src", row.Promosi_File1);
            } else {
                $("#ImagesPromosi_File1").attr("src", "/Content/images/default-image.png");
                //$(".cr-image")[0].setAttribute("alt", "");
            }
            $("#Promosi_File1").val(row.Promosi_File1);
                        
            if (row.Promosi_File2 != null) {
                $("#ImagesPromosi_File2").attr("src", row.Promosi_File2);
                //$(".cr-image")[1].setAttribute("src", row.Promosi_File2);
            } else {
                $("#ImagesPromosi_File2").attr("src", "/Content/images/default-image.png");
                //$(".cr-image")[1].setAttribute("alt", "");
            }
            $("#Promosi_File2").val(row.Promosi_File2);

        } else if (row.Promosi_Type == 2) {
            $("#radio1").prop("checked", false).removeClass("Radios");
            $("#radio2").prop("checked", true).addClass("Radios");
            $("#radio3").prop("checked", false).removeClass("Radios");
            $("#collapse1").collapse("hide");
            $("#collapse2").collapse("show");
            $("#collapse3").collapse("hide");
            $('#Promosi_Image_File1').removeAttr('name');
            $('#Promosi_Image_File2').removeAttr('name');
            $('#vidio1').text(row.Promosi_File1);
            $('#vidio2').text(row.Promosi_File2);
            $('#Promosi_Link_File1').removeAttr('name');
            $('#Promosi_Link_File2').removeAttr('name');
            $('#Promosi_Vidio_File1').val("").attr('name', 'Promosi_File1');
            $('#Promosi_Vidio_File2').val("").attr('name', 'Promosi_File2');
            $('#Promosi_Vidio_File11').val(row.Promosi_File1);
            $('#Promosi_Vidio_File22').val(row.Promosi_File2);
            $('#Promosi_Image_File11').removeAttr('name');
            $('#Promosi_Image_File22').removeAttr('name');

        } else if (row.Promosi_Type == 3) {
            $("#radio1").prop("checked", false).removeClass("Radios");
            $("#radio2").prop("checked", false).removeClass("Radios");
            $("#radio3").prop("checked", true).addClass("Radios");
            $("#collapse1").collapse("hide");
            $("#collapse2").collapse("hide");
            $("#collapse3").collapse("show");
            $('#Promosi_Image_File1').removeAttr('name');
            $('#Promosi_Image_File2').removeAttr('name');
            $('#Promosi_Vidio_File1').removeAttr('name');
            $('#Promosi_Vidio_File2').removeAttr('name');
            $('#Promosi_Link_File1').val(row.Promosi_File1).attr('name', 'Promosi_File1');
            $('#Promosi_Link_File2').val(row.Promosi_File2).attr('name', 'Promosi_File2');
        }

        $("#Promosi_IsActive").selectpicker("val", row.Promosi_IsActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Promosi_Name);
        $("#idDel").val(row.Promosi_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");

    var Rd = $('.Radios').val();
    if (Rd == 2) {
        var formdata = new FormData();
        var Promosi_File1 = document.getElementById('Promosi_Vidio_File1');
        for (i = 0; i < Promosi_File1.files.length; i++) {
            formdata.set(Promosi_File1.files[i].name, Promosi_File1.files[i]);
            formdata.set("Promosi_File1", Promosi_File1.files[i].name);
        }

        var Promosi_File2 = document.getElementById('Promosi_Vidio_File2');
        for (i = 0; i < Promosi_File2.files.length; i++) {
            formdata.append(Promosi_File2.files[i].name, Promosi_File2.files[i]);
            formdata.set("Promosi_File2", Promosi_File2.files[i].name);
        }

        var Promosi_ID = document.getElementById("id");
        formdata.set("Promosi_ID", Promosi_ID.value);

        var Promosi_Code = document.getElementById("Promosi_Code");
        formdata.set("Promosi_Code", Promosi_Code.value);

        var Promosi_Name = document.getElementById("Promosi_Name");
        formdata.set("Promosi_Name", Promosi_Name.value);

        var Promosi_Desc = document.getElementById("Promosi_Desc");
        formdata.set("Promosi_Desc", Promosi_Desc.value);

        var Promosi_File11 = document.getElementById("vidio1");
        formdata.set("Promosi_File11", Promosi_File11.textContent);

        var Promosi_File22 = document.getElementById("vidio2");
        formdata.set("Promosi_File22", Promosi_File22.textContent);

        formdata.set("Radios", Rd);

        var Promosi_CategoryCode = document.getElementById("Promosi_CategoryCode");
        formdata.set("Promosi_CategoryCode", Promosi_CategoryCode.value);

        var Promosi_IsActive = document.getElementById("Promosi_IsActive");
        formdata.set("Promosi_IsActive", Promosi_IsActive.value);

        $.ajax({
            url: $(this).data('request-url'),
            type: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('.errorCode').addClass('d-none');
                $('.errorName').addClass('d-none');
                $('.errorBrand').addClass('d-none');
                $('.errorDesc').addClass('d-none');

                if (data == "success") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Success", "add " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else if (data == "failed") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Failed!", "failed", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                } else if (data == "update") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Success", "update " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else if (data == "has") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("oopppsss!", "data with that code already exists!", {
                        icon: "warning",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else {
                    if ((data.errorModel.length > 0)) {
                        $(".addL").addClass("d-none");
                        $(".add").removeClass("d-none");

                        for (var i = 0; i < data.errorModel.length; i++) {
                            if (data.errorModel[i]["key"] == "Promosi_Code") {
                                $('.errorCode').removeClass('d-none');
                                $('.errorCode').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_Name") {
                                $('.errorName').removeClass('d-none');
                                $('.errorName').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_CategoryCode") {
                                $('.errorBrand').removeClass('d-none');
                                $('.errorBrand').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_Desc") {
                                $('.errorDesc').removeClass('d-none');
                                $('.errorDesc').text(data.errorModel[i]["errors"]);
                            }
                        }
                    }
                }
            }
        });
    } else if (Rd == 3) {
        $.ajax({
            type: 'POST',
            url: $(this).data('request-url'),
            data: {
                'Promosi_ID': $("#id").val(),
                'Promosi_Code': $("#Promosi_Code").val(),
                'Promosi_Name': $("#Promosi_Name").val(),
                'Promosi_Desc': $("#Promosi_Desc").val(),
                'Radios': Rd,
                'Promosi_File1': $(".Promosi_File1").val(),
                'Promosi_File2': $(".Promosi_File2").val(),
                'Promosi_CategoryCode': $("#Promosi_CategoryCode").val(),
                'Promosi_IsActive': $("#Promosi_IsActive").val()
            },
            success: function (data) {
                $('.errorCode').addClass('d-none');
                $('.errorName').addClass('d-none');
                $('.errorBrand').addClass('d-none');
                $('.errorDesc').addClass('d-none');

                if (data == "success") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Success", "add " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else if (data == "failed") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Failed!", "failed", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                } else if (data == "update") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Success", "update " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else if (data == "has") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("oopppsss!", "data with that code already exists!", {
                        icon: "warning",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else {
                    if ((data.errorModel.length > 0)) {
                        $(".addL").addClass("d-none");
                        $(".add").removeClass("d-none");

                        for (var i = 0; i < data.errorModel.length; i++) {
                            if (data.errorModel[i]["key"] == "Promosi_Code") {
                                $('.errorCode').removeClass('d-none');
                                $('.errorCode').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_Name") {
                                $('.errorName').removeClass('d-none');
                                $('.errorName').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_CategoryCode") {
                                $('.errorBrand').removeClass('d-none');
                                $('.errorBrand').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_Desc") {
                                $('.errorDesc').removeClass('d-none');
                                $('.errorDesc').text(data.errorModel[i]["errors"]);
                            }
                        }
                    }
                }
            },
        });
    } else { 
        $.ajax({
            type: 'POST',
            url: $(this).data('request-url'),
            data: {
                'Promosi_ID': $("#id").val(),
                'Promosi_Code': $("#Promosi_Code").val(),
                'Promosi_Name': $("#Promosi_Name").val(),
                'Promosi_Desc': $("#Promosi_Desc").val(),
                'Radios': Rd,
                'Promosi_File1': $("#Promosi_File1").val(),
                'Promosi_File2': $("#Promosi_File2").val(),
                'Promosi_CategoryCode': $("#Promosi_CategoryCode").val(),
                'Promosi_IsActive': $("#Promosi_IsActive").val()
            },
            success: function (data) {
                $('.errorCode').addClass('d-none');
                $('.errorName').addClass('d-none');
                $('.errorBrand').addClass('d-none');
                $('.errorDesc').addClass('d-none');

                if (data == "success") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Success", "add " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else if (data == "failed") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Failed!", "failed", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                } else if (data == "update") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("Success", "update " + ViewBag + " successfully", {
                        icon: "success",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else if (data == "has") {
                    cleanValue();
                    $('#ModalAU').modal('hide');
                    swal("oopppsss!", "data with that code already exists!", {
                        icon: "warning",
                        buttons: false,
                        timer: 2000,
                    });
                    document.getElementsByName("refresh")[0].click();

                } else {
                    if ((data.errorModel.length > 0)) {
                        $(".addL").addClass("d-none");
                        $(".add").removeClass("d-none");

                        for (var i = 0; i < data.errorModel.length; i++) {
                            if (data.errorModel[i]["key"] == "Promosi_Code") {
                                $('.errorCode').removeClass('d-none');
                                $('.errorCode').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_Name") {
                                $('.errorName').removeClass('d-none');
                                $('.errorName').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_CategoryCode") {
                                $('.errorBrand').removeClass('d-none');
                                $('.errorBrand').text(data.errorModel[i]["errors"]);
                            }

                            if (data.errorModel[i]["key"] == "Promosi_Desc") {
                                $('.errorDesc').removeClass('d-none');
                                $('.errorDesc').text(data.errorModel[i]["errors"]);
                            }
                        }
                    }
                }
            },
        });
    }
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Promosi_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#ImagesPromosi_File1").attr("src", "/Content/images/default-image.png");
    $("#ImagesPromosi_File2").attr("src", "/Content/images/default-image.png");
    $(".cr-image").attr("src", "/Content/images/default-image.png");    
    $("#Promosi_File1").val("");
    $("#Promosi_File2").val("");
    $("#item_Image1").val("");
    $("#item_Image2").val("");

    $("#Promosi_Vidio_File1").val("");
    $("#Promosi_Vidio_File2").val("");
    $("#Promosi_Vidio_File11").val("");
    $("#Promosi_Vidio_File22").val("");
    $("#Promosi_Link_File1").val("");
    $("#Promosi_Link_File2").val("");

    $("#uploadPromosi_File1").val("");
    $("#uploadPromosi_File2").val("");

    $("#Promosi_ID").val("");
    $("#Promosi_Code").val("").prop("disabled", false);
    $("#Promosi_Name").val("");
    $("#Promosi_Desc").val("");
    $("#Promosi_CategoryCode").selectpicker("val", 0);
    $("#Promosi_IsActive").val("");
    $("#active").addClass('d-none');

    $("#radio1").prop("checked", false).removeClass("Radios");
    $("#radio2").prop("checked", false).removeClass("Radios");
    $("#radio3").prop("checked", false).removeClass("Radios");
    $("#collapse1").removeClass("show");
    $("#collapse2").removeClass("show");
    $("#collapse3").removeClass("show");

    $('.errorCode').addClass('d-none');
    $('.errorName').addClass('d-none');
    $('.errorBrand').addClass('d-none');
    $('.errorDesc').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

////import
//$('.import').click(function () {
//    $(".modal-title").text('Import ' + ViewBag);
//    $("#ModalImport").modal('show');
//    $("#input-excel").val("");
//    $("#result-table").addClass("d-none");
//    $(".Import").removeClass("d-none");
//    $(".ImportL").addClass("d-none");
//    $("#qw").empty();
//});

//$(document).ready(function () {
//    var input = document.getElementById('input-excel')
//    input.addEventListener('change', function () {
//        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
//            $('.errorImport').addClass('d-none');
//            $("#result-table").removeClass("d-none");
//            var json_object = JSON.stringify(data);
//            var k = JSON.parse(json_object);
//            for (g = 1; g < k.length; g++) {
//                $('#qw').append(
//                    '<tr>' +
//                    '<td>' + k[g][0] + '</td>' +
//                    '<td>' + k[g][1] + '</td>' +
//                    '<td>' + k[g][2] + '</td>' +
//                    '</tr>'
//                );
//            }

//            //$('#table_import').DataTable({
//            //    //responsive: true
//            //});

//        }, (error) => {
//            console.error(error)
//            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
//        })
//    })
//});

//$('.Import').on('click', function () {
//    if (document.getElementById("input-excel").value.length == 0) {
//        $('.errorImport').removeClass('d-none');
//    } else {
//        $(".ImportL").removeClass("d-none");
//        $(".Import").addClass("d-none");
//        var fileInput = document.getElementById('input-excel');
//        var formdata = new FormData();

//        for (i = 0; i < fileInput.files.length; i++) {
//            formdata.append(fileInput.files[i].name, fileInput.files[i]);
//        }

//        $.ajax({
//            url: $(this).data('request-url'),
//            type: 'POST',
//            data: formdata,
//            cache: false,
//            contentType: false,
//            processData: false,
//            success: function (data) {
//                $("#ModalImport").modal('hide');
//                if (data == "success") {
//                    document.getElementsByName("refresh")[0].click();
//                    swal("Success", "import " + ViewBag + " successfully", {
//                        icon: "success",
//                        buttons: false,
//                        timer: 2000,
//                    });
//                } else {
//                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
//                        icon: "error",
//                        buttons: false,
//                        timer: 2000,
//                    });
//                }
//            }
//        });
//    }
//});

////image
//$uploadPromosi_File1 = $('#upload-Promosi_File1').croppie({
//    enableExif: true,
//    viewport: {
//        width: 250,
//        height: 250
//    },
//    boundary: {
//        width: 300,
//        height: 300
//    }
//});

//$('#uploadPromosi_File1').on('change', function () {
//    var reader1 = new FileReader();
//    reader1.onload = function (e) {
//        $uploadPromosi_File1.croppie('bind', {
//            url: e.target.result
//        }).then(function () {
//            console.log('jQuery bind complete');
//        });

//    }
//    reader1.readAsDataURL(this.files[0]);
//});

//$('.upload-Promosi_File1').on('click', function (ev) {
//    $uploadPromosi_File1.croppie('result', {
//        type: 'canvas',
//        size: 'viewport'
//    }).then(function (img) {
//        //$("#Images1TYPE_ICON").attr("src", img);
//        $("#ImagesPromosi_File1").attr("src", img);
//        $("#ChooseImagePromosi_File1").modal("hide");
//        $("#AddModal").modal("show");
//        $("#Promosi_File1").val(img);
//    });
//});


////image2
//$uploadPromosi_File2 = $('#upload-Promosi_File2').croppie({
//    enableExif: true,
//    viewport: {
//        width: 250,
//        height: 250
//    },
//    boundary: {
//        width: 300,
//        height: 300
//    }
//});

//$('#uploadPromosi_File2').on('change', function () {
//    var reader1 = new FileReader();
//    reader1.onload = function (e) {
//        $uploadPromosi_File2.croppie('bind', {
//            url: e.target.result
//        }).then(function () {
//            console.log('jQuery bind complete');
//        });

//    }
//    reader1.readAsDataURL(this.files[0]);
//});

//$('.upload-Promosi_File2').on('click', function (ev) {
//    $uploadPromosi_File2.croppie('result', {
//        type: 'canvas',
//        size: 'viewport'
//    }).then(function (img) {
//        //$("#Images1TYPE_ICON").attr("src", img);
//        $("#ImagesPromosi_File2").attr("src", img);
//        $("#ChooseImagePromosi_File2").modal("hide");
//        $("#AddModal").modal("show");
//        $("#Promosi_File2").val(img);
//    });
//});

function image() {
    $("#radio1").addClass("Radios").prop("checked", true);
    $("#radio2").removeClass("Radios").prop("checked", false);
    $("#radio3").removeClass("Radios").prop("checked", false);

    $("#Promosi_File1").addClass("Promosi_File1");
    $("#Promosi_File2").addClass("Promosi_File2");

    $("#Promosi_Vidio_File1").removeClass("Promosi_File1");
    $("#Promosi_Vidio_File2").removeClass("Promosi_File2");

    $("#Promosi_Link_File1").removeClass("Promosi_File1");
    $("#Promosi_Link_File2").removeClass("Promosi_File2");

    $("#collapse1").collapse("show");
    $("#collapse2").collapse("hide");
    $('#Promosi_Vidio_File1').removeAttr('name');
    $('#Promosi_Vidio_File2').removeAttr('name');
    $("#collapse3").collapse("hide");
    $('#Promosi_Link_File1').removeAttr('name');
    $('#Promosi_Link_File2').removeAttr('name');
    $('#Promosi_Image_File1').attr('name', 'Promosi_File1').val("");
    $('#Promosi_Image_File2').attr('name', 'Promosi_File2').val("");
}
function video() {
    $("#radio1").removeClass("Radios").prop("checked", false);
    $("#radio2").addClass("Radios").prop("checked", true);
    $("#radio3").removeClass("Radios").prop("checked", false);

    $("#Promosi_File1").removeClass("Promosi_File1");
    $("#Promosi_File2").removeClass("Promosi_File2");

    $("#Promosi_Vidio_File1").addClass("Promosi_File1");
    $("#Promosi_Vidio_File2").addClass("Promosi_File2");

    $("#Promosi_Link_File1").removeClass("Promosi_File1");
    $("#Promosi_Link_File2").removeClass("Promosi_File2");

    $("#collapse1").collapse("hide");
    $('#Promosi_Image_File1').removeAttr('name');
    $('#Promosi_Image_File2').removeAttr('name');
    $('#Images1').attr('src', '');
    $('#Images2').attr('src', '');
    $("#collapse2").collapse("show");
    $("#collapse3").collapse("hide");
    $('#Promosi_Link_File1').removeAttr('name');
    $('#Promosi_Link_File2').removeAttr('name');
    $('#Promosi_Vidio_File1').val("").attr('name', 'Promosi_File1');
    $('#Promosi_Vidio_File2').val("").attr('name', 'Promosi_File2');
}
function link() {
    $("#radio1").removeClass("Radios").prop("checked", false);
    $("#radio2").removeClass("Radios").prop("checked", false);
    $("#radio3").addClass("Radios").prop("checked", true);

    $("#Promosi_File1").removeClass("Promosi_File1");
    $("#Promosi_File2").removeClass("Promosi_File2");

    $("#Promosi_Vidio_File1").removeClass("Promosi_File1");
    $("#Promosi_Vidio_File2").removeClass("Promosi_File2");

    $("#Promosi_Link_File1").addClass("Promosi_File1");
    $("#Promosi_Link_File2").addClass("Promosi_File2");

    $("#collapse1").collapse("hide");
    $('#Promosi_Image_File1').removeAttr('name');
    $('#Promosi_Image_File2').removeAttr('name');
    $('#Images1').attr('src', '');
    $('#Images2').attr('src', '');
    $("#collapse2").collapse("hide");
    $('#Promosi_Vidio_File1').removeAttr('name');
    $('#Promosi_Vidio_File2').removeAttr('name');
    $('#Promosi_Link_File1').val("").attr('name', 'Promosi_File1');
    $('#Promosi_Link_File2').val("").attr('name', 'Promosi_File2');
    $("#collapse3").collapse("show");
}