﻿
// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        console.log(row);
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorName').addClass('d-none');
        $('.errorRepeat').addClass('d-none');

        $("#active").removeClass('d-none');
        $("#id").val(row.Repeat_IDX);
        $("#Repeat_Name").val(row.Repeat_Name).prop("disabled", true);
        $("#Repeat_Type").selectpicker('val', row.Repeat_Type);

        if (row.Repeat_Type == "Daily") {
            $("#Every_Daily").removeClass("d-none");
            $("#Every_Weekly").addClass("d-none");
            $("#Every_Monthly").addClass("d-none");
            $("#On_Weekly").addClass("d-none");
            $("#On_Monthly").addClass("d-none");

            $('#Repeat_Daily').val(row.Repeat_Every);

            for (w = 0; w <= 6; w++) {
                $('#day' + w).prop('checked', false);
            }

            for (w = 1; w <= 31; w++) {
                $('#RepeatOn_' + w).prop('checked', false);
            }


        } else if (row.Repeat_Type == "Weekly") {
            $("#Every_Daily").addClass("d-none");
            $("#Every_Weekly").removeClass("d-none");
            $("#Every_Monthly").addClass("d-none");
            $("#On_Weekly").removeClass("d-none");
            $("#On_Monthly").addClass("d-none");

            $('#Repeat_Weekly').val(row.Repeat_Every);

            for (w = 0; w <= 6; w++) {
                $('#day' + w).prop('checked', false);
            }

            for (w = 1; w <= 31; w++) {
                $('#RepeatOn_' + w).prop('checked', false);
            }

            var a = row.Repeat_On.split(",");
            var day = [];
            for (p = 0; p < a.length; p++) {
                if (a[p] == "Sunday") {
                    day.push(0);
                }

                if (a[p] == "Monday") {
                    day.push(1);
                }

                if (a[p] == "Tuesday") {
                    day.push(2);
                }

                if (a[p] == "Wednesday") {
                    day.push(3);
                }

                if (a[p] == "Thursday") {
                    day.push(4);
                }

                if (a[p] == "Friday") {
                    day.push(5);
                }

                if (a[p] == "Saturday") {
                    day.push(6);
                }
            } 
            
            for (w = 0; w < day.length; w++) {
                $('#day' + day[w]).prop('checked', true);
            }


        } else if (row.Repeat_Type == "Monthly") {
            $("#Every_Daily").addClass("d-none");
            $("#Every_Weekly").addClass("d-none");
            $("#Every_Monthly").removeClass("d-none");
            $("#On_Weekly").addClass("d-none");
            $("#On_Monthly").removeClass("d-none");

            $('#Repeat_Monthly').val(row.Repeat_Every);

            for (w = 0; w <= 6; w++) {
                $('#day' + w).prop('checked', false);
            }

            for (w = 1; w <= 31; w++) {
                $('#RepeatOn_' + w).prop('checked', false);
            }

            var a = row.Repeat_On.split(",");
            for (w = 0; w < a.length; w++) {
                $('#RepeatOn_' + a[w]).prop('checked', true);
            }

        }

        //$("#Role_IsActive").selectpicker('val', row.Role_IsActive.toString());
        //$("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Repeat_Name);
        $("#idDel").val(row.Repeat_IDX);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");

    var dataPost = null;  

    if ($("#Repeat_Daily").val()) { 
        dataPost = {
            'Repeat_IDX': $("#id").val(),
            'Repeat_Name': $("#Repeat_Name").val(),
            'Repeat_Type': $("#Repeat_Type").val(),
            'Repeat_Every1': $("#Repeat_Daily").val()
        }
    } else if ($("#Repeat_Weekly").val()) {
        dataPost = {
            'Repeat_IDX': $("#id").val(),
            'Repeat_Name': $("#Repeat_Name").val(),
            'Repeat_Type': $("#Repeat_Type").val(),
            'Repeat_Every2': $("#Repeat_Weekly").val(),
            'RepeatOn_1': $("#day0:checked").val(),
            'RepeatOn_2': $("#day1:checked").val(),
            'RepeatOn_3': $("#day2:checked").val(),
            'RepeatOn_4': $("#day3:checked").val(),
            'RepeatOn_5': $("#day4:checked").val(),
            'RepeatOn_6': $("#day5:checked").val(),
            'RepeatOn_7': $("#day6:checked").val()
        }
    } else if ($("#Repeat_Monthly").val()) {
        dataPost = {
            'Repeat_IDX': $("#id").val(),
            'Repeat_Name': $("#Repeat_Name").val(),
            'Repeat_Type': $("#Repeat_Type").val(),
            'Repeat_Every3': $("#Repeat_Monthly").val(),
            'RepeatOn_1': $("#RepeatOn_1:checked").val(),
            'RepeatOn_2': $("#RepeatOn_2:checked").val(),
            'RepeatOn_3': $("#RepeatOn_3:checked").val(),
            'RepeatOn_4': $("#RepeatOn_4:checked").val(),
            'RepeatOn_5': $("#RepeatOn_5:checked").val(),
            'RepeatOn_6': $("#RepeatOn_6:checked").val(),
            'RepeatOn_7': $("#RepeatOn_7:checked").val(),
            'RepeatOn_8': $("#RepeatOn_8:checked").val(),
            'RepeatOn_9': $("#RepeatOn_9:checked").val(),
            'RepeatOn_10': $("#RepeatOn_10:checked").val(),
            'RepeatOn_11': $("#RepeatOn_11:checked").val(),
            'RepeatOn_12': $("#RepeatOn_12:checked").val(),
            'RepeatOn_13': $("#RepeatOn_13:checked").val(),
            'RepeatOn_14': $("#RepeatOn_14:checked").val(),
            'RepeatOn_15': $("#RepeatOn_15:checked").val(),
            'RepeatOn_16': $("#RepeatOn_16:checked").val(),
            'RepeatOn_17': $("#RepeatOn_17:checked").val(),
            'RepeatOn_18': $("#RepeatOn_18:checked").val(),
            'RepeatOn_19': $("#RepeatOn_19:checked").val(),
            'RepeatOn_20': $("#RepeatOn_20:checked").val(),
            'RepeatOn_21': $("#RepeatOn_21:checked").val(),
            'RepeatOn_22': $("#RepeatOn_22:checked").val(),
            'RepeatOn_23': $("#RepeatOn_23:checked").val(),
            'RepeatOn_24': $("#RepeatOn_24:checked").val(),
            'RepeatOn_25': $("#RepeatOn_25:checked").val(),
            'RepeatOn_26': $("#RepeatOn_26:checked").val(),
            'RepeatOn_27': $("#RepeatOn_27:checked").val(),
            'RepeatOn_28': $("#RepeatOn_28:checked").val(),
            'RepeatOn_29': $("#RepeatOn_29:checked").val(),
            'RepeatOn_30': $("#RepeatOn_30:checked").val(),
            'RepeatOn_31': $("#RepeatOn_31:checked").val()
        }
    }

    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: dataPost,
        success: function (data) {
            $('.errorName').addClass('d-none');
            $('.errorRepeat').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data with that code already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");
                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Repeat_Name") {
                            $('.errorName').removeClass('d-none');
                            $('.errorName').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Repeat_Type") {
                            $('.errorRepeat').removeClass('d-none');
                            $('.errorRepeat').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Repeat_IDX': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#id").val("");
    $('#Repeat_Name').val("").prop("disabled", false);
    $('#Repeat_Daily').val("");
    $('#Repeat_Weekly').val("");
    $('#Repeat_Monthly').val("");

    $("#Every_Daily").addClass("d-none");
    $("#Every_Weekly").addClass("d-none");
    $("#Every_Monthly").addClass("d-none");
    $("#On_Weekly").addClass("d-none");
    $("#On_Monthly").addClass("d-none");

    for (w = 0; w <= 6; w++) {
        $('#day' + w).prop("checked", false);
    }

    for (w = 1; w <= 31; w++) {
        $('#RepeatOn_' + w).prop("checked", false);
    }

    $('.selectpicker').selectpicker('val', 0);
    
    //$("#Role_IsActive").val("");
    //$("#active").addClass('d-none');

    $('.errorName').addClass('d-none');
    $('.errorRepeat').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}

////import
//$('.import').click(function () {
//    $(".modal-title").text('Import ' + ViewBag);
//    $("#ModalImport").modal('show');
//    $("#input-excel").val("");
//    $("#result-table").addClass("d-none");
//    $(".Import").removeClass("d-none");
//    $(".ImportL").addClass("d-none");
//    $("#qw").empty();
//});

//$(document).ready(function () {
//    var input = document.getElementById('input-excel')
//    input.addEventListener('change', function () {
//        readXlsxFile(input.files[0], { dateFormat: 'MM/dd/yyyy' }).then(function (data) {
//            $('.errorImport').addClass('d-none');
//            $("#result-table").removeClass("d-none");
//            var json_object = JSON.stringify(data);
//            var k = JSON.parse(json_object);
//            for (g = 1; g < k.length; g++) {
//                $('#qw').append(
//                    '<tr>' +
//                    '<td>' + k[g][0] + '</td>' +
//                    '<td>' + k[g][1] + '</td>' +
//                    '<td>' + k[g][2] + '</td>' +
//                    '</tr>'
//                );
//            }

//            //$('#table_import').DataTable({
//            //    //responsive: true
//            //});

//        }, (error) => {
//            console.error(error)
//            alert("Error while parsing Excel file, change your type excel to Microsoft Excel Worksheet.")
//        })
//    })
//});

//$('.Import').on('click', function () {
//    if (document.getElementById("input-excel").value.length == 0) {
//        $('.errorImport').removeClass('d-none');
//    } else {
//        $(".ImportL").removeClass("d-none");
//        $(".Import").addClass("d-none");
//        var fileInput = document.getElementById('input-excel');
//        var formdata = new FormData();

//        for (i = 0; i < fileInput.files.length; i++) {
//            formdata.append(fileInput.files[i].name, fileInput.files[i]);
//        }

//        $.ajax({
//            url: $(this).data('request-url'),
//            type: 'POST',
//            data: formdata,
//            cache: false,
//            contentType: false,
//            processData: false,
//            success: function (data) {
//                $("#ModalImport").modal('hide');
//                if (data == "success") {
//                    document.getElementsByName("refresh")[0].click();
//                    swal("Success", "import " + ViewBag + " successfully", {
//                        icon: "success",
//                        buttons: false,
//                        timer: 2000,
//                    });
//                } else {
//                    swal("Failed!", "import " + ViewBag + " failed, Check your data", {
//                        icon: "error",
//                        buttons: false,
//                        timer: 2000,
//                    });
//                }
//            }
//        });
//    }
//});

$('#Repeat_Type').change(function () {
    var val = $("#Repeat_Type option:selected").val();
    if (val == "Daily") {
        $("#Every_Daily").removeClass("d-none");
        $("#Repeat_Daily").val("");
        $("#Every_Weekly").addClass("d-none");
        $("#Repeat_Weekly").val("");
        $("#Every_Monthly").addClass("d-none");
        $("#Repeat_Monthly").val("");
        $("#On_Weekly").addClass("d-none");
        $("#On_Monthly").addClass("d-none");

    } else if (val == "Weekly") {
        $("#Every_Daily").addClass("d-none");
        $("#Repeat_Daily").val("");
        $("#Every_Weekly").removeClass("d-none");
        $("#Repeat_Weekly").val("");
        $("#Every_Monthly").addClass("d-none");
        $("#Repeat_Monthly").val("");
        $("#On_Weekly").removeClass("d-none");
        $("#On_Monthly").addClass("d-none");


    } else if (val == "Monthly") {
        $("#Every_Daily").addClass("d-none");
        $("#Repeat_Daily").val("");
        $("#Every_Weekly").addClass("d-none");
        $("#Repeat_Weekly").val("");
        $("#Every_Monthly").removeClass("d-none");
        $("#Repeat_Monthly").val("");
        $("#On_Weekly").addClass("d-none");
        $("#On_Monthly").removeClass("d-none");
    }
});