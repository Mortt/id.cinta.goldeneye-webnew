﻿// create event
$('.create').click(function () {
    $(".modal-title").text('Add ' + ViewBag);
    cleanValue();
    $("#ModalAU").modal('show');
});

window.operateEvents = {
    'click .like': function (e, value, row, index) {
        $(".modal-title").text('Edit ' + ViewBag);
        $("#ModalAU").modal('show');

        $('.errorName').addClass('d-none');
        $('.errorDesc').addClass('d-none');

        $("#id").val(row.Type_Outlet_ID);
        $("#Type_Outlet_Name").val(row.Type_Outlet_Name).prop("disabled", true);
        $("#Type_Outlet_Desc").val(row.Type_Outlet_Desc);
        $("#Type_Outlet_IsActive").selectpicker("val", row.Type_Outlet_IsActive.toString());
        $("#active").removeClass("d-none");
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Delete ' + ViewBag);
        $("#nameDel").text(row.Type_Outlet_Name);
        $("#idDel").val(row.Type_Outlet_ID);
        $("#ModalDelete").modal('show');
    }
}

$('.modal-footer').on('click', '.add', function () {
    $(".addL").removeClass("d-none");
    $(".add").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Type_Outlet_ID': $("#id").val(),
            'Type_Outlet_Name': $("#Type_Outlet_Name").val(),
            'Type_Outlet_Desc': $("#Type_Outlet_Desc").val(),
            'Type_Outlet_IsActive': $("#Type_Outlet_IsActive").val()
        },
        success: function (data) {
            $('.errorName').addClass('d-none');
            $('.errorDesc').addClass('d-none');

            if (data == "success") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "add " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "failed") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Failed!", "failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            } else if (data == "update") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("Success", "update " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else if (data == "has") {
                cleanValue();
                $('#ModalAU').modal('hide');
                swal("oopppsss!", "data already exists!", {
                    icon: "warning",
                    buttons: false,
                    timer: 2000,
                });
                document.getElementsByName("refresh")[0].click();

            } else {
                if ((data.errorModel.length > 0)) {
                    $(".addL").addClass("d-none");
                    $(".add").removeClass("d-none");

                    for (var i = 0; i < data.errorModel.length; i++) {
                        if (data.errorModel[i]["key"] == "Type_Outlet_Name") {
                            $('.errorName').removeClass('d-none');
                            $('.errorName').text(data.errorModel[i]["errors"]);
                        }

                        if (data.errorModel[i]["key"] == "Type_Outlet_Desc") {
                            $('.errorDesc').removeClass('d-none');
                            $('.errorDesc').text(data.errorModel[i]["errors"]);
                        }
                    }
                }
            }
        },
    });
});

$('.modal-footer').on('click', '.delete', function () {
    $(".deleteL").removeClass("d-none");
    $(".delete").addClass("d-none");
    $.ajax({
        type: 'POST',
        url: $(this).data('request-url'),
        data: {
            'Type_Outlet_ID': $("#idDel").val(),
        },
        success: function (data) {
            $("#ModalDelete").modal('hide');
            $(".deleteL").addClass("d-none");
            $(".delete").removeClass("d-none");
            if (data == 1) {
                document.getElementsByName("refresh")[0].click();
                swal("Success", "delete " + ViewBag + " successfully", {
                    icon: "success",
                    buttons: false,
                    timer: 2000,
                });
            } else {
                swal("Failed!", "delete " + ViewBag + " failed", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }
        },
    });
});

function cleanValue() {
    $("#id").val("");
    $(".selectpicker").selectpicker("val", 0);
    $("#Type_Outlet_ID").val("");
    $("#Type_Outlet_Name").val("").prop("disabled", false);
    $("#Type_Outlet_Desc").val("");
    $("#Type_Outlet_IsActive").val("");
    $("#active").addClass('d-none');

    $('.errorName').addClass('d-none');
    $('.errorDesc').addClass('d-none');

    $(".addL").addClass("d-none");
    $(".add").removeClass("d-none");
}
