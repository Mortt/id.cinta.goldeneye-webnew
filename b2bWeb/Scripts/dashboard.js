﻿//Load Data in Table when documents is ready  
$(document).ready(function () {
    loadData();
});

function ToJavaScriptTime(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return ("0" + dt.getHours()).slice(-2) + ":" + ("0" + dt.getMinutes()).slice(-2);
}
function ToJavaScriptDateTime(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return dt.getFullYear() + "-" +
        ("0" + (dt.getMonth() + 1)).slice(-2) + "-" +
        ("0" + dt.getDate()).slice(-2) + " " +
        ("0" + dt.getHours()).slice(-2) + ":" +
        ("0" + dt.getMinutes()).slice(-2);
}
function ToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return dt.getFullYear() + "-" +
        ("0" + (dt.getMonth() + 1)).slice(-2) + "-" +
        ("0" + dt.getDate()).slice(-2);
}

const numberWithCommas = (x) => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

//Load Data function  
//function loadData() {
//    $.ajax({
//        url: "/Dashboard/List",
//        type: "GET",
//        contentType: "application/json;charset=utf-8",
//        dataType: "json",
//        success: function (result) {
//            var html = '</br></br>';
//            $.each(result, function (key, item) { 
//                html += '<div class="row">'
//                html += '<div class="d-flex flex-wrap justify-content-center align-content-between flex-wrap">';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/product.jpg" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Master Products : </p ><span class="badge badge-dark">' + item.Dashboard_Product + '</span></div></h9> <a href="../Item/" class="btn btn-primary">Product</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/Employee.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Master Employee : </p ><span class="badge badge-dark">' + item.Dashboard_Employee + '</span></div></h9> <a href="../Home/" class="btn btn-primary">Employee</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/brand.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Master Brand : </p ><span class="badge badge-dark">' + item.Dashboard_Brand + '</span></div></h9> <a href="../Brand/" class="btn btn-primary">Brand</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/outlet.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Master Outlet : </p ><span class="badge badge-dark">' + item.Dashboard_Outlet + '</span></div></h9> <a href="../Outlet/" class="btn btn-primary">Outlet</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/route.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Master Route : </p ><span class="badge badge-dark">' + item.Dashboard_Route + '</span></div></h9> <a href="../Route/" class="btn btn-primary">Route</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/area.jpg" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Master Area/COC : </p ><span class="badge badge-dark">' + item.Dashboard_Area + '</span></div></h9> <a href="../Area/" class="btn btn-primary">Area</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/respond.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Master Respond : </p ><span class="badge badge-dark">' + item.Dashboard_Respond + '</span></div></h9> <a href="../Respond/" class="btn btn-primary">Respond</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/visit.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Visit : </p ><span class="badge badge-dark">' + item.Dashboard_Visit + '</span></div></h9> <a href="../Visit/" class="btn btn-primary">Visit</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/Display.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Display : </p ><span class="badge badge-dark">' + item.Dashboard_Display + '</span></div></h9> <a href="../Display/" class="btn btn-primary">Display</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/opname.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Opname : </p ><span class="badge badge-dark">' + item.Dashboard_Opname + '</span></div></h9> <a href="../Opname/" class="btn btn-primary">Opname</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/compact.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx CompAct : </p ><span class="badge badge-dark">' + item.Dashboard_CompAct + '</span></div></h9> <a href="../CompAct/" class="btn btn-primary">CompAct</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/feedback.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Feedback : </p ><span class="badge badge-dark">' + item.Dashboard_Feedback + '</span></div></h9> <a href="../Feedback/" class="btn btn-primary">Feedback</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/sampling.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Sampling : </p ><span class="badge badge-dark">' + item.Dashboard_Sampling + '</span></div></h9> <a href="../Sampling/" class="btn btn-primary">Sampling</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/loyalty.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Loyalti : </p ><span class="badge badge-dark">' + item.Dashboard_Loyalti + '</span></div></h9> <a href="../Loyalti/" class="btn btn-primary">Loyalti</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/selling.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Selling : </p ><span class="badge badge-dark">' + item.Dashboard_Selling + '</span></div></h9> <a href="../Selling/" class="btn btn-primary">Selling</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '<div class="card mb-3" style="width: 18rem;"><img class="card-img-top" src="https://foodiegost17.blob.core.windows.net/b2b/APA/image/coaching.png" alt="Card image cap"><div class="card-body">  <h9 class="card-title"><div class="alert alert-dark" role="alert"><p class="card-text">Jumlah Trx Coaching : </p ><span class="badge badge-dark">' + item.Dashboard_Coaching + '</span></div></h9> <a href="../Coaching/" class="btn btn-primary">Coaching</a></div></div>&nbsp;&nbsp;&nbsp;';
//                html += '</div>';
//            });
//            $('.tbody').html(html);
//        },
//        error: function (errormessage) {
//            alert(errormessage.responseText);
//        }
//    });
//}

//Add Data Function   
function Add() {
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        EmployeeID: $('#EmployeeID').val(),
        Name: $('#Name').val(),
        Age: $('#Age').val(),
        State: $('#State').val(),
        Country: $('#Country').val()
    };
    $.ajax({
        url: "/Home/Add",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

//Function for getting the Data Based upon Employee ID  
function getbyID(VisID) {
    $('#Name').css('border-color', 'lightgrey');
    $('#Age').css('border-color', 'lightgrey');
    $('#State').css('border-color', 'lightgrey');
    $('#Country').css('border-color', 'lightgrey');
    $.ajax({
        url: "/Visit/getbyID/" + VisID,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#EmployeeID').val(result.Visit_ID);
            $('#Name').val(result.Visit_Checkout);
            $('#Age').val(result.Visit_Desc);
            $('#State').val(result.Visit_InsertOn);
            $('#Country').val(result.Visit_OutletCode);

            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

//function for updating employee's record  
function Update() {
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        EmployeeID: $('#EmployeeID').val(),
        Name: $('#Name').val(),
        Age: $('#Age').val(),
        State: $('#State').val(),
        Country: $('#Country').val(),
    };
    $.ajax({
        url: "/Home/Update",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#EmployeeID').val("");
            $('#Name').val("");
            $('#Age').val("");
            $('#State').val("");
            $('#Country').val("");
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

//function for deleting employee's record  
function Delele(ID) {
    var ans = confirm("Are you sure you want to delete this Record?");
    if (ans) {
        $.ajax({
            url: "/Home/Delete/" + ID,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                loadData();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}

//Function for clearing the textboxes  
function clearTextBox() {
    $('#EmployeeID').val("");
    $('#Name').val("");
    $('#Age').val("");
    $('#State').val("");
    $('#Country').val("");
    $('#btnUpdate').hide();
    $('#btnAdd').show();
    $('#Name').css('border-color', 'lightgrey');
    $('#Age').css('border-color', 'lightgrey');
    $('#State').css('border-color', 'lightgrey');
    $('#Country').css('border-color', 'lightgrey');
}
//Valdidation using jquery  
function validate() {
    var isValid = true;
    if ($('#Name').val().trim() == "") {
        $('#Name').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Name').css('border-color', 'lightgrey');
    }
    if ($('#Age').val().trim() == "") {
        $('#Age').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Age').css('border-color', 'lightgrey');
    }
    if ($('#State').val().trim() == "") {
        $('#State').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#State').css('border-color', 'lightgrey');
    }
    if ($('#Country').val().trim() == "") {
        $('#Country').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Country').css('border-color', 'lightgrey');
    }
    return isValid;
}  