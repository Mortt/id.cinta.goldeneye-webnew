﻿//image1
$uploadCrop1 = $('#upload-demo1').croppie({
    enableExif: true,
    viewport: {
        width: 250,
        height: 250
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('#upload1').on('change', function () {
    var reader1 = new FileReader();
    reader1.onload = function (e) {
        $uploadCrop1.croppie('bind', {
            url: e.target.result
        }).then(function () {
            console.log('jQuery bind complete');
        });

    }
    reader1.readAsDataURL(this.files[0]);
});

$('.upload-result1').on('click', function (ev) {
    $uploadCrop1.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (img) {
        $("#Images11").attr("src", img);
        $("#Images1").attr("src", img);
        $("#ChooseImage1").modal("hide");
        $("#AddModal").modal("show");
        $("#Merchant_Image1").val(img);
    });
});
