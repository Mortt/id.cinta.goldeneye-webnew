﻿function ToJavaScriptTime(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return ("0" + dt.getHours()).slice(-2) + ":" + ("0" + dt.getMinutes()).slice(-2);
}
function ToJavaScriptDateTime(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return dt.getFullYear() + "-" +
        ("0" + (dt.getMonth() + 1)).slice(-2) + "-" +
        ("0" + dt.getDate()).slice(-2) + " " +
        ("0" + dt.getHours()).slice(-2) + ":" +
        ("0" + dt.getMinutes()).slice(-2);
}
function ToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return dt.getFullYear() + "-" +
        ("0" + (dt.getMonth() + 1)).slice(-2) + "-" +
        ("0" + dt.getDate()).slice(-2);
}

function ToJavaScriptDateMY(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return dt.getFullYear() + "-" +
        ("0" + (dt.getMonth() + 1)).slice(-2);
}

const numberWithCommas = (x) => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}