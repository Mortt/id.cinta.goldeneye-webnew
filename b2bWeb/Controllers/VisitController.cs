﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace b2bWeb.Controllers
{
    public class VisitController : Controller
    {
        VisitDB vstDB = new VisitDB();
        public static string ToHours(int minutes)
        {
            TimeSpan Menit = TimeSpan.FromMinutes(minutes);
            string Jam = string.Format("{00:00}:{1:00}", (int)Menit.TotalHours, Menit.Minutes);
            return Jam;
        }

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Visit";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Visit(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    var Visit = vstDB.ListExport(g, c);

                    var json_vstDB = JsonConvert.SerializeObject(Visit);
                    DataTable dt_vstDB = (DataTable)JsonConvert.DeserializeObject(json_vstDB, (typeof(DataTable)));

                    if (dt_vstDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Visit");
                    }

                    dt_vstDB.Columns.Add("Visit_InsertOn1", typeof(string));
                    dt_vstDB.Columns.Add("Visit_UpdateOn1", typeof(string));

                    foreach (DataRow objCon in dt_vstDB.Rows)
                    {
                        objCon["Visit_InsertOn1"] = Convert.ToString(objCon["Visit_InsertOn"]);
                        objCon["Visit_UpdateOn1"] = Convert.ToString(objCon["Visit_UpdateOn"]);
                    }

                    dt_vstDB.Columns.Remove("Visit_ID");
                    dt_vstDB.Columns.Remove("Visit_Code");
                    //dt_vstDB.Columns.Remove("Visit_Image");                
                    dt_vstDB.Columns.Remove("Visit_Longitude");
                    dt_vstDB.Columns.Remove("Visit_Latitude");
                    dt_vstDB.Columns.Remove("Visit_Delta");
                    dt_vstDB.Columns.Remove("Visit_Isdelete");
                    dt_vstDB.Columns.Remove("Visit_RespondID");
                    dt_vstDB.Columns.Remove("Visit_InsertOn");
                    dt_vstDB.Columns.Remove("Visit_UpdateOn");

                    dt_vstDB.Columns.Add("Visit_InsertOn", typeof(string));
                    dt_vstDB.Columns.Add("Visit_UpdateOn", typeof(string));
                    //dt_vstDB.Columns.Add("Visit_Image1");
                    //dt_vstDB.Columns.Add("Visit_Name", typeof(string));
                    //dt_vstDB.Columns.Add("Visit_AreaName", typeof(string));


                    for (int a = 0; a < dt_vstDB.Rows.Count; a++)
                    {
                        if (Convert.ToString(dt_vstDB.Rows[a]["Visit_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                        {
                            dt_vstDB.Rows[a]["Visit_UpdateOn"] = "-";
                        }
                        else
                        {
                            dt_vstDB.Rows[a]["Visit_UpdateOn"] = dt_vstDB.Rows[a]["Visit_UpdateOn1"];
                        }

                        dt_vstDB.Rows[a]["Visit_Duration"] = ToHours(Convert.ToInt16(dt_vstDB.Rows[a]["Visit_Durations"]));                        
                    }                   

                    //dt_vstDB.Columns.Remove("Visit_Image");
                    //dt_vstDB.Columns.Add("Visit_Image");

                    foreach (DataRow objCon in dt_vstDB.Rows)
                    {
                        objCon["Visit_InsertOn"] = Convert.ToDateTime(objCon["Visit_InsertOn1"]).ToString("dd-MM-yyyy");
                        //objCon["Visit_Image"] = objCon["Visit_Image1"];
                        //objCon["Visit_Name"] = Convert.ToString(objCon["Insert_Name"]);
                        //objCon["Visit_AreaName"] = Convert.ToString(objCon["Area_Name"]);                    
                    }

                    //dt_vstDB.Columns.Remove("Visit_Image1");
                    dt_vstDB.Columns.Remove("Visit_InsertOn1");
                    dt_vstDB.Columns.Remove("Visit_UpdateOn1");
                    dt_vstDB.Columns.Remove("Visit_Durations");
                    dt_vstDB.Columns.Remove("Visit_InsertBy");
                    dt_vstDB.Columns.Remove("Visit_UpdateOn");
                    dt_vstDB.Columns.Remove("Visit_Update_Name");
                    dt_vstDB.Columns.Remove("Visit_UpdateBy");
                    dt_vstDB.Columns.Remove("Visit_Respond_Name");


                    dt_vstDB.AcceptChanges();

                    //dt_vstDB.DefaultView.Sort = "Visit_ID DESC";
                    //dt_vstDB = dt_vstDB.DefaultView.ToTable();

                    dt_vstDB.Columns["Visit_Nik"].SetOrdinal(0);
                    dt_vstDB.Columns["Visit_Insert_Name"].SetOrdinal(1);
                    dt_vstDB.Columns["Visit_Image"].SetOrdinal(2);
                    dt_vstDB.Columns["Visit_AreaID"].SetOrdinal(3);
                    dt_vstDB.Columns["Visit_Area_Name"].SetOrdinal(4);
                    dt_vstDB.Columns["Visit_Outlet_Code"].SetOrdinal(5);
                    dt_vstDB.Columns["Visit_Outlet_Name"].SetOrdinal(6);
                    dt_vstDB.Columns["Visit_Desc"].SetOrdinal(7);
                    dt_vstDB.Columns["Visit_Checkin"].SetOrdinal(8);
                    dt_vstDB.Columns["Visit_Checkout"].SetOrdinal(9);
                    //dt_vstDB.Columns["Visit_Respond_Name"].SetOrdinal(10);
                    dt_vstDB.Columns["Visit_Duration"].SetOrdinal(10);
                    dt_vstDB.Columns["Visit_InsertOn"].SetOrdinal(11);
                    //dt_vstDB.Columns["Visit_Insert_Name"].SetOrdinal(13);
                    //dt_vstDB.Columns["Visit_UpdateOn"].SetOrdinal(14);
                    //dt_vstDB.Columns["Visit_Update_Name"].SetOrdinal(15);

                    dt_vstDB.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_vstDB;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        for (int a = 0; a < dt_vstDB.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_vstDB.Rows[a]["Visit_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_vstDB.Rows[a]["Visit_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_vstDB;

                        int row = 1;
                        int column = 3;
                        string TableImageName = "Visit_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }                   

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }            
        }
                        
        public ActionResult GetDataVisit()
        {
            Parameter param = new Parameter();

            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {                
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;            

            var Result = vstDB.GetDataVisit(param) ?? new List<JsonVisit>();
            
            if(Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonVisit Baru = new JsonVisit();
                Baru.total = 0;
                Baru.rows = new List<rowsVisit>();
                
                return Json(Baru, JsonRequestBehavior.AllowGet);
            }            
        }
    }
}