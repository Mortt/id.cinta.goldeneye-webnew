﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web.Security;

namespace b2bWeb.Controllers
{
    public class BillingController : Controller
    {
        public ActionResult Index(Billing model, string request)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                DateTime tgl = DateTime.Now;
            if (string.IsNullOrWhiteSpace(request))
            {                    
                    ViewBag.Data = BillingDB.GetData(Convert.ToDateTime(tgl)) ?? new List<Billing>();
            }
            else
            {
                    ViewData["tgl"] = request;
                    string rs = Convert.ToDateTime(request).ToString("MM/yyyy");
                    ViewBag.Data = BillingDB.GetData(Convert.ToDateTime(rs)) ?? new List<Billing>();
            }

            ViewBag.Title = "data";
            return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        //[HttpPost]
        //public ActionResult ExportToExcel(DateTime startDate, DateTime endDate)
        //{
        //    if (Session["Username"] != null && Session["CompanyID"] != null)
        //    {
        //        var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
        //        var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

        //        var gv = new GridView();

        //        gv.DataSource = vstDB.ListAll().Where(p => Convert.ToDateTime(p.Visit_InsertOn.ToString("MM/dd/yyyy")) >= g && Convert.ToDateTime(p.Visit_InsertOn.ToString("MM/dd/yyyy")) <= c).OrderByDescending(s => s.Visit_InsertOn);
        //        gv.DataBind();
        //        Response.ClearContent();
        //        Response.Buffer = true;
        //        Response.AddHeader("content-disposition", "attachment; filename=Visit( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xls");
        //        Response.ContentType = "application/ms-excel";
        //        Response.Charset = "";
        //        StringWriter objStringWriter = new StringWriter();
        //        HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
        //        gv.RenderControl(objHtmlTextWriter);
        //        Response.Output.Write(objStringWriter.ToString());
        //        Response.Flush();
        //        Response.End();
        //        return View("");
        //    }
        //    else
        //    {
        //        return Redirect("/");
        //    }
        //}
       
    }
}