﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using System.Data;
using Newtonsoft.Json;

namespace b2bWeb.Controllers
{
    public class MTaskController : Controller
    {               
        EmployeeDB employeeDB = new EmployeeDB();
        MTaskDB mtaskDB = new MTaskDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Employee = employeeDB.ListAll() ?? new List<Employee>();
                ViewBag.Title = "Master / Task";
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataMTask()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = mtaskDB.GetDataMTask(param) ?? new List<JsonMTask>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonMTask Baru = new JsonMTask();
                Baru.total = 0;
                Baru.rows = new List<rowsMTask>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetbyID(MTask data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                Parameter da = new Parameter();
                da.Company_ID = Convert.ToInt32(Session["CompanyID"]);
                da.Get_Type = "ID";
                da.Search = Convert.ToString(data.Task_ID);

                var OneMTask = mtaskDB.ListID(da);
                return Json(OneMTask, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        List<_DET> listDET = new List<_DET>();

        [HttpPost]
        public ActionResult AddUp(MTask ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (ID.Task_ID == null)
                    {
                        try
                        {
                            string checkbase64 = "base64";
                            if (Request.Form["Task_Image_Example"].Contains(checkbase64))
                            {
                                string base64 = Request.Form["Task_Image_Example"];
                                string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                string b = DashboardController.RandomString("B2bGoldeneye");
                                string c = "taskImages" + a + b + ".png";
                                ID.Task_Image_Example = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                DashboardController.UploadToAzure(base64, c);
                            }
                            else if (Request.Form["Task_Image_Example"] != "")
                            {
                                ID.Task_Image_Example = ID.Task_Image_Example;
                            }
                            else
                            {
                                ID.Task_Image_Example = null;
                            }

                            for (int o = 0; o < ID.Numpang.Count(); o++)
                            {
                                _DET details = new _DET()
                                {
                                    Employee_NIK = ID.Numpang[o],
                                };
                                listDET.Add(details);
                            }

                            MTask dv = new MTask
                            {
                                Task_IsPublish = ID.Task_IsPublish,
                                Task_Image_Example = ID.Task_Image_Example,
                                Task_Title = ID.Task_Title,
                                Task_Desc = ID.Task_Desc,
                                Task_Deadline = ID.Task_Deadline,
                                Task_CompanyID = Convert.ToInt16(Session["CompanyID"]),
                                Username = Convert.ToString(Session["Username"]),
                                DET = listDET
                            };

                            if (MTaskDB.AddUpdate(dv))
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            string checkbase64 = "base64";
                            if (Request.Form["Task_Image_Example"].Contains(checkbase64))
                            {
                                string base64 = Request.Form["Task_Image_Example"];
                                string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                string b = DashboardController.RandomString("B2bGoldeneye");
                                string c = "taskImages" + a + b + ".png";
                                ID.Task_Image_Example = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                DashboardController.UploadToAzure(base64, c);
                            }
                            else if (Request.Form["Task_Image_Example"] != "")
                            {
                                ID.Task_Image_Example = ID.Task_Image_Example;
                            }
                            else
                            {
                                ID.Task_Image_Example = null;
                            }

                            for (int o = 0; o < ID.Numpang.Count(); o++)
                            {
                                _DET details = new _DET()
                                {
                                    Employee_NIK = ID.Numpang[o],
                                };
                                listDET.Add(details);
                            }

                            MTask dv = new MTask
                            {
                                Task_IsPublish = ID.Task_IsPublish,
                                Task_ID = ID.Task_ID,
                                Task_Image_Example = ID.Task_Image_Example,
                                Task_Title = ID.Task_Title,
                                Task_Desc = ID.Task_Desc,
                                Task_Deadline = ID.Task_Deadline,
                                Task_CompanyID = Convert.ToInt16(Session["CompanyID"]),
                                Username = Convert.ToString(Session["Username"]),
                                DET = listDET
                            };

                            if (MTaskDB.AddUpdate(dv))
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(MTask data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Task_UpdateBy = Convert.ToString(Session["Username"]);

                    if (MTaskDB.Delete(data) == true)
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    TempData["Error"] = ex.Message.ToString();
                    return RedirectToAction("Index", "MTask");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
                
        //public ActionResult ExportToExcel()
        //{
        //    if (Session["Username"] != null && Session["CompanyID"] != null)
        //    {
        //        string we = "EXCEL";
        //        var json_mtaskDB = JsonConvert.SerializeObject(mtaskDB.ListAll(we));
        //        DataTable dt_mtaskDB = (DataTable)JsonConvert.DeserializeObject(json_mtaskDB, (typeof(DataTable)));

        //        if (dt_mtaskDB.Rows.Count == 0)
        //        {
        //            return RedirectToAction("Index", "MTask");
        //        }

        //        dt_mtaskDB.Columns.Add("Task_InsertOn1", typeof(string));
        //        dt_mtaskDB.Columns.Add("Task_UpdateOn1", typeof(string));
        //        dt_mtaskDB.Columns.Add("Task_Deadline1", typeof(string));

        //        foreach (DataRow objCon in dt_mtaskDB.Rows)
        //        {
        //            objCon["Task_InsertOn1"] = Convert.ToString(objCon["Task_InsertOn"]);
        //            objCon["Task_UpdateOn1"] = Convert.ToString(objCon["Task_UpdateOn"]);
        //            objCon["Task_Deadline1"] = Convert.ToDateTime(objCon["Task_Deadline"]).ToString("dd/MM/yyyy");
        //        }

        //        dt_mtaskDB.Columns.Remove("Task_InsertOn");
        //        dt_mtaskDB.Columns.Remove("Task_UpdateOn");
        //        dt_mtaskDB.Columns.Remove("Images");
        //        dt_mtaskDB.Columns.Remove("Task_ID");
        //        dt_mtaskDB.Columns.Remove("Task_CompanyID");
        //        dt_mtaskDB.Columns.Remove("Task_IsDelete");
        //        dt_mtaskDB.Columns.Remove("Search");
        //        dt_mtaskDB.Columns.Remove("CompanyID");
        //        dt_mtaskDB.Columns.Remove("GetType");
        //        dt_mtaskDB.Columns.Remove("Username");
        //        dt_mtaskDB.Columns.Remove("Numpang");
        //        dt_mtaskDB.Columns.Remove("Task_Deadline");

        //        dt_mtaskDB.Columns.Add("Task_InsertOn", typeof(string));
        //        dt_mtaskDB.Columns.Add("Task_UpdateOn", typeof(string));
        //        dt_mtaskDB.Columns.Add("Task_Deadline", typeof(string));

        //        dt_mtaskDB.Columns.Remove("DET");
        //        dt_mtaskDB.Columns.Remove("Employee_NIK");
        //        dt_mtaskDB.Columns.Remove("Task_InsertBy");
        //        dt_mtaskDB.Columns.Remove("Task_UpdateBy");

        //        for (int a = 0; a < dt_mtaskDB.Rows.Count; a++)
        //        {
        //            if (Convert.ToString(dt_mtaskDB.Rows[a]["Task_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
        //            {
        //                dt_mtaskDB.Rows[a]["Task_UpdateOn"] = "-";
        //            }
        //            else
        //            {
        //                dt_mtaskDB.Rows[a]["Task_UpdateOn"] = dt_mtaskDB.Rows[a]["Task_UpdateOn1"];
        //            }
        //        }

        //        foreach (DataRow objCon in dt_mtaskDB.Rows)
        //        {
        //            objCon["Task_InsertOn"] = Convert.ToString(objCon["Task_InsertOn1"]);
        //            objCon["Task_Deadline"] = Convert.ToString(objCon["Task_Deadline1"]);
        //        }

        //        dt_mtaskDB.Columns.Remove("Task_InsertOn1");
        //        dt_mtaskDB.Columns.Remove("Task_Deadline1");
        //        dt_mtaskDB.Columns.Remove("Task_UpdateOn1");

        //        dt_mtaskDB.AcceptChanges();

        //        dt_mtaskDB.Columns["Task_Title"].SetOrdinal(0);
        //        dt_mtaskDB.Columns["Task_Image_Example"].SetOrdinal(1);
        //        dt_mtaskDB.Columns["Task_Desc"].SetOrdinal(2);
        //        dt_mtaskDB.Columns["Employee_Name"].SetOrdinal(3);
        //        dt_mtaskDB.Columns["Task_Deadline"].SetOrdinal(4);
        //        dt_mtaskDB.Columns["Task_Insert_Name"].SetOrdinal(5);
        //        dt_mtaskDB.Columns["Task_InsertOn"].SetOrdinal(6);
        //        dt_mtaskDB.Columns["Task_Update_Name"].SetOrdinal(7);
        //        dt_mtaskDB.Columns["Task_UpdateOn"].SetOrdinal(8);

        //        dt_mtaskDB.AcceptChanges();

        //        XLWorkbook wbook = new XLWorkbook();
        //        var wr = wbook.Worksheets.Add(dt_mtaskDB, "Sheet1");
        //        wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
        //        wr.Tables.FirstOrDefault().ShowAutoFilter = false;

        //        // Prepare the response
        //        HttpResponseBase httpResponse = Response;
        //        httpResponse.Clear();
        //        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        //Provide you file name here
        //        httpResponse.AddHeader("content-disposition", "attachment;filename=\"Task.xlsx\"");

        //        // Flush the workbook to the Response.OutputStream
        //        using (MemoryStream memoryStream = new MemoryStream())
        //        {
        //            wbook.SaveAs(memoryStream);
        //            memoryStream.WriteTo(httpResponse.OutputStream);
        //            memoryStream.Close();
        //        }

        //        httpResponse.End();

        //        return View("");
        //    }
        //    else
        //    {
        //        return Redirect("/");
        //    }
        //}
    }
}