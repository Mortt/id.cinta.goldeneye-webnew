﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace b2bWeb.Controllers
{
    public class TargetController : Controller
    {
        TargetDB TargetDB = new TargetDB();
        OutletDB otlDB = new OutletDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / Outlet Target";
                ViewBag.Outlet = otlDB.ListAll() ?? new List<Outlet>();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataTarget()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = TargetDB.GetDataTarget(param) ?? new List<JsonTarget>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonTarget Baru = new JsonTarget();
                Baru.total = 0;
                Baru.rows = new List<rowsTarget>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUp(Target data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Target_ID == null)
                    {
                        try
                        {
                            data.Target_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Target_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (TargetDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    TargetDB.AddUpdate(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.Target_UpdateBy = Convert.ToString(Session["Username"]);

                            if (TargetDB.AddUpdate(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return RedirectToAction("Index", "Target");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Target data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {               
                    try
                    {
                        data.Target_UpdateBy = Convert.ToString(Session["Username"]);
                        TargetDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_itmDB = JsonConvert.SerializeObject(TargetDB.ListAll());
                DataTable dt_itmDB = (DataTable)JsonConvert.DeserializeObject(json_itmDB, (typeof(DataTable)));

                if (dt_itmDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Target");
                }

                dt_itmDB.Columns.Add("Target_Periode1", typeof(string));
                dt_itmDB.Columns.Add("Target_InsertOn1", typeof(string));
                dt_itmDB.Columns.Add("Target_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["Target_Periode1"] = Convert.ToDateTime(objCon["Target_Periode"]).ToString("dd/MM/yyy");
                    objCon["Target_InsertOn1"] = Convert.ToString(objCon["Target_InsertOn"]);
                    objCon["Target_UpdateOn1"] = Convert.ToString(objCon["Target_UpdateOn"]);
                }

                dt_itmDB.Columns.Remove("Target_ID");
                dt_itmDB.Columns.Remove("Target_UpdateOn");
                dt_itmDB.Columns.Remove("Target_InsertOn");
                dt_itmDB.Columns.Remove("Target_Periode");
                dt_itmDB.Columns.Remove("jml");
                dt_itmDB.Columns.Remove("Target_CompanyID");

                dt_itmDB.Columns.Add("Target_InsertOn", typeof(string));
                dt_itmDB.Columns.Add("Target_UpdateOn", typeof(string));
                dt_itmDB.Columns.Add("Target_Periode", typeof(string));

                for (int a = 0; a < dt_itmDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_itmDB.Rows[a]["Target_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_itmDB.Rows[a]["Target_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_itmDB.Rows[a]["Target_UpdateOn"] = dt_itmDB.Rows[a]["Target_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["Target_InsertOn"] = Convert.ToString(objCon["Target_InsertOn1"]);
                    objCon["Target_Periode"] = Convert.ToDateTime(objCon["Target_Periode1"]).ToString("dd/yyyy");
                }

                dt_itmDB.Columns.Remove("Target_InsertOn1");
                dt_itmDB.Columns.Remove("Target_Periode1");
                dt_itmDB.Columns.Remove("Target_UpdateOn1");

                dt_itmDB.AcceptChanges();

                dt_itmDB.Columns["Target_OutletCode"].SetOrdinal(0);
                dt_itmDB.Columns["Target_OutletName"].SetOrdinal(1);
                dt_itmDB.Columns["Target_Amount"].SetOrdinal(2);
                dt_itmDB.Columns["Target_Periode"].SetOrdinal(3);
                dt_itmDB.Columns["Target_IsActive"].SetOrdinal(4);
                dt_itmDB.Columns["Target_InsertOn"].SetOrdinal(5);
                dt_itmDB.Columns["Target_InsertBy"].SetOrdinal(6);
                dt_itmDB.Columns["Target_UpdateOn"].SetOrdinal(7);
                dt_itmDB.Columns["Target_UpdateBy"].SetOrdinal(8);

                dt_itmDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_itmDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Outlet_Target.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();

                return RedirectToAction("index", "target");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Outlet_Code", "Amount", "Periode" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Outlet_Code") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Periode1", typeof(DateTime));
                                            dt.Columns.Add("Target_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Target_InsertBy", typeof(string));
                                            dt.Columns.Add("Target_CompanyID", typeof(int));
                                            dt.Columns.Add("Target_IsActive", typeof(bool));
                                            dt.Columns.Add("Target_Isdelete", typeof(bool));


                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Periode"] != null)
                                                {
                                                    dt.Rows[a]["Periode1"] = Convert.ToDateTime(dt.Rows[a]["Periode"]).ToString("MM/yyyy");
                                                }

                                                dt.Rows[a]["Target_InsertOn"] = today;
                                                dt.Rows[a]["Target_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Target_CompanyID"] = Session["CompanyID"];
                                                dt.Rows[a]["Target_IsActive"] = true;
                                                dt.Rows[a]["Target_Isdelete"] = false;
                                            }

                                            dt.Columns.Remove("Periode");
                                            dt.Columns.Add("Periode", typeof(DateTime));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Periode1"] != null)
                                                {
                                                    dt.Rows[a]["Periode"] = dt.Rows[a]["Periode1"];
                                                }
                                            }

                                            dt.Columns.Remove("Periode1");

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_TargetOutlet (Target_Periode date, Target_OutletCode nvarchar(30), Target_Amount decimal(18, 2), Target_CompanyID int, Target_InsertBy nvarchar(MAX), Target_InsertOn datetime, Target_IsActive bit, Target_Isdelete bit)", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_TargetOutlet";

                                                    sqlBulkCopy.ColumnMappings.Add("Periode", "Target_Periode");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Code", "Target_OutletCode");
                                                    sqlBulkCopy.ColumnMappings.Add("Amount", "Target_Amount");
                                                    sqlBulkCopy.ColumnMappings.Add("Target_CompanyID", "Target_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Target_InsertBy", "Target_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("Target_InsertOn", "Target_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Target_IsActive", "Target_IsActive");
                                                    sqlBulkCopy.ColumnMappings.Add("Target_Isdelete", "Target_Isdelete");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_TargetOutlet t2 " + Environment.NewLine;
                                                mapping += " USING #M_TargetOutlet t1 ON (t2.[Target_Periode] = t1.[Target_Periode] AND" + Environment.NewLine;
                                                mapping += " t2.[Target_OutletCode] = t1.[Target_OutletCode] AND t2.[Target_CompanyID] = t1.[Target_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Target_Periode=t1.Target_Periode , t2.Target_Amount=t1.Target_Amount, t2.Target_UpdateOn=t1.Target_InsertOn, t2.Target_UpdateBy=t1.Target_InsertBy, t2.Target_Isdelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Target_Periode], [Target_OutletCode], [Target_Amount], [Target_CompanyID], [Target_InsertBy], [Target_InsertOn], [Target_IsActive], [Target_Isdelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Target_Periode], t1.[Target_OutletCode], t1.[Target_Amount], t1.[Target_CompanyID], t1.[Target_InsertBy], t1.[Target_InsertOn], t1.[Target_IsActive], t1.[Target_Isdelete]); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_TargetOutlet] ([Target_Periode], [Target_OutletCode], [Target_Amount], [Target_CompanyID], [Target_InsertBy], [Target_InsertOn], [Target_IsActive], [Target_Isdelete])" +
                                                //                   " SELECT t1.[Target_Periode], t1.[Target_OutletCode], t1.[Target_Amount], t1.[Target_CompanyID], t1.[Target_InsertBy], t1.[Target_InsertOn], t1.[Target_IsActive], t1.[Target_Isdelete] FROM #M_TargetOutlet AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_TargetOutlet AS t2 WHERE t2.[Target_Periode] = t1.[Target_Periode] AND t2.[Target_OutletCode] = t1.[Target_OutletCode] AND t2.[Target_CompanyID] = t1.[Target_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_TargetOutlet";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }                    
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Outlet_Code", typeof(string));
            table.Columns.Add("Outlet_Name", typeof(string));
            table.Columns.Add("Amount", typeof(string));
            table.Columns.Add("Periode", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("OJKT001", "HQ Cinta.ID", "17000000", "Jun-19");
            table.Rows.Add("3-1000036", "PT. Gondowangi Traditional Kos", "10000000", "Aug-19");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Outlet_Target.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "target");
        }
    }
}