﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class CompetitorController : Controller
    {
        CompetitorDB CompActDB = new CompetitorDB();

        public ActionResult Index(string competitor_desc, string pilih, string date, int? page)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Competitor";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataCompetitor()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = CompActDB.GetDataCompetitor(param) ?? new List<JsonCompetitor>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonCompetitor Baru = new JsonCompetitor();
                Baru.total = 0;
                Baru.rows = new List<rowsCompetitor>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }
       
        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            try
            {
                var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                string name = "Competitor(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                var Competitor = CompActDB.ListExport(g, c);

                var json_CompActDB = JsonConvert.SerializeObject(Competitor);
                DataTable dt_CompActDB = (DataTable)JsonConvert.DeserializeObject(json_CompActDB, (typeof(DataTable)));

                if (dt_CompActDB.Rows.Count == 0)
                {
                    TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                    return RedirectToAction("index", "Competitor");
                }

                dt_CompActDB.Columns.Add("CompActivity_InsertOn1", typeof(string));

                foreach (DataRow objCon in dt_CompActDB.Rows)
                {
                    objCon["CompActivity_InsertOn1"] = Convert.ToString(objCon["CompActivity_InsertOn"]);
                }

                dt_CompActDB.Columns.Remove("CompActivity_isActive");
                dt_CompActDB.Columns.Remove("CompActivity_UpdateOn");
                dt_CompActDB.Columns.Remove("CompActivity_UpdateBy");
                //dt_CompActDB.Columns.Remove("CompActivity_Image");
                dt_CompActDB.Columns.Remove("CompActivity_InsertOn");
                dt_CompActDB.Columns.Remove("CompActivity_OutletCode");

                dt_CompActDB.Columns.Add("CompActivity_InsertOn", typeof(string));
                dt_CompActDB.Columns.Add("CompActivity_ProductName", typeof(string));
                dt_CompActDB.Columns.Add("CompActivity_InsertByName", typeof(string));


                foreach (DataRow objCon in dt_CompActDB.Rows)
                {
                    objCon["CompActivity_InsertOn"] = Convert.ToString(objCon["CompActivity_InsertOn1"]);
                    objCon["CompActivity_ProductName"] = Convert.ToString(objCon["Item_Name"]);
                    objCon["CompActivity_InsertByName"] = Convert.ToString(objCon["Nama_SPG"]);
                }

                dt_CompActDB.Columns.Remove("CompActivity_InsertOn1");
                dt_CompActDB.Columns.Remove("Item_Name");
                dt_CompActDB.Columns.Remove("Nama_SPG");
                dt_CompActDB.Columns.Remove("CompActivity_ID");

                dt_CompActDB.AcceptChanges();

                //dt_CompActDB.DefaultView.Sort = "CompActivity_ID DESC";
                //dt_CompActDB = dt_CompActDB.DefaultView.ToTable();

                dt_CompActDB.Columns["CompActivity_ProductCode"].SetOrdinal(0);
                dt_CompActDB.Columns["CompActivity_ProductName"].SetOrdinal(1);
                dt_CompActDB.Columns["CompActivity_Desc"].SetOrdinal(2);
                dt_CompActDB.Columns["CompActivity_Image"].SetOrdinal(3);
                dt_CompActDB.Columns["CompActivity_InsertBy"].SetOrdinal(4);
                dt_CompActDB.Columns["CompActivity_InsertByName"].SetOrdinal(5);
                dt_CompActDB.Columns["CompActivity_InsertOn"].SetOrdinal(6);
                dt_CompActDB.Columns["CompActivity_Isdelete"].SetOrdinal(7);

                dt_CompActDB.AcceptChanges();

                if (image == false)
                {
                    TempData["DT_JSON"] = dt_CompActDB;
                    return RedirectToAction("ToExcel", "Dashboard", new { name });
                }
                else
                {
                    dt_CompActDB.AcceptChanges();

                    for (int a = 0; a < dt_CompActDB.Rows.Count; a++)
                    {
                        string url = Convert.ToString(dt_CompActDB.Rows[a]["CompActivity_Image"]);
                        string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                        dt_CompActDB.Rows[a]["CompActivity_Image"] = fileName;
                    }

                    TempData["DT_JSON"] = dt_CompActDB;

                    int row = 1;
                    int column = 4;
                    string TableImageName = "CompActivity_Image";
                    return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return RedirectToAction("Error", "Dashboard");
            }
        }
        
    }
}