﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using System.Data;

namespace b2bWeb.Controllers
{
    public class AbsentController : Controller
    {
        AbsentDB AbsentDB = new AbsentDB();
        AttendanceDurationDB AttendanceDurationDB = new AttendanceDurationDB();
        public ActionResult Absent()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (Convert.ToInt32(Session["CompanyID"]) == 1)
                {
                    return RedirectToAction("index", "absentday");
                }

                try
                {
                    string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                    if (DashboardController.Paket(actionName) == true)
                    {

                    }
                    else
                    {
                        return RedirectToAction("Error", "Dashboard");
                    }

                    ViewBag.HMenu = DashboardController.HeaderMenu();
                    ViewBag.Title = "Report / Attendance";
                    return View();

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataAttendanceDuration()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;
            param.Get_Type = "";

            var Result = AttendanceDurationDB.GetDataAttendanceDuration(param) ?? new List<JsonAttendanceDuration>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonAttendanceDuration Baru = new JsonAttendanceDuration();
                Baru.total = 0;
                Baru.rows = new List<rowsAttendanceDuration>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExportToExcel(DateTime StartDate, DateTime EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(StartDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(EndDate.ToString("MM/dd/yyyy"));

                string S = "";
                DataTable A = AbsentDB.Absent(g, c, S);

                A.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(A, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Attendance( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + ").xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }
        
    }
}