﻿using ClosedXML.Excel;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b2bWeb.Controllers
{
    public class AbsentDayController : Controller
    {
        AbsentDB AbsentDB = new AbsentDB();
        AttendanceDurationDB AttendanceDurationDB = new AttendanceDurationDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                    //if (DashboardController.Paket(actionName) == true)
                    //{

                    //}
                    //else
                    //{
                    //    return RedirectToAction("Error", "Dashboard");
                    //}

                    ViewBag.HMenu = DashboardController.HeaderMenu();
                    ViewBag.Title = "Report / Attendance";
                    return View();

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataAttendanceDays()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;
            param.Get_Type = "Day";


            var Result = AttendanceDurationDB.GetDataAttendanceDuration(param) ?? new List<JsonAttendanceDuration>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonAttendanceDuration Baru = new JsonAttendanceDuration();
                Baru.total = 0;
                Baru.rows = new List<rowsAttendanceDuration>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel(DateTime StartDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(StartDate.ToString("MM/dd/yyyy"));

                DataTable A = AbsentDB.AbsentDay(g);

                A.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(A, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Attendance( " + g.ToString("MMMM yyyy")+").xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }
    }    
}