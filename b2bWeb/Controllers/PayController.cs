﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;

namespace b2bWeb.Controllers
{
    public class PayController : Controller
    {
        PayDB payDB = new PayDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Billing";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataPay()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = payDB.GetDataPay(param) ?? new List<JsonPay>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonPay Baru = new JsonPay();
                Baru.total = 0;
                Baru.rows = new List<rowsPay>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }
    }
}