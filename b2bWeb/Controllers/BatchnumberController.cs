﻿using ClosedXML.Excel;
using MvcPaging;
using Newtonsoft.Json;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace b2bWeb.Controllers
{
    public class BatchnumberController : Controller
    {
        ItemDB itemDB = new ItemDB();
        M_Batch_NumberDB M_Batch_NumberDB = new M_Batch_NumberDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / BatchNumber";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Item = itemDB.ListAll() ?? new List<Item>();
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataBatchnumber()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = M_Batch_NumberDB.GetDataBatch_Number(param) ?? new List<JsonM_Batch_Number>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonM_Batch_Number Baru = new JsonM_Batch_Number();
                Baru.total = 0;
                Baru.rows = new List<rowsM_Batch_Number>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddUp(M_Batch_Number data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.BN_id == null)
                    {
                        try
                        {
                            data.BN_CompanyID = Convert.ToInt32(Session["CompanyID"]);
                            data.BN_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                M_Batch_NumberDB.AddUd(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);

                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.BN_UpdateBy = Convert.ToString(Session["Username"]);

                            if (M_Batch_NumberDB.AddUd(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(M_Batch_Number data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.BN_UpdateBy = Convert.ToString(Session["Username"]);
                    M_Batch_NumberDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var BatchNumber = M_Batch_NumberDB.ListAll();

                var json_BatchNumber = JsonConvert.SerializeObject(BatchNumber);
                DataTable dt_json_BatchNumber = (DataTable)JsonConvert.DeserializeObject(json_BatchNumber, (typeof(DataTable)));

                if (dt_json_BatchNumber.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Batchnumber");
                }

                dt_json_BatchNumber.Columns.Add("BN_InsertOn1", typeof(string));
                dt_json_BatchNumber.Columns.Add("BN_UpdateOn1", typeof(string));
                dt_json_BatchNumber.Columns.Add("Brand", typeof(string));

                foreach (DataRow objCon in dt_json_BatchNumber.Rows)
                {
                    objCon["BN_InsertOn1"] = Convert.ToString(objCon["BN_InsertOn"]);
                    objCon["BN_UpdateOn1"] = Convert.ToString(objCon["BN_UpdateOn"]);
                    objCon["Brand"] = Convert.ToString(objCon["Category_Name"]);
                }

                dt_json_BatchNumber.Columns.Remove("BN_InsertOn");
                dt_json_BatchNumber.Columns.Remove("BN_UpdateOn");
                dt_json_BatchNumber.Columns.Remove("BN_CompanyID");
                dt_json_BatchNumber.Columns.Remove("BN_InsertBy");
                dt_json_BatchNumber.Columns.Remove("BN_UpdateBy");
                dt_json_BatchNumber.Columns.Remove("BN_id");
                dt_json_BatchNumber.Columns.Remove("BN_Item_Code");
                dt_json_BatchNumber.Columns.Remove("Category_Name");

                dt_json_BatchNumber.Columns.Add("BN_InsertOn", typeof(string));
                dt_json_BatchNumber.Columns.Add("BN_UpdateOn", typeof(string));
                dt_json_BatchNumber.Columns.Add("BN_InsertBy", typeof(string));
                dt_json_BatchNumber.Columns.Add("BN_UpdateBy", typeof(string));

                for (int a = 0; a < dt_json_BatchNumber.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_json_BatchNumber.Rows[a]["BN_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_json_BatchNumber.Rows[a]["BN_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_json_BatchNumber.Rows[a]["BN_UpdateOn"] = dt_json_BatchNumber.Rows[a]["BN_UpdateOn1"];
                    }

                    dt_json_BatchNumber.Rows[a]["BN_InsertOn"] = dt_json_BatchNumber.Rows[a]["BN_InsertOn1"];
                    dt_json_BatchNumber.Rows[a]["BN_InsertBy"] = dt_json_BatchNumber.Rows[a]["Insert_Name"];
                    dt_json_BatchNumber.Rows[a]["BN_UpdateBy"] = dt_json_BatchNumber.Rows[a]["Update_Name"];
                }

                dt_json_BatchNumber.Columns.Remove("BN_InsertOn1");
                dt_json_BatchNumber.Columns.Remove("BN_UpdateOn1");
                dt_json_BatchNumber.Columns.Remove("BN_IsActive");
                dt_json_BatchNumber.Columns.Remove("BN_IsDelete");
                dt_json_BatchNumber.Columns.Remove("Insert_Name");
                dt_json_BatchNumber.Columns.Remove("Update_Name");

                dt_json_BatchNumber.AcceptChanges();

                dt_json_BatchNumber.Columns["Brand"].SetOrdinal(0);
                dt_json_BatchNumber.Columns["Product_Name"].SetOrdinal(1);
                dt_json_BatchNumber.Columns["BN_BatchNumber"].SetOrdinal(2);
                dt_json_BatchNumber.Columns["BN_Exp_Date"].SetOrdinal(3);
                dt_json_BatchNumber.Columns["BN_InsertOn"].SetOrdinal(4);
                dt_json_BatchNumber.Columns["BN_InsertBy"].SetOrdinal(5);
                dt_json_BatchNumber.Columns["BN_UpdateOn"].SetOrdinal(6);
                dt_json_BatchNumber.Columns["BN_UpdateBy"].SetOrdinal(7);

                dt_json_BatchNumber.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_json_BatchNumber, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"BatchNumber.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();

                return RedirectToAction("index", "batchnumber");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int j = 0; j < Request.Files.Count; j++)
                {
                    HttpPostedFileBase postedFile = Request.Files[j];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Product_Code", "Product_Name", "BatchNumber", "Exp_Date" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            DataTable dt = dtNew.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull || string.IsNullOrWhiteSpace(field as string))).CopyToDataTable();

                                            dt.AcceptChanges();

                                            dt.Columns.Add("BN_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("BN_InsertBy", typeof(string));
                                            dt.Columns.Add("BN_CompanyID", typeof(int));
                                            dt.Columns.Add("BN_IsActive", typeof(int));
                                            dt.Columns.Add("BN_IsDelete", typeof(int));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                dt.Rows[a]["BN_InsertOn"] = today;
                                                dt.Rows[a]["BN_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["BN_CompanyID"] = Session["CompanyID"];
                                                dt.Rows[a]["BN_IsActive"] = 1;
                                                dt.Rows[a]["BN_IsDelete"] = 0;
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Batch_Number (BN_Item_Code nvarchar(30), BN_BatchNumber nvarchar(100), BN_Exp_Date datetime, BN_CompanyID int, BN_InsertOn datetime, BN_InsertBy nvarchar(30), BN_IsActive bit, BN_IsDelete bit)", con);
                                                command.ExecuteNonQuery();

                                                var cmd = new SqlCommand("SELECT * FROM M_Item WHERE Item_CompanyID = " + Convert.ToInt32(Session["CompanyID"]) + "", con);
                                                DataTable dtItem = new DataTable();
                                                dtItem.Load(cmd.ExecuteReader());

                                                for (int i = 0; i < dt.Rows.Count; i++)
                                                {
                                                    DataRow[] foundCodeItem = dtItem.Select("Item_Code = '" + Convert.ToString(dt.Rows[i]["Product_Code"]) + "'");
                                                    if (foundCodeItem.Length > 0)
                                                    {
                                                    }
                                                    else
                                                    {
                                                        TempData["Error"] = "Maaf, Product_Code " + dt.Rows[i]["Product_Code"] + "tidak terdaftar pada sistem";
                                                        return RedirectToAction("Import", "Batchnumber");
                                                    }
                                                }

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Batch_Number";

                                                    sqlBulkCopy.ColumnMappings.Add("Product_Code", "BN_Item_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("BatchNumber", "BN_BatchNumber");
                                                    sqlBulkCopy.ColumnMappings.Add("Exp_Date", "BN_Exp_Date");
                                                    sqlBulkCopy.ColumnMappings.Add("BN_CompanyID", "BN_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("BN_InsertOn", "BN_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("BN_InsertBy", "BN_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("BN_IsActive", "BN_IsActive");
                                                    sqlBulkCopy.ColumnMappings.Add("BN_IsDelete", "BN_IsDelete");
                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Batch_Number t2 " + Environment.NewLine;
                                                mapping += " USING #M_Batch_Number t1 ON (CAST(t2.[BN_Exp_Date] AS DATE) = t1.[BN_Exp_Date] AND" + Environment.NewLine;
                                                mapping += " t2.[BN_Item_Code] = t1.[BN_Item_Code] AND t2.[BN_CompanyID] = t1.[BN_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.BN_Item_Code=t1.BN_Item_Code, t2.BN_BatchNumber=t1.BN_BatchNumber, t2.BN_UpdateOn=t1.BN_InsertOn, t2.BN_UpdateBy=t1.BN_InsertBy, t2.BN_IsDelete = t1.BN_IsDelete" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([BN_Item_Code], [BN_BatchNumber], [BN_Exp_Date], [BN_CompanyID], [BN_InsertOn], [BN_InsertBy], [BN_IsActive], [BN_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[BN_Item_Code], t1.[BN_BatchNumber], t1.[BN_Exp_Date], t1.[BN_CompanyID], t1.[BN_InsertOn], t1.[BN_InsertBy], t1.[BN_IsActive], t1.[BN_IsDelete]); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Batch_Number";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //ex.Message.ToString();
                            //return Json(ex.Message.ToString(), JsonRequestBehavior.AllowGet);
                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Product_Code", typeof(string));
            table.Columns.Add("Product_Name", typeof(string));
            table.Columns.Add("BatchNumber", typeof(string));
            table.Columns.Add("Exp_Date", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("PKT001", "PERMEN ASAM", "ASD123443", "11/1/2020");
            table.Rows.Add("CKT001", "COKLAT MANIS", "ASD123445", "11/7/2020");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_BatchNumber.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Batchnumber");
        }
    }
}