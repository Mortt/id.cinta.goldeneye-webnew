﻿using ClosedXML.Excel;
using MvcPaging;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace b2bWeb.Controllers
{
    public class GroupOutletController : Controller
    {
        GroupOutletDB gpDB = new GroupOutletDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Outlet Group";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataGroupOutlet()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = gpDB.GetDataGroupOutlet(param) ?? new List<JsonGroupOutlet>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonGroupOutlet Baru = new JsonGroupOutlet();
                Baru.total = 0;
                Baru.rows = new List<rowsGroupOutlet>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddUp(GroupOutlet data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.GroupOutlet_ID == null)
                    {
                        try
                        {
                            data.GroupOutlet_CompanyID = Convert.ToString(Session["CompanyID"]);
                            data.GroupOutlet_InsertBy = Convert.ToString(Session["Username"]);

                            if(gpDB.Cek(data) != 0)
                            {
                                return Json("has", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                gpDB.AddUpdate(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }                            
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.GroupOutlet_UpdateBy = Convert.ToString(Session["Username"]);

                            if (gpDB.AddUpdate(data))
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }                
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(GroupOutlet data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.GroupOutlet_UpdateBy = Convert.ToString(Session["Username"]);
                    if (gpDB.Delete(data))
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json("failed", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_goDB = JsonConvert.SerializeObject(gpDB.ListAll());
                DataTable dt_goDB = (DataTable)JsonConvert.DeserializeObject(json_goDB, (typeof(DataTable)));

                if (dt_goDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "GroupOutlet");
                }

                dt_goDB.Columns.Add("GroupOutlet_InsertOn1", typeof(string));
                dt_goDB.Columns.Add("GroupOutlet_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_goDB.Rows)
                {
                    objCon["GroupOutlet_InsertOn1"] = Convert.ToString(objCon["GroupOutlet_InsertOn"]);
                    objCon["GroupOutlet_UpdateOn1"] = Convert.ToString(objCon["GroupOutlet_UpdateOn"]);
                }

                dt_goDB.Columns.Remove("GroupOutlet_InsertOn");
                dt_goDB.Columns.Remove("GroupOutlet_UpdateOn");
                dt_goDB.Columns.Remove("jml");
                dt_goDB.Columns.Remove("GroupOutlet_CompanyID");

                dt_goDB.Columns.Add("GroupOutlet_InsertOn", typeof(string));
                dt_goDB.Columns.Add("GroupOutlet_UpdateOn", typeof(string));
                dt_goDB.Columns.Add("GroupOutlet_InsertByName", typeof(string));
                dt_goDB.Columns.Add("GroupOutlet_UpdateByName", typeof(string));

                for (int a = 0; a < dt_goDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_goDB.Rows[a]["GroupOutlet_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_goDB.Rows[a]["GroupOutlet_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_goDB.Rows[a]["GroupOutlet_UpdateOn"] = dt_goDB.Rows[a]["GroupOutlet_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_goDB.Rows)
                {
                    objCon["GroupOutlet_InsertOn"] = Convert.ToString(objCon["GroupOutlet_InsertOn1"]);
                    objCon["GroupOutlet_InsertByName"] = Convert.ToString(objCon["GroupOutlet_InsertBy_Name"]);
                    objCon["GroupOutlet_UpdateByName"] = Convert.ToString(objCon["GroupOutlet_UpdateBy_Name"]);
                }

                dt_goDB.Columns.Remove("GroupOutlet_InsertOn1");
                dt_goDB.Columns.Remove("GroupOutlet_UpdateOn1");
                dt_goDB.Columns.Remove("GroupOutlet_InsertBy_Name");
                dt_goDB.Columns.Remove("GroupOutlet_UpdateBy_Name");
                dt_goDB.Columns.Remove("GroupOutlet_isDelete");

                dt_goDB.AcceptChanges();

                dt_goDB.Columns["GroupOutlet_ID"].SetOrdinal(0);
                dt_goDB.Columns["GroupOutlet_Code"].SetOrdinal(1);
                dt_goDB.Columns["GroupOutlet_Name"].SetOrdinal(2);
                dt_goDB.Columns["GroupOutlet_Desc"].SetOrdinal(3);
                dt_goDB.Columns["GroupOutlet_InsertBy"].SetOrdinal(4);
                dt_goDB.Columns["GroupOutlet_InsertByName"].SetOrdinal(5);
                dt_goDB.Columns["GroupOutlet_InsertOn"].SetOrdinal(6);
                dt_goDB.Columns["GroupOutlet_UpdateBy"].SetOrdinal(7);
                dt_goDB.Columns["GroupOutlet_UpdateByName"].SetOrdinal(8);
                dt_goDB.Columns["GroupOutlet_UpdateOn"].SetOrdinal(9);
                dt_goDB.Columns["GroupOutlet_IsActive"].SetOrdinal(10);

                dt_goDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_goDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"OutletGroup.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();
                return RedirectToAction("index", "GroupOutlet");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("GroupOutlet_Code", typeof(string));
            table.Columns.Add("GroupOutlet_Name", typeof(string));
            table.Columns.Add("GroupOutlet_Desc", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("CINTA2", "GROUP CINTA2", "GROUP CINTA yang ke 2");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_GroupOutlet.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "GroupOutlet");
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "GroupOutlet_Code", "GroupOutlet_Name", "GroupOutlet_Desc" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("GroupOutlet_Code") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("GroupOutlet_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("GroupOutlet_InsertBy", typeof(string));
                                            dt.Columns.Add("GroupOutlet_CompanyID", typeof(int));
                                            dt.Columns.Add("GroupOutlet_Code1", typeof(string));
                                            dt.Columns.Add("GroupOutlet_Name1", typeof(string));
                                            dt.Columns.Add("GroupOutlet_Desc1", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {

                                                dt.Rows[a]["GroupOutlet_Code1"] = Convert.ToString(dt.Rows[a]["GroupOutlet_Code"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["GroupOutlet_Name1"] = Convert.ToString(dt.Rows[a]["GroupOutlet_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["GroupOutlet_Desc1"] = Convert.ToString(dt.Rows[a]["GroupOutlet_Desc"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["GroupOutlet_InsertOn"] = today;
                                                dt.Rows[a]["GroupOutlet_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["GroupOutlet_CompanyID"] = Session["CompanyID"];
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_GroupOutlet (GroupOutlet_Code nvarchar(30), GroupOutlet_Name nvarchar(MAX), GroupOutlet_Desc nvarchar(30), GroupOutlet_CompanyID int, GroupOutlet_InsertOn datetime, GroupOutlet_InsertBy nvarchar(30))", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_GroupOutlet";

                                                    sqlBulkCopy.ColumnMappings.Add("GroupOutlet_Code1", "GroupOutlet_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("GroupOutlet_Name1", "GroupOutlet_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("GroupOutlet_Desc1", "GroupOutlet_Desc");
                                                    sqlBulkCopy.ColumnMappings.Add("GroupOutlet_CompanyID", "GroupOutlet_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("GroupOutlet_InsertOn", "GroupOutlet_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("GroupOutlet_InsertBy", "GroupOutlet_InsertBy");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_GroupOutlet t2 " + Environment.NewLine;
                                                mapping += " USING #M_GroupOutlet t1 ON (t2.[GroupOutlet_Code] = t1.[GroupOutlet_Code] AND" + Environment.NewLine;
                                                mapping += " t2.[GroupOutlet_CompanyID] = t1.[GroupOutlet_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.GroupOutlet_Name=t1.GroupOutlet_Name, t2.GroupOutlet_Desc=t1.GroupOutlet_Desc, t2.GroupOutlet_UpdateOn=t1.GroupOutlet_InsertOn, t2.GroupOutlet_UpdateBy=t1.GroupOutlet_InsertBy, t2.GroupOutlet_isDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([GroupOutlet_Code], [GroupOutlet_Name], [GroupOutlet_Desc], [GroupOutlet_CompanyID], [GroupOutlet_InsertOn], [GroupOutlet_InsertBy], [GroupOutlet_isActive], [GroupOutlet_isDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[GroupOutlet_Code], t1.[GroupOutlet_Name], t1.[GroupOutlet_Desc], t1.[GroupOutlet_CompanyID], t1.[GroupOutlet_InsertOn], t1.[GroupOutlet_InsertBy], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_GroupOutlet";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}