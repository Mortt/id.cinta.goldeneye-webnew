﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using System.Data;
using ClosedXML.Excel;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace b2bWeb.Controllers
{
    public class CollectionController : Controller
    {
        CollectionDB collectionDB = new CollectionDB();
        EmployeeDB employeeDB = new EmployeeDB();
        OutletDB otlDB = new OutletDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Employee = employeeDB.ListAll();
                ViewBag.Outlet = otlDB.ListAll();
                ViewBag.Title = "Transaction / Collection";
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataCollection()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = collectionDB.GetDataCollection(param) ?? new List<JsonCollection>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonCollection Baru = new JsonCollection();
                Baru.total = 0;
                Baru.rows = new List<rowsCollection>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUp(Collection ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (ID.TransID == null)
                    {
                        try
                        {
                            ID.Inv_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            ID.Inv_InsertBy = Convert.ToString(Session["Username"]);

                            if(CollectionDB.Cek(ID) > 0)
                            {
                                return Json("has", JsonRequestBehavior.AllowGet);
                            }

                            if (CollectionDB.AddUpdate(ID))
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            ID.Inv_UpdateBy = Convert.ToString(Session["Username"]);

                            if (CollectionDB.AddUpdate(ID))
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }
               
        public ActionResult Delete(Collection data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Inv_UpdateBy = Convert.ToString(Session["Username"]);

                    if (CollectionDB.Delete(data))
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Collection(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    DataTable Collection = collectionDB.ListExport(g, c);
                    
                    if (Collection.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "collection");
                    }

                    Collection.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = Collection;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        Collection.AcceptChanges();

                        for (int a = 0; a < Collection.Rows.Count; a++)
                        {
                            string url = Convert.ToString(Collection.Rows[a]["Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            Collection.Rows[a]["Image"] = fileName;
                        }

                        TempData["DT_JSON"] = Collection;

                        int row = 1;
                        int column = 9;
                        string TableImageName = "Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Invoice_No", typeof(string));
            table.Columns.Add("Due_Date", typeof(string));
            table.Columns.Add("Outlet_Code", typeof(string));
            table.Columns.Add("Outlet_Name", typeof(string));
            table.Columns.Add("Employee_NIK", typeof(string));
            table.Columns.Add("Employee_Name", typeof(string));
            table.Columns.Add("Amount", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("1234", "10/31/2019", "3-1000143", "kedai bossque", "083824271074", "Nursidik", "5000000");
            table.Rows.Add("1235", "10/30/2019", "3-1000143", "kedai bossque", "083824271074", "Nursidik", "500000");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Collection.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (var memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
                //return File(memoryStream, "application/vnd.ms-excel", "Template_Area.xlsx");
            }
            httpResponse.End();
            return RedirectToAction("Index", "Collection");
            //return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];

                    ViewBag.HMenu = DashboardController.HeaderMenu();
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);
                                            connExcel.Close();
                                        }

                                        string[] selectedColumns = new[] { "Invoice_No", "Due_Date", "Outlet_Code", "Employee_NIK", "Amount" };
                                        DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                        var query = from myRow in dtNew.AsEnumerable() select myRow;

                                        DataTable dt = query.CopyToDataTable();

                                        dt.Columns.Add("Inv_CompanyID");
                                        dt.Columns.Add("Inv_InsertOn");
                                        dt.Columns.Add("Inv_InsertBy");
                                        dt.Columns.Add("Inv_IsDelete");
                                        dt.Columns.Add("Inv_IsActive");                                        
                                            
                                        for (int u = 0; u < dt.Rows.Count; u++)
                                        {
                                            dt.Rows[u]["Inv_CompanyID"] = Session["CompanyID"];
                                            dt.Rows[u]["Inv_InsertOn"] = today;
                                            dt.Rows[u]["Inv_InsertBy"] = Session["Username"];
                                            dt.Rows[u]["Inv_IsDelete"] = "FALSE";
                                            dt.Rows[u]["Inv_IsActive"] = "TRUE";
                                        }

                                        dt.AcceptChanges();
                                        
                                        var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                        using (SqlConnection con = new SqlConnection(conStringDB))
                                        {
                                            con.Open();
                                            
                                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                            {
                                                sqlBulkCopy.DestinationTableName = "Tr_Collection";

                                                sqlBulkCopy.ColumnMappings.Add("Invoice_No", "Inv_No");
                                                sqlBulkCopy.ColumnMappings.Add("Amount", "Inv_Amount");
                                                sqlBulkCopy.ColumnMappings.Add("Due_Date", "Inv_DueDate");
                                                sqlBulkCopy.ColumnMappings.Add("Outlet_Code", "Inv_OutletCode");
                                                sqlBulkCopy.ColumnMappings.Add("Employee_NIK", "Inv_PIC");
                                                sqlBulkCopy.ColumnMappings.Add("Inv_CompanyID", "Inv_CompanyID");
                                                sqlBulkCopy.ColumnMappings.Add("Inv_InsertOn", "Inv_InsertOn");
                                                sqlBulkCopy.ColumnMappings.Add("Inv_InsertBy", "Inv_InsertBy");
                                                sqlBulkCopy.ColumnMappings.Add("Inv_IsDelete", "Inv_IsDelete");
                                                sqlBulkCopy.ColumnMappings.Add("Inv_IsActive", "Inv_IsActive");

                                                sqlBulkCopy.WriteToServer(dt);
                                            }

                                            //get Tr_Collection
                                            var command = new SqlCommand("select top " + dt.Rows.Count + " * from Tr_Collection order by TransID desc", con);
                                            DataTable TableHasil = new DataTable();
                                            TableHasil.Load(command.ExecuteReader());

                                            for (int h = 0; h < TableHasil.Rows.Count; h++)
                                            {
                                                for (int x = 0; x < dt.Rows.Count; x++)
                                                {
                                                    //update TransCode
                                                    var com = new SqlCommand("UPDATE Tr_Collection SET TransCode = dbo.Encrypt2String(" + TableHasil.Rows[h]["TransID"] + ") WHERE TransID = " + TableHasil.Rows[h]["TransID"] + "", con);
                                                    com.ExecuteNonQuery();
                                                }
                                            }

                                            con.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();

                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}