﻿
using Newtonsoft.Json;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace b2bWeb.Controllers
{
    public class ReceiveController : Controller
    {
        RecieveDB recieveDB = new RecieveDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Receive";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataReceive()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = recieveDB.GetDataReceive(param) ?? new List<JsonRecieve>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonRecieve Baru = new JsonRecieve();
                Baru.total = 0;
                Baru.rows = new List<rowsRecieve>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Receive(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    var Recieve = recieveDB.ListExport(g, c);

                    var json_recieveDB = JsonConvert.SerializeObject(Recieve);
                    DataTable dt_recieveDB = (DataTable)JsonConvert.DeserializeObject(json_recieveDB, (typeof(DataTable)));

                    if (dt_recieveDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Recieve");
                    }

                    dt_recieveDB.Columns.Add("Recieve_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_recieveDB.Rows)
                    {
                        objCon["Recieve_InsertOn1"] = Convert.ToString(objCon["Recieve_InsertOn"]);
                    }

                    dt_recieveDB.Columns.Remove("Recieve_Suggestion");
                    dt_recieveDB.Columns.Remove("Recieve_InsertBy");
                    //dt_recieveDB.Columns.Remove("Recieve_ProductCode");
                    dt_recieveDB.Columns.Remove("Recieve_No");
                    dt_recieveDB.Columns.Remove("Recieve_Delta");
                    dt_recieveDB.Columns.Remove("Recieve_UpdateBy");
                    dt_recieveDB.Columns.Remove("Recieve_UpdateOn");
                    dt_recieveDB.Columns.Remove("Recieve_CompanyID");
                    dt_recieveDB.Columns.Remove("Recieve_ClosedBy");
                    dt_recieveDB.Columns.Remove("Recieve_ClosedOn");
                    dt_recieveDB.Columns.Remove("Recieve_isClosed");
                    //dt_recieveDB.Columns.Remove("Recieve_ImageTTD");
                    //dt_recieveDB.Columns.Remove("Recieve_Image2");
                    //dt_recieveDB.Columns.Remove("Recieve_Image");
                    dt_recieveDB.Columns.Remove("Recieve_InsertOn");

                    dt_recieveDB.Columns.Add("Recieve_InsertOn", typeof(string));

                    foreach (DataRow objCon in dt_recieveDB.Rows)
                    {
                        objCon["Recieve_InsertOn"] = Convert.ToString(objCon["Recieve_InsertOn1"]);
                    }

                    dt_recieveDB.Columns.Remove("Recieve_InsertOn1");
                    dt_recieveDB.Columns.Remove("Recieve_ID");

                    dt_recieveDB.AcceptChanges();

                    dt_recieveDB.Columns.Add("Receive_Employee_Name", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_OutletCode", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Outlet_Name", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Image", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_ImageTTD", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Image2", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_ProductCode", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Product_Name", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Product_Image", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Stock", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Desc", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Amount", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_InsertOn", typeof(string));
                    dt_recieveDB.Columns.Add("Receive_Isdelete", typeof(string));

                    foreach (DataRow objCon in dt_recieveDB.Rows)
                    {
                        objCon["Receive_Employee_Name"] = Convert.ToString(objCon["Recieve_Employee_Name"]);
                        objCon["Receive_OutletCode"] = Convert.ToString(objCon["Recieve_OutletCode"]);
                        objCon["Receive_Outlet_Name"] = Convert.ToString(objCon["Recieve_Outlet_Name"]);
                        objCon["Receive_Image"] = Convert.ToString(objCon["Recieve_Image"]);
                        objCon["Receive_ImageTTD"] = Convert.ToString(objCon["Recieve_ImageTTD"]);
                        objCon["Receive_Image2"] = Convert.ToString(objCon["Recieve_Image2"]);
                        objCon["Receive_ProductCode"] = Convert.ToString(objCon["Recieve_ProductCode"]);
                        objCon["Receive_Product_Name"] = Convert.ToString(objCon["Recieve_Product_Name"]);
                        objCon["Receive_Product_Image"] = Convert.ToString(objCon["Recieve_Product_Image"]);
                        objCon["Receive_Stock"] = Convert.ToString(objCon["Recieve_Stock"]);
                        objCon["Receive_Desc"] = Convert.ToString(objCon["Recieve_Desc"]);
                        objCon["Receive_Amount"] = Convert.ToString(objCon["Recieve_Amount"]);
                        objCon["Receive_InsertOn"] = Convert.ToString(objCon["Recieve_InsertOn"]);
                        objCon["Receive_Isdelete"] = Convert.ToString(objCon["Recieve_Isdelete"]);
                    }

                    dt_recieveDB.Columns.Remove("Recieve_Employee_Name");
                    dt_recieveDB.Columns.Remove("Recieve_OutletCode");
                    dt_recieveDB.Columns.Remove("Recieve_Outlet_Name");
                    dt_recieveDB.Columns.Remove("Recieve_Image");
                    dt_recieveDB.Columns.Remove("Recieve_ImageTTD");
                    dt_recieveDB.Columns.Remove("Recieve_Image2");
                    dt_recieveDB.Columns.Remove("Recieve_ProductCode");
                    dt_recieveDB.Columns.Remove("Recieve_Product_Name");
                    dt_recieveDB.Columns.Remove("Recieve_Product_Image");
                    dt_recieveDB.Columns.Remove("Recieve_Stock");
                    dt_recieveDB.Columns.Remove("Recieve_Desc");
                    dt_recieveDB.Columns.Remove("Recieve_Amount");
                    dt_recieveDB.Columns.Remove("Recieve_InsertOn");
                    dt_recieveDB.Columns.Remove("Recieve_Isdelete");

                    dt_recieveDB.Columns["Receive_Employee_Name"].SetOrdinal(0);
                    dt_recieveDB.Columns["Receive_OutletCode"].SetOrdinal(1);
                    dt_recieveDB.Columns["Receive_Outlet_Name"].SetOrdinal(2);
                    dt_recieveDB.Columns["Receive_Image"].SetOrdinal(3);
                    dt_recieveDB.Columns["Receive_ImageTTD"].SetOrdinal(4);
                    dt_recieveDB.Columns["Receive_Image2"].SetOrdinal(5);
                    dt_recieveDB.Columns["Receive_ProductCode"].SetOrdinal(6);
                    dt_recieveDB.Columns["Receive_Product_Name"].SetOrdinal(7);
                    dt_recieveDB.Columns["Receive_Product_Image"].SetOrdinal(8);
                    dt_recieveDB.Columns["Receive_Stock"].SetOrdinal(9);
                    dt_recieveDB.Columns["Receive_Desc"].SetOrdinal(10);
                    dt_recieveDB.Columns["Receive_Amount"].SetOrdinal(11);
                    dt_recieveDB.Columns["Receive_InsertOn"].SetOrdinal(12);
                    dt_recieveDB.Columns["Receive_Isdelete"].SetOrdinal(13);

                    dt_recieveDB.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_recieveDB;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        dt_recieveDB.Columns.Remove("Receive_ImageTTD");
                        dt_recieveDB.Columns.Remove("Receive_Image2");
                        dt_recieveDB.Columns.Remove("Receive_Product_Image");

                        dt_recieveDB.AcceptChanges();

                        for (int a = 0; a < dt_recieveDB.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_recieveDB.Rows[a]["Receive_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_recieveDB.Rows[a]["Receive_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_recieveDB;

                        int row = 1;
                        int column = 4;
                        string TableImageName = "Receive_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}