﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace b2bWeb.Controllers
{
    public class RouteController : Controller
    {
        AreaDB Area = new AreaDB();
        RouteDB rotDB = new RouteDB();
        EmployeeDB eDB = new EmployeeDB();
        RecurringDB RecurringDB = new RecurringDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Area = Area.ListAll() ?? new List<Area>();                
                ViewBag.Recurring = RecurringDB.ListAll() ?? new List<Recurring>();
                ViewBag.Employee = eDB.ListAll() ?? new List<Employee>();

                ViewBag.Title = "Master / Route";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataRoute()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = rotDB.GetDataRoute(param) ?? new List<JsonRoute>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonRoute Baru = new JsonRoute();
                Baru.total = 0;
                Baru.rows = new List<rowsRoute>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult AddUpd(Route data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Route_ID == null)
                    {
                        try
                        {
                            data.Route_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Route_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (rotDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    rotDB.AddUpdate(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failes", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.Route_UpdateBy = Convert.ToString(Session["Username"]);

                            if (rotDB.AddUpdate(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }                    
                }
                return RedirectToAction("index", "Route");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Route data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Route_UpdateBy = Convert.ToString(Session["Username"]);
                    rotDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_rotDB = JsonConvert.SerializeObject(rotDB.ListAll());
                DataTable dt_rotDB = (DataTable)JsonConvert.DeserializeObject(json_rotDB, (typeof(DataTable)));

                if (dt_rotDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Route");
                }

                dt_rotDB.Columns.Add("Route_InsertOn1", typeof(string));
                dt_rotDB.Columns.Add("Route_UpdateOn1", typeof(string));
                dt_rotDB.Columns.Add("Route_SuggestionTime1", typeof(string));

                foreach (DataRow objCon in dt_rotDB.Rows)
                {
                    objCon["Route_InsertOn1"] = Convert.ToString(objCon["Route_InsertOn"]);
                    objCon["Route_UpdateOn1"] = Convert.ToString(objCon["Route_UpdateOn"]);
                    objCon["Route_SuggestionTime1"] = Convert.ToDateTime(objCon["Route_SuggestionTime"]).ToString("HH:mm:ss");
                }

                dt_rotDB.Columns.Remove("Route_InsertOn");
                dt_rotDB.Columns.Remove("Route_UpdateOn");
                dt_rotDB.Columns.Remove("jml");
                dt_rotDB.Columns.Remove("Route_CompanyID");
                dt_rotDB.Columns.Remove("Route_SuggestionTime");
                dt_rotDB.Columns.Remove("Route_ID");
                dt_rotDB.Columns.Remove("Route_isActive");

                dt_rotDB.Columns.Add("Route_InsertOn", typeof(string));
                dt_rotDB.Columns.Add("Route_UpdateOn", typeof(string));
                dt_rotDB.Columns.Add("Route_EmployeeName", typeof(string));
                dt_rotDB.Columns.Add("Route_AreaName", typeof(string));
                dt_rotDB.Columns.Add("Route_InsertByName", typeof(string));
                dt_rotDB.Columns.Add("Route_UpdateByName", typeof(string));
                dt_rotDB.Columns.Add("Route_SuggestionTime", typeof(string));
                dt_rotDB.Columns.Add("Route_Type1", typeof(string));

                for (int a = 0; a < dt_rotDB.Rows.Count; a++)
                {
                    if(Convert.ToString(dt_rotDB.Rows[a]["Route_Type"]) == "True")
                    {
                        string AA = "Area";
                        dt_rotDB.Rows[a]["Route_Type1"] = AA;
                    }
                    else
                    {
                        string AA = "OUTLET";
                        dt_rotDB.Rows[a]["Route_Type1"] = AA;
                    }

                    if (Convert.ToString(dt_rotDB.Rows[a]["Route_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_rotDB.Rows[a]["Route_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_rotDB.Rows[a]["Route_UpdateOn"] = dt_rotDB.Rows[a]["Route_UpdateOn1"];
                    }
                }

                dt_rotDB.Columns.Remove("Route_Type");
                dt_rotDB.Columns.Add("Route_Type", typeof(string));

                for (int a = 0; a < dt_rotDB.Rows.Count; a++)
                {
                    dt_rotDB.Rows[a]["Route_Type"] = dt_rotDB.Rows[a]["Route_Type1"];
                }


                foreach (DataRow objCon in dt_rotDB.Rows)
                {
                    objCon["Route_InsertOn"] = Convert.ToString(objCon["Route_InsertOn1"]);
                    objCon["Route_EmployeeName"] = Convert.ToString(objCon["Nama_SPG"]);
                    objCon["Route_AreaName"] = Convert.ToString(objCon["Area_Nama"]);
                    objCon["Route_InsertByName"] = Convert.ToString(objCon["Insert_Oleh"]);
                    objCon["Route_UpdateByName"] = Convert.ToString(objCon["Update_Oleh"]);
                    objCon["Route_SuggestionTime"] = Convert.ToDateTime(objCon["Route_SuggestionTime1"]).ToString("HH:mm:ss");
                }

                dt_rotDB.Columns.Remove("Route_Type1");
                dt_rotDB.Columns.Remove("Route_InsertOn1");
                dt_rotDB.Columns.Remove("Route_UpdateOn1");
                dt_rotDB.Columns.Remove("Nama_SPG");
                dt_rotDB.Columns.Remove("Area_Nama");
                dt_rotDB.Columns.Remove("Insert_Oleh");
                dt_rotDB.Columns.Remove("Update_Oleh");
                dt_rotDB.Columns.Remove("Route_SuggestionTime1");

                dt_rotDB.AcceptChanges();
                dt_rotDB.Columns.Remove("Route_Day");
                dt_rotDB.Columns.Remove("Repeat_Name");
                dt_rotDB.Columns.Remove("Route_Repeat");

                dt_rotDB.Columns["Route_EmployeeNik"].SetOrdinal(0);
                dt_rotDB.Columns["Route_EmployeeName"].SetOrdinal(1);
                dt_rotDB.Columns["Route_Queque"].SetOrdinal(2);
                //dt_rotDB.Columns["Route_Day"].SetOrdinal(3);
                dt_rotDB.Columns["Route_SuggestionTime"].SetOrdinal(3);
                dt_rotDB.Columns["Route_AreaID"].SetOrdinal(4);
                dt_rotDB.Columns["Route_AreaName"].SetOrdinal(5);
                dt_rotDB.Columns["Route_Type"].SetOrdinal(6);
                dt_rotDB.Columns["Route_InsertBy"].SetOrdinal(7);
                dt_rotDB.Columns["Route_InsertByName"].SetOrdinal(8);
                dt_rotDB.Columns["Route_InsertOn"].SetOrdinal(9);
                dt_rotDB.Columns["Route_UpdateBy"].SetOrdinal(10);
                dt_rotDB.Columns["Route_UpdateByName"].SetOrdinal(11);
                dt_rotDB.Columns["Route_UpdateOn"].SetOrdinal(12);
                //dt_rotDB.Columns["Route_isActive"].SetOrdinal(14);
                
                dt_rotDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_rotDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Route.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();
                return RedirectToAction("index", "route");
            }
            else
            {
                return Redirect("/");
            }
        }
        
        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];

                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Route_EmployeeNik", "Route_EmployeeName", "Route_Queque", "Route_Day", "Route_SuggestionTime", "Route_AreaID", "Route_AreaName", "Route_Type" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Route_EmployeeNik") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Route_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Route_InsertBy", typeof(string));
                                            dt.Columns.Add("Route_CompanyID", typeof(int));
                                            dt.Columns.Add("Route_Type1", typeof(int));
                                            dt.Columns.Add("Route_SuggestionTime1", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                dt.Rows[a]["Route_InsertOn"] = today;
                                                dt.Rows[a]["Route_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Route_CompanyID"] = Session["CompanyID"];
                                                dt.Rows[a]["Route_SuggestionTime1"] = Convert.ToDateTime(dt.Rows[a]["Route_SuggestionTime"]).ToString("HH:mm:ss");

                                                if (Convert.ToString(dt.Rows[a]["Route_Type"]).ToLower() == "area")
                                                {
                                                    dt.Rows[a]["Route_Type1"] = 1;
                                                }
                                                else
                                                {
                                                    dt.Rows[a]["Route_Type1"] = 0;
                                                }
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Route (Route_EmployeeNik nvarchar(30), Route_Queque int, Route_Day int, Route_SuggestionTime time(7), Route_AreaID int, Route_InsertBy nvarchar(50), Route_InsertOn datetime, Route_CompanyID int, Route_Type bit)", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Route";

                                                    sqlBulkCopy.ColumnMappings.Add("Route_EmployeeNik", "Route_EmployeeNik");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_Queque", "Route_Queque");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_Day", "Route_Day");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_SuggestionTime1", "Route_SuggestionTime");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_AreaID", "Route_AreaID");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_InsertBy", "Route_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_InsertOn", "Route_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_CompanyID", "Route_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Route_Type1", "Route_Type");
                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Route t2 " + Environment.NewLine;
                                                mapping += " USING #M_Route t1 ON (t2.[Route_EmployeeNik] = t1.[Route_EmployeeNik] AND" + Environment.NewLine;
                                                mapping += " t2.[Route_AreaID] = t1.[Route_AreaID] AND t2.[Route_CompanyID] = t1.[Route_CompanyID] AND t2.[Route_Type] = t1.[Route_Type]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine; 
                                                mapping += " UPDATE SET t2.Route_Queque=t1.Route_Queque, t2.Route_Day=t1.Route_Day, t2.Route_SuggestionTime=t1.Route_SuggestionTime, t2.Route_UpdateOn=t1.Route_InsertOn, t2.Route_UpdateBy=t1.Route_InsertBy, t2.Route_Isdelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Route_EmployeeNik], [Route_Queque], [Route_Day], [Route_SuggestionTime], [Route_AreaID], [Route_CompanyID], [Route_InsertOn], [Route_InsertBy], [Route_Type], [Route_IsActive], [Route_Isdelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Route_EmployeeNik], t1.[Route_Queque], t1.[Route_Day], t1.[Route_SuggestionTime], t1.[Route_AreaID], t1.[Route_CompanyID], t1.[Route_InsertOn], t1.[Route_InsertBy], t1.[Route_Type], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Route] ([Route_EmployeeNik], [Route_Queque], [Route_Day], [Route_SuggestionTime], [Route_AreaID], [Route_CompanyID], [Route_InsertOn], [Route_InsertBy], [Route_Type])" +
                                                //                   " SELECT t1.[Route_EmployeeNik], t1.[Route_Queque], t1.[Route_Day], t1.[Route_SuggestionTime], t1.[Route_AreaID], t1.[Route_CompanyID], t1.[Route_InsertOn], t1.[Route_InsertBy], t1.[Route_Type] FROM #M_Route AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Route AS t2 WHERE t2.[Route_EmployeeNik] = t1.[Route_EmployeeNik] AND t2.[Route_AreaID] = t1.[Route_AreaID] AND t2.[Route_CompanyID] = t1.[Route_CompanyID] AND t2.[Route_Type] = t1.[Route_Type])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Route";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {                            
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }                    
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }
        
        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Route_EmployeeNik", typeof(string));
            table.Columns.Add("Route_EmployeeName", typeof(string));
            table.Columns.Add("Route_Queque", typeof(string));
            table.Columns.Add("Route_Day", typeof(string));
            table.Columns.Add("Route_SuggestionTime", typeof(string));
            table.Columns.Add("Route_AreaID", typeof(string));
            table.Columns.Add("Route_AreaName", typeof(string));
            table.Columns.Add("Route_Type", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("081289858986", "Rico P Situmeang", "1", "1", "08:00:00", "450", "JAKARTA", "Area");
            table.Rows.Add("081289858986", "Rico P Situmeang", "1", "1", "08:00:00", "450", "JAKARTA", "OUTLET");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Route.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Route");
        }
    }
}