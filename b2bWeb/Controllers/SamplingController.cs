﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class SamplingController : Controller
    {
        SamplingDB samDB = new SamplingDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Sampling";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataSampling()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = samDB.GetDataSampling(param) ?? new List<JsonSampling>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonSampling Baru = new JsonSampling();
                Baru.total = 0;
                Baru.rows = new List<rowsSampling>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Sampling(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    var Sampling = samDB.ListExport(g, c);

                    var json_samDB = JsonConvert.SerializeObject(Sampling);
                    DataTable dt_samDB = (DataTable)JsonConvert.DeserializeObject(json_samDB, (typeof(DataTable)));

                    if (dt_samDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Sampling");
                    }

                    dt_samDB.Columns.Add("Sampling_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_samDB.Rows)
                    {
                        objCon["Sampling_InsertOn1"] = Convert.ToString(objCon["Sampling_InsertOn"]);
                    }

                    //dt_samDB.Columns.Remove("Sampling_Image");
                    dt_samDB.Columns.Remove("Sampling_UpdateBy");
                    dt_samDB.Columns.Remove("Sampling_UpdateOn");
                    dt_samDB.Columns.Remove("Nama_Outlet");
                    dt_samDB.Columns.Remove("Nama_Category");
                    dt_samDB.Columns.Remove("Sampling_InsertOn");
                    dt_samDB.Columns.Remove("Sampling_CategoryCode");
                    dt_samDB.Columns.Remove("Sampling_OutletCode");

                    dt_samDB.Columns.Add("Sampling_InsertOn", typeof(string));
                    dt_samDB.Columns.Add("Sampling_ProductName", typeof(string));
                    dt_samDB.Columns.Add("Sampling_RespondName", typeof(string));
                    dt_samDB.Columns.Add("Sampling_InsertByName", typeof(string));

                    foreach (DataRow objCon in dt_samDB.Rows)
                    {
                        objCon["Sampling_InsertOn"] = Convert.ToString(objCon["Sampling_InsertOn1"]);
                        objCon["Sampling_ProductName"] = Convert.ToString(objCon["Nama_Item"]);
                        objCon["Sampling_RespondName"] = Convert.ToString(objCon["Nama_Respond"]);
                        objCon["Sampling_InsertByName"] = Convert.ToString(objCon["Nama_SPG"]);
                    }

                    dt_samDB.Columns.Remove("Sampling_InsertOn1");
                    dt_samDB.Columns.Remove("Nama_Item");
                    dt_samDB.Columns.Remove("Nama_Respond");
                    dt_samDB.Columns.Remove("Nama_SPG");
                    dt_samDB.Columns.Remove("Sampling_ID");

                    dt_samDB.AcceptChanges();

                    dt_samDB.Columns["Sampling_ProductCode"].SetOrdinal(0);
                    dt_samDB.Columns["Sampling_ProductName"].SetOrdinal(1);
                    dt_samDB.Columns["Sampling_QTY"].SetOrdinal(2);
                    dt_samDB.Columns["Sampling_Desc"].SetOrdinal(3);
                    dt_samDB.Columns["Sampling_Image"].SetOrdinal(4);
                    dt_samDB.Columns["Sampling_RespondID"].SetOrdinal(5);
                    dt_samDB.Columns["Sampling_RespondName"].SetOrdinal(6);
                    dt_samDB.Columns["Sampling_InsertBy"].SetOrdinal(7);
                    dt_samDB.Columns["Sampling_InsertByName"].SetOrdinal(8);
                    dt_samDB.Columns["Sampling_InsertOn"].SetOrdinal(9);
                    dt_samDB.Columns["Sampling_Isdelete"].SetOrdinal(10);

                    dt_samDB.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_samDB;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {                       
                        dt_samDB.AcceptChanges();

                        for (int a = 0; a < dt_samDB.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_samDB.Rows[a]["Sampling_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_samDB.Rows[a]["Sampling_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_samDB;

                        int row = 1;
                        int column = 5;
                        string TableImageName = "Sampling_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}