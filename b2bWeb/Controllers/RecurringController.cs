﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using System.Data;


namespace b2bWeb.Controllers
{
    public class RecurringController : Controller
    {
        RecurringDB RecurringDB = new RecurringDB();
        public static string Day(string day)
        {
            var t = string.Join(",",
                 from g in day.Split(new char[] { ',' })
                 select Enum.GetName(typeof(DayOfWeek), Convert.ToInt32(g)));

            return t;
        }

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / Recurring";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataRecurring()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = RecurringDB.GetDataRecurring(param) ?? new List<JsonRecurring>();

            if (Result.Count != 0)
            {
                for(int i = 0; i< Result[0].total; i++)
                {
                    if (Result[0].rows[i].Repeat_Type == "Weekly")
                    {
                        string a = Result[0].rows[i].Repeat_On;
                        Result[0].rows[i].Repeat_On = Day(a);
                    }
            }
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonRecurring Baru = new JsonRecurring();
                Baru.total = 0;
                Baru.rows = new List<rowsRecurring>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUp(Recurring R)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (R.Repeat_IDX == null)
                    {
                        try
                        {
                            if (R.RepeatOn_1 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_1);
                            }
                            if (R.RepeatOn_2 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_2);
                            }
                            if (R.RepeatOn_3 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_3);
                            }
                            if (R.RepeatOn_4 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_4);
                            }
                            if (R.RepeatOn_5 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_5);
                            }
                            if (R.RepeatOn_6 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_6);
                            }
                            if (R.RepeatOn_7 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_7);
                            }
                            if (R.RepeatOn_8 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_8);
                            }
                            if (R.RepeatOn_9 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_9);
                            }
                            if (R.RepeatOn_10 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_10);
                            }
                            if (R.RepeatOn_11 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_11);
                            }
                            if (R.RepeatOn_12 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_12);
                            }
                            if (R.RepeatOn_13 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_13);
                            }
                            if (R.RepeatOn_14 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_14);
                            }
                            if (R.RepeatOn_15 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_15);
                            }
                            if (R.RepeatOn_16 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_16);
                            }
                            if (R.RepeatOn_17 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_17);
                            }
                            if (R.RepeatOn_18 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_18);
                            }
                            if (R.RepeatOn_19 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_19);
                            }
                            if (R.RepeatOn_20 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_20);
                            }
                            if (R.RepeatOn_21 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_21);
                            }
                            if (R.RepeatOn_22 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_22);
                            }
                            if (R.RepeatOn_23 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_23);
                            }
                            if (R.RepeatOn_24 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_24);
                            }
                            if (R.RepeatOn_24 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_25);
                            }
                            if (R.RepeatOn_26 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_26);
                            }
                            if (R.RepeatOn_27 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_27);
                            }
                            if (R.RepeatOn_28 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_28);
                            }
                            if (R.RepeatOn_29 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_29);
                            }
                            if (R.RepeatOn_30 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_30);
                            }
                            if (R.RepeatOn_31 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_31);
                            }

                            if (R.Repeat_On != null)
                            {
                                if (R.Repeat_On.StartsWith(","))
                                {
                                    R.Repeat_On = Convert.ToString(R.Repeat_On).Substring(1);
                                }
                            }

                            if (R.Repeat_Every1 != null)
                            {
                                R.Repeat_Every = Convert.ToInt32(R.Repeat_Every1);
                            }

                            if (R.Repeat_Every2 != null)
                            {
                                R.Repeat_Every = Convert.ToInt32(R.Repeat_Every2);
                            }

                            if (R.Repeat_Every3 != null)
                            {
                                R.Repeat_Every = Convert.ToInt32(R.Repeat_Every3);
                            }

                            R.Repeat_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            R.Repeat_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (RecurringDB.AddUpdate(R))
                                {
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json("failed", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (R.RepeatOn_1 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_1);
                            }
                            if (R.RepeatOn_2 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_2);
                            }
                            if (R.RepeatOn_3 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_3);
                            }
                            if (R.RepeatOn_4 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_4);
                            }
                            if (R.RepeatOn_5 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_5);
                            }
                            if (R.RepeatOn_6 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_6);
                            }
                            if (R.RepeatOn_7 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_7);
                            }
                            if (R.RepeatOn_8 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_8);
                            }
                            if (R.RepeatOn_9 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_9);
                            }
                            if (R.RepeatOn_10 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_10);
                            }
                            if (R.RepeatOn_11 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_11);
                            }
                            if (R.RepeatOn_12 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_12);
                            }
                            if (R.RepeatOn_13 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_13);
                            }
                            if (R.RepeatOn_14 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_14);
                            }
                            if (R.RepeatOn_15 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_15);
                            }
                            if (R.RepeatOn_16 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_16);
                            }
                            if (R.RepeatOn_17 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_17);
                            }
                            if (R.RepeatOn_18 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_18);
                            }
                            if (R.RepeatOn_19 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_19);
                            }
                            if (R.RepeatOn_20 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_20);
                            }
                            if (R.RepeatOn_21 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_21);
                            }
                            if (R.RepeatOn_22 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_22);
                            }
                            if (R.RepeatOn_23 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_23);
                            }
                            if (R.RepeatOn_24 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_24);
                            }
                            if (R.RepeatOn_24 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_25);
                            }
                            if (R.RepeatOn_26 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_26);
                            }
                            if (R.RepeatOn_27 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_27);
                            }
                            if (R.RepeatOn_28 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_28);
                            }
                            if (R.RepeatOn_29 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_29);
                            }
                            if (R.RepeatOn_30 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_30);
                            }
                            if (R.RepeatOn_31 != null)
                            {
                                R.Repeat_On = Convert.ToString(R.Repeat_On) + "," + Convert.ToString(R.RepeatOn_31);
                            }

                            if (R.Repeat_On != null)
                            {
                                if (R.Repeat_On.StartsWith(","))
                                {
                                    R.Repeat_On = Convert.ToString(R.Repeat_On).Substring(1);
                                    R.Repeat_On = Convert.ToString(R.Repeat_On).Replace(",,", ",");
                                }
                            }

                            if (R.Repeat_Every1 != null)
                            {
                                R.Repeat_Every = Convert.ToInt32(R.Repeat_Every1);
                            }

                            if (R.Repeat_Every2 != null)
                            {
                                R.Repeat_Every = Convert.ToInt32(R.Repeat_Every2);
                            }

                            if (R.Repeat_Every3 != null)
                            {
                                R.Repeat_Every = Convert.ToInt32(R.Repeat_Every3);
                            }

                            R.Repeat_UpdateBy = Convert.ToString(Session["Username"]);

                            if (RecurringDB.AddUpdate(R))
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }                            
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        //delete 
        public ActionResult Delete(Recurring data)
        {
            if (Convert.ToString(Session["Username"]) != "")
            {
                data.Repeat_UpdateBy = Convert.ToString(Session["Username"]);
                if (RecurringDB.Delete(data))
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}