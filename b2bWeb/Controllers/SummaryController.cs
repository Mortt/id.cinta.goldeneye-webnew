﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web.Security;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace b2bWeb.Controllers
{
    public class SummaryController : Controller
    {
        SummaryDB SummaryDB = new SummaryDB();
        public ActionResult Employee(string StartDate, string EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                try
                {
                    string Month = DateTime.Now.Month.ToString();
                    string Year = DateTime.Now.Year.ToString();
                    string all = Month + "/1/" + Year;

                    ViewBag.Title = "Report / Summary Employee";

                    DateTime a = Convert.ToDateTime(all);
                    DateTime b = DateTime.Now;
                    
                    DataTable A = new DataTable();

                    if (Convert.ToString(StartDate) == null && Convert.ToString(EndDate) == null)
                    {
                        A = SummaryDB.Employee(a, b);                        
                        ViewData["StartDate"] = a;
                        ViewData["EndDate"] = b;
                    }
                    else
                    {
                        A = SummaryDB.Employee(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate));
                        ViewData["StartDate"] = StartDate;
                        ViewData["EndDate"] = EndDate;
                    }

                    return View(A);        

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcelEmployee(DateTime StartDate, DateTime EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(StartDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(EndDate.ToString("MM/dd/yyyy"));

                DataTable A = SummaryDB.Employee(g, c);

                A.AcceptChanges();

                if (A.Rows.Count < 1)
                {
                    return RedirectToAction("employee", "summary");
                }

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(A, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Summary Employee( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Outlet(string StartDate, string EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                try
                {
                    string Month = DateTime.Now.Month.ToString();
                    string Year = DateTime.Now.Year.ToString();
                    string all = Month + "/1/" + Year;
                    ViewBag.Title = "Report / Summary Outelt";
                    DateTime a = Convert.ToDateTime(all);
                    DateTime b = DateTime.Now;

                    DataTable A = new DataTable();
                    
                    if (Convert.ToString(StartDate) == null && Convert.ToString(EndDate) == null)
                    {
                        A = SummaryDB.Outlet(a, b);
                        //ViewBag.data = A;
                        ViewData["StartDate"] = a;
                        ViewData["EndDate"] = b;
                    }
                    else
                    {
                        A = SummaryDB.Outlet(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate));
                        ViewData["StartDate"] = StartDate;
                        ViewData["EndDate"] = EndDate;
                    }

                    return View(A);

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcelOutlet(DateTime StartDate, DateTime EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(StartDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(EndDate.ToString("MM/dd/yyyy"));

                DataTable A = SummaryDB.Outlet(g, c);

                A.AcceptChanges();

                if (A.Rows.Count < 1)
                {
                    return RedirectToAction("outlet", "summary");
                }

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(A, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Summary Outlet( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Product(string StartDate, string EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                try
                {
                    string Month = DateTime.Now.Month.ToString();
                    string Year = DateTime.Now.Year.ToString();
                    string all = Month + "/1/" + Year;
                    ViewBag.Title = "Report / Summary Product";
                    DateTime a = Convert.ToDateTime(all);
                    DateTime b = DateTime.Now;

                    DataTable A = new DataTable();
                    
                    if (Convert.ToString(StartDate) == null && Convert.ToString(EndDate) == null)
                    {
                        A = SummaryDB.Product(a, b);
                        //ViewBag.data = A;
                        ViewData["StartDate"] = a;
                        ViewData["EndDate"] = b;
                    }
                    else
                    {
                        A = SummaryDB.Product(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate));
                        ViewData["StartDate"] = StartDate;
                        ViewData["EndDate"] = EndDate;
                    }

                    return View(A);

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcelProduct(DateTime StartDate, DateTime EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(StartDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(EndDate.ToString("MM/dd/yyyy"));

                DataTable A = SummaryDB.Product(g, c);

                A.AcceptChanges();

                if (A.Rows.Count < 1)
                {
                    return RedirectToAction("product", "summary");
                }

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(A, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Summary Product( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}