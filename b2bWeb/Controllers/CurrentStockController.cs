﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class CurrentStockController : Controller
    {
        CurrentStockDB CurrentStockDB = new CurrentStockDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                //if (Convert.ToInt64(Session["CompanyID"]) == 2)
                //{
                //    return RedirectToAction("index", "stocktrinity");
                //}

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Report / Current Stock";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataCurrentStock()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = CurrentStockDB.GetDataCurrentStock(param) ?? new List<JsonCurrentStock>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonCompetitor Baru = new JsonCompetitor();
                Baru.total = 0;
                Baru.rows = new List<rowsCompetitor>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel(DateTime startDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    string name = "CurrentStock(" + startDate.ToString("MMMM/yyyy") + ")";

                    List<CurrentStock> CurrentStocks = new List<CurrentStock>();
                    CurrentStocks = CurrentStockDB.ListAll(startDate);
                    
                    var json = JsonConvert.SerializeObject(CurrentStocks);
                    DataTable dt_A = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                    if (dt_A.Rows.Count == 0)
                    {
                        TempData["Error"] = "data tidak ditemukan";
                        return RedirectToAction("index", "CurrentStock");
                    }

                    dt_A.Columns.Remove("Company_ID");
                    
                    dt_A.Columns.Add("Outlet_Code", typeof(string));
                    dt_A.Columns.Add("Item_Code", typeof(string));


                    for (int a = 0; a < dt_A.Rows.Count; a++)
                    {
                        dt_A.Rows[a]["Current_Stock"] = Convert.ToInt16(dt_A.Rows[a]["Opname_Stock"]) - Convert.ToInt16(dt_A.Rows[a]["H1_Sales_from_lastOpname"]);

                        dt_A.Rows[a]["Outlet_Code"] = Convert.ToString(dt_A.Rows[a]["Opname_OutletCode"]);
                        dt_A.Rows[a]["Item_Code"] = Convert.ToString(dt_A.Rows[a]["Opname_ProductCode"]);
                    }

                    dt_A.Columns.Remove("Opname_ProductCode");
                    dt_A.Columns.Remove("Opname_OutletCode");

                    dt_A.Columns["Opname_InsertON"].SetOrdinal(0);
                    dt_A.Columns["Outlet_Code"].SetOrdinal(1);
                    dt_A.Columns["Outlet_Name"].SetOrdinal(2);
                    dt_A.Columns["Item_Code"].SetOrdinal(3);
                    dt_A.Columns["Item_Name"].SetOrdinal(4);
                    dt_A.Columns["Opname_Stock"].SetOrdinal(5);
                    dt_A.Columns["H1_Sales_from_lastOpname"].SetOrdinal(6);
                    dt_A.Columns["Current_Stock"].SetOrdinal(7);
                    dt_A.Columns["STOCK_INSERTON"].SetOrdinal(8);

                    XLWorkbook wbook = new XLWorkbook();
                    var wr = wbook.Worksheets.Add(dt_A, "Sheet1");
                    wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                    wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                    // Prepare the response
                    HttpResponseBase httpResponse = Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Provide you file name here
                    httpResponse.AddHeader("content-disposition", "attachment;filename=\"" + name + " ).xlsx\"");

                    // Flush the workbook to the Response.OutputStream
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        wbook.SaveAs(memoryStream);
                        memoryStream.WriteTo(httpResponse.OutputStream);
                        memoryStream.Close();
                    }
                    httpResponse.End();
                    return View();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

    }
}