﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using Newtonsoft.Json;

namespace b2bWeb.Controllers
{
    public class AreaController : Controller
    {
        AutoCompleteDistrictDB acDB = new AutoCompleteDistrictDB();
        AreaDB arDB = new AreaDB();  

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                { }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Area";                
                ViewBag.HMenu = DashboardController.HeaderMenu();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataArea()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = arDB.GetDataArea(param) ?? new List<JsonArea>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonArea Baru = new JsonArea();
                Baru.total = 0;
                Baru.rows = new List<rowsArea>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ViewAddUpArea(decimal Lat, decimal Long)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                ViewBag.District = acDB.ListAllDistrict() ?? new List<AutoComplete>();
                Area data = new Area();

                data.Area_Lat = Lat;
                data.Area_Long = Long;
                return PartialView(data);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult AddUp(Area data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {

                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys where ModelState[x].Errors.Count > 0
                    select new
                    {
                        key = x,
                        errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                    };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Area_ID == null)
                    {
                        try
                        {
                            data.Area_PrincipalID = 1;
                            data.Area_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Area_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (arDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    arDB.AddUpd(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.Area_UpdateBy = Convert.ToString(Session["Username"]);

                            if (arDB.AddUpd(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }                

                return RedirectToAction("index", "Area");
            }
            else
            {
                return Redirect("/");
            }
        }
               
        public ActionResult Delete(Area data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Area_UpdateBy = Convert.ToString(Session["Username"]);
                    arDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_arDB = JsonConvert.SerializeObject(arDB.ListAll());
                DataTable dt_arDB = (DataTable)JsonConvert.DeserializeObject(json_arDB, (typeof(DataTable)));

                if (dt_arDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Area");
                }

                dt_arDB.Columns.Add("Area_InsertOn1", typeof(string));
                dt_arDB.Columns.Add("Area_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_arDB.Rows)
                {
                    objCon["Area_InsertOn1"] = Convert.ToString(objCon["Area_InsertOn"]);
                    objCon["Area_UpdateOn1"] = Convert.ToString(objCon["Area_UpdateOn"]);
                }

                dt_arDB.Columns.Remove("Area_InsertOn");
                dt_arDB.Columns.Remove("Area_UpdateOn");
                dt_arDB.Columns.Remove("jml");
                dt_arDB.Columns.Remove("Area_PrincipalID");
                dt_arDB.Columns.Remove("Area_CompanyID");
                dt_arDB.Columns.Remove("Area_Image");

                dt_arDB.Columns.Add("Area_InsertOn", typeof(string));
                dt_arDB.Columns.Add("Area_UpdateOn", typeof(string));
                dt_arDB.Columns.Add("Area_District_Name", typeof(string));
                dt_arDB.Columns.Add("Area_InsertByName", typeof(string));
                dt_arDB.Columns.Add("Area_UpdateByName", typeof(string));

                for (int a = 0; a < dt_arDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_arDB.Rows[a]["Area_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_arDB.Rows[a]["Area_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_arDB.Rows[a]["Area_UpdateOn"] = dt_arDB.Rows[a]["Area_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_arDB.Rows)
                {
                    objCon["Area_InsertOn"] = Convert.ToString(objCon["Area_InsertOn1"]);
                    objCon["Area_District_Name"] = Convert.ToString(objCon["District_Name"]);
                    objCon["Area_InsertByName"] = Convert.ToString(objCon["Insert_Oleh"]);
                    objCon["Area_UpdateByName"] = Convert.ToString(objCon["Update_Oleh"]);
                }

                dt_arDB.Columns.Remove("Area_InsertOn1");
                dt_arDB.Columns.Remove("Area_UpdateOn1");
                dt_arDB.Columns.Remove("District_Name");
                dt_arDB.Columns.Remove("Insert_Oleh");
                dt_arDB.Columns.Remove("Update_Oleh");

                dt_arDB.AcceptChanges();

                dt_arDB.DefaultView.Sort = "Area_ID desc";
                dt_arDB = dt_arDB.DefaultView.ToTable();

                dt_arDB.Columns["Area_ID"].SetOrdinal(0);
                dt_arDB.Columns["Area_Name"].SetOrdinal(1);
                dt_arDB.Columns["Area_Desc"].SetOrdinal(2);
                dt_arDB.Columns["Area_Long"].SetOrdinal(3);
                dt_arDB.Columns["Area_Lat"].SetOrdinal(4);
                dt_arDB.Columns["Area_isActive"].SetOrdinal(5);
                dt_arDB.Columns["Area_DistrictID"].SetOrdinal(6);
                dt_arDB.Columns["Area_District_Name"].SetOrdinal(7);
                dt_arDB.Columns["Area_InsertBy"].SetOrdinal(8);
                dt_arDB.Columns["Area_InsertByName"].SetOrdinal(9);
                dt_arDB.Columns["Area_InsertOn"].SetOrdinal(10);
                dt_arDB.Columns["Area_UpdateBy"].SetOrdinal(11);
                dt_arDB.Columns["Area_UpdateByName"].SetOrdinal(12);
                dt_arDB.Columns["Area_UpdateOn"].SetOrdinal(13);

                dt_arDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_arDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Area.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();

                return RedirectToAction("Index", "Area");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i]; 

                    ViewBag.HMenu = DashboardController.HeaderMenu();
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);
                                            connExcel.Close();
                                        }

                                        string[] selectedColumns = new[] { "Area_Name", "Area_Desc", "Area_Long", "Area_Lat", "Area_DistrictID" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Area_Name") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Area_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Area_InsertBy", typeof(string));
                                            dt.Columns.Add("Area_CompanyID", typeof(int));
                                            dt.Columns.Add("Area_PrincipalID", typeof(int));
                                            dt.Columns.Add("Area_Name1", typeof(string));
                                            dt.Columns.Add("Area_Desc1", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                dt.Rows[a]["Area_PrincipalID"] = 1;
                                                dt.Rows[a]["Area_Name1"] = Convert.ToString(dt.Rows[a]["Area_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Area_Desc1"] = Convert.ToString(dt.Rows[a]["Area_Desc"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Area_InsertOn"] = today;
                                                dt.Rows[a]["Area_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Area_CompanyID"] = Session["CompanyID"];
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Area (Area_PrincipalID int, Area_Name nvarchar(30), Area_Desc nvarchar(MAX), Area_Long decimal(10, 7), Area_Lat decimal(10, 7), Area_DistrictID bigint, Area_CompanyID int, Area_InsertOn datetime, Area_InsertBy nvarchar(30))", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Area";

                                                    sqlBulkCopy.ColumnMappings.Add("Area_Name1", "Area_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_Desc1", "Area_Desc");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_Long", "Area_Long");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_Lat", "Area_Lat");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_DistrictID", "Area_DistrictID");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_PrincipalID", "Area_PrincipalID");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_CompanyID", "Area_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_InsertOn", "Area_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Area_InsertBy", "Area_InsertBy");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Area t2 " + Environment.NewLine;
                                                mapping += " USING #M_Area t1 ON (t2.[Area_CompanyID] = t1.[Area_CompanyID] AND" + Environment.NewLine;
                                                mapping += " t2.[Area_Name] = t1.[Area_Name]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Area_Name=t1.Area_Name, t2.Area_Desc=t1.Area_Desc, t2.Area_Long = t1.Area_Long, t2.Area_Lat = t1.Area_Lat, t2.Area_DistrictID = t1.Area_DistrictID, t2.Area_UpdateOn=t1.Area_InsertOn, t2.Area_UpdateBy=t1.Area_InsertBy, t2.Area_Isdelete = 0, t2.Area_isActive = 1" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Area_PrincipalID], [Area_Name], [Area_Desc], [Area_Long], [Area_Lat], [Area_DistrictID], [Area_CompanyID], [Area_InsertOn], [Area_InsertBy], Area_isActive, Area_Isdelete) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Area_PrincipalID], t1.[Area_Name], t1.[Area_Desc], t1.[Area_Long], t1.[Area_Lat], t1.[Area_DistrictID], t1.[Area_CompanyID], t1.[Area_InsertOn], t1.[Area_InsertBy], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Area] ([Area_PrincipalID], [Area_Name], [Area_Desc], [Area_Long], [Area_Lat], [Area_DistrictID], [Area_CompanyID], [Area_InsertOn], [Area_InsertBy])" +
                                                //                   " SELECT t1.[Area_PrincipalID], t1.[Area_Name], t1.[Area_Desc], t1.[Area_Long], t1.[Area_Lat], t1.[Area_DistrictID], t1.[Area_CompanyID], t1.[Area_InsertOn], t1.[Area_InsertBy] FROM #M_Area AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Area AS t2 WHERE t2.[Area_CompanyID] = t1.[Area_CompanyID] AND t2.[Area_Name] = t1.[Area_Name])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Area";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }
                                            //int rows = dt.Rows.Count;                                            
                                            //TempData["Rows"] = rows;
                                        
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();

                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {                        
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Area_Name", typeof(string));
            table.Columns.Add("Area_Desc", typeof(string));
            table.Columns.Add("Area_Long", typeof(string));
            table.Columns.Add("Area_Lat", typeof(string));
            table.Columns.Add("Area_DistrictID", typeof(string));
            table.Columns.Add("Area_District_Name", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("JAKARTA", "Area Jakarta", "107.6095733", "-6.1853805", "3172080", "CAKUNG");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Area.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (var memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
                //return File(memoryStream, "application/vnd.ms-excel", "Template_Area.xlsx");
            }
            httpResponse.End();
            return RedirectToAction("Index", "Area");
            //return Json(1, JsonRequestBehavior.AllowGet);
        }
    }
}