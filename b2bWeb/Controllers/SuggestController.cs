﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace b2bWeb.Controllers
{
    public class SuggestController : Controller
    {
        StockDB stockDB = new StockDB();
        ItemDB itmDB = new ItemDB();
        OutletDB outletDB = new OutletDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Item = itmDB.ListAll() ?? new List<Item>();
                ViewBag.Outlet = outletDB.ListAll() ?? new List<Outlet>();
                ViewBag.Title = "Master / Suggestion Stock";
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataSuggest()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = stockDB.GetDataSuggest(param) ?? new List<JsonStock>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonStock Baru = new JsonStock();
                Baru.total = 0;
                Baru.rows = new List<rowsStock>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_stockDB = JsonConvert.SerializeObject(stockDB.ListAll());
                DataTable dt_stockDB = (DataTable)JsonConvert.DeserializeObject(json_stockDB, (typeof(DataTable)));

                dt_stockDB.Columns.Add("Stock_InsertOn1", typeof(string));
                dt_stockDB.Columns.Add("Stock_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_stockDB.Rows)
                {
                    objCon["Stock_InsertOn1"] = Convert.ToString(objCon["Stock_InsertOn"]);
                    objCon["Stock_UpdateOn1"] = Convert.ToString(objCon["Stock_UpdateOn"]);
                }

                dt_stockDB.Columns.Remove("Stock_InsertOn");
                dt_stockDB.Columns.Remove("Stock_UpdateOn");         
                dt_stockDB.Columns.Remove("Stock_CompanyID");

                dt_stockDB.Columns.Add("Stock_InsertOn", typeof(string));
                dt_stockDB.Columns.Add("Stock_UpdateOn", typeof(string));
                dt_stockDB.Columns.Add("Stock_InsertByName", typeof(string));
                dt_stockDB.Columns.Add("Stock_UpdateByName", typeof(string));

                for (int a = 0; a < dt_stockDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_stockDB.Rows[a]["Stock_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_stockDB.Rows[a]["Stock_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_stockDB.Rows[a]["Stock_UpdateOn"] = dt_stockDB.Rows[a]["Stock_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_stockDB.Rows)
                {
                    objCon["Stock_InsertOn"] = Convert.ToString(objCon["Stock_InsertOn1"]);
                    objCon["Stock_InsertByName"] = Convert.ToString(objCon["Stock_Insert_Name"]);
                    objCon["Stock_UpdateByName"] = Convert.ToString(objCon["Stock_Update_Name"]);
                }

                dt_stockDB.Columns.Remove("Stock_InsertOn1");
                dt_stockDB.Columns.Remove("Stock_UpdateOn1");
                dt_stockDB.Columns.Remove("Stock_Insert_Name");
                dt_stockDB.Columns.Remove("Stock_Update_Name");
                dt_stockDB.Columns.Remove("jml");
                dt_stockDB.Columns.Remove("Stock_OutletCode");
                dt_stockDB.Columns.Remove("Stock_Outlet_Name");
                dt_stockDB.Columns.Remove("Stock_ID");
                dt_stockDB.Columns.Remove("Stock_IsDelete");                

                dt_stockDB.AcceptChanges();               

                //dt_stockDB.Columns["Stock_OutletCode"].SetOrdinal(0);
                dt_stockDB.Columns["Stock_ItemCode"].SetOrdinal(0);
                dt_stockDB.Columns["Stock_Item_Name"].SetOrdinal(1);
                dt_stockDB.Columns["Stock_Suggestion"].SetOrdinal(2);
                dt_stockDB.Columns["Stock_InsertBy"].SetOrdinal(3);
                dt_stockDB.Columns["Stock_InsertByName"].SetOrdinal(4);
                dt_stockDB.Columns["Stock_InsertOn"].SetOrdinal(5);
                dt_stockDB.Columns["Stock_UpdateBy"].SetOrdinal(6);
                dt_stockDB.Columns["Stock_UpdateByName"].SetOrdinal(7);
                dt_stockDB.Columns["Stock_UpdateOn"].SetOrdinal(8);
                dt_stockDB.Columns["Stock_IsActive"].SetOrdinal(9);

                dt_stockDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_stockDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Suggestion.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();
                return RedirectToAction("index", "suggest");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult AddUp(Stock data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Stock_ID == null)
                    {
                        try
                        {
                            data.Stock_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Stock_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (stockDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    stockDB.AddUpd(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.Stock_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Stock_UpdateBy = Convert.ToString(Session["Username"]);

                            if (stockDB.AddUpd(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    return RedirectToAction("Index", "Suggest");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Stock data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Stock_UpdateBy = Convert.ToString(Session["Username"]);
                    stockDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Product_Code", typeof(string));
            table.Columns.Add("Product_Name", typeof(string));
            table.Columns.Add("Suggestion", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("CINTA1", "COKLAT CINTA", "14");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Suggestion_Stock.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Suggest");
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Product_Code", "Suggestion" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Product_Code") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Stock_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Stock_InsertBy", typeof(string));
                                            dt.Columns.Add("Stock_CompanyID", typeof(int));
                                            dt.Columns.Add("Stock_ItemCode", typeof(string));
                                            dt.Columns.Add("Stock_Suggestion", typeof(int));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {

                                                dt.Rows[a]["Stock_ItemCode"] = Convert.ToString(dt.Rows[a]["Product_Code"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Stock_Suggestion"] = Convert.ToString(dt.Rows[a]["Suggestion"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Stock_InsertOn"] = today;
                                                dt.Rows[a]["Stock_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Stock_CompanyID"] = Session["CompanyID"];
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Stock (Stock_ItemCode nvarchar(30), Stock_Suggestion int, Stock_CompanyID int, Stock_InsertOn datetime, Stock_InsertBy nvarchar(30))", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Stock";

                                                    sqlBulkCopy.ColumnMappings.Add("Stock_ItemCode", "Stock_ItemCode");
                                                    sqlBulkCopy.ColumnMappings.Add("Stock_Suggestion", "Stock_Suggestion");
                                                    sqlBulkCopy.ColumnMappings.Add("Stock_CompanyID", "Stock_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Stock_InsertOn", "Stock_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Stock_InsertBy", "Stock_InsertBy");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Stock t2 " + Environment.NewLine;
                                                mapping += " USING #M_Stock t1 ON (t2.[Stock_ItemCode] = t1.[Stock_ItemCode] AND" + Environment.NewLine;
                                                mapping += " t2.[Stock_CompanyID] = t1.[Stock_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Stock_Suggestion=t1.Stock_Suggestion, t2.Stock_UpdateOn=t1.Stock_InsertOn, t2.Stock_UpdateBy=t1.Stock_InsertBy, t2.Stock_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Stock_ItemCode], [Stock_Suggestion], [Stock_CompanyID], [Stock_InsertOn], [Stock_InsertBy], [Stock_IsActive], [Stock_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Stock_ItemCode], t1.[Stock_Suggestion], t1.[Stock_CompanyID], t1.[Stock_InsertOn], t1.[Stock_InsertBy], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Stock] ([Stock_Code], [Stock_Name], [Stock_Desc], [Stock_CompanyID], [Stock_InsertOn], [Stock_InsertBy])" +
                                                //                   " SELECT t1.[Stock_Code], t1.[Stock_Name], t1.[Stock_Desc], t1.[Stock_CompanyID], t1.[Stock_InsertOn], t1.[Stock_InsertBy] FROM #M_Stock AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Stock AS t2 WHERE t2.[Stock_Code] = t1.[Stock_Code] AND t2.[Stock_CompanyID] = t1.[Stock_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Stock";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();

                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}