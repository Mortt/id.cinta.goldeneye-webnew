﻿using ClosedXML.Excel;
using MvcPaging;
using Newtonsoft.Json;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b2bWeb.Controllers
{
    public class CallTargetController : Controller
    {
        CallTargetDB CallTargetDB = new CallTargetDB();
        EmployeeDB empDB = new EmployeeDB();
        RoleDB rolDB = new RoleDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Call Target";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                //ViewBag.Employee = empDB.ListAll() ?? new List<Employee>();
                ViewBag.Role = rolDB.ListAll() ?? new List<Role>();
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataCallTarget()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = CallTargetDB.GetDataCallTarget(param) ?? new List<JsonCallTarget>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonCallTarget Baru = new JsonCallTarget();
                Baru.total = 0;
                Baru.rows = new List<rowsCallTarget>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUp(CallTarget data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.TC_id == null)
                    {
                        try
                        {
                            data.TC_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.TC_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (CallTargetDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    CallTargetDB.AddUpdate(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }                                
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.TC_UpdateBy = Convert.ToString(Session["Username"]);

                            if (CallTargetDB.AddUpdate(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    return RedirectToAction("Index", "calltarget");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(CallTarget data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                  try
                    {
                        data.TC_UpdateBy = Convert.ToString(Session["Username"]);
                        CallTargetDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Role_ID", "Call_Target", "Periode" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Role_ID") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Periode1", typeof(DateTime));
                                            dt.Columns.Add("TC_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("TC_InsertBy", typeof(string));
                                            dt.Columns.Add("TC_CompanyID", typeof(int));
                                            dt.Columns.Add("TC_IsActive", typeof(bool));
                                            dt.Columns.Add("TC_IsDelete", typeof(bool));


                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Periode"] != null)
                                                {
                                                    dt.Rows[a]["Periode1"] = Convert.ToDateTime(dt.Rows[a]["Periode"]).ToString("MM/yyyy");
                                                }

                                                dt.Rows[a]["TC_InsertOn"] = today;
                                                dt.Rows[a]["TC_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["TC_CompanyID"] = Session["CompanyID"];
                                                dt.Rows[a]["TC_IsActive"] = true;
                                                dt.Rows[a]["TC_IsDelete"] = false;
                                            }

                                            dt.Columns.Remove("Periode");
                                            dt.Columns.Add("Periode", typeof(DateTime));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Periode1"] != null)
                                                {
                                                    dt.Rows[a]["Periode"] = dt.Rows[a]["Periode1"];
                                                }
                                            }

                                            dt.Columns.Remove("Periode1");

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_TargetCall (TC_Periode date, TC_Role_ID int, TC_Qty int, TC_CompanyID int, TC_InsertBy nvarchar(MAX), TC_InsertOn datetime, TC_IsActive bit, TC_IsDelete bit)", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_TargetCall";

                                                    sqlBulkCopy.ColumnMappings.Add("Periode", "TC_Periode");
                                                    sqlBulkCopy.ColumnMappings.Add("Role_ID", "TC_Role_ID");
                                                    sqlBulkCopy.ColumnMappings.Add("Call_Target", "TC_Qty");
                                                    sqlBulkCopy.ColumnMappings.Add("TC_CompanyID", "TC_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("TC_InsertBy", "TC_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("TC_InsertOn", "TC_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("TC_IsActive", "TC_IsActive");
                                                    sqlBulkCopy.ColumnMappings.Add("TC_IsDelete", "TC_IsDelete");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_TargetCall t2 " + Environment.NewLine;
                                                mapping += " USING #M_TargetCall t1 ON (t2.[TC_Periode] = t1.[TC_Periode] AND" + Environment.NewLine;
                                                mapping += " t2.[TC_Role_ID] = t1.[TC_Role_ID] AND t2.[TC_CompanyID] = t1.[TC_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.TC_Periode=t1.TC_Periode, t2.TC_Qty=t1.TC_Qty, t2.TC_UpdateOn=t1.TC_InsertOn, t2.TC_UpdateBy=t1.TC_InsertBy, t2.TC_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([TC_Role_ID], [TC_Periode], [TC_Qty], [TC_CompanyID], [TC_InsertBy], [TC_InsertOn], [TC_IsActive], [TC_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[TC_Role_ID], t1.[TC_Periode], t1.[TC_Qty], t1.[TC_CompanyID], t1.[TC_InsertBy], t1.[TC_InsertOn], t1.[TC_IsActive], t1.[TC_IsDelete]); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_TargetCall] ([TC_Employee_NIK], [TC_Periode], [TC_Qty], [TC_CompanyID], [TC_InsertBy], [TC_InsertOn], [TC_IsActive], [TC_IsDelete])" +
                                                //                   " SELECT t1.[TC_Employee_NIK], t1.[TC_Periode], t1.[TC_Qty], t1.[TC_CompanyID], t1.[TC_InsertBy], t1.[TC_InsertOn], t1.[TC_IsActive], t1.[TC_IsDelete] FROM #M_TargetCall AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_TargetCall AS t2 WHERE t2.[TC_Periode] = t1.[TC_Periode] AND t2.[TC_Employee_NIK] = t1.[TC_Employee_NIK] AND t2.[TC_CompanyID] = t1.[TC_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_TargetCall";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }                    
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Role_ID", typeof(string));
            table.Columns.Add("Role_Name", typeof(string));
            table.Columns.Add("Call_Target", typeof(string));
            table.Columns.Add("Periode", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("22", "SPG", "8", "Jun-19");
            table.Rows.Add("23", "TL", "8", "Jun-19");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Call_Target.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "calltarget");
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_itmDB = JsonConvert.SerializeObject(CallTargetDB.ListAll());
                DataTable dt_itmDB = (DataTable)JsonConvert.DeserializeObject(json_itmDB, (typeof(DataTable)));

                if (dt_itmDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "calltarget");
                }

                dt_itmDB.Columns.Add("TC_Periode1", typeof(string));
                dt_itmDB.Columns.Add("TC_InsertOn1", typeof(string));
                dt_itmDB.Columns.Add("TC_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["TC_Periode1"] = Convert.ToDateTime(objCon["TC_Periode"]).ToString("dd/MM/yyy");
                    objCon["TC_InsertOn1"] = Convert.ToString(objCon["TC_InsertOn"]);
                    objCon["TC_UpdateOn1"] = Convert.ToString(objCon["TC_UpdateOn"]);
                }

                dt_itmDB.Columns.Remove("TC_CompanyID");
                dt_itmDB.Columns.Remove("TC_UpdateOn");
                dt_itmDB.Columns.Remove("TC_InsertOn");
                dt_itmDB.Columns.Remove("TC_Periode");
                dt_itmDB.Columns.Remove("TC_id");
                dt_itmDB.Columns.Remove("TC_IsDelete");

                dt_itmDB.Columns.Add("TC_InsertOn", typeof(string));
                dt_itmDB.Columns.Add("TC_UpdateOn", typeof(string));
                dt_itmDB.Columns.Add("TC_Periode", typeof(string));

                for (int a = 0; a < dt_itmDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_itmDB.Rows[a]["TC_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_itmDB.Rows[a]["TC_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_itmDB.Rows[a]["TC_UpdateOn"] = dt_itmDB.Rows[a]["TC_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["TC_InsertOn"] = Convert.ToString(objCon["TC_InsertOn1"]);
                    objCon["TC_Periode"] = Convert.ToDateTime(objCon["TC_Periode1"]).ToString("dd/yyyy");
                }

                dt_itmDB.Columns.Remove("TC_InsertOn1");
                dt_itmDB.Columns.Remove("TC_Periode1");
                dt_itmDB.Columns.Remove("TC_UpdateOn1");
                dt_itmDB.Columns.Remove("TC_EmployeeName");
                dt_itmDB.Columns.Remove("jml");
                dt_itmDB.Columns.Remove("TC_Role_ID");

                dt_itmDB.AcceptChanges();
                
                dt_itmDB.Columns["TC_RoleName"].SetOrdinal(0);                
                dt_itmDB.Columns["TC_Qty"].SetOrdinal(1);
                dt_itmDB.Columns["TC_Periode"].SetOrdinal(2);
                dt_itmDB.Columns["TC_IsActive"].SetOrdinal(3);
                dt_itmDB.Columns["TC_InsertOn"].SetOrdinal(4);
                dt_itmDB.Columns["TC_InsertBy"].SetOrdinal(5);
                dt_itmDB.Columns["TC_UpdateOn"].SetOrdinal(6);
                dt_itmDB.Columns["TC_UpdateBy"].SetOrdinal(7);

                dt_itmDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_itmDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Call_Target.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();

                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}