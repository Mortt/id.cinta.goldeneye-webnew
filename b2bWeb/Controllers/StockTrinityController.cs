﻿using MvcPaging;
using Newtonsoft.Json;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b2bWeb.Controllers
{
    public class StockTrinityController : Controller
    {
        ExpDateDB ExpDateDB = new ExpDateDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                //string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                //if (DashboardController.Paket(actionName) == true)
                //{

                //}
                //else
                //{
                //    return RedirectToAction("Error", "Dashboard");
                //}

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Stock";
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataStockTrinity()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = ExpDateDB.GetDataExpDate(param) ?? new List<JsonExpDate>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonExpDate Baru = new JsonExpDate();
                Baru.total = 0;
                Baru.rows = new List<rowsExpDate>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                string name = "Stock(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                var Expdate = ExpDateDB.ListExport(g, c);

                var json_expdateDB = JsonConvert.SerializeObject(Expdate);
                DataTable dt_expdateDB = (DataTable)JsonConvert.DeserializeObject(json_expdateDB, (typeof(DataTable)));

                if (dt_expdateDB.Rows.Count == 0)
                {
                    TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                    return RedirectToAction("index", "stocktrinity");
                }

                dt_expdateDB.Columns.Add("ExpDate_InsertOn1", typeof(string));

                foreach (DataRow objCon in dt_expdateDB.Rows)
                {
                    objCon["ExpDate_InsertOn1"] = Convert.ToString(objCon["ExpDate_InsertOn"]);
                }

                dt_expdateDB.Columns.Remove("ExpDate_ID");
                dt_expdateDB.Columns.Remove("ExpDate_ReasonID");
                dt_expdateDB.Columns.Remove("ExpDate_InsertBy");
                dt_expdateDB.Columns.Remove("ExpDate_UpdateBy");
                dt_expdateDB.Columns.Add("ExpDate_Product_Name", typeof(string));
                dt_expdateDB.Columns.Add("ExpDate_Outlet_Name", typeof(string));
                dt_expdateDB.Columns.Add("ExpDate_InsertBy", typeof(string));
                dt_expdateDB.Columns.Add("ExpDate_Respond_Name", typeof(string));

                foreach (DataRow objCon in dt_expdateDB.Rows)
                {
                    objCon["ExpDate_InsertOn"] = Convert.ToString(objCon["ExpDate_InsertOn1"]);
                    objCon["ExpDate_Product_Name"] = Convert.ToString(objCon["Item_Name"]);
                    objCon["ExpDate_Outlet_Name"] = Convert.ToString(objCon["Outlet_Name"]);
                    objCon["ExpDate_InsertBy"] = Convert.ToString(objCon["Employee_Name"]);
                    objCon["ExpDate_Respond_Name"] = Convert.ToString(objCon["Respond_Name"]);
                }

                dt_expdateDB.Columns.Remove("ExpDate_InsertOn1");
                dt_expdateDB.Columns.Remove("Item_Name");
                dt_expdateDB.Columns.Remove("Area_Name");
                dt_expdateDB.Columns.Remove("Outlet_Name");
                dt_expdateDB.Columns.Remove("Employee_Name");
                dt_expdateDB.Columns.Remove("Respond_Name");
                dt_expdateDB.Columns.Remove("ExpDate_Image2");
                dt_expdateDB.Columns.Remove("ExpDate_Image3");
                dt_expdateDB.Columns.Remove("ExpDate_Image4");
                dt_expdateDB.Columns.Remove("ExpDate_Image5");
                dt_expdateDB.Columns.Remove("ExpDate_Respond_Name");
                dt_expdateDB.Columns.Remove("ExpDate_Date");
                dt_expdateDB.Columns.Remove("Item_Image1");
                dt_expdateDB.Columns.Remove("ExpDate_Isdelete");
                dt_expdateDB.Columns.Remove("ExpDate_UpdateOn");
                

                dt_expdateDB.AcceptChanges();                

                dt_expdateDB.Columns["ExpDate_InsertOn"].ColumnName = "Insert_On";
                dt_expdateDB.Columns["ExpDate_OutletCode"].ColumnName = "Outlet_Code";
                dt_expdateDB.Columns["ExpDate_Outlet_Name"].ColumnName = "Outlet_Name";
                dt_expdateDB.Columns["ExpDate_ProductCode"].ColumnName = "Product_Code";
                dt_expdateDB.Columns["ExpDate_Product_Name"].ColumnName = "Product_Name";
                dt_expdateDB.Columns["ExpDate_BatchNo"].ColumnName = "Batch_No";
                dt_expdateDB.Columns["ExpDate_Qty"].ColumnName = "Quantity";
                dt_expdateDB.Columns["ExpDate_InsertBy"].ColumnName = "Employee_Name";
                dt_expdateDB.Columns["ExpDate_Image"].ColumnName = "Image";
                //dt_expdateDB.Columns["ExpDate_UpdateOn"].ColumnName = "Update_On";          

                dt_expdateDB.Columns["Insert_On"].SetOrdinal(0);
                dt_expdateDB.Columns["Outlet_Code"].SetOrdinal(1);
                dt_expdateDB.Columns["Outlet_Name"].SetOrdinal(2);
                dt_expdateDB.Columns["Product_Code"].SetOrdinal(3);
                dt_expdateDB.Columns["Product_Name"].SetOrdinal(4);
                dt_expdateDB.Columns["Batch_No"].SetOrdinal(5);
                dt_expdateDB.Columns["Quantity"].SetOrdinal(6);
                dt_expdateDB.Columns["Employee_Name"].SetOrdinal(7);
                dt_expdateDB.Columns["Image"].SetOrdinal(8);
                //dt_expdateDB.Columns["Update_On"].SetOrdinal(9);

                dt_expdateDB.AcceptChanges();

                if (image == false)
                {
                    TempData["DT_JSON"] = dt_expdateDB;
                    return RedirectToAction("ToExcel", "Dashboard", new { name });
                }
                else
                {                    
                    dt_expdateDB.AcceptChanges();

                    for (int a = 0; a < dt_expdateDB.Rows.Count; a++)
                    {
                        string url = Convert.ToString(dt_expdateDB.Rows[a]["Image"]);
                        string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                        dt_expdateDB.Rows[a]["Image"] = fileName;
                    }

                    TempData["DT_JSON"] = dt_expdateDB;

                    int row = 1;
                    int column = 9;
                    string TableImageName = "Image";
                    return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}