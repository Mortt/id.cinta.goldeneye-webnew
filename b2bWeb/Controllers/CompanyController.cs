﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web.Security;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class CompanyController : Controller
    {
        CompanyDB copmDB = new CompanyDB();
        // GET: Company        
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Company";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Data = CompanyDB.ListAll() ?? new List<Company>();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Upd(Company model)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (model.Images != null)
                {
                    try
                    {
                        DashboardController.UploadToAzure(Request.Files["Images"]);
                        model.Company_Logo = "https://foodiegost17.blob.core.windows.net/images/" + Request.Files["Images"].FileName;
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                else
                {
                    model.Company_Logo = model.Images_Name;
                }

                model.Company_Distance = Convert.ToDecimal(model.Company_Distance1.ToString().Replace(",", ""));
                model.Company_DistanceOutlet = Convert.ToDecimal(model.Company_DistanceOutlet1.ToString().Replace(",", ""));
                model.Company_Updateby = Convert.ToString(Session["Username"]);
                model.Company_ID = Convert.ToInt16(Session["CompanyID"]);
                model.Company_Name = Convert.ToString(Session["Company_Name"]);        
                model.Company_Packet_ID = Convert.ToInt16(Session["Paket"]);
                model.Company_IsActive = true;

                if (copmDB.UpdateCompany(model) == 0)
                {
                    TempData["Error"] = "fail";
                    return RedirectToAction("Index", "Company");
                }
                else
                {
                    TempData["Success"] = "update " + model.Company_Name + " successfully";
                    return RedirectToAction("Index", "Company");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

       
    }
}