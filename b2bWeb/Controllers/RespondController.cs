﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace b2bWeb.Controllers
{
    public class RespondController : Controller
    {
        RespondDB rspDB = new RespondDB();
       
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / Respond";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataRespond()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = rspDB.GetDataRespond(param) ?? new List<JsonRespond>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonRespond Baru = new JsonRespond();
                Baru.total = 0;
                Baru.rows = new List<rowsRespond>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUp(Respond data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Respond_ID == null)
                    {
                        try
                        {
                            data.Respond_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Respond_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (rspDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    rspDB.AddUpdate(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.Respond_UpdateBy = Convert.ToString(Session["Username"]);
                            data.Respond_CompanyID = Convert.ToInt16(Session["CompanyID"]);

                            if (rspDB.AddUpdate(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }                    
                }
                return RedirectToAction("Index", "Respond");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Respond data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Respond_UpdateBy = Convert.ToString(Session["Username"]);
                    rspDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_rspDB = JsonConvert.SerializeObject(rspDB.ListAll());
                DataTable dt_rspDB = (DataTable)JsonConvert.DeserializeObject(json_rspDB, (typeof(DataTable)));

                if(dt_rspDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Respond");
                }

                dt_rspDB.Columns.Add("Respond_InsertOn1", typeof(string));
                dt_rspDB.Columns.Add("Respond_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_rspDB.Rows)
                {
                    objCon["Respond_InsertOn1"] = Convert.ToString(objCon["Respond_InsertOn"]);
                    objCon["Respond_UpdateOn1"] = Convert.ToString(objCon["Respond_UpdateOn"]);
                }

                dt_rspDB.Columns.Remove("Respond_InsertOn");
                dt_rspDB.Columns.Remove("Respond_UpdateOn");
                dt_rspDB.Columns.Remove("Jml");
                dt_rspDB.Columns.Remove("Respond_CompanyID");

                dt_rspDB.Columns.Add("Respond_InsertOn", typeof(string));
                dt_rspDB.Columns.Add("Respond_UpdateOn", typeof(string));
                dt_rspDB.Columns.Add("Respond_InsertByName", typeof(string));
                dt_rspDB.Columns.Add("Respond_UpdateByName", typeof(string));

                for (int a = 0; a < dt_rspDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_rspDB.Rows[a]["Respond_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_rspDB.Rows[a]["Respond_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_rspDB.Rows[a]["Respond_UpdateOn"] = dt_rspDB.Rows[a]["Respond_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_rspDB.Rows)
                {
                    objCon["Respond_InsertOn"] = Convert.ToString(objCon["Respond_InsertOn1"]);
                    objCon["Respond_InsertByName"] = Convert.ToString(objCon["Insert_Name"]);
                    objCon["Respond_UpdateByName"] = Convert.ToString(objCon["Update_Name"]);
                }

                dt_rspDB.Columns.Remove("Respond_InsertOn1");
                dt_rspDB.Columns.Remove("Respond_UpdateOn1");
                dt_rspDB.Columns.Remove("Insert_Name");
                dt_rspDB.Columns.Remove("Update_Name");

                dt_rspDB.AcceptChanges();

                dt_rspDB.Columns["Respond_ID"].SetOrdinal(0);
                dt_rspDB.Columns["Respond_Code"].SetOrdinal(1);                
                dt_rspDB.Columns["Respond_Name"].SetOrdinal(2);
                dt_rspDB.Columns["Respond_Desc"].SetOrdinal(3);
                dt_rspDB.Columns["Respond_Module"].SetOrdinal(4);
                dt_rspDB.Columns["Respond_InsertBy"].SetOrdinal(5);
                dt_rspDB.Columns["Respond_InsertByName"].SetOrdinal(6);
                dt_rspDB.Columns["Respond_InsertOn"].SetOrdinal(7);
                dt_rspDB.Columns["Respond_UpdateBy"].SetOrdinal(8);
                dt_rspDB.Columns["Respond_UpdateByName"].SetOrdinal(9);
                dt_rspDB.Columns["Respond_UpdateOn"].SetOrdinal(10);
                dt_rspDB.Columns["Respond_IsActive"].SetOrdinal(11);

                dt_rspDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_rspDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Respond.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();
                return RedirectToAction("index", "respond");
            }
            else
            {
                return Redirect("/");
            }
        }
        
        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];

                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Respond_Code", "Respond_Name", "Respond_Desc", "Respond_Module" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Respond_Code") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Respond_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Respond_InsertBy", typeof(string));
                                            dt.Columns.Add("Respond_CompanyID", typeof(int));
                                            dt.Columns.Add("Respond_Code1", typeof(string));
                                            dt.Columns.Add("Respond_Name1", typeof(string));
                                            dt.Columns.Add("Respond_Desc1", typeof(string));
                                            dt.Columns.Add("Respond_Module1", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {

                                                dt.Rows[a]["Respond_Code1"] = Convert.ToString(dt.Rows[a]["Respond_Code"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Respond_Name1"] = Convert.ToString(dt.Rows[a]["Respond_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Respond_Desc1"] = Convert.ToString(dt.Rows[a]["Respond_Desc"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Respond_Module1"] = Convert.ToString(dt.Rows[a]["Respond_Module"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Respond_InsertOn"] = today;
                                                dt.Rows[a]["Respond_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Respond_CompanyID"] = Session["CompanyID"];
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Respond (Respond_Code nvarchar(30), Respond_Name nvarchar(100), Respond_Desc nvarchar(MAX), Respond_Module nvarchar(30), Respond_InsertOn datetime, Respond_InsertBy nvarchar(30), Respond_CompanyID int)", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Respond";

                                                    sqlBulkCopy.ColumnMappings.Add("Respond_Code1", "Respond_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("Respond_Name1", "Respond_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("Respond_Desc1", "Respond_Desc");
                                                    sqlBulkCopy.ColumnMappings.Add("Respond_Module1", "Respond_Module");
                                                    sqlBulkCopy.ColumnMappings.Add("Respond_InsertOn", "Respond_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Respond_InsertBy", "Respond_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("Respond_CompanyID", "Respond_CompanyID");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Respond t2 " + Environment.NewLine;
                                                mapping += " USING #M_Respond t1 ON (t2.[Respond_Code] = t1.[Respond_Code] AND" + Environment.NewLine;
                                                mapping += " t2.[Respond_CompanyID] = t1.[Respond_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Respond_Name=t1.Respond_Name, t2.Respond_Desc=t1.Respond_Desc, t2.Respond_Module=t1.Respond_Module, t2.Respond_UpdateOn=t1.Respond_InsertOn, t2.Respond_UpdateBy=t1.Respond_InsertBy, t2.Respond_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Respond_Code], [Respond_Name], [Respond_Desc], [Respond_Module], [Respond_InsertOn], [Respond_InsertBy], [Respond_CompanyID], [Respond_IsActive], [Respond_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Respond_Code], t1.[Respond_Name], t1.[Respond_Desc], t1.[Respond_Module], t1.[Respond_InsertOn], t1.[Respond_InsertBy], t1.[Respond_CompanyID], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Respond] ([Respond_Code], [Respond_Name], [Respond_Desc], [Respond_Module], [Respond_InsertOn], [Respond_InsertBy], [Respond_CompanyID])" +
                                                //                   " SELECT t1.[Respond_Code], t1.[Respond_Name], t1.[Respond_Desc], t1.[Respond_Module], t1.[Respond_InsertOn], t1.[Respond_InsertBy], t1.[Respond_CompanyID] FROM #M_Respond AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Respond AS t2 WHERE t2.[Respond_Code] = t1.[Respond_Code] AND t2.[Respond_CompanyID] = t1.[Respond_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Respond";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }                    
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {            
            DataTable table = new DataTable();
            table.Columns.Add("Respond_Code", typeof(string));
            table.Columns.Add("Respond_Name", typeof(string));
            table.Columns.Add("Respond_Desc", typeof(string));
            table.Columns.Add("Respond_Module", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("OKE TES", "TES", "ini adalah untuk contoh", "PLANOGRAM");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Respond.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (var memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
                //return File(memoryStream, "application/vnd.ms-excel", "Template_Area.xlsx");
            }
            httpResponse.End();
            return RedirectToAction("Index", "Respond");
            //return Json(1, JsonRequestBehavior.AllowGet);
        }
    }
}