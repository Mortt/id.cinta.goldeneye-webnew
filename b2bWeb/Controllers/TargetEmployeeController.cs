﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace b2bWeb.Controllers
{
    public class TargetEmployeeController : Controller
    {
        TargetEmployeeDB TargetEmployeeDB = new TargetEmployeeDB();
        EmployeeDB empDB = new EmployeeDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Sales Target";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Employee = empDB.ListAll() ?? new List<Employee>();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }
        public ActionResult GetDataTargetEmployee()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = TargetEmployeeDB.GetDataTargetEmployee(param) ?? new List<JsonTargetEmployee>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonTargetEmployee Baru = new JsonTargetEmployee();
                Baru.total = 0;
                Baru.rows = new List<rowsTargetEmployee>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AddUp(TargetEmployee data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.TargetByEmployee_ID == null)
                    {
                        try
                        {
                            data.TargetByEmployee_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.TargetByEmployee_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (TargetEmployeeDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    TargetEmployeeDB.AddUp(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.TargetByEmployee_UpdateBy = Convert.ToString(Session["Username"]);

                            //if (TargetEmployeeDB.Cek(data) != 0)
                            //{
                            //    return Json("has", JsonRequestBehavior.AllowGet);
                            //}
                            //else
                            //{
                                if (TargetEmployeeDB.AddUp(data) >= 0)
                                {
                                    return Json("update", JsonRequestBehavior.AllowGet);
                                }
                            //}
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return RedirectToAction("index", "TargetEmployee");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(TargetEmployee data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.TargetByEmployee_UpdateBy = Convert.ToString(Session["Username"]);
                    TargetEmployeeDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_itmDB = JsonConvert.SerializeObject(TargetEmployeeDB.ListAll());
                DataTable dt_itmDB = (DataTable)JsonConvert.DeserializeObject(json_itmDB, (typeof(DataTable)));

                if (dt_itmDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "TargetEmployee");
                }

                dt_itmDB.Columns.Add("TargetByEmployee_Periode1", typeof(string));
                dt_itmDB.Columns.Add("TargetByEmployee_InsertOn1", typeof(string));
                dt_itmDB.Columns.Add("TargetByEmployee_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["TargetByEmployee_Periode1"] = Convert.ToDateTime(objCon["TargetByEmployee_Periode"]).ToString("dd/MM/yyy");
                    objCon["TargetByEmployee_InsertOn1"] = Convert.ToString(objCon["TargetByEmployee_InsertOn"]);
                    objCon["TargetByEmployee_UpdateOn1"] = Convert.ToString(objCon["TargetByEmployee_UpdateOn"]);
                }

                dt_itmDB.Columns.Remove("TargetByEmployee_ID");
                dt_itmDB.Columns.Remove("TargetByEmployee_UpdateOn");
                dt_itmDB.Columns.Remove("TargetByEmployee_InsertOn");
                dt_itmDB.Columns.Remove("TargetByEmployee_Periode");
                dt_itmDB.Columns.Remove("jml");
                dt_itmDB.Columns.Remove("TargetByEmployee_CompanyID");

                dt_itmDB.Columns.Add("TargetByEmployee_InsertOn", typeof(string));
                dt_itmDB.Columns.Add("TargetByEmployee_UpdateOn", typeof(string));
                dt_itmDB.Columns.Add("TargetByEmployee_Periode", typeof(string));

                for (int a = 0; a < dt_itmDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_itmDB.Rows[a]["TargetByEmployee_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_itmDB.Rows[a]["TargetByEmployee_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_itmDB.Rows[a]["TargetByEmployee_UpdateOn"] = dt_itmDB.Rows[a]["TargetByEmployee_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["TargetByEmployee_InsertOn"] = Convert.ToString(objCon["TargetByEmployee_InsertOn1"]);
                    objCon["TargetByEmployee_Periode"] = Convert.ToDateTime(objCon["TargetByEmployee_Periode1"]).ToString("dd/yyyy");
                }

                dt_itmDB.Columns.Remove("TargetByEmployee_InsertOn1");
                dt_itmDB.Columns.Remove("TargetByEmployee_Periode1");
                dt_itmDB.Columns.Remove("TargetByEmployee_UpdateOn1");
                dt_itmDB.Columns.Remove("TargetByEmployee_Isdelete");

                dt_itmDB.AcceptChanges();

                dt_itmDB.Columns["TargetByEmployee_EmployeeNIK"].SetOrdinal(0);
                dt_itmDB.Columns["TargetByEmployee_EmployeeName"].SetOrdinal(1);
                dt_itmDB.Columns["TargetByEmployee_Amount"].SetOrdinal(2);
                dt_itmDB.Columns["TargetByEmployee_Periode"].SetOrdinal(3);
                dt_itmDB.Columns["TargetByEmployee_IsActive"].SetOrdinal(4);
                dt_itmDB.Columns["TargetByEmployee_InsertOn"].SetOrdinal(5);
                dt_itmDB.Columns["TargetByEmployee_InsertBy"].SetOrdinal(6);
                dt_itmDB.Columns["TargetByEmployee_UpdateOn"].SetOrdinal(7);
                dt_itmDB.Columns["TargetByEmployee_UpdateBy"].SetOrdinal(8);

                dt_itmDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_itmDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Employee_Target.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();

                return RedirectToAction("index", "targetemployee");
            }
            else
            {
                return Redirect("/");
            }
        }
        
        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Employee_NIK", "Amount", "Periode" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Employee_NIK") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Periode1", typeof(DateTime));
                                            dt.Columns.Add("TargetByEmployee_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("TargetByEmployee_InsertBy", typeof(string));
                                            dt.Columns.Add("TargetByEmployee_CompanyID", typeof(int));
                                            dt.Columns.Add("TargetByEmployee_IsActive", typeof(bool));
                                            dt.Columns.Add("TargetByEmployee_Isdelete", typeof(bool));


                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Periode"] != null)
                                                {
                                                    dt.Rows[a]["Periode1"] = Convert.ToDateTime(dt.Rows[a]["Periode"]).ToString("MM/yyyy");
                                                }

                                                dt.Rows[a]["TargetByEmployee_InsertOn"] = today;
                                                dt.Rows[a]["TargetByEmployee_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["TargetByEmployee_CompanyID"] = Session["CompanyID"];
                                                dt.Rows[a]["TargetByEmployee_IsActive"] = true;
                                                dt.Rows[a]["TargetByEmployee_Isdelete"] = false;
                                            }

                                            dt.Columns.Remove("Periode");
                                            dt.Columns.Add("Periode", typeof(DateTime));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Periode1"] != null)
                                                {
                                                    dt.Rows[a]["Periode"] = dt.Rows[a]["Periode1"];
                                                }
                                            }

                                            dt.Columns.Remove("Periode1");

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_TargetEmployee (TargetByEmployee_Periode date, TargetByEmployee_EmployeeNIK nvarchar(30), TargetByEmployee_Amount decimal(18, 2), TargetByEmployee_CompanyID int, TargetByEmployee_InsertBy nvarchar(MAX), TargetByEmployee_InsertOn datetime, TargetByEmployee_IsActive bit, TargetByEmployee_Isdelete bit)", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_TargetEmployee";

                                                    sqlBulkCopy.ColumnMappings.Add("Periode", "TargetByEmployee_Periode");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_NIK", "TargetByEmployee_EmployeeNIK");
                                                    sqlBulkCopy.ColumnMappings.Add("Amount", "TargetByEmployee_Amount");
                                                    sqlBulkCopy.ColumnMappings.Add("TargetByEmployee_CompanyID", "TargetByEmployee_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("TargetByEmployee_InsertBy", "TargetByEmployee_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("TargetByEmployee_InsertOn", "TargetByEmployee_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("TargetByEmployee_IsActive", "TargetByEmployee_IsActive");
                                                    sqlBulkCopy.ColumnMappings.Add("TargetByEmployee_Isdelete", "TargetByEmployee_Isdelete");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_TargetEmployee t2 " + Environment.NewLine;
                                                mapping += " USING #M_TargetEmployee t1 ON (t2.[TargetByEmployee_Periode] = t1.[TargetByEmployee_Periode] AND" + Environment.NewLine;                                                
                                                mapping += " t2.[TargetByEmployee_EmployeeNIK] = t1.[TargetByEmployee_EmployeeNIK] AND t2.[TargetByEmployee_CompanyID] = t1.[TargetByEmployee_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.TargetByEmployee_Periode=t1.TargetByEmployee_Periode, t2.TargetByEmployee_Amount=t1.TargetByEmployee_Amount, t2.TargetByEmployee_UpdateOn=t1.TargetByEmployee_InsertOn, t2.TargetByEmployee_UpdateBy=t1.TargetByEmployee_InsertBy, t2.TargetByEmployee_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([TargetByEmployee_Periode], [TargetByEmployee_EmployeeNIK], [TargetByEmployee_Amount], [TargetByEmployee_CompanyID], [TargetByEmployee_InsertBy], [TargetByEmployee_InsertOn], [TargetByEmployee_IsActive], [TargetByEmployee_Isdelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[TargetByEmployee_Periode], t1.[TargetByEmployee_EmployeeNIK], t1.[TargetByEmployee_Amount], t1.[TargetByEmployee_CompanyID], t1.[TargetByEmployee_InsertBy], t1.[TargetByEmployee_InsertOn], t1.[TargetByEmployee_IsActive], t1.[TargetByEmployee_Isdelete]); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_TargetEmployee] ([TargetByEmployee_Periode], [TargetByEmployee_EmployeeNIK], [TargetByEmployee_Amount], [TargetByEmployee_CompanyID], [TargetByEmployee_InsertBy], [TargetByEmployee_InsertOn], [TargetByEmployee_IsActive], [TargetByEmployee_Isdelete])" +
                                                //                   " SELECT t1.[TargetByEmployee_Periode], t1.[TargetByEmployee_EmployeeNIK], t1.[TargetByEmployee_Amount], t1.[TargetByEmployee_CompanyID], t1.[TargetByEmployee_InsertBy], t1.[TargetByEmployee_InsertOn], t1.[TargetByEmployee_IsActive], t1.[TargetByEmployee_Isdelete] FROM #M_TargetEmployee AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_TargetEmployee AS t2 WHERE t2.[TargetByEmployee_Periode] = t1.[TargetByEmployee_Periode] AND t2.[TargetByEmployee_EmployeeNIK] = t1.[TargetByEmployee_EmployeeNIK] " +
                                                //                   "AND t2.[TargetByEmployee_CompanyID] = t1.[TargetByEmployee_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_TargetEmployee";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Employee_NIK", typeof(string));
            table.Columns.Add("Employee_Name", typeof(string));
            table.Columns.Add("Amount", typeof(string));
            table.Columns.Add("Periode", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("087888467625", "Barid Cahyo", "2000000", "Jun-19");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Employee_Target.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "targetemployee");
        }
    }
}