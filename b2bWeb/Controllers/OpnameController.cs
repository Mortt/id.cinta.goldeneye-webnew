﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class OpnameController : Controller
    {
        OpnameDB opnameDB = new OpnameDB();
        private IList<Opname> allOpname = new List<Opname>();
        DateTime Now = DateTime.Now;
        string B;
        string T;

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if(Convert.ToInt64(Session["CompanyID"]) == 2)
                {
                    return RedirectToAction("index", "stocktrinity");
                }

                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Stock";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataOpname()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = opnameDB.GetDataOpname(param) ?? new List<JsonOpname>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonOpname Baru = new JsonOpname();
                Baru.total = 0;
                Baru.rows = new List<rowsOpname>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Receive(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    var Opname = opnameDB.ListExport(g, c);

                    var json_opnameDB = JsonConvert.SerializeObject(Opname);
                    DataTable dt_opnameDB = (DataTable)JsonConvert.DeserializeObject(json_opnameDB, (typeof(DataTable)));

                    if (dt_opnameDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Opname");
                    }

                    dt_opnameDB.Columns.Add("Opname_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_opnameDB.Rows)
                    {
                        objCon["Opname_InsertOn1"] = Convert.ToString(objCon["Opname_InsertOn"]);
                    }

                    dt_opnameDB.Columns.Remove("Opname_InsertOn");

                    dt_opnameDB.Columns.Add("Opname_InsertOn", typeof(string));
                    dt_opnameDB.Columns.Add("Opname_Quantity", typeof(string));

                    foreach (DataRow objCon in dt_opnameDB.Rows)
                    {
                        objCon["Opname_InsertOn"] = Convert.ToString(objCon["Opname_InsertOn1"]);
                        objCon["Opname_Quantity"] = Convert.ToString(objCon["Opname_Stock"]);
                    }

                    dt_opnameDB.Columns.Remove("Opname_InsertOn1");
                    dt_opnameDB.Columns.Remove("Opname_UpdateBy");
                    dt_opnameDB.Columns.Remove("Opname_ID");
                    dt_opnameDB.Columns.Remove("Opname_Stock");
                    dt_opnameDB.Columns.Remove("Opname_No");
                    dt_opnameDB.Columns.Remove("Opname_Suggestion");
                    dt_opnameDB.Columns.Remove("Opname_InsertBy");
                    dt_opnameDB.Columns.Remove("Opname_UpdateOn");

                    dt_opnameDB.AcceptChanges();

                    dt_opnameDB.Columns["Opname_OutletCode"].SetOrdinal(0);
                    dt_opnameDB.Columns["Opname_Outlet_Name"].SetOrdinal(1);
                    dt_opnameDB.Columns["Opname_ProductCode"].SetOrdinal(2);
                    dt_opnameDB.Columns["Opname_Product_Name"].SetOrdinal(3);
                    dt_opnameDB.Columns["Opname_Product_Image"].SetOrdinal(4);
                    dt_opnameDB.Columns["Opname_Quantity"].SetOrdinal(5);
                    dt_opnameDB.Columns["Opname_Employee_Name"].SetOrdinal(6);
                    dt_opnameDB.Columns["Opname_Desc"].SetOrdinal(7);
                    dt_opnameDB.Columns["Opname_Image"].SetOrdinal(8);
                    dt_opnameDB.Columns["Opname_InsertOn"].SetOrdinal(9);
                    dt_opnameDB.Columns["Opname_Isdelete"].SetOrdinal(10);

                    dt_opnameDB.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_opnameDB;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        dt_opnameDB.Columns.Remove("Opname_Product_Image");

                        dt_opnameDB.AcceptChanges();

                        for (int a = 0; a < dt_opnameDB.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_opnameDB.Rows[a]["Opname_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_opnameDB.Rows[a]["Opname_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_opnameDB;

                        int row = 1;
                        int column = 8;
                        string TableImageName = "Opname_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

    }
}