﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;


namespace b2bWeb.Controllers
{
    public class HomeController : Controller
    {
        EmployeeDB empDB = new EmployeeDB();
        RoleDB roleDB = new RoleDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Employee";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Employee = empDB.ListAll();
                ViewBag.Role = roleDB.ListAll() ?? new List<Role>();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataEmployee()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = empDB.GetDataEmployee(param) ?? new List<JsonEmployee>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonEmployee Baru = new JsonEmployee();
                Baru.total = 0;
                Baru.rows = new List<rowsEmployee>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_empDB = JsonConvert.SerializeObject(empDB.ListAll());
                DataTable dt_empDB = (DataTable)JsonConvert.DeserializeObject(json_empDB, (typeof(DataTable)));

                if (dt_empDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Home");
                }

                dt_empDB.Columns.Add("Employee_InsertOn1", typeof(string));
                dt_empDB.Columns.Add("Employee_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_empDB.Rows)
                {
                    objCon["Employee_InsertOn1"] = Convert.ToString(objCon["Employee_InsertOn"]);
                    objCon["Employee_UpdateOn1"] = Convert.ToString(objCon["Employee_UpdateOn"]);
                }

                dt_empDB.Columns.Remove("Employee_InsertOn");
                dt_empDB.Columns.Remove("Employee_UpdateOn");
                dt_empDB.Columns.Remove("jml");
                dt_empDB.Columns.Remove("Employee_PrincipalID");
                dt_empDB.Columns.Remove("Employee_CompanyID");
                dt_empDB.Columns.Remove("DeCode64Pass");
                //dt_empDB.Columns.Remove("Employee_Photo");
                dt_empDB.Columns.Remove("Employee_Password");

                dt_empDB.Columns.Add("Employee_InsertOn", typeof(string));
                dt_empDB.Columns.Add("Employee_UpdateOn", typeof(string));
                dt_empDB.Columns.Add("Employee_InsertByName", typeof(string));
                dt_empDB.Columns.Add("Employee_UpdateByName", typeof(string));
                dt_empDB.Columns.Add("Employee_RoleName", typeof(string));

                for (int a = 0; a < dt_empDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_empDB.Rows[a]["Employee_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_empDB.Rows[a]["Employee_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_empDB.Rows[a]["Employee_UpdateOn"] = dt_empDB.Rows[a]["Employee_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_empDB.Rows)
                {
                    objCon["Employee_InsertOn"] = Convert.ToString(objCon["Employee_InsertOn1"]);
                    objCon["Employee_InsertByName"] = Convert.ToString(objCon["Insert_Name"]);
                    objCon["Employee_UpdateByName"] = Convert.ToString(objCon["Update_Name"]);
                    objCon["Employee_RoleName"] = Convert.ToString(objCon["Role_Name"]);
                }

                dt_empDB.Columns.Remove("Employee_InsertOn1");
                dt_empDB.Columns.Remove("Employee_UpdateOn1");
                dt_empDB.Columns.Remove("Insert_Name");
                dt_empDB.Columns.Remove("Update_Name");
                dt_empDB.Columns.Remove("Role_Name");

                dt_empDB.AcceptChanges();

                dt_empDB.DefaultView.Sort = "Employee_InsertOn ASC";
                dt_empDB = dt_empDB.DefaultView.ToTable();

                dt_empDB.Columns["Employee_NIK"].SetOrdinal(0);
                dt_empDB.Columns["Employee_Name"].SetOrdinal(1);
                dt_empDB.Columns["Employee_Phone"].SetOrdinal(2);
                dt_empDB.Columns["Employee_Email"].SetOrdinal(3);
                dt_empDB.Columns["Employee_LeaderNik"].SetOrdinal(4);
                dt_empDB.Columns["Employee_Role"].SetOrdinal(5);
                dt_empDB.Columns["Employee_RoleName"].SetOrdinal(6);
                dt_empDB.Columns["Employee_isB2b"].SetOrdinal(7);
                dt_empDB.Columns["Employee_Photo"].SetOrdinal(8);
                dt_empDB.Columns["Employee_IsActive"].SetOrdinal(9);
                dt_empDB.Columns["Employee_InsertBy"].SetOrdinal(10);
                dt_empDB.Columns["Employee_InsertByName"].SetOrdinal(11);
                dt_empDB.Columns["Employee_InsertOn"].SetOrdinal(12);
                dt_empDB.Columns["Employee_UpdateBy"].SetOrdinal(13);
                dt_empDB.Columns["Employee_UpdateByName"].SetOrdinal(14);
                dt_empDB.Columns["Employee_UpdateOn"].SetOrdinal(15);

                dt_empDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_empDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Employee or Merchadiser.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();

                return RedirectToAction("index", "home");
            }
            else
            {
                return Redirect("/");
            }
        }
                
        public ActionResult AddUpd(Employee data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (empDB.Cek(data) != 1)
                    {
                        string checkbase64 = "base64";
                        if (Request.Form["Employee_Photo"].Contains(checkbase64))
                        {
                            string base64 = Request.Form["Employee_Photo"];
                            string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                            string b = DashboardController.RandomString("B2bGoldeneye");
                            string c = "employeeImages" + a + b + ".png";
                            data.Employee_Photo = "https://foodiegost17.blob.core.windows.net/images/" + c;
                            DashboardController.UploadToAzure(base64, c);
                        }
                        else if (Request.Form["Employee_Photo"] != "")
                        {
                            data.Employee_Photo = data.Employee_Photo;
                        }
                        else
                        {
                            data.Employee_Photo = null;
                        }

                        try
                        {
                            data.Employee_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Employee_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {                                
                                 empDB.AddUd(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            string checkbase64 = "base64";
                            if (Request.Form["Employee_Photo"].Contains(checkbase64))
                            {
                                string base64 = Request.Form["Employee_Photo"];
                                string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                string b = DashboardController.RandomString("B2bGoldeneye");
                                string c = "employeeImages" + a + b + ".png";
                                data.Employee_Photo = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                DashboardController.UploadToAzure(base64, c);
                            }
                            else if (Request.Form["Employee_Photo"] != "")
                            {
                                data.Employee_Photo = data.Employee_Photo;
                            }
                            else
                            {
                                data.Employee_Photo = null;
                            }

                            data.Employee_UpdateBy = Convert.ToString(Session["Username"]);

                            if (empDB.AddUd(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }                
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Employee data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Employee_UpdateBy = Convert.ToString(Session["Username"]);
                    empDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Employee_NIK", "Employee_Name", "Employee_Password", "Employee_Phone", "Employee_Email", "Employee_LeaderNik", "Employee_Role", "Employee_isB2b" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Employee_NIK") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Employee_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Employee_InsertBy", typeof(string));
                                            dt.Columns.Add("Employee_CompanyID", typeof(int));
                                            dt.Columns.Add("Employee_Name1", typeof(string));
                                            dt.Columns.Add("Employee_Password1", typeof(string));


                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {

                                                dt.Rows[a]["Employee_Password1"] = Base64Encode(Convert.ToString(dt.Rows[a]["Employee_Password"]));
                                                dt.Rows[a]["Employee_Name1"] = Convert.ToString(dt.Rows[a]["Employee_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Employee_InsertOn"] = today;
                                                dt.Rows[a]["Employee_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Employee_CompanyID"] = Session["CompanyID"];

                                                if (Convert.ToString(dt.Rows[a]["Employee_isB2b"]) == "1")
                                                {
                                                    dt.Rows[a]["Employee_isB2b"] = true;
                                                }
                                                else
                                                {
                                                    dt.Rows[a]["Employee_isB2b"] = false;
                                                }
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Employee (Employee_NIK nvarchar(30), Employee_Name nvarchar(100), Employee_Phone nvarchar(30), Employee_Email nvarchar(100), Employee_Password nvarchar(30), Employee_LeaderNik nvarchar(30), Employee_Role nvarchar(30), Employee_CompanyID int, Employee_InsertOn datetime, Employee_isB2b bit, Employee_InsertBy nvarchar(30) )", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Employee";

                                                    sqlBulkCopy.ColumnMappings.Add("Employee_NIK", "Employee_NIK");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_Name1", "Employee_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_Phone", "Employee_Phone");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_Email", "Employee_Email");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_Password1", "Employee_Password");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_LeaderNik", "Employee_LeaderNik");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_Role", "Employee_Role");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_CompanyID", "Employee_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_InsertOn", "Employee_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_isB2b", "Employee_isB2b");
                                                    sqlBulkCopy.ColumnMappings.Add("Employee_InsertBy", "Employee_InsertBy");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Employee t2 " + Environment.NewLine;
                                                mapping += " USING #M_Employee t1 ON (t2.[Employee_NIK] = t1.[Employee_NIK] AND" + Environment.NewLine;
                                                mapping += " t2.[Employee_CompanyID] = t1.[Employee_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Employee_Name=t1.Employee_Name, t2.Employee_Phone=t1.Employee_Phone, " + Environment.NewLine;
                                                mapping += " t2.Employee_Email=t1.Employee_Email, t2.Employee_Password=t1.Employee_Password, " + Environment.NewLine;
                                                mapping += " t2.Employee_LeaderNik=t1.Employee_LeaderNik, t2.Employee_Role=t1.Employee_Role, " + Environment.NewLine;
                                                mapping += " t2.Employee_isB2b=t1.Employee_isB2b, " + Environment.NewLine;
                                                mapping += " t2.Employee_UpdateOn=t1.Employee_InsertOn, t2.Employee_UpdateBy=t1.Employee_InsertBy, t2.Employee_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Employee_NIK], [Employee_Name], [Employee_Phone], [Employee_Email], [Employee_Password], [Employee_LeaderNik], [Employee_Role], [Employee_CompanyID], [Employee_InsertOn], [Employee_isB2b], [Employee_InsertBy], [Employee_IsActive], [Employee_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Employee_NIK], t1.[Employee_Name], t1.[Employee_Phone], t1.[Employee_Email], t1.[Employee_Password], t1.[Employee_LeaderNik], t1.[Employee_Role], t1.[Employee_CompanyID], t1.[Employee_InsertOn], t1.[Employee_isB2b], t1.[Employee_InsertBy], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Employee] ([Employee_NIK], [Employee_Name], [Employee_Phone], [Employee_Email], [Employee_Password], [Employee_LeaderNik], [Employee_Role], [Employee_CompanyID], [Employee_InsertOn], [Employee_isB2b], [Employee_InsertBy])" +
                                                //                   " SELECT t1.[Employee_NIK], t1.[Employee_Name], t1.[Employee_Phone], t1.[Employee_Email], t1.[Employee_Password], t1.[Employee_LeaderNik], t1.[Employee_Role], t1.[Employee_CompanyID], t1.[Employee_InsertOn], t1.[Employee_isB2b], t1.[Employee_InsertBy] FROM #M_Employee AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Employee AS t2 WHERE t2.[Employee_NIK] = t1.[Employee_NIK] AND t2.[Employee_CompanyID] = t1.[Employee_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Employee";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Employee_NIK", typeof(string));
            table.Columns.Add("Employee_Name", typeof(string));
            table.Columns.Add("Employee_Password", typeof(string));
            table.Columns.Add("Employee_Phone", typeof(string));
            table.Columns.Add("Employee_Email", typeof(string));
            table.Columns.Add("Employee_LeaderNik", typeof(string));
            table.Columns.Add("Employee_Role", typeof(string));
            table.Columns.Add("Employee_RoleName", typeof(string));
            table.Columns.Add("Employee_isB2b", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("081289858986", "Rico P Situmeang", "rico123", "081289858986", "rico@cinta.id", "081289858986", "37", "TL", "1");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Employee_or_Merchandiser.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Home");
        }

    }
}
