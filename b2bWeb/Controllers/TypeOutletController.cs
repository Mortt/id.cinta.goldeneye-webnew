﻿using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b2bWeb.Controllers
{
    public class TypeOutletController : Controller
    {
        Type_OutletDB TO = new Type_OutletDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                { }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / GoldenEye Menu";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataTypeOutlet()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = TO.GetDataType_Outlet(param) ?? new List<JsonType_Outlet>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonType_Outlet Baru = new JsonType_Outlet();
                Baru.total = 0;
                Baru.rows = new List<rowsType_Outlet>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUd(Type_Outlet data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Type_Outlet_ID == null)
                    {
                        try
                        {
                            data.Type_Outlet_Company_ID = Convert.ToInt16(Session["CompanyID"]);
                            data.Type_Outlet_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (TO.Cek(data) > 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                TO.AddUp(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.Type_Outlet_UpdateBy = Convert.ToString(Session["Username"]);

                            if (TO.AddUp(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Type_Outlet data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Type_Outlet_UpdateBy = Convert.ToString(Session["Username"]);
                    TO.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Redirect("/");
            }
        }
    }
}