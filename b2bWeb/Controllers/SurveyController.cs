﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;


namespace b2bWeb.Controllers
{
    public class SurveyController : Controller
    {
        SurveyDB surveyDB = new SurveyDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / Survey";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataSurvey()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = surveyDB.GetDataSurvey(param) ?? new List<JsonSurvey>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonSurvey Baru = new JsonSurvey();
                Baru.total = 0;
                Baru.rows = new List<rowsSurvey>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetbyID(int ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                TaskGet da = new TaskGet();
                da.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                da.GetType = "ID";
                da.Search = "";
                da.SVY_ID = ID;

                var OneDelivery = surveyDB.ListAll(da);
                return Json(OneDelivery, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Survey data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.SVY_UpdateBy = Convert.ToString(Session["Username"]);

                    if (SurveyDB.Delete(data))
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json("failed", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult AddUpQuest(Survey ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    int IDnya;
                    if (ID.SVY_ID == 0)
                    {
                        try
                        {
                            ID.SVY_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            ID.Username = Convert.ToString(Session["Username"]);
                            ID.SVY_Desc = Convert.ToString(ID.SVY_Desc).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim();
                            ID.SVY_Subject = Convert.ToString(ID.SVY_Subject).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim();

                            IList<Survey> allSurvey = new List<Survey>();

                            if (SurveyDB.AddUpdate(ID))
                            {
                                TaskGet da = new TaskGet();
                                da.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                                da.GetType = "";
                                da.Search = "";
                                allSurvey = surveyDB.ListAll(da);

                                IDnya = allSurvey.First().SVY_ID;
                                TempData["Success"] = "add Survey successfully";
                            }
                            else
                            {
                                TempData["Error"] = "add Survey failed";
                                return RedirectToAction("Index", "Survey");
                            }

                        }
                        catch (Exception ex)
                        {
                            TempData["Error"] = ex.Message.ToString();
                            return RedirectToAction("Index", "Survey");
                        }
                    }
                    else
                    {
                        try
                        {
                            ID.SVY_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            ID.Username = Convert.ToString(Session["Username"]);
                            ID.SVY_Desc = Convert.ToString(ID.SVY_Desc).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim();
                            ID.SVY_Subject = Convert.ToString(ID.SVY_Subject).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim();

                            if (SurveyDB.AddUpdate(ID))
                            {
                                IDnya = ID.SVY_ID;
                                TempData["Success"] = "Update Survey successfully";
                            }
                            else
                            {
                                TempData["Error"] = "Update Survey failed";
                                return RedirectToAction("Index", "Survey");
                            }
                        }
                        catch (Exception ex)
                        {
                            TempData["Error"] = ex.Message.ToString();
                            return RedirectToAction("Index", "Survey");
                        }
                    }

                    for (int w = 0; w < ID.AnswerType.Count(); w++)
                    {
                        string[] QuestIDGet = ID.AnswerType[w].Split('|');
                        if (QuestIDGet[0] == "Spinner")
                        {
                            List<SurveyD1> listSurveyD1 = new List<SurveyD1>();
                            List<SurveyD2> listSurveyD2 = new List<SurveyD2>();

                            string cekType = ID.AnswerType[w];

                            var value = Array.FindAll(ID.AnswerTypeValue,
                                           element => element.Contains(cekType));

                            for (int u = 0; u < ID.AnswerTypeValue.Count(); u++)
                            {
                                if (ID.AnswerTypeValue[u] == cekType)
                                {
                                    if (ID.Answer[u] != "")
                                    {
                                        SurveyD2 Answer = new SurveyD2()
                                        {
                                            SVY_ID = IDnya,
                                            //SVY_QuestionID = w + 1,
                                            SVY_Desc = Convert.ToString(ID.Answer[u]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim()
                                        };
                                        listSurveyD2.Add(Answer);
                                    }
                                }
                            }

                            var updateValue = from s in listSurveyD2
                                              where s.SVY_ID == IDnya && s.SVY_Value == null
                                              select s;

                            List<SurveyD2> asd = updateValue.ToList();

                            for (int y = 0; y < asd.Count(); y++)
                            {
                                if (asd[y].SVY_Value == null)
                                {
                                    asd[y].SVY_Value = Convert.ToString(y + 1);
                                }
                            }

                            SurveyD1 Quest = new SurveyD1()
                            {
                                SVY_ID = IDnya,
                                //SVY_QuestionID = w+1,
                                SVY_Question = Convert.ToString(ID.Question[w]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim(),
                                SVY_Type = QuestIDGet[0],
                                SurveyD2 = asd
                            };

                            listSurveyD1.Add(Quest);

                            string M_SurveyD1_Json = JsonConvert.SerializeObject(listSurveyD1);

                            if (SurveyDB.AddUpdateQuest(M_SurveyD1_Json))
                            {
                            }
                            else
                            {
                                TempData["Error"] = "add Question for " + ID.SVY_Desc + " failed";
                                return RedirectToAction("Index", "Survey");
                            }

                        }
                        else if (QuestIDGet[0] == "TextBox")
                        {
                            List<SurveyD1> listSurveyD1 = new List<SurveyD1>();
                            List<SurveyD2> listSurveyD2 = new List<SurveyD2>();

                            string cekType = ID.AnswerType[w];

                            var value = Array.FindAll(ID.AnswerTypeValue,
                                           element => element.Contains(cekType));

                            for (int u = 0; u < ID.AnswerTypeValue.Count(); u++)
                            {
                                if (ID.AnswerTypeValue[u] == cekType)
                                {
                                    SurveyD2 Answer = new SurveyD2()
                                    {
                                        SVY_ID = IDnya,
                                        //SVY_QuestionID = w + 1,
                                        SVY_Desc = Convert.ToString(ID.Answer[u]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim()
                                    };
                                    listSurveyD2.Add(Answer);
                                }
                            }

                            var updateValue = from s in listSurveyD2
                                              where s.SVY_ID == IDnya && s.SVY_Value == null
                                              select s;

                            List<SurveyD2> asd = updateValue.ToList();

                            for (int y = 0; y < asd.Count(); y++)
                            {
                                if (asd[y].SVY_Value == null)
                                {
                                    asd[y].SVY_Value = "11";
                                }
                            }

                            SurveyD1 Quest = new SurveyD1()
                            {
                                SVY_ID = IDnya,
                                //SVY_QuestionID = w+1,
                                SVY_Question = Convert.ToString(ID.Question[w]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim(),
                                SVY_Type = QuestIDGet[0],
                                SurveyD2 = asd
                            };

                            listSurveyD1.Add(Quest);

                            string M_SurveyD1_Json = JsonConvert.SerializeObject(listSurveyD1);

                            if (SurveyDB.AddUpdateQuest(M_SurveyD1_Json))
                            {
                            }
                            else
                            {
                                TempData["Error"] = "add Question for " + ID.SVY_Desc + " failed";
                                return RedirectToAction("Index", "Survey");
                            }
                        }
                        else if (QuestIDGet[0] == "TextArea")
                        {
                            List<SurveyD1> listSurveyD1 = new List<SurveyD1>();
                            List<SurveyD2> listSurveyD2 = new List<SurveyD2>();

                            string cekType = ID.AnswerType[w];

                            var value = Array.FindAll(ID.AnswerTypeValue,
                                           element => element.Contains(cekType));

                            for (int u = 0; u < ID.AnswerTypeValue.Count(); u++)
                            {
                                if (ID.AnswerTypeValue[u] == cekType)
                                {
                                    SurveyD2 Answer = new SurveyD2()
                                    {
                                        SVY_ID = IDnya,
                                        //SVY_QuestionID = w + 1,
                                        SVY_Desc = Convert.ToString(ID.Answer[u]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim()
                                    };
                                    listSurveyD2.Add(Answer);
                                }
                            }

                            var updateValue = from s in listSurveyD2
                                              where s.SVY_ID == IDnya && s.SVY_Value == null
                                              select s;

                            List<SurveyD2> asd = updateValue.ToList();

                            for (int y = 0; y < asd.Count(); y++)
                            {
                                if (asd[y].SVY_Value == null)
                                {
                                    asd[y].SVY_Value = "12";
                                }
                            }

                            SurveyD1 Quest = new SurveyD1()
                            {
                                SVY_ID = IDnya,
                                //SVY_QuestionID = w+1,
                                SVY_Question = Convert.ToString(ID.Question[w]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim(),
                                SVY_Type = QuestIDGet[0],
                                SurveyD2 = asd
                            };

                            listSurveyD1.Add(Quest);

                            string M_SurveyD1_Json = JsonConvert.SerializeObject(listSurveyD1);

                            if (SurveyDB.AddUpdateQuest(M_SurveyD1_Json))
                            {
                            }
                            else
                            {
                                TempData["Error"] = "add Question for " + ID.SVY_Desc + " failed";
                                return RedirectToAction("Index", "Survey");
                            }
                        }
                        else if (QuestIDGet[0] == "Radio")
                        {
                            List<SurveyD1> listSurveyD1 = new List<SurveyD1>();
                            List<SurveyD2> listSurveyD2 = new List<SurveyD2>();

                            string cekType = ID.AnswerType[w];

                            var value = Array.FindAll(ID.AnswerTypeValue,
                                           element => element.Contains(cekType));

                            for (int u = 0; u < ID.AnswerTypeValue.Count(); u++)
                            {
                                if (ID.AnswerTypeValue[u] == cekType)
                                {
                                    if (ID.Answer[u] != "")
                                    {
                                        SurveyD2 Answer = new SurveyD2()
                                        {
                                            SVY_ID = IDnya,
                                            //SVY_QuestionID = w + 1,
                                            SVY_Desc = Convert.ToString(ID.Answer[u]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim()
                                        };
                                        listSurveyD2.Add(Answer);
                                    }
                                }
                            }

                            var updateValue = from s in listSurveyD2
                                              where s.SVY_ID == IDnya && s.SVY_Value == null
                                              select s;

                            List<SurveyD2> asd = updateValue.ToList();

                            for (int y = 0; y < asd.Count(); y++)
                            {
                                if (asd[y].SVY_Value == null)
                                {
                                    asd[y].SVY_Value = Convert.ToString(y + 1);
                                }
                            }

                            SurveyD1 Quest = new SurveyD1()
                            {
                                SVY_ID = IDnya,
                                //SVY_QuestionID = w+1,
                                SVY_Question = Convert.ToString(ID.Question[w]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim(),
                                SVY_Type = QuestIDGet[0],
                                SurveyD2 = asd
                            };

                            listSurveyD1.Add(Quest);

                            string M_SurveyD1_Json = JsonConvert.SerializeObject(listSurveyD1);

                            if (SurveyDB.AddUpdateQuest(M_SurveyD1_Json))
                            {
                            }
                            else
                            {
                                TempData["Error"] = "add Question for " + ID.SVY_Desc + " failed";
                                return RedirectToAction("Index", "Survey");
                            }
                        }
                        else if (QuestIDGet[0] == "Image")
                        {
                            List<SurveyD1> listSurveyD1 = new List<SurveyD1>();
                            List<SurveyD2> listSurveyD2 = new List<SurveyD2>();

                            string cekType = ID.AnswerType[w];

                            var value = Array.FindAll(ID.AnswerTypeValue,
                                           element => element.Contains(cekType));

                            for (int u = 0; u < ID.AnswerTypeValue.Count(); u++)
                            {
                                if (ID.AnswerTypeValue[u] == cekType)
                                {
                                    SurveyD2 Answer = new SurveyD2()
                                    {
                                        SVY_ID = IDnya,
                                        //SVY_QuestionID = w + 1,
                                        SVY_Desc = Convert.ToString(ID.Answer[u]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim()
                                    };
                                    listSurveyD2.Add(Answer);
                                }
                            }

                            var updateValue = from s in listSurveyD2
                                              where s.SVY_ID == IDnya && s.SVY_Value == null
                                              select s;

                            List<SurveyD2> asd = updateValue.ToList();

                            for (int y = 0; y < asd.Count(); y++)
                            {
                                if (asd[y].SVY_Value == null)
                                {
                                    asd[y].SVY_Value = "13";
                                }
                            }

                            SurveyD1 Quest = new SurveyD1()
                            {
                                SVY_ID = IDnya,
                                //SVY_QuestionID = w+1,
                                SVY_Question = Convert.ToString(ID.Question[w]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "").Trim(),
                                SVY_Type = QuestIDGet[0],
                                SurveyD2 = asd
                            };

                            listSurveyD1.Add(Quest);

                            string M_SurveyD1_Json = JsonConvert.SerializeObject(listSurveyD1);

                            if (SurveyDB.AddUpdateQuest(M_SurveyD1_Json))
                            {
                            }
                            else
                            {
                                TempData["Error"] = "add Question for " + ID.SVY_Desc + " failed";
                                return RedirectToAction("Index", "Survey");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["Error"] = ex.Message.ToString();
                    return RedirectToAction("Index", "Survey");
                }

                return RedirectToAction("Index", "Survey");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult AddSurvey()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult UpdateSurvey(int ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();

                TaskGet da = new TaskGet();
                da.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                da.GetType = "ID";
                da.Search = "";
                da.SVY_ID = ID;

                var OneDelivery = surveyDB.ListAll(da);
                ViewBag.StarNumber = OneDelivery.First().SurveyD1.Count;
                //return Json(OneDelivery, JsonRequestBehavior.AllowGet);

                return View(OneDelivery);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}