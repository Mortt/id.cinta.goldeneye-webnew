﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class StockReportController : Controller
    {
        StockReportDB StockReportDB = new StockReportDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Report / Sales and Stock";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataStockReport()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = StockReportDB.GetDataStockReport(param) ?? new List<JsonStockReport>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonStockReport Baru = new JsonStockReport();
                Baru.total = 0;
                Baru.rows = new List<rowsStockReport>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel(DateTime StartDate, DateTime EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(StartDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(EndDate.ToString("MM/dd/yyyy"));

                List<StockReport> allStockReport = StockReportDB.ListAll(g, c) ?? new List<StockReport>();

                var json_StockReport = JsonConvert.SerializeObject(allStockReport);
                DataTable dt_json_StockReport = (DataTable)JsonConvert.DeserializeObject(json_StockReport, (typeof(DataTable)));

                if (dt_json_StockReport.Rows.Count == 0)
                {
                    TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                    return RedirectToAction("index", "StockReport");
                }

                dt_json_StockReport.Columns.Add("Sales_Total", typeof(string));

                for (int d = 0; d < dt_json_StockReport.Rows.Count; d++)
                {
                    if (Convert.ToString(dt_json_StockReport.Rows[d]["Sales_LineTotal"]) != "")
                    {
                        dt_json_StockReport.Rows[d]["Sales_Total"] = dt_json_StockReport.Rows[d]["Sales_LineTotal"];
                        dt_json_StockReport.Rows[d]["Sisa_Stock"] = Convert.ToInt32(dt_json_StockReport.Rows[d]["Opname_stock"]) - Convert.ToInt32(dt_json_StockReport.Rows[d]["Sales_Qty"]);
                    }
                }

                dt_json_StockReport.Columns.Remove("Sales_LineTotal");
                dt_json_StockReport.Columns.Remove("Opname_stock");
                dt_json_StockReport.Columns.Remove("Opname_Suggestion");
                dt_json_StockReport.Columns.Remove("Opname_Delta");

                dt_json_StockReport.Columns["Outlet_Code"].SetOrdinal(0);
                dt_json_StockReport.Columns["Outlet_Name"].SetOrdinal(1);
                dt_json_StockReport.Columns["Product_Code"].SetOrdinal(2);
                dt_json_StockReport.Columns["Product_Name"].SetOrdinal(3);
                dt_json_StockReport.Columns["Employee_NIK"].SetOrdinal(4);
                dt_json_StockReport.Columns["Employee_Name"].SetOrdinal(5);
                dt_json_StockReport.Columns["Sales_TransDate"].SetOrdinal(6);
                //dt_json_StockReport.Columns["Opname_stock"].SetOrdinal(7);
                dt_json_StockReport.Columns["Sales_Qty"].SetOrdinal(7);
                dt_json_StockReport.Columns["Sisa_Stock"].SetOrdinal(8);
                dt_json_StockReport.Columns["Sales_Total"].SetOrdinal(9);
                //dt_json_StockReport.Columns["Opname_Suggestion"].SetOrdinal(10);
                //dt_json_StockReport.Columns["Opname_Delta"].SetOrdinal(11);

                dt_json_StockReport.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_json_StockReport, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Sales and Stock( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}