﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using Newtonsoft.Json;
using System.Data;
using Newtonsoft.Json.Linq;

namespace b2bWeb.Controllers
{
    public class ActivitiesController : Controller
    {
        ActivitiesDB ActivitiesDB = new ActivitiesDB();       

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Activities";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataActivities()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = ActivitiesDB.GetDataActivities(param) ?? new List<JsonActivities>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonActivities Baru = new JsonActivities();
                Baru.total = 0;
                Baru.rows = new List<rowsActivities>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));
                    string name = "Activities(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";
                    var Activities = ActivitiesDB.ListExport(g,c);

                    var json_ActivitiesDB = JsonConvert.SerializeObject(Activities);
                    DataTable dt_ActivitiesDB = (DataTable)JsonConvert.DeserializeObject(json_ActivitiesDB, (typeof(DataTable)));

                    if (dt_ActivitiesDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Activities");
                    }

                    dt_ActivitiesDB.Columns.Add("Activities_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_ActivitiesDB.Rows)
                    {
                        objCon["Activities_InsertOn1"] = Convert.ToString(objCon["Activities_InsertOn"]);
                    }

                    dt_ActivitiesDB.Columns.Remove("Employee_NIK");
                    dt_ActivitiesDB.Columns.Remove("Employee_Email");
                    //dt_ActivitiesDB.Columns.Remove("Visit_Image");                
                    dt_ActivitiesDB.Columns.Remove("Employee_LeaderNik");
                    dt_ActivitiesDB.Columns.Remove("Employee_Role");
                    dt_ActivitiesDB.Columns.Remove("Activities_ID");
                    dt_ActivitiesDB.Columns.Remove("Activities_Company_ID");
                    dt_ActivitiesDB.Columns.Remove("Activities_NIK");
                    dt_ActivitiesDB.Columns.Remove("Activities_Outlet_Code");
                    dt_ActivitiesDB.Columns.Remove("Activities_Respons_ID");
                    dt_ActivitiesDB.Columns.Remove("Activities_InsertBy");
                    dt_ActivitiesDB.Columns.Remove("Activities_UpdateOn");
                    dt_ActivitiesDB.Columns.Remove("Activities_Updateby");
                    dt_ActivitiesDB.Columns.Remove("Activities_IsDelete");
                    dt_ActivitiesDB.Columns.Remove("Activities_IsActive");
                    dt_ActivitiesDB.Columns.Remove("Outlet_Code");
                    dt_ActivitiesDB.Columns.Remove("Respond_Code");
                    dt_ActivitiesDB.Columns.Remove("Activities_InsertOn");

                    dt_ActivitiesDB.Columns.Add("Activities_InsertOn", typeof(string));
                    
                    foreach (DataRow objCon in dt_ActivitiesDB.Rows)
                    {
                        objCon["Activities_InsertOn"] = Convert.ToDateTime(objCon["Activities_InsertOn1"]).ToString("dd-MM-yyyy");
                    }

                    dt_ActivitiesDB.Columns.Remove("Activities_InsertOn1");

                    dt_ActivitiesDB.AcceptChanges();

                    dt_ActivitiesDB.Columns["Employee_Name"].SetOrdinal(0);
                    dt_ActivitiesDB.Columns["Activities_Image"].SetOrdinal(1);
                    dt_ActivitiesDB.Columns["Outlet_Name"].SetOrdinal(2);
                    dt_ActivitiesDB.Columns["Respond_Name"].SetOrdinal(3);
                    dt_ActivitiesDB.Columns["Activities_Desc"].SetOrdinal(4);
                    dt_ActivitiesDB.Columns["Activities_InsertOn"].SetOrdinal(5);
                    dt_ActivitiesDB.AcceptChanges();                    

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_ActivitiesDB;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        for (int a = 0; a < dt_ActivitiesDB.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_ActivitiesDB.Rows[a]["Activities_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_ActivitiesDB.Rows[a]["Activities_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_ActivitiesDB;

                        int row = 1;
                        int column = 2;
                        string TableImageName = "Activities_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }                    
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}