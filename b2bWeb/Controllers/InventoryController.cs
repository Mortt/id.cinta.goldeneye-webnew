﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b2bWeb.Controllers
{
    public class InventoryController : Controller
    {
        ItemDB itemDB = new ItemDB();
        InventoryDB inventoryDB = new InventoryDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Item = itemDB.ListAll() ?? new List<Item>();
                ViewBag.Title = "Master / Inventory";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataInventory()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = inventoryDB.GetDataInventory(param) ?? new List<JsonInventory>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonInventory Baru = new JsonInventory();
                Baru.total = 0;
                Baru.rows = new List<rowsInventory>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUpd(Inventory data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Inventory_Id == null)
                    {
                        try
                        {                            
                            data.Inventory_CompanyID = Convert.ToInt32(Session["CompanyID"]);
                            data.Inventory_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                inventoryDB.AddUpdDel(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {                            
                            data.Inventory_UpdateBy = Convert.ToString(Session["Username"]);

                            if (inventoryDB.AddUpdDel(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }                    
                }
                return RedirectToAction("Index", "Inventory");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Inventory data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Inventory_UpdateBy = Convert.ToString(Session["Username"]);
                    inventoryDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var Inventory = inventoryDB.ListAll();

                var json_Inventory = JsonConvert.SerializeObject(Inventory);
                DataTable dt_Inventory = (DataTable)JsonConvert.DeserializeObject(json_Inventory, (typeof(DataTable)));

                if (dt_Inventory.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Inventory");
                }

                dt_Inventory.Columns.Add("Inventory_InsertOn1", typeof(string));
                dt_Inventory.Columns.Add("Inventory_UpdateOn1", typeof(string));
                dt_Inventory.Columns.Add("Product_Code", typeof(string));

                foreach (DataRow objCon in dt_Inventory.Rows)
                {
                    objCon["Inventory_InsertOn1"] = Convert.ToString(objCon["Inventory_InsertOn"]);
                    objCon["Inventory_UpdateOn1"] = Convert.ToString(objCon["Inventory_UpdateOn"]);
                    objCon["Product_Code"] = Convert.ToString(objCon["Inventory_Item_Code"]);
                }

                dt_Inventory.Columns.Remove("Inventory_InsertOn");
                dt_Inventory.Columns.Remove("Inventory_UpdateOn");
                dt_Inventory.Columns.Remove("Inventory_CompanyID");
                dt_Inventory.Columns.Remove("Inventory_InsertBy");
                dt_Inventory.Columns.Remove("Inventory_UpdateBy");
                dt_Inventory.Columns.Remove("Inventory_Id");
                dt_Inventory.Columns.Remove("Inventory_Item_Code");

                dt_Inventory.Columns.Add("Inventory_InsertOn", typeof(string));
                dt_Inventory.Columns.Add("Inventory_UpdateOn", typeof(string));
                dt_Inventory.Columns.Add("Inventory_InsertBy", typeof(string));
                dt_Inventory.Columns.Add("Inventory_UpdateBy", typeof(string));

                for (int a = 0; a < dt_Inventory.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_Inventory.Rows[a]["Inventory_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_Inventory.Rows[a]["Inventory_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_Inventory.Rows[a]["Inventory_UpdateOn"] = dt_Inventory.Rows[a]["Inventory_UpdateOn1"];
                    }

                    dt_Inventory.Rows[a]["Inventory_InsertOn"] = dt_Inventory.Rows[a]["Inventory_InsertOn1"];
                    dt_Inventory.Rows[a]["Inventory_InsertBy"] = dt_Inventory.Rows[a]["Insert_Name"];
                    dt_Inventory.Rows[a]["Inventory_UpdateBy"] = dt_Inventory.Rows[a]["Update_Name"];
                }

                dt_Inventory.Columns.Remove("Inventory_InsertOn1");
                dt_Inventory.Columns.Remove("Inventory_UpdateOn1");
                dt_Inventory.Columns.Remove("Inventory_IsActive");
                dt_Inventory.Columns.Remove("Inventory_IsDelete");
                dt_Inventory.Columns.Remove("Insert_Name");
                dt_Inventory.Columns.Remove("Update_Name");

                dt_Inventory.AcceptChanges();

                dt_Inventory.Columns["Product_Code"].SetOrdinal(0);
                dt_Inventory.Columns["Product_Name"].SetOrdinal(1);
                dt_Inventory.Columns["Inventory_Unit_Code"].SetOrdinal(2);
                dt_Inventory.Columns["Product_Price"].SetOrdinal(3);
                dt_Inventory.Columns["Inventory_Qty"].SetOrdinal(4);
                dt_Inventory.Columns["Inventory_InsertOn"].SetOrdinal(5);
                dt_Inventory.Columns["Inventory_InsertBy"].SetOrdinal(6);
                dt_Inventory.Columns["Inventory_UpdateOn"].SetOrdinal(7);
                dt_Inventory.Columns["Inventory_UpdateBy"].SetOrdinal(8);

                dt_Inventory.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_Inventory, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Inventory.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();

                return RedirectToAction("index", "inventory");
            }
            else
            {
                return Redirect("/");
            }
        }
        
        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int j = 0; j < Request.Files.Count; j++)
                {
                    HttpPostedFileBase postedFile = Request.Files[j];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Product_Code", "Product_Name", "Unit_Code", "Qty" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            DataTable dt = dtNew.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull || string.IsNullOrWhiteSpace(field as string))).CopyToDataTable();

                                            dt.AcceptChanges();

                                            dt.Columns.Add("Inventory_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Inventory_InsertBy", typeof(string));
                                            dt.Columns.Add("Inventory_CompanyID", typeof(int));
                                            dt.Columns.Add("Inventory_IsActive", typeof(int));
                                            dt.Columns.Add("Inventory_IsDelete", typeof(int));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                dt.Rows[a]["Inventory_InsertOn"] = today;
                                                dt.Rows[a]["Inventory_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Inventory_CompanyID"] = Session["CompanyID"];
                                                dt.Rows[a]["Inventory_IsActive"] = 1;
                                                dt.Rows[a]["Inventory_IsDelete"] = 0;
                                                dt.Rows[a]["Unit_Code"] = Convert.ToString(dt.Rows[a]["Unit_Code"]).ToUpper();
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Inventory (Inventory_Item_Code nvarchar(30), Inventory_Qty int, Inventory_Unit_Code nvarchar(100), Inventory_CompanyID int, Inventory_InsertOn datetime, Inventory_InsertBy nvarchar(30), Inventory_IsActive bit, Inventory_IsDelete bit)", con);
                                                command.ExecuteNonQuery();

                                                var cmd = new SqlCommand("SELECT * FROM M_Item WHERE Item_CompanyID = " + Convert.ToInt32(Session["CompanyID"]) + "", con);
                                                DataTable dtItem = new DataTable();
                                                dtItem.Load(cmd.ExecuteReader());

                                                for (int i = 0; i < dt.Rows.Count; i++)
                                                {
                                                    DataRow[] foundCodeItem = dtItem.Select("Item_Code = '" + Convert.ToString(dt.Rows[i]["Product_Code"]) + "'");
                                                    if (foundCodeItem.Length > 0)
                                                    {
                                                    }
                                                    else
                                                    {
                                                        TempData["Error"] = "Maaf, Product_Code " + dt.Rows[i]["Product_Code"] + "tidak terdaftar pada sistem";
                                                        return RedirectToAction("Import", "Inventory");
                                                    }
                                                }

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Inventory";

                                                    sqlBulkCopy.ColumnMappings.Add("Product_Code", "Inventory_Item_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("Qty", "Inventory_Qty");
                                                    sqlBulkCopy.ColumnMappings.Add("Unit_Code", "Inventory_Unit_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("Inventory_CompanyID", "Inventory_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Inventory_InsertOn", "Inventory_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Inventory_InsertBy", "Inventory_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("Inventory_IsActive", "Inventory_IsActive");
                                                    sqlBulkCopy.ColumnMappings.Add("Inventory_IsDelete", "Inventory_IsDelete");
                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                //for (int i = 0; i < dt.Rows.Count; i++)
                                                //{
                                                //    command.CommandText = "DELETE FROM M_Inventory WHERE Inventory_Item_Code = '" + Convert.ToString(dt.Rows[i]["Product_Code"]) + "' AND Inventory_CompanyID = '" + Convert.ToInt32(Session["CompanyID"]) + "'";
                                                //    command.ExecuteNonQuery();
                                                //}


                                                string mapping = " MERGE M_Inventory t2 " + Environment.NewLine;
                                                mapping += " USING #M_Inventory t1 ON (t2.[Inventory_Item_Code] = t1.[Inventory_Item_Code] AND" + Environment.NewLine;
                                                mapping += " t2.[Inventory_CompanyID] = t1.[Inventory_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Inventory_Qty=t1.Inventory_Qty, t2.Inventory_Unit_Code=t1.Inventory_Unit_Code, t2.Inventory_UpdateOn=t1.Inventory_InsertOn, t2.Inventory_UpdateBy=t1.Inventory_InsertBy, t2.Inventory_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Inventory_Item_Code],[Inventory_Qty],[Inventory_Unit_Code],[Inventory_CompanyID],[Inventory_InsertOn],[Inventory_InsertBy],[Inventory_IsActive],[Inventory_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Inventory_Item_Code], t1.[Inventory_Qty], t1.[Inventory_Unit_Code], t1.[Inventory_CompanyID], t1.[Inventory_InsertOn], t1.[Inventory_InsertBy], t1.[Inventory_IsActive], t1.[Inventory_IsDelete]); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Inventory] ([Inventory_Item_Code],[Inventory_Qty],[Inventory_Unit_Code],[Inventory_CompanyID],[Inventory_InsertOn],[Inventory_InsertBy],[Inventory_IsActive],[Inventory_IsDelete])" +
                                                //                   " SELECT t1.[Inventory_Item_Code], t1.[Inventory_Qty], t1.[Inventory_Unit_Code], t1.[Inventory_CompanyID], t1.[Inventory_InsertOn], t1.[Inventory_InsertBy], t1.[Inventory_IsActive], t1.[Inventory_IsDelete] FROM #M_Inventory AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Inventory AS t2 WHERE t2.[Inventory_Item_Code] = t1.[Inventory_Item_Code] AND t2.[Inventory_CompanyID] = t1.[Inventory_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Inventory";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }                    
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Product_Code", typeof(string));
            table.Columns.Add("Product_Name", typeof(string));
            table.Columns.Add("Unit_Code", typeof(string));
            table.Columns.Add("Qty", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("PKT001", "PERMEN ASAM", "PCS", "10");
            table.Rows.Add("CKT001", "COKLAT MANIS", "PCS", "20");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Inventory.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Inventory");
        }

    }
}