﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using b2bWeb.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace b2bWeb.Controllers
{
    class parameterOke
    {
        public string CompanyID { set; get; }
        public string NIK { set; get; }
    };
    public class RouteOptimizeController : Controller
    {
        EmployeeDB employeeDB = new EmployeeDB();
        private IList<Employee> allEmployee = new List<Employee>();
        // GET: RouteOptimize
        public ActionResult Index(string name1)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                allEmployee = employeeDB.ListAll();

                var orderEmployee = from A in allEmployee
                                    orderby A.Employee_Name
                                    select A;
                               

                if (name1 != null)
                {
                    string[] GetData = name1.Split('-');


                    parameterOke bis = new parameterOke
                    {
                        CompanyID = GetData[1],
                        NIK = GetData[0]
                    };

                    string jsonBis = JsonConvert.SerializeObject(bis);

                    string bisBase64 = LoginController.Base64Encode(jsonBis);

                    string url = "http://development.loves.web.id/api/RouteTSP/081315412482/Y2ludGEuSUQ=/201810040442/MDgxMzE1NDEyNDgyXkNpbnRhLklELUwzeFkqME1UKjgzQCRUITNeMjAxODEwMDQwNDQy/" + bisBase64;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    WebResponse response = request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader sreader = new StreamReader(dataStream);
                    string responsereader = sreader.ReadToEnd();

                    //string output = JsonConvert.SerializeObject(responsereader);
                    if (GetData[2] == "1")
                    {
                        ViewData["name1"] = GetData[0];
                        ViewBag.Data1 = responsereader;
                    }
                    else
                    {
                        ViewData["name2"] = GetData[0];
                        ViewBag.Data = responsereader;
                    }
                }
                else
                {
                    ViewBag.Data1 = 0;
                    ViewBag.Data = 0;
                }

                ViewBag.Employee = orderEmployee;
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}