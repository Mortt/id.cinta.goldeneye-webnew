﻿using b2bWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace b2bWeb.Controllers
{
    public class LoginController : Controller
    {
        readonly string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                return View();
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Login objUser)
        {
            if (Session["Username"] == null && Session["CompanyID"] == null)
            {
                if (objUser != null)
                {
                    if (ModelState.IsValid)
                    {
                        using (SqlConnection con = new SqlConnection(cs))
                        {
                            con.Open();
                            SqlCommand com = new SqlCommand("SP_Get_Logins", con)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                            com.Parameters.AddWithValue("@username", objUser.Employee_NIK);
                            com.Parameters.AddWithValue("@password", Base64Encode(objUser.Employee_Password));
                            com.Parameters.AddWithValue("@type", "isi");
                            SqlDataReader rdr = com.ExecuteReader();
                            while (rdr.Read())
                            {
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                IEnumerable<Login> result = JsonConvert.DeserializeObject<IEnumerable<Login>>(rdr.GetString(0).ToString(), settings);

                                bool cek = result.First().Employee_isB2b;

                                if (cek == true)
                                {
                                    if (Session["Username"] == null)
                                    {
                                        Session["Username"] = result.First().Employee_NIK.ToString();
                                        //Session["Email"] = result.First().Employee_Email.ToString();
                                        Session["Name"] = result.First().Employee_Name.ToString();
                                        Session["Phone"] = result.First().Employee_NIK.ToString();
                                        Session["Paket"] = result.First().Company_Packet_ID.ToString();

                                        if (result.First().Employee_Photo != null)
                                        {
                                            Session["Photo"] = result.First().Employee_Photo.ToString();
                                        }

                                        Session["CompanyID"] = result.First().Employee_CompanyID.ToString();

                                        if (result.First().Company_Logo != null)
                                        {
                                            Session["Logo"] = result.First().Company_Logo.ToString();
                                        }
                                    }
                                }
                                else
                                {
                                    TempData["fail"] = "you can't login !";
                                    return Redirect("/");
                                }
                            }
                            con.Close();
                        }

                        if (Session["CompanyID"] != null)
                        {
                            using (SqlConnection com = new SqlConnection(cs))
                            {
                                com.Open();
                                SqlCommand company = new SqlCommand("SP_GET_COMPANY", com)
                                {
                                    CommandType = CommandType.StoredProcedure
                                };
                                company.Parameters.AddWithValue("Company_ID", Session["CompanyID"]);
                                SqlDataReader rdrCompany = company.ExecuteReader();
                                while (rdrCompany.Read())
                                {
                                    var settings = new JsonSerializerSettings
                                    {
                                        NullValueHandling = NullValueHandling.Ignore,
                                        MissingMemberHandling = MissingMemberHandling.Ignore
                                    };

                                    IEnumerable<Company> results = JsonConvert.DeserializeObject<IEnumerable<Company>>(rdrCompany.GetString(0).ToString(), settings);
                                    Session["Company_Name"] = results.First().Company_Name.ToString();

                                    ViewBag.HMenu = DashboardController.HeaderMenu();
                                    return RedirectToAction("index", "Dashboard");
                                }
                            }
                        }
                    }
                }
                else
                {
                    return Redirect("/");
                }

                TempData["fail"] = "username or password is incorrect";
                return View(objUser);
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }
        }

        public ActionResult Dashboard()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                ViewBag.Menu = MenuDB.GetMenu();
                return View("~/Views/Dashboard/Index.cshtml");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return Redirect("/");
        }
    }
}
