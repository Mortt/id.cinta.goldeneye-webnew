﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using System.Web.Security;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;

using ClosedXML.Excel;
using System.Drawing;
using System.Threading.Tasks;
using System.Text;
using DocumentFormat.OpenXml.Drawing;
using GemBox.Spreadsheet;
using System.Reflection;
using ClosedXML.Excel.Drawings;

namespace b2bWeb.Controllers
{
    public class DashboardController : Controller
    {
        DashboardDB DbdDB = new DashboardDB();

        // GET: Dashboard
        public ActionResult Index(string date)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {

                    string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                    if (DashboardController.Paket(actionName) == true)
                    {

                    }
                    else
                    {
                        return RedirectToAction("Error", "Dashboard");
                    }

                    ViewBag.HMenu = HeaderMenu();

                    DateTime Now = DateTime.Now;
                    string B;
                    string T;
                    if (date == null)
                    {
                        B = Convert.ToString(Now.Month);
                        T = Convert.ToString(Now.Year);
                        ViewBag.Tgl = Now.ToString("MM/yyyy");
                    }
                    else
                    {
                        B = Convert.ToDateTime(date).ToString("MM");
                        T = Convert.ToDateTime(date).ToString("yyyy");
                        ViewBag.Tgl = date;
                    }

                    string Chart = JsonConvert.SerializeObject(DbdDB.ListSellingChart(B, T));
                    DataTable Charts = JsonConvert.DeserializeObject<DataTable>(Chart);

                    if (Charts.Rows.Count != 0)
                    {
                        Charts.Columns.Add("Amount", typeof(decimal));

                        for (int a = 0; a < Charts.Rows.Count; a++)
                        {
                            Charts.Rows[a]["Amount"] = Convert.ToDecimal(Charts.Rows[a]["Total_AMOUNT"]).ToString("#,##0.00");
                        }

                        var Label = Charts.AsEnumerable()
                        .Select(s => s.Field<string>("Sales_Date"))
                        .ToList();

                        var data = Charts.AsEnumerable()
                        .Select(s => s.Field<Int64>("Total_QTY"))
                        .ToList();

                        var amount = Charts.AsEnumerable()
                       .Select(s => s.Field<decimal>("Amount"))
                       .ToList();

                        object Quantity;
                        object TotalAmount;

                        Quantity = Charts.Compute("Sum([" + "Total_QTY" + "])", string.Empty);
                        TotalAmount = Charts.Compute("Sum([" + "Total_AMOUNT" + "])", string.Empty);
                        var nfi = new NumberFormatInfo { NumberDecimalSeparator = ",", NumberGroupSeparator = "." };
                        TempData["Quantity"] = Convert.ToDecimal(Quantity).ToString("#,##0", nfi);
                        TempData["TotalAmount"] = Convert.ToDecimal(TotalAmount).ToString("#,##0.00", nfi);

                        ViewBag.Chart = JsonConvert.SerializeObject(Label);
                        ViewBag.Qty = JsonConvert.SerializeObject(data);
                        ViewBag.Amount = JsonConvert.SerializeObject(amount);
                        ViewBag.Dashboard = DbdDB.ListAll();

                        string ChartAll = JsonConvert.SerializeObject(DbdDB.ChartAll(B, T));
                        DataTable ChartsAll = JsonConvert.DeserializeObject<DataTable>(ChartAll);

                        var Label2 = ChartsAll.AsEnumerable()
                       .Select(s => s.Field<string>("x"))
                       .ToList();

                        var data2 = ChartsAll.AsEnumerable()
                        .Select(s => s.Field<Int64>("y"))
                        .ToList();

                        ViewBag.All = JsonConvert.SerializeObject(Label2);
                        ViewBag.ChartAll = JsonConvert.SerializeObject(data2);

                        string ChartProduct = JsonConvert.SerializeObject(DbdDB.ChartProduct(B, T));
                        DataTable ChartsProduct = JsonConvert.DeserializeObject<DataTable>(ChartProduct);

                        var Label3 = ChartsProduct.AsEnumerable()
                        .Select(s => s.Field<string>("item_Name").ToLower())
                        .ToList();

                        var data3 = ChartsProduct.AsEnumerable()
                        .Select(s => s.Field<Int64>("Total_QTY"))
                        .ToList();

                        var data4 = ChartsProduct.AsEnumerable()
                       .Select(s => s.Field<double>("Total_AMOUNT"))
                       .ToList();

                        ViewBag.PItem = JsonConvert.SerializeObject(Label3);
                        ViewBag.PQty = JsonConvert.SerializeObject(data3);
                        ViewBag.PAmount = JsonConvert.SerializeObject(data4);
                    }
                    else
                    {
                        ViewBag.Dashboard = DbdDB.ListAll();
                    }

                    string TransDisplay = JsonConvert.SerializeObject(DbdDB.ChartTrans("DISPLAY", B, T));
                    DataTable ChartsDisplay = JsonConvert.DeserializeObject<DataTable>(TransDisplay);
                    if (ChartsDisplay.Rows.Count > 0)
                    {
                        var LabelDisplay = ChartsDisplay.AsEnumerable()
                            .Select(s => s.Field<string>("Display_Date"))
                            .ToList();

                        var dataDisplay = ChartsDisplay.AsEnumerable()
                            .Select(s => s.Field<Int64>("Total_Display"))
                            .ToList();

                        ViewBag.LabelDisplay = JsonConvert.SerializeObject(LabelDisplay);
                        ViewBag.dataDisplay = JsonConvert.SerializeObject(dataDisplay);
                    }
                    else
                    {
                        ViewBag.LabelDisplay = "[0]";
                        ViewBag.dataDisplay = "[0]";
                    }

                    string TransStock = JsonConvert.SerializeObject(DbdDB.ChartTrans("STOCK", B, T));
                    DataTable ChartsStock = JsonConvert.DeserializeObject<DataTable>(TransStock);
                    if (ChartsStock.Rows.Count > 0)
                    {
                        var LabelStock = ChartsStock.AsEnumerable()
                            .Select(s => s.Field<string>("Stock_Date"))
                            .ToList();

                        var dataStock = ChartsStock.AsEnumerable()
                            .Select(s => s.Field<Int64>("Total_Stock"))
                            .ToList();

                        ViewBag.LabelStock = JsonConvert.SerializeObject(LabelStock);
                        ViewBag.dataStock = JsonConvert.SerializeObject(dataStock);
                    }
                    else
                    {
                        ViewBag.LabelStock = "[0]";
                        ViewBag.dataStock = "[0]";
                    }

                    string TransComp = JsonConvert.SerializeObject(DbdDB.ChartTrans("COMPETITOR", B, T));
                    DataTable ChartsComp = JsonConvert.DeserializeObject<DataTable>(TransComp);
                    if (ChartsComp.Rows.Count > 0)
                    {
                        var LabelComp = ChartsComp.AsEnumerable()
                            .Select(s => s.Field<string>("Competitor_Date"))
                            .ToList();

                        var dataComp = ChartsComp.AsEnumerable()
                            .Select(s => s.Field<Int64>("Total_Competitor"))
                            .ToList();

                        ViewBag.LabelComp = JsonConvert.SerializeObject(LabelComp);
                        ViewBag.dataComp = JsonConvert.SerializeObject(dataComp);
                    }
                    else
                    {
                        ViewBag.LabelComp = "[0]";
                        ViewBag.dataComp = "[0]";
                    }

                    string TransFeedback = JsonConvert.SerializeObject(DbdDB.ChartTrans("FEEDBACK", B, T));
                    DataTable ChartsFeedback = JsonConvert.DeserializeObject<DataTable>(TransFeedback);
                    if (ChartsFeedback.Rows.Count > 0)
                    {
                        var LabelFeedback = ChartsFeedback.AsEnumerable()
                            .Select(s => s.Field<string>("Feedback_Date"))
                            .ToList();

                        var dataFeedback = ChartsFeedback.AsEnumerable()
                            .Select(s => s.Field<Int64>("Total_Feedback"))
                            .ToList();

                        ViewBag.LabelFeedback = JsonConvert.SerializeObject(LabelFeedback);
                        ViewBag.dataFeedback = JsonConvert.SerializeObject(dataFeedback);
                    }
                    else
                    {
                        ViewBag.LabelFeedback = "[0]";
                        ViewBag.dataFeedback = "[0]";
                    }

                    string TransSampling = JsonConvert.SerializeObject(DbdDB.ChartTrans("SAMPLING", B, T));
                    DataTable ChartsSampling = JsonConvert.DeserializeObject<DataTable>(TransSampling);
                    if (ChartsSampling.Rows.Count > 0)
                    {
                        var LabelSampling = ChartsSampling.AsEnumerable()
                            .Select(s => s.Field<string>("Sampling_Date"))
                            .ToList();

                        var dataSampling = ChartsSampling.AsEnumerable()
                            .Select(s => s.Field<Int64>("Total_QTY"))
                            .ToList();

                        ViewBag.LabelSampling = JsonConvert.SerializeObject(LabelSampling);
                        ViewBag.dataSampling = JsonConvert.SerializeObject(dataSampling);
                    }
                    else
                    {
                        ViewBag.LabelSampling = "[0]";
                        ViewBag.dataSampling = "[0]";
                    }

                    string TransCoaching = JsonConvert.SerializeObject(DbdDB.ChartTrans("COACHING", B, T));
                    DataTable ChartsCoaching = JsonConvert.DeserializeObject<DataTable>(TransCoaching);
                    if (ChartsCoaching.Rows.Count > 0)
                    {
                        var LabelCoaching = ChartsCoaching.AsEnumerable()
                            .Select(s => s.Field<string>("Coaching_Date"))
                            .ToList();

                        var dataCoaching = ChartsCoaching.AsEnumerable()
                            .Select(s => s.Field<Int64>("Total_Coaching"))
                            .ToList();

                        ViewBag.LabelCoaching = JsonConvert.SerializeObject(LabelCoaching);
                        ViewBag.dataCoaching = JsonConvert.SerializeObject(dataCoaching);
                    }
                    else
                    {
                        ViewBag.LabelCoaching = "[0]";
                        ViewBag.dataCoaching = "[0]";
                    }

                    return View();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }

            }
            else
            {
                return Redirect("/");
            }
        }
    

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return Redirect("/");
        }

        public static bool UploadToAzure(HttpPostedFileBase image)
        {
            if (image != null && image.ContentLength > 0)
            {
                try
                {
                    CloudStorageAccount storageAccount = new CloudStorageAccount(
                            new StorageCredentials(
                            "foodiegost17",
                            "tQ1Ua9gQHYuocpXW+iQzZea2utz9k0Zh69JZttDVkFGyIXbGeOlQNI7Slk6AVQOd3ElFnRRrx237ZW0N7wdPqg=="),
                            "core.windows.net",
                            true);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    CloudBlobContainer container = blobClient.GetContainerReference("images");

                    var blob = blobClient.GetContainerReference("images").GetBlockBlobReference(image.FileName);

                    if (blob.Exists())
                    {
                        blob.Delete();
                        BlobContainerPermissions permissions = container.GetPermissions();
                        permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                        container.SetPermissions(permissions);

                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(image.FileName);

                        // Upload the file
                        blockBlob.UploadFromStreamAsync(image.InputStream);
                    }
                    else
                    {
                        BlobContainerPermissions permissions = container.GetPermissions();
                        permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                        container.SetPermissions(permissions);

                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(image.FileName);

                        // Upload the file
                        blockBlob.UploadFromStreamAsync(image.InputStream);
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public static bool UploadToAzure(string image, string c)
        {
            if (image != "")
            {
                try
                {
                    CloudStorageAccount storageAccount = new CloudStorageAccount(
                            new StorageCredentials(
                            "foodiegost17",
                            "tQ1Ua9gQHYuocpXW+iQzZea2utz9k0Zh69JZttDVkFGyIXbGeOlQNI7Slk6AVQOd3ElFnRRrx237ZW0N7wdPqg=="),
                            "core.windows.net",
                            true);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    CloudBlobContainer container = blobClient.GetContainerReference("images");

                    var bytes = Convert.FromBase64String(image.Split(',')[1]);

                    var blob = blobClient.GetContainerReference("images").GetBlockBlobReference(c);

                    if (blob.Exists())
                    {
                        blob.Delete();
                        BlobContainerPermissions permissions = container.GetPermissions();
                        permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                        container.SetPermissions(permissions);

                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(c);

                        // Upload the file
                        //blockBlob.UploadFromStreamAsync(image.InputStream);
                        using (var stream = new MemoryStream(bytes))
                        {
                            blockBlob.UploadFromStream(stream);
                        }
                    }
                    else
                    {
                        BlobContainerPermissions permissions = container.GetPermissions();
                        permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                        container.SetPermissions(permissions);

                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(c);

                        // Upload the file
                        //blockBlob.UploadFromStreamAsync(image.InputStream);
                        using (var stream = new MemoryStream(bytes))
                        {
                            blockBlob.UploadFromStream(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public List<Menu> Menu
        {
            get
            {
                List<Menu> menu = MenuDB.GetMenu();
                return menu;
            }
        }

        public ActionResult Error()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                ViewBag.HMenu = HeaderMenu();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public static List<HeaderMenu> HeaderMenu()
        {
            LoginPacket baru = new LoginPacket
            {
                PacketID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Paket"]),
                Search = ""
            };

            List<HeaderMenu> Menu = MenuDB.ListMenu(baru);
            return Menu;
        }

        public static bool Paket(string Cekmenu)
        {

            var cekMenu = HeaderMenu();


            //var names = cekMenu.Select(x => x.Menu.First().Menu_Controller).Distinct().ToList();
            var names = from o in cekMenu
                        from l in o.Menu
                        where l.Menu_Controller.Contains(Cekmenu.ToLower())
                        select l.Menu_Controller;
            //productCodes = productCodes.Distinct();

            if (names.AsEnumerable().Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ActionResult ToExcel(string name)
        {
            DataTable dt = TempData["DT_JSON"] as DataTable;

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(dt, "Sheet1");
            //wr.Tables.FirstOrDefault().Theme = XLTableTheme.TableStyleLight1;
            //wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"" + name + ".xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();

            return View("");
        }

        public ActionResult ToExcelImage(string name, string TableImageName, int row, int column)
        {
            DataTable dt = TempData["DT_JSON"] as DataTable;            

            if(dt.Rows.Count == 0)
            {
                return RedirectToAction("Error", "Dashboard");
            }

            using (XLWorkbook wb = new XLWorkbook())
            {                 
                var WorkSheet = wb.Worksheets.Add(dt, "Sheet1");
                //IXLWorksheet WorkSheet = wb.Worksheets.Add(dt, WorkSheetName);

                //WorkSheet.Tables.FirstOrDefault().Theme = XLTableTheme.TableStyleLight1;
                //WorkSheet.Tables.FirstOrDefault().ShowAutoFilter = false;

                for (int i = 0; i < dt.Rows.Count; i++)
                {   
                    CloudStorageAccount storageAccount = new CloudStorageAccount(
                     new StorageCredentials(
                     "foodiegost17",
                     "tQ1Ua9gQHYuocpXW+iQzZea2utz9k0Zh69JZttDVkFGyIXbGeOlQNI7Slk6AVQOd3ElFnRRrx237ZW0N7wdPqg=="),
                     "core.windows.net",
                     true);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    CloudBlobContainer container = blobClient.GetContainerReference("images");

                    var blob = blobClient.GetContainerReference("images").GetBlockBlobReference(Convert.ToString(dt.Rows[i][TableImageName]));
                    Bitmap bitmap;

                    var ms = new MemoryStream();

                    using (ms)
                    {
                        if (blob.Exists())
                        {
                            blob.DownloadToStream(ms);

                            using (Image img = Image.FromStream(ms))
                            {
                                int h = 100;
                                int w = 100;

                                using (Bitmap b = new Bitmap(img, new Size(w, h)))
                                {
                                    using (MemoryStream ms2 = new MemoryStream())
                                    {
                                        b.Save(ms2, System.Drawing.Imaging.ImageFormat.Jpeg);
                                        bitmap = new Bitmap(new MemoryStream(ms2.ToArray()));

                                        WorkSheet.Row(i + 1 + row).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                                        var row1 = WorkSheet.Row(i + 1 + row);
                                        row1.Height = 100;

                                        var clm1 = WorkSheet.Column(column);
                                        clm1.Width = 19;

                                        WorkSheet.Cell(i + 1 + row, column).Value = "";                                        

                                        WorkSheet.AddPicture(bitmap)
                                            .MoveTo(WorkSheet.Cell(i + 1 + row, column))
                                            .Scale(1.26, true);
                                    }
                                }
                            }
                        }
                    }
                }

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=\"" + name + ".xlsx\"");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            return View("");
        }

        public static string RandomString(string name)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            return finalString;
        }
    }
}
