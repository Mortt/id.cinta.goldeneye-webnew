﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Collections.Generic;

namespace b2bWeb.Controllers
{
    public class AchievementEmployeeController : Controller
    {
        AchievementEmployeeDB AchievementEmployeeDB = new AchievementEmployeeDB();
        DateTime Now = DateTime.Now;

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Report / Achievement Employee";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataAchievementEmployee()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = AchievementEmployeeDB.GetDataAchievementEmployee(param) ?? new List<JsonAchievementEmployee>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonAchievementEmployee Baru = new JsonAchievementEmployee();
                Baru.total = 0;
                Baru.rows = new List<rowsAchievementEmployee>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));

                    string name = "AchievementEmployee(" + g.ToString("MMMM/yyyy")+ ")";

                    var Expdate = AchievementEmployeeDB.ListExport(g);

                    var json_expdateDB = JsonConvert.SerializeObject(Expdate);
                    DataTable dt_A = (DataTable)JsonConvert.DeserializeObject(json_expdateDB, (typeof(DataTable)));

                    if (dt_A.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "AchievementEmployee");
                    }

                    dt_A.Columns.Add("Sales_Total", typeof(decimal));
                    dt_A.Columns.Add("Achievement(%)", typeof(decimal));
                    dt_A.Columns.Add("Target_Periode", typeof(string));
                    dt_A.Columns.Add("Target_Amount", typeof(decimal));

                    foreach (DataRow objCon in dt_A.Rows)
                    {
                        objCon["Sales_Total"] = Convert.ToString(objCon["Sales_LineTotal"]);
                        objCon["Achievement(%)"] = Convert.ToDecimal(objCon["Achievements"]).ToString("0.00");
                        objCon["Target_Amount"] = Convert.ToDecimal(objCon["TargetByEmployee_Amount"]);
                        objCon["Target_Periode"] = Convert.ToDateTime(objCon["TargetByEmployee_Periode"]).ToString("MM/yyyy");
                    }

                    dt_A.Columns.Remove("Achievements");
                    dt_A.Columns.Remove("Sales_LineTotal");
                    dt_A.Columns.Remove("TargetByEmployee_ID");
                    dt_A.Columns.Remove("Sales_QTY");
                    dt_A.Columns.Remove("Bulan");
                    dt_A.Columns.Remove("Tahun");

                    dt_A.Columns.Remove("TargetByEmployee_Amount");
                    dt_A.Columns.Remove("TargetByEmployee_Periode");
                    dt_A.Columns.Remove("TargetByEmployee_EmployeeNIK");
                    dt_A.Columns.Remove("TargetByEmployee_CompanyID");
                    dt_A.Columns.Remove("Target_EmployeeName");

                    dt_A.Columns["Employee_NIK"].SetOrdinal(0);
                    dt_A.Columns["Employee_Name"].SetOrdinal(1);
                    dt_A.Columns["Target_Periode"].SetOrdinal(2);
                    dt_A.Columns["Sales_Total"].SetOrdinal(3);
                    dt_A.Columns["Target_Amount"].SetOrdinal(4);
                    dt_A.Columns["Achievement(%)"].SetOrdinal(5);

                    XLWorkbook wbook = new XLWorkbook();
                    var wr = wbook.Worksheets.Add(dt_A, "Sheet1");
                    wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                    wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                    // Prepare the response
                    HttpResponseBase httpResponse = Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Provide you file name here
                    httpResponse.AddHeader("content-disposition", "attachment;filename=\"Achievement_Employee( " + name + " ).xlsx\"");

                    // Flush the workbook to the Response.OutputStream
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        wbook.SaveAs(memoryStream);
                        memoryStream.WriteTo(httpResponse.OutputStream);
                        memoryStream.Close();
                    }
                    httpResponse.End();
                    return View();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}