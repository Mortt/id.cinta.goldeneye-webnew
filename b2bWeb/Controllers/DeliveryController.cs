﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using System.Data;
using ClosedXML.Excel;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace b2bWeb.Controllers
{
    public class DeliveryController : Controller
    {
        ItemDB itemDB = new ItemDB();
        EmployeeDB employeeDB = new EmployeeDB();
        OutletDB otlDB = new OutletDB();
        DeliveryDB deliveryDB = new DeliveryDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Item = itemDB.ListAll() ?? new List<Item>();
                ViewBag.Employee = employeeDB.ListAll() ?? new List<Employee>();
                ViewBag.Outlet = otlDB.ListAll() ?? new List<Outlet>();
                ViewBag.Title = "Transaction / Delivery";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataDelivery()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = deliveryDB.GetDataDelivery(param) ?? new List<JsonDelivery>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonDelivery Baru = new JsonDelivery();
                Baru.total = 0;
                Baru.rows = new List<rowsDelivery>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        List<_Z> listZ = new List<_Z>();

        [HttpPost]        
        public ActionResult AddUp(Delivery ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (ID.TransID == null)
                    {
                        try
                        {
                            ID.Numpang = ID.Numpang.Replace("%7C", "|").Replace("Numpang=", "");
                            ID.QTY = ID.QTY.Replace("QTY=", "");
                            
                            char[] splitNumpang = new char[] { '&' };
                            string[] splitResultNumpang = ID.Numpang.Split(splitNumpang);

                            
                            char[] splitQty = new char[] { '&' };
                            string[] splitResultQty = ID.QTY.Split(splitQty);

                            for (int y = 0; y < splitResultNumpang.Length; y++)
                            {
                                char[] splitNumpang2 = new char[] { '|' };
                                string[] splitResultNumpang2 = splitResultNumpang[y].Split(splitNumpang2);
                                
                                if (splitResultQty[y] != "")
                                {
                                    int Item_ID = Convert.ToInt32(splitResultNumpang2[0]);
                                    int Item_Price = Convert.ToInt32(splitResultNumpang2[1]);
                                    int Quantity = Convert.ToInt32(splitResultQty[y]);

                                    _Z itm = new _Z()
                                    {
                                        DO_Item_ID = Convert.ToInt32(Item_ID),
                                        DO_Qty = Convert.ToInt32(Quantity),
                                        DO_Price = Item_Price,
                                        DO_LineTotal = Item_Price * Convert.ToInt32(Quantity),
                                    };
                                    listZ.Add(itm);
                                }
                            }                            

                            Delivery dv = new Delivery
                            {
                                DO_CompanyID = Convert.ToInt16(Session["CompanyID"]),
                                DO_OutletCode = ID.DO_OutletCode,
                                DO_PIC = ID.DO_PIC,
                                DO_Transdate = ID.DO_Transdate,
                                Username = Convert.ToString(Session["Username"]),
                                DO_Total_Amount = listZ.Sum(f => f.DO_LineTotal),
                                DO_Total_Qty = listZ.Sum(f => f.DO_Qty),
                                Z = listZ
                            };

                            if (DeliveryDB.Cek(dv) > 0)
                            {
                                return Json("has", JsonRequestBehavior.AllowGet);                                
                            }

                            if(DeliveryDB.AddUpdate(dv))
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            ID.Numpang = ID.Numpang.Replace("%7C", "|").Replace("Numpang=", "");
                            ID.QTY = ID.QTY.Replace("QTY=", "");

                            char[] splitNumpang = new char[] { '&' };
                            string[] splitResultNumpang = ID.Numpang.Split(splitNumpang);


                            char[] splitQty = new char[] { '&' };
                            string[] splitResultQty = ID.QTY.Split(splitQty);

                            for (int y = 0; y < splitResultNumpang.Length; y++)
                            {
                                char[] splitNumpang2 = new char[] { '|' };
                                string[] splitResultNumpang2 = splitResultNumpang[y].Split(splitNumpang2);

                                if (splitResultQty[y] != "")
                                {
                                    int Item_ID = Convert.ToInt32(splitResultNumpang2[0]);
                                    int Item_Price = Convert.ToInt32(splitResultNumpang2[1]);
                                    int Quantity = Convert.ToInt32(splitResultQty[y]);

                                    _Z itm = new _Z()
                                    {
                                        DO_Item_ID = Convert.ToInt32(Item_ID),
                                        DO_Qty = Convert.ToInt32(Quantity),
                                        DO_Price = Item_Price,
                                        DO_LineTotal = Item_Price * Convert.ToInt32(Quantity),
                                    };
                                    listZ.Add(itm);
                                }
                            }

                            Delivery dv = new Delivery
                            {
                                TransCode = ID.TransCode,
                                TransID = ID.TransID,
                                DO_CompanyID = Convert.ToInt16(Session["CompanyID"]),
                                DO_OutletCode = ID.DO_OutletCode,
                                DO_PIC = ID.DO_PIC,
                                DO_Transdate = ID.DO_Transdate,
                                Username = Convert.ToString(Session["Username"]),
                                DO_Total_Amount = listZ.Sum(f => f.DO_LineTotal),
                                DO_Total_Qty = listZ.Sum(f => f.DO_Qty),
                                Z = listZ
                            };

                            if (DeliveryDB.AddUpdate(dv))
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetbyID(Delivery data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                Parameter param = new Parameter();

                param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
                param.PageNumber = Convert.ToInt32(0);
                param.RowspPage = Convert.ToInt32(0);
                param.Search = Convert.ToString(data.TransID);
                param.Get_Type = "ID";
                
                var Result = deliveryDB.GetDataDeliveryDetails(param) ?? new List<_Z>();
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Delivery data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.DO_UpdateBy = Convert.ToString(Session["Username"]);

                    if (DeliveryDB.Delete(data))
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Delivery(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";
                    DataTable Delivery = deliveryDB.ListExport(g, c);


                    if (Delivery.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "delivery");
                    }

                    Delivery.Columns.Remove("TransID");
                    Delivery.Columns.Remove("Item_Image1");
                    Delivery.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = Delivery;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        for (int a = 0; a < Delivery.Rows.Count; a++)
                        {
                            string url = Convert.ToString(Delivery.Rows[a]["IMAGE"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));                            
                            Delivery.Rows[a]["IMAGE"] = fileName;
                        }

                        TempData["DT_JSON"] = Delivery;

                        int row = 1;
                        int column = 8;
                        string TableImageName = "IMAGE";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("DO_Date", typeof(string));
            table.Columns.Add("Employee_NIK", typeof(string));
            table.Columns.Add("Employee_Name", typeof(string));
            table.Columns.Add("Outlet_Code", typeof(string));
            table.Columns.Add("Outlet_Name", typeof(string));
            table.Columns.Add("Product_Code", typeof(string));
            table.Columns.Add("Product_Name", typeof(string));
            table.Columns.Add("Quantity", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("10/30/2019", "083824271074", "Nursidik", "3-1000143", "kedai bossque", "G0004", "CAKE COKLAT", "24");
            table.Rows.Add("10/30/2019", "083824271074", "Nursidik", "3-1000143", "kedai bossque", "PKT001", "PERMEN LOLLIPOP", "25");
            table.Rows.Add("10/30/2019", "083824271074", "Nursidik", "3-1000144", "sasuki", "G0004", "CAKE COKLAT", "24");
            table.Rows.Add("10/30/2019", "083824271074", "Nursidik", "3-1000144", "sasuki", "PKT001", "PERMEN LOLLIPOP", "25");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Delivery.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (var memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
                //return File(memoryStream, "application/vnd.ms-excel", "Template_Area.xlsx");
            }
            httpResponse.End();
            return RedirectToAction("Index", "Delivery");
            //return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];

                    ViewBag.HMenu = DashboardController.HeaderMenu();
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);
                                            connExcel.Close();
                                        }

                                        string[] selectedColumns = new[] { "DO_Date", "Employee_NIK", "Outlet_Code", "Product_Code", "Quantity" };
                                        DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                        var query = from myRow in dtNew.AsEnumerable() select myRow;

                                        DataTable dt = query.CopyToDataTable();
                                        dt.AcceptChanges();

                                        Parameter param = new Parameter
                                        {
                                            Get_Type = "CODE",                                            
                                            Company_ID = Convert.ToInt32(Session["CompanyID"]),
                                        };

                                        var Item = itemDB.ListAllByCode(param);

                                        string json_Item = JsonConvert.SerializeObject(Item);
                                        DataTable dt_Item = JsonConvert.DeserializeObject<DataTable>(json_Item);

                                        //add item price
                                        dt.Columns.Add("Item_ID", typeof(int));
                                        dt.Columns.Add("Product_Price", typeof(double));
                                        dt.Columns.Add("Total_Amount_PerLine", typeof(double));

                                        for (int y=0; y < dt.Rows.Count; y++)
                                        {
                                            for(int u=0; u < dt_Item.Rows.Count; u++)
                                            {
                                                if(Convert.ToString(dt.Rows[y]["Product_Code"]) == Convert.ToString(dt_Item.Rows[u]["Item_Code"]))
                                                {
                                                    dt.Rows[y]["Item_ID"] = dt_Item.Rows[u]["Item_ID"];
                                                    dt.Rows[y]["Product_Price"] = dt_Item.Rows[u]["Item_Price"];
                                                    dt.Rows[y]["Total_Amount_PerLine"] = Convert.ToInt32(dt.Rows[y]["Quantity"]) * Convert.ToInt32(dt.Rows[y]["Product_Price"]);
                                                }
                                            }
                                        }

                                        //get distinct data for header
                                        var distinct_outlet_employee = dt.AsEnumerable()
                                           .Select(s => new
                                           {
                                               DO_Date = s.Field<string>("DO_Date"),
                                               Employee_NIK = s.Field<string>("Employee_NIK"),
                                               Outlet_Code = s.Field<string>("Outlet_Code")
                                           })
                                           .Distinct().ToList();

                                        string json_distinct_outlet_employee = JsonConvert.SerializeObject(distinct_outlet_employee);
                                        DataTable dt_distinct_outlet_employee = JsonConvert.DeserializeObject<DataTable>(json_distinct_outlet_employee);

                                        dt_distinct_outlet_employee.Columns.Add("Total_Qty", typeof(string));
                                        dt_distinct_outlet_employee.Columns.Add("Total_Amount", typeof(string));

                                        for (int l = 0; l < dt.Rows.Count; l++)
                                        {
                                            for (int o = 0; o < dt_distinct_outlet_employee.Rows.Count; o++)
                                            {
                                                if ((Convert.ToString(dt_distinct_outlet_employee.Rows[o]["DO_Date"]) == Convert.ToString(dt.Rows[l]["DO_Date"])) && (Convert.ToString(dt_distinct_outlet_employee.Rows[o]["Employee_NIK"]) == Convert.ToString(dt.Rows[l]["Employee_NIK"])) && (Convert.ToString(dt_distinct_outlet_employee.Rows[o]["Outlet_Code"]) == Convert.ToString(dt.Rows[l]["Outlet_Code"])))
                                                {
                                                    if (o < dt_distinct_outlet_employee.Rows.Count && Convert.ToString(dt_distinct_outlet_employee.Rows[o]["Total_Qty"]) == "")
                                                    {
                                                        dt_distinct_outlet_employee.Rows[o]["Total_Qty"] = Convert.ToInt32(dt.Rows[l]["Quantity"]) + Convert.ToInt32(dt.Rows[l + 1]["Quantity"]);
                                                        dt_distinct_outlet_employee.Rows[o]["Total_Amount"] = Convert.ToInt32(dt.Rows[l]["Total_Amount_PerLine"]) + Convert.ToInt32(dt.Rows[l + 1]["Total_Amount_PerLine"]);
                                                    }
                                                }
                                            }
                                        }

                                        dt_distinct_outlet_employee.Columns.Add("DO_CompanyID");
                                        dt_distinct_outlet_employee.Columns.Add("DO_InsertOn");
                                        dt_distinct_outlet_employee.Columns.Add("DO_InsertBy");
                                        dt_distinct_outlet_employee.Columns.Add("DO_IsDelete");
                                        dt_distinct_outlet_employee.Columns.Add("DO_SendingStatus");
                                        dt_distinct_outlet_employee.Columns.Add("TransCode");

                                        for (int m=0; m < dt_distinct_outlet_employee.Rows.Count; m++)
                                        {
                                            dt_distinct_outlet_employee.Rows[m]["DO_CompanyID"] = Session["CompanyID"];
                                            dt_distinct_outlet_employee.Rows[m]["DO_InsertOn"] = today;
                                            dt_distinct_outlet_employee.Rows[m]["DO_InsertBy"] = Session["Username"];
                                            dt_distinct_outlet_employee.Rows[m]["DO_IsDelete"] = "FALSE";
                                            dt_distinct_outlet_employee.Rows[m]["DO_SendingStatus"] = "FALSE";
                                            dt_distinct_outlet_employee.Rows[m]["DO_Date"] = Convert.ToString(dt_distinct_outlet_employee.Rows[m]["DO_Date"]).Replace("-", "/");
                                        }

                                        DataColumn col = new DataColumn("DO_Transdate", typeof(DateTime));
                                        dt_distinct_outlet_employee.Columns.Add(col);
                                        col.Expression = "DO_Date";

                                        var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                        using (SqlConnection con = new SqlConnection(conStringDB))
                                        {
                                            con.Open();                                            

                                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                            {
                                                sqlBulkCopy.DestinationTableName = "Tr_DOH";

                                                sqlBulkCopy.ColumnMappings.Add("DO_Transdate", "DO_Transdate");
                                                sqlBulkCopy.ColumnMappings.Add("Outlet_Code", "DO_OutletCode");
                                                sqlBulkCopy.ColumnMappings.Add("Employee_NIK", "DO_PIC");
                                                sqlBulkCopy.ColumnMappings.Add("Total_Amount", "DO_Total_Amount");
                                                sqlBulkCopy.ColumnMappings.Add("Total_Qty", "DO_Total_Qty");
                                                sqlBulkCopy.ColumnMappings.Add("DO_IsDelete", "DO_IsDelete");
                                                sqlBulkCopy.ColumnMappings.Add("DO_CompanyID", "DO_CompanyID");
                                                sqlBulkCopy.ColumnMappings.Add("DO_InsertOn", "DO_InsertOn");
                                                sqlBulkCopy.ColumnMappings.Add("DO_InsertBy", "DO_InsertBy");
                                                sqlBulkCopy.ColumnMappings.Add("DO_SendingStatus", "DO_SendingStatus");

                                                sqlBulkCopy.WriteToServer(dt_distinct_outlet_employee);
                                            }

                                            //get TR_DOH
                                            var command = new SqlCommand("select top " + dt_distinct_outlet_employee.Rows.Count + " * from Tr_DOH order by TransID desc", con);                                           
                                            DataTable TableHasil = new DataTable();
                                            TableHasil.Load(command.ExecuteReader());

                                            //add transID column
                                            dt.Columns.Add("TransID");
                                            dt.Columns.Add("DO_No");

                                            for (int h=0; h< TableHasil.Rows.Count; h++)
                                            {
                                                for (int x = 0; x < dt.Rows.Count; x++)
                                                {
                                                    if ((Convert.ToDateTime(TableHasil.Rows[h]["DO_Transdate"]).ToString("MM/dd/yyyy") == Convert.ToString(dt.Rows[x]["DO_Date"])) && (Convert.ToString(TableHasil.Rows[h]["DO_PIC"]) == Convert.ToString(dt.Rows[x]["Employee_NIK"])) && (Convert.ToString(TableHasil.Rows[h]["DO_OutletCode"]) == Convert.ToString(dt.Rows[x]["Outlet_Code"])))
                                                    {
                                                        //update TransCode
                                                        var com = new SqlCommand("UPDATE Tr_DOH SET TransCode = dbo.Encrypt2String("+ TableHasil.Rows[h]["TransID"] + ") WHERE TransID = "+ TableHasil.Rows[h]["TransID"] + "", con);
                                                        com.ExecuteNonQuery();

                                                        dt.Rows[x]["TransID"] = TableHasil.Rows[h]["TransID"];
                                                        dt.Rows[x]["DO_No"] = 0;
                                                    }
                                                }
                                            }                                                                                        

                                            //bulkcopyy to TR_DOD
                                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                            {
                                                sqlBulkCopy.DestinationTableName = "Tr_DOD";

                                                sqlBulkCopy.ColumnMappings.Add("TransID", "TransID");
                                                sqlBulkCopy.ColumnMappings.Add("DO_No", "DO_No");
                                                sqlBulkCopy.ColumnMappings.Add("Item_ID", "DO_Item_ID");
                                                sqlBulkCopy.ColumnMappings.Add("Quantity", "DO_Qty");
                                                sqlBulkCopy.ColumnMappings.Add("Product_Price", "DO_Price");
                                                sqlBulkCopy.ColumnMappings.Add("Total_Amount_PerLine", "DO_LineTotal");

                                                sqlBulkCopy.WriteToServer(dt);
                                            }

                                            con.Close();
                                        }                                        
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();

                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}