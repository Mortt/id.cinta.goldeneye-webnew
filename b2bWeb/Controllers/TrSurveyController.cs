﻿using MvcPaging;
using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ClosedXML.Excel;
using System.IO;

namespace b2bWeb.Controllers
{
    public class TrSurveyController : Controller
    {
        SurveyDB surveyDB = new SurveyDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Survey";
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataTrSurvey()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = surveyDB.GetDataSurvey(param) ?? new List<JsonSurvey>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonSurvey Baru = new JsonSurvey();
                Baru.total = 0;
                Baru.rows = new List<rowsSurvey>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DetailsSurvey(int ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();

                TaskGet da = new TaskGet();
                da.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                da.GetType = "TR";
                da.Search = "";
                da.SVY_ID = ID;

                TaskGet db = new TaskGet();
                db.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                db.GetType = "ID";
                db.Search = "";
                db.SVY_ID = ID;

                List<Survey> Ques = surveyDB.ListAll(db) ?? new List<Survey>();
                List<DetailsSurvey> Dts = surveyDB.ListDetails(da) ?? new List<DetailsSurvey>();

                if(Ques.Count == 0)
                {
                    TempData["Error"] = "No Question";
                    return RedirectToAction("index", "TrSurvey");
                }

                for (int i = 0; i < Ques.Count; i++)
                {
                    for (int h = 0; h < Ques[i].SurveyD1.Count; h++)
                    {
                        for (int j = 0; j < Ques[i].SurveyD1[h].SurveyD2.Count; j++)
                        {
                            int _no = 1;
                            for (int t = 0; t < Dts.Count; t++)
                            {
                                for (int u = 0; u < Dts[t].DS.Count; u++)
                                {
                                    for (int y = 0; y < Dts[t].DS[u].DS1.Count; y++)
                                    {
                                        if (Dts[t].DS[u].DS1[y].SVY_Type == "Spinner")
                                        {
                                            
                                            if (Ques[i].SurveyD1[h].SurveyD2[j].SVY_QuestionID == Dts[t].DS[u].DS1[y].SVY_QuestionID)
                                            {
                                                string ValueNya = Dts[t].DS[u].DS1[y].SVY_Value.Trim();
                                                if (Ques[i].SurveyD1[h].SurveyD2[j].SVY_Value == ValueNya)
                                                {
                                                    if (Ques[i].SurveyD1[h].SurveyD2[j].SVY_Desc == "")
                                                    {

                                                    }
                                                    else
                                                    {
                                                        Ques[i].SurveyD1[h].SurveyD2[j].SVY_Total = _no;
                                                        _no += 1;
                                                    }
                                                }
                                            }
                                                
                                        }

                                        if (Dts[t].DS[u].DS1[y].SVY_Type == "Radio")
                                        {
                                            if (Ques[i].SurveyD1[h].SurveyD2[j].SVY_QuestionID == Dts[t].DS[u].DS1[y].SVY_QuestionID)
                                            {
                                                string valueNya = Dts[t].DS[u].DS1[y].SVY_Value.Trim();
                                                if (Ques[i].SurveyD1[h].SurveyD2[j].SVY_Value == valueNya)
                                                {
                                                    if (Ques[i].SurveyD1[h].SurveyD2[j].SVY_Desc == "")
                                                    {

                                                    }
                                                    else
                                                    {
                                                        Ques[i].SurveyD1[h].SurveyD2[j].SVY_Total = _no;
                                                        _no += 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (Dts.Count == 0)
                {
                    ViewBag.TotalRespond = 0;
                    ViewBag.Subject = "";
                }
                else
                {
                    ViewBag.TotalRespond = Dts[0].DS.Count;
                    ViewBag.Subject = Convert.ToString(Dts[0].SVY_Subject);
                }

                ViewBag.Question = Ques;
                ViewBag.Narasumber = Dts;
                ViewBag.ID = ID;                

                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Excel(int ID, string Subject)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                DataTable A = surveyDB.ToExcel(ID);

                A.AcceptChanges();
                
                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(A, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\""+ Subject + ".xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}