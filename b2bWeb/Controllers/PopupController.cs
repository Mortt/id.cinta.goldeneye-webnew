﻿using System;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using System.Data;
using System.Collections.Generic;

namespace b2bWeb.Controllers
{
    public class PopupController : Controller
    {
        PopupDB PopupDB = new PopupDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / Popup";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataPopup()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = PopupDB.GetDataPopup(param) ?? new List<JsonPopup>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonPopup Baru = new JsonPopup();
                Baru.total = 0;
                Baru.rows = new List<rowsPopup>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUp(Popup data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Pop_ID == null)
                    {
                        if(data.Pop_Tipe == 1)
                        {
                            string checkbase64 = "base64";
                            if (Request.Form["Pop_Pict"].Contains(checkbase64))
                            {
                                string base64 = Request.Form["Pop_Pict"];
                                string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                string b = DashboardController.RandomString("B2bGoldeneye");
                                string c = "popupImages1" + a + b + ".png";
                                data.Pop_Pict = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                DashboardController.UploadToAzure(base64, c);
                            }
                            else if (Request.Form["Pop_Pict"] != "")
                            {
                                data.Pop_Pict = data.Pop_Pict;
                            }
                            else
                            {
                                data.Pop_Pict = null;
                            }

                            data.Pop_Text = null;
                        }
                        else
                        {
                            data.Pop_Pict = null;
                        }                        
                                               
                        try
                        {
                            data.Pop_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Pop_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                //if (PopupDB.Cek(ID) == 2)
                                //{
                                //    TempData["Error"] = "Maaf, anda tidak bisa menambahkan popup "+ ID.Pop_Module +" lebih dari 2";
                                //    return RedirectToAction("Index", "Popup");
                                //}
                                //else
                                //{
                                PopupDB.AddUpd(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                                //}
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (data.Pop_Tipe == 1)
                            {
                                string checkbase64 = "base64";
                                if (Request.Form["Pop_Pict"].Contains(checkbase64))
                                {
                                    string base64 = Request.Form["Pop_Pict"];
                                    string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                    string b = DashboardController.RandomString("B2bGoldeneye");
                                    string c = "popupImages1" + a + b + ".png";
                                    data.Pop_Pict = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                    DashboardController.UploadToAzure(base64, c);
                                }
                                else if (Request.Form["Pop_Pict"] != "")
                                {
                                    data.Pop_Pict = data.Pop_Pict;
                                }
                                else
                                {
                                    data.Pop_Pict = null;
                                }

                                data.Pop_Text = null;
                            }
                            else
                            {
                                data.Pop_Pict = null;
                            }

                            data.Pop_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Pop_EditBy = Convert.ToString(Session["Username"]);

                            //if (PopupDB.Cek(ID) == 2)
                            //{
                            //    TempData["Error"] = "Maaf, anda tidak bisa menambahkan popup " + ID.Pop_Module + " lebih dari 2";
                            //    return RedirectToAction("Index", "Popup");
                            //}
                            //else
                            if (PopupDB.AddUpd(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return RedirectToAction("Index", "Popup");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Popup data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Pop_EditBy = Convert.ToString(Session["Username"]);
                    PopupDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}