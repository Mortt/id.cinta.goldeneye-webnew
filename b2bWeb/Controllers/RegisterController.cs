﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web.Security;
using Newtonsoft.Json;
using System.Data;
using System.Net.Mail;
using System.Net;

namespace b2bWeb.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            List<Paket> GetAll = PaketDB.ListAll() ?? new List<Paket>();
            var json = JsonConvert.SerializeObject(GetAll);
            DataTable dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
            dt.Rows.Add(new Object[]{
                0,
                "Pilih Paket",
           });

            DataView view = dt.DefaultView;
            view.Sort = "Packet_ID ASC";
            DataTable sortedID = view.ToTable();

            ViewBag.Data = ToSelectList(sortedID, "Packet_ID", "Packet_Name");

            return View();
        }

        [HttpPost]
        public ActionResult Index(Company data)
        {
            if (ModelState.IsValid)
            {
                if(Request.Form["Images_Name"] != "")
                {
                    string base64 = Request.Form["Images_Name"];
                    string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                    string b = RandomString("sidik");
                    string c = "logo" + a + b + ".png";
                    data.Company_Logo = "https://foodiegost17.blob.core.windows.net/images/" + c;
                    DashboardController.UploadToAzure(base64, c);
                }
                else
                {
                    data.Company_Logo = "";
                }
                
                data.Company_Name = data.Company_Name.ToUpper();
                data.Company_Insertby = data.Company_No_Telp;

                int Oke = PaketDB.addCompany(data);
                if (Oke > 1)
                {
                    string smtpAddress4 = "smtp.gmail.com";
                    int portNumber4 = 587;
                    bool enableSSL4 = true;
                    string emailFrom4 = "goldeneye.cintaid@gmail.com";
                    string password4 = "cinta2018";

                    string subject4 = "Customer baru untuk Goldeneye.id";

                    //string emailTo = "xns2427@gmail.com";
                    string emailTo = "cs@cinta.id";
                    string body = "<html>" +
                        "Kepada Yth" +
                        "<br>" +
                        "Farras Tsany" +
                        "<br>" +
                        "Di tempat" +
                        "<br>" +
                        "<br>" +
                        "Diberitahukan bahwa terdapat Customer baru untuk Goldeneye.id yaitu:" +
                        "<br>" +
                        "Nama Perusahaan        : "+data.Company_Name+";" +
                        "<br>" +
                        "Email Perusahaan       : " + data.Company_Email + ";" +
                        "<br>" +
                        "No Telp/WA Perusahaan   : " + data.Company_No_Telp + "/ "+data.Company_Wa+";" +
                        "<br>" +
                        "Alamat Perusahaan   : " + data.Company_Address + ";" +
                        "<br>" +
                        "<br>" +
                        "Mohon untuk segera di tindaklanjuti." +
                        "<br>" +
                        "Sekian pemberitahuan dari kami, Atas perhatiannya diucapkan terima kasih.." +
                        "<br>" +
                        "<br>" +
                        "Best Regards" +
                        "<br>" +
                        "<br>" +
                        "PT. CIPTA INOVATIF ABADI" +
                        "</html>"
                        ;

                    MailMessage YesterdayRhtmail = new MailMessage
                    {
                        From = new MailAddress(emailFrom4, "Goldeneye.id")
                    };

                    string EmailnyaCC = "farras.tsany@cinta.id,ian.paul@cinta.id,nursidik@cinta.id";
                    
                    string[] CCId = EmailnyaCC.Split(',');

                    foreach (string CCEmail in CCId)
                    {
                        YesterdayRhtmail.CC.Add(new MailAddress(CCEmail));
                    }

                    YesterdayRhtmail.To.Add(new MailAddress(emailTo));
                    YesterdayRhtmail.Subject = subject4;
                    YesterdayRhtmail.Body = body;
                    YesterdayRhtmail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(smtpAddress4, portNumber4))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom4, password4);
                        smtp.EnableSsl = enableSSL4;
                        smtp.Send(YesterdayRhtmail);
                    }

                    TempData["Success"] = "oke";
                    TempData["company"] = data.Company_Name;
                    TempData["email"] = data.Company_Email;
                    return RedirectToAction("Validation", "register");
                }
            }

            List<Paket> GetAll = PaketDB.ListAll() ?? new List<Paket>();
            var json = JsonConvert.SerializeObject(GetAll);
            DataTable dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

            dt.Rows.Add(new Object[]{
                0,
                "Pilih Paket",
           });

            DataView view = dt.DefaultView;
            view.Sort = "Packet_ID ASC";
            DataTable sortedID = view.ToTable();

            ViewBag.Data = ToSelectList(sortedID, "Packet_ID", "Packet_Name");

            return View();
        }

        public ActionResult Validation()
        {
            return View();
        }

        public static string RandomString(string name)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            return finalString;
        }

        [NonAction]
        public SelectList ToSelectList(DataTable table, string valueField, string textField)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            foreach (DataRow row in table.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row[textField].ToString(),
                    Value = row[valueField].ToString()
                });
            }

            return new SelectList(list, "Value", "Text");
        }
    }
}