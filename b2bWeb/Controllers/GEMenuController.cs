﻿using b2bWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b2bWeb.Controllers
{
    public class GEMenuController : Controller
    {
        GoldenEyeMenuDB GEMenu = new GoldenEyeMenuDB();
        Menu_MobileDB MM = new Menu_MobileDB();
        RoleDB Role = new RoleDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                { }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / GoldenEye Menu";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.GEMenu = GEMenu.ListAll();
                ViewBag.Role = Role.ListAll() ?? new List<Role>();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataMobile_Menu()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = MM.GetDataMenuMobile(param) ?? new List<JsonMobile_Menu>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonMobile_Menu Baru = new JsonMobile_Menu();
                Baru.total = 0;
                Baru.rows = new List<rowsMobile_Menu>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUd(Mobile_Menu data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.MM_ID == null)
                    {
                        try
                        {
                            data.MM_Company_ID = Convert.ToInt16(Session["CompanyID"]);
                            data.MM_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (MM.Cek(data) > 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                MM.AddUp(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.MM_UpdateBy = Convert.ToString(Session["Username"]);

                            if (MM.Cek(data) > 0)
                            {
                                return Json("has", JsonRequestBehavior.AllowGet);
                            }

                            if(MM.AddUp(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Mobile_Menu data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.MM_UpdateBy = Convert.ToString(Session["Username"]);
                    MM.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Redirect("/");
            }
        }
    }
}