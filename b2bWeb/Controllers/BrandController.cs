﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace b2bWeb.Controllers
{
    public class BrandController : Controller
    {

        BrandDB brdDB = new BrandDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                { }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / Brand(Category)";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataBrand()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = brdDB.GetDataBrand(param) ?? new List<JsonBrand>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonBrand Baru = new JsonBrand();
                Baru.total = 0;
                Baru.rows = new List<rowsBrand>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUd(Brand data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (brdDB.Cek(data) != 1)
                    {
                        string checkbase64 = "base64";
                        if (Request.Form["Category_Image"].Contains(checkbase64))
                        {
                            string base64 = Request.Form["Category_Image"];
                            string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                            string b = DashboardController.RandomString("B2bGoldeneye");
                            string c = "categoryImages" + a + b + ".png";
                            data.Category_Image = "https://foodiegost17.blob.core.windows.net/images/" + c;
                            DashboardController.UploadToAzure(base64, c);
                        }
                        else if (Request.Form["Outlet_Picture"] != "")
                        {
                            data.Category_Image = data.Category_Image;
                        }
                        else
                        {
                            data.Category_Image = null;
                        }

                        try
                        {
                            data.Category_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Category_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                brdDB.AddUpdate(data);
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            string checkbase64 = "base64";
                            if (Request.Form["Category_Image"].Contains(checkbase64))
                            {
                                string base64 = Request.Form["Category_Image"];
                                string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                string b = DashboardController.RandomString("B2bGoldeneye");
                                string c = "categoryImages" + a + b + ".png";
                                data.Category_Image = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                DashboardController.UploadToAzure(base64, c);
                            }
                            else if (Request.Form["Category_Image"] != "")
                            {
                                data.Category_Image = data.Category_Image;
                            }
                            else
                            {
                                data.Category_Image = null;
                            }

                            data.Category_UpdateBy = Convert.ToString(Session["Username"]);

                            if (brdDB.AddUpdate(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Brand data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Category_UpdateBy = Convert.ToString(Session["Username"]);
                    brdDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_brdDB = JsonConvert.SerializeObject(brdDB.ListAll());
                DataTable dt_abrdDB = (DataTable)JsonConvert.DeserializeObject(json_brdDB, (typeof(DataTable)));

                if (dt_abrdDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Brand");
                }

                dt_abrdDB.Columns.Add("Category_InsertOn1", typeof(string));
                dt_abrdDB.Columns.Add("Category_UpdateON1", typeof(string));


                foreach (DataRow objCon in dt_abrdDB.Rows)
                {
                    objCon["Category_InsertOn1"] = Convert.ToString(objCon["Category_InsertOn"]);
                    objCon["Category_UpdateON1"] = Convert.ToString(objCon["Category_UpdateON"]);
                }

                dt_abrdDB.Columns.Remove("jml");
                dt_abrdDB.Columns.Remove("Category_CompanyID");
                //dt_abrdDB.Columns.Remove("Category_Image");
                dt_abrdDB.Columns.Remove("Images_Name");
                dt_abrdDB.Columns.Remove("Category_IsDelete");
                dt_abrdDB.Columns.Remove("Category_InsertOn");
                dt_abrdDB.Columns.Remove("Category_UpdateON");

                dt_abrdDB.Columns.Add("Category_InsertOn", typeof(string));
                dt_abrdDB.Columns.Add("Category_UpdateON", typeof(string));
                dt_abrdDB.Columns.Add("Category_InsertByName", typeof(string));
                dt_abrdDB.Columns.Add("Category_UpdateByName", typeof(string));


                for (int a = 0; a < dt_abrdDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_abrdDB.Rows[a]["Category_UpdateON1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_abrdDB.Rows[a]["Category_UpdateON"] = "-";
                    }
                    else
                    {
                        dt_abrdDB.Rows[a]["Category_UpdateON"] = dt_abrdDB.Rows[a]["Category_UpdateON1"];
                    }
                }

                foreach (DataRow objCon in dt_abrdDB.Rows)
                {
                    objCon["Category_InsertOn"] = Convert.ToString(objCon["Category_InsertOn1"]);
                    objCon["Category_InsertByName"] = Convert.ToString(objCon["Insert_Name"]);
                    objCon["Category_UpdateByName"] = Convert.ToString(objCon["Update_Name"]);
                }

                dt_abrdDB.Columns.Remove("Category_InsertOn1");
                dt_abrdDB.Columns.Remove("Category_UpdateON1");
                dt_abrdDB.Columns.Remove("Insert_Name");
                dt_abrdDB.Columns.Remove("Update_Name");

                dt_abrdDB.AcceptChanges();

                dt_abrdDB.DefaultView.Sort = "Category_InsertOn desc";
                dt_abrdDB = dt_abrdDB.DefaultView.ToTable();

                dt_abrdDB.Columns["Category_Code"].SetOrdinal(0);
                dt_abrdDB.Columns["Category_Name"].SetOrdinal(1);
                dt_abrdDB.Columns["Category_Image"].SetOrdinal(2);
                dt_abrdDB.Columns["Category_Desc"].SetOrdinal(3);
                dt_abrdDB.Columns["Category_IsActive"].SetOrdinal(4);
                dt_abrdDB.Columns["Category_InsertBy"].SetOrdinal(5);
                dt_abrdDB.Columns["Category_InsertByName"].SetOrdinal(6);
                dt_abrdDB.Columns["Category_InsertOn"].SetOrdinal(7);
                dt_abrdDB.Columns["Category_UpdateBy"].SetOrdinal(8);
                dt_abrdDB.Columns["Category_UpdateByName"].SetOrdinal(9);
                dt_abrdDB.Columns["Category_UpdateON"].SetOrdinal(10);

                dt_abrdDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_abrdDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Brand.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();

                return RedirectToAction("Index", "brand");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Category_Code", "Category_Name", "Category_Desc" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Category_Code") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Category_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Category_InsertBy", typeof(string));
                                            dt.Columns.Add("Category_CompanyID", typeof(int));
                                            dt.Columns.Add("Category_Code1", typeof(string));
                                            dt.Columns.Add("Category_Name1", typeof(string));
                                            dt.Columns.Add("Category_Desc1", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {

                                                dt.Rows[a]["Category_Code1"] = Convert.ToString(dt.Rows[a]["Category_Code"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Category_Name1"] = Convert.ToString(dt.Rows[a]["Category_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Category_Desc1"] = Convert.ToString(dt.Rows[a]["Category_Desc"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Category_InsertOn"] = today;
                                                dt.Rows[a]["Category_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Category_CompanyID"] = Session["CompanyID"];
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Category (Category_Code nvarchar(30), Category_Name nvarchar(100), Category_Desc nvarchar(MAX), Category_CompanyID int, Category_InsertOn datetime, Category_InsertBy nvarchar(30))", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Category";

                                                    sqlBulkCopy.ColumnMappings.Add("Category_Code1", "Category_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("Category_Name1", "Category_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("Category_Desc1", "Category_Desc");
                                                    sqlBulkCopy.ColumnMappings.Add("Category_CompanyID", "Category_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Category_InsertOn", "Category_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Category_InsertBy", "Category_InsertBy");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Category t2 " + Environment.NewLine;
                                                mapping += " USING #M_Category t1 ON (t2.[Category_Code] = t1.[Category_Code] AND" + Environment.NewLine;
                                                mapping += " t2.[Category_CompanyID] = t1.[Category_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Category_Name=t1.Category_Name, t2.Category_Desc=t1.Category_Desc, t2.Category_UpdateON=t1.Category_InsertOn, t2.Category_UpdateBy=t1.Category_InsertBy, t2.Category_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Category_Code], [Category_Name], [Category_Desc], [Category_CompanyID], [Category_InsertOn], [Category_InsertBy], [Category_IsActive], [Category_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Category_Code], t1.[Category_Name], t1.[Category_Desc], t1.[Category_CompanyID], t1.[Category_InsertOn], t1.[Category_InsertBy], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Category] ([Category_Code], [Category_Name], [Category_Desc], [Category_CompanyID], [Category_InsertOn], [Category_InsertBy])" +
                                                //                   " SELECT t1.[Category_Code], t1.[Category_Name], t1.[Category_Desc], t1.[Category_CompanyID], t1.[Category_InsertOn], t1.[Category_InsertBy] FROM #M_Category AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Category AS t2 WHERE t2.[Category_Code] = t1.[Category_Code] AND t2.[Category_CompanyID] = t1.[Category_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Category";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();

                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Category_Code", typeof(string));
            table.Columns.Add("Category_Name", typeof(string));
            table.Columns.Add("Category_Desc", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("C12", "COKLAT", "");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Brand.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Item");
        }
    }
}