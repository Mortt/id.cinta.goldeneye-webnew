﻿using b2bWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace b2bWeb.Controllers
{
    public class PerformanceController : Controller
    {
        AttendanceDurationDB PerformanceDB = new AttendanceDurationDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Report / Performance";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataPerformance()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('/');
                param.startDate = ValueOke[0] + "/01/" + ValueOke[1];
                //param.endDate = ValueOke[1];
            }
            else
            {
                string Bln = DateTime.Now.Month.ToString();
                string Thn = DateTime.Now.Year.ToString();
                string all = Bln + "/1/" + Thn;
                param.startDate = all;
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Get_Type = "Day";
            param.Search = searchText;

            var Result = PerformanceDB.GetDataAttendanceDuration(param) ?? new List<JsonAttendanceDuration>();

            if (Result.Count != 0)
            {
                for (int i = 0; i < Result[0].rows.Count; i++){
                    if (Result[0].rows[i].TC_Qty == null)
                    {                        
                        
                    }
                    else
                    {
                        int a = Convert.ToInt32(Result[0].rows[i].Average);
                        int b = Convert.ToInt32(Result[0].rows[i].TC_Qty);
                        double DevideResult = (double)a / b;
                        double TimesResult = DevideResult * 100;
                        Result[0].rows[i].Achievement = Convert.ToString(TimesResult);
                    }                  
                }

                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonAttendanceDuration Baru = new JsonAttendanceDuration();
                Baru.total = 0;
                Baru.rows = new List<rowsAttendanceDuration>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel(DateTime startDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));

                    string name = "Performance(" + g.ToString("MMMM/yyyy") + ")";

                    var Visit = PerformanceDB.ListExport(g);

                    var json_Data = JsonConvert.SerializeObject(Visit);
                    DataTable dt_Data = (DataTable)JsonConvert.DeserializeObject(json_Data, (typeof(DataTable)));

                    if (dt_Data.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Performance");
                    }

                    dt_Data.Columns.Remove("Visit_ID");
                    dt_Data.Columns.Remove("Visit_CompanyID");
                    dt_Data.Columns.Remove("Visit_InsertOn");
                    dt_Data.Columns.Remove("Visit_NIK");
                    dt_Data.Columns.Remove("Visit_Checkout");
                    dt_Data.Columns.Remove("Visit_Checkin");
                    dt_Data.Columns.Remove("Duration");

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        if (Convert.ToString(dt_Data.Rows[i]["TC_Qty"]) == "")
                        {

                        }
                        else
                        {
                            int a = Convert.ToInt32(dt_Data.Rows[i]["Average"]);
                            int b = Convert.ToInt32(dt_Data.Rows[i]["TC_Qty"]);
                            double DevideResult = (double)a / b;
                            double TimesResult = DevideResult * 100;
                            string Value = String.Format("{0:0.00}", TimesResult);
                            dt_Data.Rows[i]["Achievement"] = Value + " %";
                        }
                    }

                    dt_Data.Columns["Month"].ColumnName = "PERIODE";
                    dt_Data.Columns["Employee_Name"].ColumnName = "EMPLOYEE NAME";
                    dt_Data.Columns["Role_Name"].ColumnName = "POSITION";
                    dt_Data.Columns["TotalDay"].ColumnName = "TOTAL WORKING DAYS";                    
                    dt_Data.Columns["Total_CheckIn"].ColumnName = "TOTAL VISIT";
                    dt_Data.Columns["Average"].ColumnName = "AVERAGE VISIT";
                    dt_Data.Columns["TC_Qty"].ColumnName = "TARGET CALL/VISIT";
                    dt_Data.Columns["Achievement"].ColumnName = "ACHIEVEMENT";

                    dt_Data.Columns["POSITION"].SetOrdinal(2);

                    dt_Data.AcceptChanges();
                    TempData["DT_JSON"] = dt_Data;
                    return RedirectToAction("ToExcel", "Dashboard", new { name });

                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}