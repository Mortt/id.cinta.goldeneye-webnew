﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class SpendingController : Controller
    {
        SpendingDB spanDB = new SpendingDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Spending";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataSpending()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = spanDB.GetDataSpending(param) ?? new List<JsonSpending>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonSpending Baru = new JsonSpending();
                Baru.total = 0;
                Baru.rows = new List<rowsSpending>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Spending(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";
                    List<Spending> allSpanding = spanDB.ListExport(g, c);

                    var json_Spanding = JsonConvert.SerializeObject(allSpanding);
                    DataTable dt_json_Spanding = (DataTable)JsonConvert.DeserializeObject(json_Spanding, (typeof(DataTable)));

                    if (dt_json_Spanding.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Spending");
                    }

                    dt_json_Spanding.Columns.Add("Spending_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_json_Spanding.Rows)
                    {
                        objCon["Spending_InsertOn1"] = Convert.ToString(objCon["Spending_InsertOn"]);
                    }
                                       
                    dt_json_Spanding.Columns.Remove("Spending_ID");
                    dt_json_Spanding.Columns.Remove("Spending_ReasonID");
                    dt_json_Spanding.Columns.Remove("Spending_CompanyID");
                    dt_json_Spanding.Columns.Remove("Spending_Isdelete");
                    dt_json_Spanding.Columns.Remove("Spending_UpdateOn");
                    dt_json_Spanding.Columns.Remove("Spending_UpdateBy");
                    dt_json_Spanding.Columns.Remove("Spending_InsertBy");
                    dt_json_Spanding.Columns.Remove("Update_Spending_Employee_Name");
                    dt_json_Spanding.Columns.Remove("Spending_InsertOn");

                    dt_json_Spanding.Columns.Add("Spending_InsertOn", typeof(string));
                    dt_json_Spanding.Columns.Add("Spending_InsertBy", typeof(string));

                    foreach (DataRow objCon in dt_json_Spanding.Rows)
                    {
                        objCon["Spending_InsertOn"] = Convert.ToString(objCon["Spending_InsertOn1"]);
                        objCon["Spending_InsertBy"] = Convert.ToString(objCon["Insert_Spending_Employee_Name"]);
                    }

                    dt_json_Spanding.Columns.Remove("Spending_InsertOn1");
                    dt_json_Spanding.Columns.Remove("Insert_Spending_Employee_Name");
                    dt_json_Spanding.AcceptChanges();

                    dt_json_Spanding.Columns["Spending_InsertOn"].SetOrdinal(0);
                    dt_json_Spanding.Columns["Spending_InsertBy"].SetOrdinal(1);
                    dt_json_Spanding.Columns["Spending_Amount"].SetOrdinal(2);
                    dt_json_Spanding.Columns["Spending_Image"].SetOrdinal(3);
                    dt_json_Spanding.Columns["Spending_Respond"].SetOrdinal(4);
                    dt_json_Spanding.Columns["Spending_Desc"].SetOrdinal(5);
                    //dt_json_Spanding.Columns["Spending_UpdateOn"].SetOrdinal(6);

                    dt_json_Spanding.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_json_Spanding;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        for (int a = 0; a < dt_json_Spanding.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_json_Spanding.Rows[a]["Spending_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_json_Spanding.Rows[a]["Spending_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_json_Spanding;

                        int row = 1;
                        int column = 4;
                        string TableImageName = "Spending_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

    }
}