﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace b2bWeb.Controllers
{
    public class OutletController : Controller
    {
        AreaDB arDB = new AreaDB();
        OutletDB otlDB = new OutletDB();
        GroupOutletDB groupOutetDB = new GroupOutletDB();
        RecurringDB recurringDB = new RecurringDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / Outlet";                
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataOutlet()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = otlDB.GetDataOutlet(param) ?? new List<JsonOutlet>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonOutlet Baru = new JsonOutlet();
                Baru.total = 0;
                Baru.rows = new List<rowsOutlet>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUpd(Outlet data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Outlet_ID == null)
                    {                       
                        string checkbase64 = "base64";
                        if (Request.Form["Outlet_Picture"].Contains(checkbase64))
                        {
                            string base64 = Request.Form["Outlet_Picture"];
                            string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                            string b = DashboardController.RandomString("B2bGoldeneye");
                            string c = "outletImages" + a + b + ".png";
                            data.Outlet_Picture = "https://foodiegost17.blob.core.windows.net/images/" + c;
                            DashboardController.UploadToAzure(base64, c);
                        }
                        else if (Request.Form["Outlet_Picture"] != "")
                        {
                            data.Outlet_Picture = data.Outlet_Picture;
                        }
                        else
                        {
                            data.Outlet_Picture = null;
                        }

                        try
                        {
                            data.Outlet_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Outlet_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (otlDB.Cek(data) != 0)
                                {
                                    return Json("failed", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    otlDB.AddUpdate(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            string checkbase64 = "base64";
                            if (Request.Form["Outlet_Picture"].Contains(checkbase64))
                            {
                                string base64 = Request.Form["Outlet_Picture"];
                                string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                string b = DashboardController.RandomString("B2bGoldeneye");
                                string c = "outletImages" + a + b + ".png";
                                data.Outlet_Picture = "https://foodiegost17.blob.core.windows.net/b2b/" + c;
                                DashboardController.UploadToAzure(base64, c);
                            }
                            else if (Request.Form["Images"] != "")
                            {
                                data.Outlet_Picture = data.Outlet_Picture;
                            }
                            else
                            {
                                data.Outlet_Picture = null;
                            }

                            data.Outlet_UpdateBy = Convert.ToString(Session["Username"]);                            

                            if (otlDB.AddUpdate(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpGet]
        public ActionResult ViewAddUpOutlet(decimal Lat, decimal Long)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                ViewBag.Area = arDB.ListAll() ?? new List<Area>();
                ViewBag.GroupOutlet = groupOutetDB.ListAll() ?? new List<GroupOutlet>();
                ViewBag.Recurring = recurringDB.ListAll() ?? new List<Recurring>();
                Outlet data = new Outlet();

                data.Outlet_Latitude = Lat;
                data.Outlet_Longitude = Long;
                return PartialView(data);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Outlet data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Outlet_UpdateBy = Convert.ToString(Session["Username"]);
                    otlDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_otlDB = JsonConvert.SerializeObject(otlDB.ListAll());
                DataTable dt_otlDB = (DataTable)JsonConvert.DeserializeObject(json_otlDB, (typeof(DataTable)));

                if (dt_otlDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Outlet");
                }

                dt_otlDB.Columns.Add("Outlet_InsertOn1", typeof(string));
                dt_otlDB.Columns.Add("Outlet_UpdateOn1", typeof(string));
                dt_otlDB.Columns.Add("Area_Name", typeof(string));

                foreach (DataRow objCon in dt_otlDB.Rows)
                {
                    objCon["Outlet_InsertOn1"] = Convert.ToString(objCon["Outlet_InsertOn"]);
                    objCon["Outlet_UpdateOn1"] = Convert.ToString(objCon["Outlet_UpdateOn"]);
                    objCon["Area_Name"] = Convert.ToString(objCon["Cari_Area"]);
                }

                dt_otlDB.Columns.Remove("Outlet_ID");
                dt_otlDB.Columns.Remove("Outlet_InsertOn");
                dt_otlDB.Columns.Remove("Outlet_UpdateOn");
                dt_otlDB.Columns.Remove("jml");
                dt_otlDB.Columns.Remove("AddOrUp");
                dt_otlDB.Columns.Remove("Images_Name");
                dt_otlDB.Columns.Remove("Outlet_CompanyID");
                //dt_otlDB.Columns.Remove("Outlet_Picture");
                dt_otlDB.Columns.Remove("Cari_Area");


                dt_otlDB.Columns.Add("Outlet_InsertOn", typeof(string));
                dt_otlDB.Columns.Add("Outlet_UpdateOn", typeof(string));
                dt_otlDB.Columns.Add("Outlet_AreaName", typeof(string));
                dt_otlDB.Columns.Add("Outlet_InsertByName", typeof(string));
                dt_otlDB.Columns.Add("Outlet_UpdateByName", typeof(string));
                dt_otlDB.Columns.Add("Outlet_Image_Front", typeof(string));
                dt_otlDB.Columns.Add("Outlet_Image_Cashier", typeof(string));
                dt_otlDB.Columns.Add("Outlet_Group_Name", typeof(string));
                dt_otlDB.Columns.Add("Outlet_Recurring", typeof(string));                

                for (int a = 0; a < dt_otlDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_otlDB.Rows[a]["Outlet_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_otlDB.Rows[a]["Outlet_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_otlDB.Rows[a]["Outlet_UpdateOn"] = dt_otlDB.Rows[a]["Outlet_UpdateOn1"];
                    }

                    if (Convert.ToString(dt_otlDB.Rows[a]["Outlet_PICFront"]) != "")
                    {
                        dt_otlDB.Rows[a]["Outlet_Image_Front"] = dt_otlDB.Rows[a]["Outlet_PICFront"];
                    }

                    if (Convert.ToString(dt_otlDB.Rows[a]["Outlet_PICCashier"]) != "")
                    {
                        dt_otlDB.Rows[a]["Outlet_Image_Cashier"] = dt_otlDB.Rows[a]["Outlet_PICCashier"];
                    }
                }

                foreach (DataRow objCon in dt_otlDB.Rows)
                {
                    objCon["Outlet_InsertOn"] = Convert.ToString(objCon["Outlet_InsertOn1"]);
                    objCon["Outlet_AreaName"] = Convert.ToString(objCon["Area_Name"]);
                    objCon["Outlet_InsertByName"] = Convert.ToString(objCon["Insert_Name"]);
                    objCon["Outlet_UpdateByName"] = Convert.ToString(objCon["Update_Name"]);
                    objCon["Outlet_Group_Name"] = Convert.ToString(objCon["GroupOutlet_Name"]);
                    objCon["Outlet_Recurring"] = Convert.ToString(objCon["Repeat_Name"]);
                }

                dt_otlDB.Columns.Remove("Outlet_InsertOn1");
                dt_otlDB.Columns.Remove("Outlet_UpdateOn1");
                dt_otlDB.Columns.Remove("Area_Name");
                dt_otlDB.Columns.Remove("Insert_Name");
                dt_otlDB.Columns.Remove("Update_Name");
                dt_otlDB.Columns.Remove("Outlet_PICCashier");
                dt_otlDB.Columns.Remove("Outlet_PICFront");
                dt_otlDB.Columns.Remove("GroupOutlet_Name");
                dt_otlDB.Columns.Remove("Outlet_Repeat_IDX");
                dt_otlDB.Columns.Remove("Repeat_Name");

                dt_otlDB.AcceptChanges();

                //dt_otlDB.DefaultView.Sort = "Outlet_InsertOn desc";
                //dt_otlDB = dt_otlDB.DefaultView.ToTable();

                dt_otlDB.Columns["Outlet_Code"].SetOrdinal(0);
                dt_otlDB.Columns["Outlet_Name"].SetOrdinal(1);
                dt_otlDB.Columns["Outlet_GroupCode"].SetOrdinal(2);
                dt_otlDB.Columns["Outlet_Group_Name"].SetOrdinal(3);
                dt_otlDB.Columns["Outlet_Recurring"].SetOrdinal(4);
                dt_otlDB.Columns["Outlet_Desc"].SetOrdinal(5);
                dt_otlDB.Columns["Outlet_Address1"].SetOrdinal(6);
                dt_otlDB.Columns["Outlet_Address2"].SetOrdinal(7);
                dt_otlDB.Columns["Outlet_Picture"].SetOrdinal(8);
                dt_otlDB.Columns["Outlet_Image_Front"].SetOrdinal(9);
                dt_otlDB.Columns["Outlet_Image_Cashier"].SetOrdinal(10);
                dt_otlDB.Columns["Outlet_Zipcode"].SetOrdinal(11);
                dt_otlDB.Columns["Outlet_Type"].SetOrdinal(12);
                dt_otlDB.Columns["Outlet_Phone"].SetOrdinal(13);
                dt_otlDB.Columns["Outlet_Email"].SetOrdinal(14);
                dt_otlDB.Columns["Outlet_Longitude"].SetOrdinal(15);
                dt_otlDB.Columns["Outlet_Latitude"].SetOrdinal(16);
                dt_otlDB.Columns["Outlet_AreaID"].SetOrdinal(17);
                dt_otlDB.Columns["Outlet_AreaName"].SetOrdinal(18);
                dt_otlDB.Columns["Outlet_Isactive"].SetOrdinal(19);
                dt_otlDB.Columns["Outlet_InsertBy"].SetOrdinal(20);
                dt_otlDB.Columns["Outlet_InsertByName"].SetOrdinal(21);
                dt_otlDB.Columns["Outlet_InsertOn"].SetOrdinal(22);
                dt_otlDB.Columns["Outlet_UpdateBy"].SetOrdinal(23);
                dt_otlDB.Columns["Outlet_UpdateByName"].SetOrdinal(24);
                dt_otlDB.Columns["Outlet_UpdateOn"].SetOrdinal(25);

                dt_otlDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_otlDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Outlet.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();

                return RedirectToAction("Index", "Outlet");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];

                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Outlet_Code", "Outlet_Name", "Outlet_GroupCode", "Outlet_Desc", "Outlet_Address1", "Outlet_Address2", "Outlet_Zipcode", "Outlet_Type", "Outlet_Phone", "Outlet_Email", "Outlet_Longitude", "Outlet_Latitude", "Outlet_AreaID" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Outlet_Code") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Outlet_Desc1", typeof(string));
                                            dt.Columns.Add("Outlet_Address11", typeof(string));
                                            dt.Columns.Add("Outlet_Address22", typeof(string));
                                            dt.Columns.Add("Outlet_Name1", typeof(string));
                                            dt.Columns.Add("Outlet_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Outlet_InsertBy", typeof(string));
                                            dt.Columns.Add("Outlet_CompanyID", typeof(int));
                                            dt.Columns.Add("Outlet_Phone1", typeof(string));


                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Outlet_Phone"] != null)
                                                {
                                                    dt.Rows[a]["Outlet_Phone1"] = Convert.ToString(dt.Rows[a]["Outlet_Phone"]).Replace(" ", "");
                                                }

                                                dt.Rows[a]["Outlet_Desc1"] = Convert.ToString(dt.Rows[a]["Outlet_Desc"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Outlet_Address11"] = Convert.ToString(dt.Rows[a]["Outlet_Address1"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Outlet_Address22"] = Convert.ToString(dt.Rows[a]["Outlet_Address2"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Outlet_Name1"] = Convert.ToString(dt.Rows[a]["Outlet_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Outlet_InsertOn"] = today;
                                                dt.Rows[a]["Outlet_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Outlet_CompanyID"] = Session["CompanyID"];
                                            }

                                            dt.Columns.Remove("Outlet_Phone");
                                            dt.Columns.Add("Outlet_Phone", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {
                                                if (dt.Rows[a]["Outlet_Phone1"] != null)
                                                {
                                                    dt.Rows[a]["Outlet_Phone"] = Convert.ToString(dt.Rows[a]["Outlet_Phone1"]);
                                                }
                                            }

                                            dt.Columns.Remove("Outlet_Phone1");

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Outlet (Outlet_Code nvarchar(30), Outlet_Name nvarchar(MAX), Outlet_GroupCode nvarchar(MAX), Outlet_Desc nvarchar(MAX), Outlet_Address1 nvarchar(MAX), Outlet_Address2 nvarchar(MAX), Outlet_Zipcode nvarchar(30), Outlet_Type nvarchar(30), Outlet_Phone nvarchar(30), Outlet_Email nvarchar(100), Outlet_Longitude float, Outlet_Latitude float, Outlet_AreaID int, Outlet_InsertBy nvarchar(50), Outlet_InsertOn datetime, Outlet_CompanyID int)", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Outlet";

                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Code", "Outlet_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Name1", "Outlet_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_GroupCode", "Outlet_GroupCode");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Desc1", "Outlet_Desc");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Address11", "Outlet_Address1");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Address22", "Outlet_Address2");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Zipcode", "Outlet_Zipcode");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Type", "Outlet_Type");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Phone", "Outlet_Phone");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Email", "Outlet_Email");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Longitude", "Outlet_Longitude");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_Latitude", "Outlet_Latitude");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_AreaID", "Outlet_AreaID");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_InsertBy", "Outlet_InsertBy");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_InsertOn", "Outlet_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Outlet_CompanyID", "Outlet_CompanyID");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Outlet t2 " + Environment.NewLine;
                                                mapping += " USING #M_Outlet t1 ON (t2.[Outlet_CompanyID] = t1.[Outlet_CompanyID] AND" + Environment.NewLine;
                                                mapping += " t2.[Outlet_Code] = t1.[Outlet_Code]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Outlet_Name=t1.Outlet_Name, t2.Outlet_GroupCode=t1.Outlet_GroupCode,"+ Environment.NewLine;
                                                mapping += " t2.Outlet_Desc=t1.Outlet_Desc, t2.Outlet_Address1=t1.Outlet_Address1," + Environment.NewLine;
                                                mapping += " t2.Outlet_Address2=t1.Outlet_Address2, t2.Outlet_Zipcode=t1.Outlet_Zipcode," + Environment.NewLine;
                                                mapping += " t2.Outlet_Type=t1.Outlet_Type, t2.Outlet_Phone=t1.Outlet_Phone," + Environment.NewLine;
                                                mapping += " t2.Outlet_Email=t1.Outlet_Email," + Environment.NewLine;
                                                mapping += " t2.Outlet_Longitude = t1.Outlet_Longitude, t2.Outlet_Latitude = t1.Outlet_Latitude, t2.Outlet_AreaID = t1.Outlet_AreaID," + Environment.NewLine;
                                                mapping += " t2.Outlet_UpdateBy = t1.Outlet_InsertBy, t2.Outlet_UpdateOn = t1.Outlet_InsertOn, t2.Outlet_Isactive = 1, t2.Outlet_Isdelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Outlet_Code], [Outlet_Name], [Outlet_GroupCode], [Outlet_Desc], [Outlet_Address1], [Outlet_Address2], [Outlet_Zipcode], [Outlet_Type], [Outlet_Phone], [Outlet_Email], [Outlet_Longitude], [Outlet_Latitude], [Outlet_AreaID], [Outlet_InsertBy], [Outlet_InsertOn], [Outlet_CompanyID], [Outlet_Isactive], [Outlet_Isdelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Outlet_Code], t1.[Outlet_Name], [Outlet_GroupCode], t1.[Outlet_Desc], t1.[Outlet_Address1], t1.[Outlet_Address2], t1.[Outlet_Zipcode], t1.[Outlet_Type], t1.[Outlet_Phone], t1.[Outlet_Email], t1.[Outlet_Longitude], t1.[Outlet_Latitude], t1.[Outlet_AreaID], t1.[Outlet_InsertBy], t1.[Outlet_InsertOn], t1.[Outlet_CompanyID], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Outlet] ([Outlet_Code], [Outlet_Name], [Outlet_GroupCode], [Outlet_Desc], [Outlet_Address1], [Outlet_Address2], [Outlet_Zipcode], [Outlet_Type], [Outlet_Phone], [Outlet_Email], [Outlet_Longitude], [Outlet_Latitude], [Outlet_AreaID], [Outlet_InsertBy], [Outlet_InsertOn], [Outlet_CompanyID])" +
                                                //                   " SELECT t1.[Outlet_Code], t1.[Outlet_Name], [Outlet_GroupCode], t1.[Outlet_Desc], t1.[Outlet_Address1], t1.[Outlet_Address2], 
                                                //t1.[Outlet_Zipcode], t1.[Outlet_Type], t1.[Outlet_Phone], t1.[Outlet_Email], 
                                                //t1.[Outlet_Longitude], t1.[Outlet_Latitude], t1.[Outlet_AreaID], t1.[Outlet_InsertBy], 
                                                //t1.[Outlet_InsertOn], t1.[Outlet_CompanyID] FROM #M_Outlet AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Outlet AS t2 WHERE t2.[Outlet_Code] = t1.[Outlet_Code] AND t2.[Outlet_CompanyID] = t1.[Outlet_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Outlet";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("error", JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Outlet_Code", typeof(string));
            table.Columns.Add("Outlet_Name", typeof(string));
            table.Columns.Add("Outlet_GroupCode", typeof(string));
            table.Columns.Add("Outlet_GroupName", typeof(string));
            table.Columns.Add("Outlet_Desc", typeof(string));
            table.Columns.Add("Outlet_Address1", typeof(string));
            table.Columns.Add("Outlet_Address2", typeof(string));
            table.Columns.Add("Outlet_Zipcode", typeof(string));
            table.Columns.Add("Outlet_Type", typeof(string));
            table.Columns.Add("Outlet_Phone", typeof(string));
            table.Columns.Add("Outlet_Email", typeof(string));
            table.Columns.Add("Outlet_Longitude", typeof(string));
            table.Columns.Add("Outlet_Latitude", typeof(string));
            table.Columns.Add("Outlet_AreaID", typeof(string));
            table.Columns.Add("Outlet_AreaName", typeof(string));


            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("CONTOH1", "WATSONS PONDOK INDAH", "CINTA1", "CINTA", "CONSIGMENT", "Jl. Metro Pondok Indah Kav IV/TA RT.1/RW.16, Pondok Pinang, Keb. Lama, Lantai 2, Unit 238, RT.6/RW.3", "", "12310", "", "(021) 75920928", "pim.watsons@watsons.co.id", "", "", "12", "JAKARTA");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Outlet.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Outlet");
        }
    }
}