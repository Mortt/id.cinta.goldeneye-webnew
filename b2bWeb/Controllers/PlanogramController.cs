﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class PlanogramController : Controller
    {
        PlanogramDB PlanogramDB = new PlanogramDB();
        DateTime Now = DateTime.Now;       
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Display";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataPlanogram()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = PlanogramDB.GetDataPlanogram(param) ?? new List<JsonPlanogram>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonPlanogram Baru = new JsonPlanogram();
                Baru.total = 0;
                Baru.rows = new List<rowsPlanogram>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Display(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    var Planogram = PlanogramDB.ListExport(g, c);

                    var json_PlanogramDB = JsonConvert.SerializeObject(Planogram);
                    DataTable dt_PlanogramDB = (DataTable)JsonConvert.DeserializeObject(json_PlanogramDB, (typeof(DataTable)));

                    if (dt_PlanogramDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Planogram");
                    }

                    dt_PlanogramDB.Columns.Add("Planogram_InsertOn1", typeof(string));
                    //dt_PlanogramDB.Columns.Add("Planogram_UpdateOn1", typeof(string));

                    foreach (DataRow objCon in dt_PlanogramDB.Rows)
                    {
                        objCon["Planogram_InsertOn1"] = Convert.ToString(objCon["Planogram_InsertOn"]);
                        //objCon["Planogram_UpdateOn1"] = Convert.ToString(objCon["Planogram_UpdateOn"]);
                    }

                    //dt_PlanogramDB.Columns.Remove("Planogram_Image");
                    //dt_PlanogramDB.Columns.Remove("Planogram_Image2");
                    //dt_PlanogramDB.Columns.Remove("Planogram_Image3");
                    //dt_PlanogramDB.Columns.Remove("Planogram_Image4");
                    //dt_PlanogramDB.Columns.Remove("Planogram_Image5");
                    dt_PlanogramDB.Columns.Remove("Planogram_ReasonID");
                    dt_PlanogramDB.Columns.Remove("Planogram_InsertBy");
                    dt_PlanogramDB.Columns.Remove("Planogram_UpdateBy");
                    dt_PlanogramDB.Columns.Remove("Planogram_UpdateOn");
                    dt_PlanogramDB.Columns.Remove("Planogram_ProductCode");
                    dt_PlanogramDB.Columns.Remove("Planogram_InsertOn");

                    dt_PlanogramDB.Columns.Add("Planogram_InsertOn", typeof(string));
                    dt_PlanogramDB.Columns.Add("Planogram_OutletName", typeof(string));
                    dt_PlanogramDB.Columns.Add("Planogram_Employee_Name", typeof(string));
                    dt_PlanogramDB.Columns.Add("Planogram_Area_Name", typeof(string));
                    dt_PlanogramDB.Columns.Add("Planogram_Respond_Module", typeof(string));

                    foreach (DataRow objCon in dt_PlanogramDB.Rows)
                    {
                        objCon["Planogram_InsertOn"] = Convert.ToString(objCon["Planogram_InsertOn1"]);
                        objCon["Planogram_OutletName"] = Convert.ToString(objCon["Outlet_Name"]);
                        objCon["Planogram_Employee_Name"] = Convert.ToString(objCon["Employee_Name"]);
                        objCon["Planogram_Area_Name"] = Convert.ToString(objCon["Area_Name"]);
                        objCon["Planogram_Respond_Module"] = Convert.ToString(objCon["Respond_Module"]);
                    }

                    dt_PlanogramDB.Columns.Remove("Planogram_InsertOn1");
                    dt_PlanogramDB.Columns.Remove("Outlet_Name");
                    dt_PlanogramDB.Columns.Remove("Employee_Name");
                    dt_PlanogramDB.Columns.Remove("Respond_Module");
                    dt_PlanogramDB.Columns.Remove("Planogram_ID");
                    dt_PlanogramDB.Columns.Remove("Area_Name");

                    dt_PlanogramDB.AcceptChanges();

                    //dt_PlanogramDB.DefaultView.Sort = "Planogram_ID DESC";
                    //dt_PlanogramDB = dt_PlanogramDB.DefaultView.ToTable();

                    dt_PlanogramDB.Columns["Planogram_Employee_Name"].SetOrdinal(0);
                    dt_PlanogramDB.Columns["Planogram_OutletCode"].SetOrdinal(1);
                    dt_PlanogramDB.Columns["Planogram_OutletName"].SetOrdinal(2);
                    dt_PlanogramDB.Columns["Planogram_Image"].SetOrdinal(3);
                    dt_PlanogramDB.Columns["Planogram_Image2"].SetOrdinal(4);
                    dt_PlanogramDB.Columns["Planogram_Image3"].SetOrdinal(5);
                    dt_PlanogramDB.Columns["Planogram_Image4"].SetOrdinal(6);
                    dt_PlanogramDB.Columns["Planogram_Image5"].SetOrdinal(7);
                    dt_PlanogramDB.Columns["Planogram_Tier"].SetOrdinal(8);
                    dt_PlanogramDB.Columns["Planogram_Area_Name"].SetOrdinal(9);
                    dt_PlanogramDB.Columns["Planogram_Respond_Module"].SetOrdinal(10);
                    dt_PlanogramDB.Columns["Planogram_Desc"].SetOrdinal(11);
                    dt_PlanogramDB.Columns["Planogram_InsertOn"].SetOrdinal(12);
                    dt_PlanogramDB.Columns["Planogram_Isdelete"].SetOrdinal(13);

                    dt_PlanogramDB.AcceptChanges();

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_PlanogramDB;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        dt_PlanogramDB.Columns.Remove("Planogram_Image2");
                        dt_PlanogramDB.Columns.Remove("Planogram_Image3");
                        dt_PlanogramDB.Columns.Remove("Planogram_Image4");
                        dt_PlanogramDB.Columns.Remove("Planogram_Image5");

                        dt_PlanogramDB.AcceptChanges();

                        for (int a = 0; a < dt_PlanogramDB.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_PlanogramDB.Rows[a]["Planogram_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_PlanogramDB.Rows[a]["Planogram_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_PlanogramDB;

                        int row = 1;
                        int column = 4;
                        string TableImageName = "Planogram_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}