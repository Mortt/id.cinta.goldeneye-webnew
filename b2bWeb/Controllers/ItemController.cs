﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using Newtonsoft.Json;
using System.Data.SqlClient;

namespace b2bWeb.Controllers
{
    public class ItemController : Controller
    {
        ItemDB itmDB = new ItemDB();
        CategoryDB categoryDB = new CategoryDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                { }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Category = categoryDB.ListAll() ?? new List<Category>();
                ViewBag.Title = "Master / Product";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataItem()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = itmDB.GetDataItem(param) ?? new List<JsonItem>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonItem Baru = new JsonItem();
                Baru.total = 0;
                Baru.rows = new List<rowsItem>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }       

        [HttpPost]
        public ActionResult AddUp(Item data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Item_ID == null)
                    {
                        string checkbase64 = "base64";
                        if (Request.Form["Item_Image1"].Contains(checkbase64))
                        {
                            string base64 = Request.Form["Item_Image1"];
                            string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                            string b = DashboardController.RandomString("B2bGoldeneye");
                            string c = "itemImages" + a + b + ".png";
                            data.Item_Image1 = "https://foodiegost17.blob.core.windows.net/images/" + c;
                            DashboardController.UploadToAzure(base64, c);
                        }
                        else if (Request.Form["Item_Image1"] != "")
                        {
                            data.Item_Image1 = data.Item_Image1;
                        }
                        else
                        {
                            data.Item_Image1 = null;
                        }

                        try
                        {
                            data.Item_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Item_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (itmDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    itmDB.AddUpdate(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            string checkbase64 = "base64";
                            if (Request.Form["Item_Image1"].Contains(checkbase64))
                            {
                                string base64 = Request.Form["Item_Image1"];
                                string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                string b = DashboardController.RandomString("B2bGoldeneye");
                                string c = "itemImages" + a + b + ".png";
                                data.Item_Image1 = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                DashboardController.UploadToAzure(base64, c);
                            }
                            else if (Request.Form["Item_Image1"] != "")
                            {
                                data.Item_Image1 = data.Item_Image1;
                            }
                            else
                            {
                                data.Item_Image1 = null;
                            }

                            data.Item_UpdateBy = Convert.ToString(Session["Username"]);

                            if (itmDB.AddUpdate(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("falied", JsonRequestBehavior.AllowGet);
                        }
                    }
                    return RedirectToAction("index", "item");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Item data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Item_UpdateBy = Convert.ToString(Session["Username"]);
                    itmDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_itmDB = JsonConvert.SerializeObject(itmDB.ListAll());
                DataTable dt_itmDB = (DataTable)JsonConvert.DeserializeObject(json_itmDB, (typeof(DataTable)));

                if (dt_itmDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Item");
                }

                dt_itmDB.Columns.Add("Item_InsertOn1", typeof(string));
                dt_itmDB.Columns.Add("Item_UpdateOn1", typeof(string));

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["Item_InsertOn1"] = Convert.ToString(objCon["Item_InsertOn"]);
                    objCon["Item_UpdateOn1"] = Convert.ToString(objCon["Item_UpdateOn"]);
                }

                dt_itmDB.Columns.Remove("Item_InsertOn");
                dt_itmDB.Columns.Remove("Item_UpdateOn");
                dt_itmDB.Columns.Remove("jml");
                dt_itmDB.Columns.Remove("Images_Name");
                dt_itmDB.Columns.Remove("Item_CompanyID");
                dt_itmDB.Columns.Remove("Item_Image2");
                //dt_itmDB.Columns.Remove("Item_Image1");

                dt_itmDB.Columns.Add("Item_InsertOn", typeof(string));
                dt_itmDB.Columns.Add("Item_UpdateOn", typeof(string));
                dt_itmDB.Columns.Add("Item_InsertByName", typeof(string));
                dt_itmDB.Columns.Add("Item_UpdateByName", typeof(string));
                dt_itmDB.Columns.Add("Item_BrandCode", typeof(string));
                dt_itmDB.Columns.Add("Item_BrandName", typeof(string));

                for (int a = 0; a < dt_itmDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_itmDB.Rows[a]["Item_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_itmDB.Rows[a]["Item_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_itmDB.Rows[a]["Item_UpdateOn"] = dt_itmDB.Rows[a]["Item_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_itmDB.Rows)
                {
                    objCon["Item_InsertOn"] = Convert.ToString(objCon["Item_InsertOn1"]);
                    objCon["Item_InsertByName"] = Convert.ToString(objCon["Insert_Name"]);
                    objCon["Item_UpdateByName"] = Convert.ToString(objCon["Update_Name"]);
                    objCon["Item_BrandName"] = Convert.ToString(objCon["Category_Name"]);
                    objCon["Item_BrandCode"] = Convert.ToString(objCon["Item_CategoryCode"]);
                }

                dt_itmDB.Columns.Remove("Item_InsertOn1");
                dt_itmDB.Columns.Remove("Item_UpdateOn1");
                dt_itmDB.Columns.Remove("Insert_Name");
                dt_itmDB.Columns.Remove("Update_Name");
                dt_itmDB.Columns.Remove("Category_Name");

                dt_itmDB.AcceptChanges();

                dt_itmDB.Columns["Item_Code"].SetOrdinal(0);
                dt_itmDB.Columns["Item_Name"].SetOrdinal(1);
                dt_itmDB.Columns["Item_Desc"].SetOrdinal(2);
                dt_itmDB.Columns["Item_Image1"].SetOrdinal(3);
                dt_itmDB.Columns["Item_BrandCode"].SetOrdinal(4);
                dt_itmDB.Columns["Item_BrandName"].SetOrdinal(5);
                dt_itmDB.Columns["Item_Price"].SetOrdinal(6);
                dt_itmDB.Columns["Item_IsActive"].SetOrdinal(7);
                dt_itmDB.Columns["Item_InsertBy"].SetOrdinal(8);
                dt_itmDB.Columns["Item_InsertByName"].SetOrdinal(9);
                dt_itmDB.Columns["Item_InsertOn"].SetOrdinal(10);
                dt_itmDB.Columns["Item_UpdateBy"].SetOrdinal(11);
                dt_itmDB.Columns["Item_UpdateByName"].SetOrdinal(12);
                dt_itmDB.Columns["Item_UpdateOn"].SetOrdinal(13);

                dt_itmDB.AcceptChanges();
                
                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_itmDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Item.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();

                return RedirectToAction("Index", "item");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Item_Code", "Item_Name", "Item_Desc", "Item_Desc_Detail", "Item_CategoryCode", "Item_Price" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Item_Code") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Item_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Item_InsertBy", typeof(string));
                                            dt.Columns.Add("Item_CompanyID", typeof(int));
                                            dt.Columns.Add("Item_Name1", typeof(string));
                                            dt.Columns.Add("Item_Desc1", typeof(string));
                                            dt.Columns.Add("Item_Desc_Detail1", typeof(string));
                                            dt.Columns.Add("Item_Code1", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {

                                                dt.Rows[a]["Item_Name1"] = Convert.ToString(dt.Rows[a]["Item_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Item_Code1"] = Convert.ToString(dt.Rows[a]["Item_Code"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Item_Desc_Detail1"] = Convert.ToString(dt.Rows[a]["Item_Desc_Detail"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Item_Desc1"] = Convert.ToString(dt.Rows[a]["Item_Desc"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Item_InsertOn"] = today;
                                                dt.Rows[a]["Item_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Item_CompanyID"] = Session["CompanyID"];
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();

                                                var cmdCategory = new SqlCommand("SELECT * FROM M_Category WHERE Category_CompanyID = " + Convert.ToInt32(Session["CompanyID"]) + "", con);
                                                DataTable dtCategory = new DataTable();
                                                dtCategory.Load(cmdCategory.ExecuteReader());

                                                for (int j = 0; j < dt.Rows.Count; j++)
                                                {
                                                    DataRow[] foundCodeItem = dtCategory.Select("Category_Code = '" + Convert.ToString(dt.Rows[j]["Item_CategoryCode"]) + "'");
                                                    if (foundCodeItem.Length > 0)
                                                    {
                                                    }
                                                    else
                                                    {
                                                        TempData["Error"] = "Maaf, Item_CategoryCode " + dt.Rows[j]["Item_CategoryCode"] + "tidak terdaftar pada sistem";
                                                        return RedirectToAction("Import", "Item");
                                                    }
                                                }

                                                var command = new SqlCommand("CREATE TABLE #M_Item (Item_Code nvarchar(30), Item_Name nvarchar(100), Item_Desc nvarchar(MAX), Item_Desc_Detail nvarchar(MAX), Item_CategoryCode nvarchar(30), Item_Price decimal(18, 2), Item_InsertOn datetime, Item_InsertBy nvarchar(30), Item_CompanyID int)", con);
                                                command.ExecuteNonQuery();

                                                //SqlCommand commandRowCount = new SqlCommand(
                                                //"SELECT COUNT(*) FROM " +
                                                //"#M_Item;",
                                                //con);
                                                //long countStart = Convert.ToInt32(
                                                //    commandRowCount.ExecuteScalar());
                                                //System.Diagnostics.Debug.WriteLine("NotifyAfter Sample");
                                                //System.Diagnostics.Debug.WriteLine("Starting row count = {0}", countStart);

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Item";

                                                    sqlBulkCopy.ColumnMappings.Add("Item_Code1", "Item_Code");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_Name1", "Item_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_Desc1", "Item_Desc");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_Desc_Detail1", "Item_Desc_Detail");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_CategoryCode", "Item_CategoryCode");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_Price", "Item_Price");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_CompanyID", "Item_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_InsertOn", "Item_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Item_InsertBy", "Item_InsertBy");

                                                    //sqlBulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(OnSqlRowsCopied);
                                                    //sqlBulkCopy.NotifyAfter = 1;

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Item t2 " + Environment.NewLine;
                                                mapping += " USING #M_Item t1 ON (t2.[Item_Code] = t1.[Item_Code] AND" + Environment.NewLine;
                                                mapping += " t2.[Item_CompanyID] = t1.[Item_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Item_Name=t1.Item_Name, t2.Item_Desc=t1.Item_Desc, t2.Item_Desc_Detail=t1.Item_Desc_Detail, " + Environment.NewLine;
                                                mapping += " t2.Item_CategoryCode=t1.Item_CategoryCode, t2.Item_Price = t1.Item_Price, " + Environment.NewLine;
                                                mapping += " t2.Item_UpdateOn=t1.Item_InsertOn, t2.Item_UpdateBy=t1.Item_InsertBy, t2.Item_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Item_Code], [Item_Name], [Item_Desc], [Item_Desc_Detail], [Item_CategoryCode], [Item_Price], [Item_CompanyID], [Item_InsertOn], [Item_InsertBy], [Item_IsActive], [Item_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Item_Code], t1.[Item_Name], t1.[Item_Desc], t1.[Item_Desc_Detail], t1.[Item_CategoryCode], t1.[Item_Price], t1.[Item_CompanyID], t1.[Item_InsertOn], t1.[Item_InsertBy], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Item] ([Item_Code], [Item_Name], [Item_Desc], [Item_Desc_Detail], [Item_CategoryCode], [Item_Price], [Item_CompanyID], [Item_InsertOn], [Item_InsertBy])" +
                                                //                   " SELECT t1.[Item_Code], t1.[Item_Name], t1.[Item_Desc], t1.[Item_Desc_Detail], t1.[Item_CategoryCode], t1.[Item_Price], t1.[Item_CompanyID], t1.[Item_InsertOn], t1.[Item_InsertBy] FROM #M_Item AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Item AS t2 WHERE t2.[Item_Code] = t1.[Item_Code ] AND t2.[Item_CompanyID] = t1.[Item_CompanyID])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Item";
                                                command.ExecuteNonQuery();
                                                con.Close();

                                                con.Open();
                                                var cmd = new SqlCommand("DELETE M_RoleD WHERE Item_ID IN (SELECT Item_ID FROM M_Item WHERE Item_CompanyID = " + Session["CompanyID"] + ") ", con);
                                                cmd.ExecuteNonQuery();

                                                SqlCommand com = new SqlCommand("select Role_ID FROM M_Role WHERE Role_CompanyID = " + Session["CompanyID"] + " and Role_Isdelete = 'false'", con);

                                                DataTable dtRole = new DataTable();
                                                dtRole.Load(com.ExecuteReader());

                                                for (int r = 0; r < dtRole.Rows.Count; r++)
                                                {
                                                    string A = Convert.ToString(dtRole.Rows[r]["Role_ID"]);

                                                    string sql = "INSERT INTO M_RoleD" +
                                                                 "(Role_ID, Item_ID)" +
                                                                 "(select " + A + ", Item_ID FROM M_Item WITH(NOLOCK) WHERE Item_CompanyID = " + Session["CompanyID"] + ")";
                                                    using (SqlCommand cmdInsert = new SqlCommand(sql, con))
                                                    {
                                                        cmdInsert.ExecuteNonQuery();
                                                    }
                                                }
                                                con.Close();
                                            }
                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }                    
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Item_Code", typeof(string));
            table.Columns.Add("Item_Name", typeof(string));
            table.Columns.Add("Item_Desc", typeof(string));
            table.Columns.Add("Item_Desc_Detail", typeof(string));
            table.Columns.Add("Item_CategoryCode", typeof(string));
            table.Columns.Add("Item_CategoryName", typeof(string));
            table.Columns.Add("Item_Price", typeof(string));
                

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("CINTA1", "COKLAT CINTA", "12 bar coklat", "", "C12", "COKLAT", "51000");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Item_or_product.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Item");
        }
    }
}