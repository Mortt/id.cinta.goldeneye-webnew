﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace b2bWeb.Controllers
{
    public class RoleController : Controller
    {
        RoleDB rolDB = new RoleDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Role";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataRole()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;


            var Result = rolDB.GetDataRole(param) ?? new List<JsonRole>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonRole Baru = new JsonRole();
                Baru.total = 0;
                Baru.rows = new List<rowsRole>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddUp(Role data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                
                    if (!ModelState.IsValid)
                    {
                        var errorModel = from x in ModelState.Keys
                                         where ModelState[x].Errors.Count > 0
                                         select new
                                         {
                                             key = x,
                                             errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                         };

                        return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                    if (data.Role_ID == null)
                    {
                        try
                        {
                            data.Role_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Role_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (rolDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    rolDB.AddUpd(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            data.Role_UpdateBy = Convert.ToString(Session["Username"]);

                            if (rolDB.AddUpd(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                }                
                return RedirectToAction("index", "role");
            }            
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Role data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Role_UpdateBy = Convert.ToString(Session["Username"]);
                    rolDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var json_rolDB = JsonConvert.SerializeObject(rolDB.ListAll());
                DataTable dt_rolDB = (DataTable)JsonConvert.DeserializeObject(json_rolDB, (typeof(DataTable)));

                if (dt_rolDB.Rows.Count == 0)
                {
                    return RedirectToAction("Index", "Item");
                }

                dt_rolDB.Columns.Add("Role_InsertOn1", typeof(string));
                dt_rolDB.Columns.Add("Role_UpdateOn1", typeof(string));
                
                foreach (DataRow objCon in dt_rolDB.Rows)
                {
                    objCon["Role_InsertOn1"] = Convert.ToString(objCon["Role_InsertOn"]);
                    objCon["Role_UpdateOn1"] = Convert.ToString(objCon["Role_UpdateOn"]);                    
                }

                dt_rolDB.Columns.Remove("Role_InsertOn");
                dt_rolDB.Columns.Remove("Role_UpdateOn");
                dt_rolDB.Columns.Remove("jml");
                dt_rolDB.Columns.Remove("Role_CompanyID");                                

                dt_rolDB.Columns.Add("Role_InsertOn", typeof(string));
                dt_rolDB.Columns.Add("Role_UpdateOn", typeof(string));
                dt_rolDB.Columns.Add("Role_InsertByName", typeof(string));
                dt_rolDB.Columns.Add("Role_UpdateByName", typeof(string));

                for (int a = 0; a < dt_rolDB.Rows.Count; a++)
                {
                    if (Convert.ToString(dt_rolDB.Rows[a]["Role_UpdateOn1"]) == "1/1/0001 12:00:00 AM")
                    {
                        dt_rolDB.Rows[a]["Role_UpdateOn"] = "-";
                    }
                    else
                    {
                        dt_rolDB.Rows[a]["Role_UpdateOn"] = dt_rolDB.Rows[a]["Role_UpdateOn1"];
                    }
                }

                foreach (DataRow objCon in dt_rolDB.Rows)
                {
                    objCon["Role_InsertOn"] = Convert.ToString(objCon["Role_InsertOn1"]);
                    objCon["Role_InsertByName"] = Convert.ToString(objCon["Insert_Name"]);
                    objCon["Role_UpdateByName"] = Convert.ToString(objCon["Update_Name"]);
                }

                dt_rolDB.Columns.Remove("Role_InsertOn1");
                dt_rolDB.Columns.Remove("Role_UpdateOn1");
                dt_rolDB.Columns.Remove("Insert_Name");
                dt_rolDB.Columns.Remove("Update_Name");

                dt_rolDB.AcceptChanges();

                dt_rolDB.Columns["Role_ID"].SetOrdinal(0);
                dt_rolDB.Columns["Role_Name"].SetOrdinal(1);
                dt_rolDB.Columns["Role_Desc"].SetOrdinal(2);
                dt_rolDB.Columns["Role_Type"].SetOrdinal(3);
                dt_rolDB.Columns["Role_InsertBy"].SetOrdinal(4);
                dt_rolDB.Columns["Role_InsertByName"].SetOrdinal(5);
                dt_rolDB.Columns["Role_InsertOn"].SetOrdinal(6);
                dt_rolDB.Columns["Role_UpdateBy"].SetOrdinal(7);
                dt_rolDB.Columns["Role_UpdateByName"].SetOrdinal(8);
                dt_rolDB.Columns["Role_UpdateOn"].SetOrdinal(9);
                dt_rolDB.Columns["Role_IsActive"].SetOrdinal(10);

                dt_rolDB.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(dt_rolDB, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Role.xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }

                httpResponse.End();
                return RedirectToAction("index", "role");
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Import()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase postedFile = Request.Files[i];
                    DateTime today = DateTime.Now;
                    string fileName = postedFile.FileName;
                    string FileExtension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    if (FileExtension == "xls" || FileExtension == "xlsx")
                    {
                        try
                        {
                            string filePath = string.Empty;
                            if (postedFile != null)
                            {
                                string path = Server.MapPath("~/Uploads/");
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }

                                filePath = path + Path.GetFileName(postedFile.FileName);
                                string extension = Path.GetExtension(postedFile.FileName);
                                postedFile.SaveAs(filePath);

                                string conString = string.Empty;
                                switch (extension)
                                {
                                    case ".xls": //Excel 97-03.
                                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                                        break;
                                    case ".xlsx": //Excel 07 and above.
                                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                                        break;
                                }

                                DataTable table = new DataTable();
                                conString = string.Format(conString, filePath);

                                using (OleDbConnection connExcel = new OleDbConnection(conString))
                                {
                                    using (OleDbCommand cmdExcel = new OleDbCommand())
                                    {
                                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                        {
                                            cmdExcel.Connection = connExcel;

                                            //Get the name of First Sheet.
                                            connExcel.Open();
                                            DataTable dtExcelSchema;
                                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                            cmdExcel.CommandText = "select * From [" + sheetName + "]";
                                            odaExcel.SelectCommand = cmdExcel;
                                            odaExcel.Fill(table);

                                            string[] selectedColumns = new[] { "Role_Name", "Role_Desc", "Role_Type" };
                                            DataTable dtNew = new DataView(table).ToTable(true, selectedColumns);

                                            var query = from myRow in dtNew.AsEnumerable()
                                                        where myRow.Field<string>("Role_Name") != null
                                                        select myRow;

                                            DataTable dt = query.CopyToDataTable();
                                            dt.AcceptChanges();

                                            dt.Columns.Add("Role_InsertOn", typeof(DateTime));
                                            dt.Columns.Add("Role_InsertBy", typeof(string));
                                            dt.Columns.Add("Role_CompanyID", typeof(int));
                                            dt.Columns.Add("Role_Name1", typeof(string));
                                            dt.Columns.Add("Role_Desc1", typeof(string));
                                            dt.Columns.Add("Role_Type1", typeof(string));

                                            for (int a = 0; a < dt.Rows.Count; a++)
                                            {

                                                dt.Rows[a]["Role_Name1"] = Convert.ToString(dt.Rows[a]["Role_Name"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Role_Desc1"] = Convert.ToString(dt.Rows[a]["Role_Desc"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");
                                                dt.Rows[a]["Role_Type1"] = Convert.ToString(dt.Rows[a]["Role_Type"]).Replace("&", "Dan").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("'", "");

                                                dt.Rows[a]["Role_InsertOn"] = today;
                                                dt.Rows[a]["Role_InsertBy"] = Session["Username"];
                                                dt.Rows[a]["Role_CompanyID"] = Session["CompanyID"];
                                            }

                                            var conStringDB = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                                            using (SqlConnection con = new SqlConnection(conStringDB))
                                            {
                                                con.Open();
                                                var command = new SqlCommand("CREATE TABLE #M_Role (Role_Name nvarchar(30), Role_Desc nvarchar(MAX), Role_Type nvarchar(30), Role_CompanyID int, Role_InsertOn datetime, Role_InsertBy nvarchar(30))", con);
                                                command.ExecuteNonQuery();

                                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                                {
                                                    sqlBulkCopy.DestinationTableName = "#M_Role";

                                                    sqlBulkCopy.ColumnMappings.Add("Role_Name1", "Role_Name");
                                                    sqlBulkCopy.ColumnMappings.Add("Role_Desc1", "Role_Desc");
                                                    sqlBulkCopy.ColumnMappings.Add("Role_Type1", "Role_Type");
                                                    sqlBulkCopy.ColumnMappings.Add("Role_CompanyID", "Role_CompanyID");
                                                    sqlBulkCopy.ColumnMappings.Add("Role_InsertOn", "Role_InsertOn");
                                                    sqlBulkCopy.ColumnMappings.Add("Role_InsertBy", "Role_InsertBy");

                                                    sqlBulkCopy.WriteToServer(dt);
                                                }

                                                string mapping = " MERGE M_Role t2 " + Environment.NewLine;
                                                mapping += " USING #M_Role t1 ON (t2.[Role_Type] = t1.[Role_Type] AND" + Environment.NewLine;
                                                mapping += " t2.[Role_CompanyID] = t1.[Role_CompanyID]) " + Environment.NewLine;
                                                mapping += " WHEN MATCHED THEN " + Environment.NewLine;
                                                mapping += " UPDATE SET t2.Role_Name=t1.Role_Name, t2.Role_Desc=t1.Role_Desc, t2.Role_UpdateOn=t1.Role_InsertOn, t2.Role_UpdateBy=t1.Role_InsertBy, t2.Role_IsDelete = 0" + Environment.NewLine;
                                                mapping += " WHEN NOT MATCHED THEN " + Environment.NewLine;
                                                mapping += " INSERT ([Role_Name], [Role_Desc], [Role_Type], [Role_CompanyID], [Role_InsertOn], [Role_InsertBy], [Role_IsActive], [Role_IsDelete]) " + Environment.NewLine;
                                                mapping += " VALUES (t1.[Role_Name], t1.[Role_Desc], t1.[Role_Type], t1.[Role_CompanyID], t1.[Role_InsertOn], t1.[Role_InsertBy], 1, 0); " + Environment.NewLine;
                                                command.CommandText = mapping;

                                                //command.CommandText = "INSERT INTO [dbo].[M_Role] ([Role_Name], [Role_Desc], [Role_Type], [Role_CompanyID], [Role_InsertOn], [Role_InsertBy])" +
                                                //                   " SELECT t1.[Role_Name], t1.[Role_Desc], t1.[Role_Type], t1.[Role_CompanyID], t1.[Role_InsertOn], t1.[Role_InsertBy] FROM #M_Role AS t1 " +
                                                //                   " WHERE NOT EXISTS ( SELECT * FROM dbo.M_Role AS t2 WHERE t2.[Role_CompanyID] = t1.[Role_CompanyID] AND t2.[Role_Type] = t1.[Role_Type])";

                                                command.ExecuteNonQuery();

                                                command.CommandText = "DROP TABLE #M_Role";
                                                command.ExecuteNonQuery();
                                                con.Close();
                                            }

                                            //int rows = dt.Rows.Count;
                                            //TempData["Rows"] = rows;

                                            connExcel.Close();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json("wrong", JsonRequestBehavior.AllowGet);
                    }                    
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportTemplate()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Role_Name", typeof(string));
            table.Columns.Add("Role_Desc", typeof(string));
            table.Columns.Add("Role_Type", typeof(string));

            // Add Three rows with those columns filled in the DataTable.
            table.Rows.Add("SPG", "Sales Promotion Girl: Untuk melakukan penjualan", "SPG");

            XLWorkbook wbook = new XLWorkbook();
            var wr = wbook.Worksheets.Add(table, "Sheet1");
            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

            // Prepare the response
            HttpResponseBase httpResponse = Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Provide you file name here
            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Template_Role.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }
            httpResponse.End();
            return RedirectToAction("Index", "Role");
        }
    }
}