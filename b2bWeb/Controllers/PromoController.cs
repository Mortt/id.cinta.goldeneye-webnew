﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using System.Data;
using System.Web;
using System.IO;

namespace b2bWeb.Controllers
{
    public class PromoController : Controller
    {
        //BrandDB BrandDB = new BrandDB();
        PromoDB PromoDB = new PromoDB();
        ItemDB itmDB = new ItemDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.Title = "Master / Promo";
                ViewBag.HMenu = DashboardController.HeaderMenu();
                //ViewBag.Brand = BrandDB.ListAll() ?? new List<Brand>();
                ViewBag.Item = itmDB.ListAll() ?? new List<Item>();
                return View();

            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataPromo()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = PromoDB.GetDataPromo(param) ?? new List<JsonPromo>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonPromo Baru = new JsonPromo();
                Baru.total = 0;
                Baru.rows = new List<rowsPromo>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddUp(Promo data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (!ModelState.IsValid)
                {
                    var errorModel = from x in ModelState.Keys
                                     where ModelState[x].Errors.Count > 0
                                     select new
                                     {
                                         key = x,
                                         errors = ModelState[x].Errors.Select(y => y.ErrorMessage).ToArray()
                                     };

                    return Json(new { errorModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.Promosi_ID == null)
                    {
                        if (data.Radios == 1)
                        {
                            try
                            {
                                string checkbase64 = "base64";
                                if (Request.Form["Promosi_File1"].Contains(checkbase64))
                                {
                                    string base64 = Request.Form["Promosi_File1"];
                                    string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                    string b = DashboardController.RandomString("B2bGoldeneye");
                                    string c = "promoImages1" + a + b + ".png";
                                    data.Promosi_File1 = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                    DashboardController.UploadToAzure(base64, c);
                                }
                                else if (Request.Form["Promosi_File1"] != "")
                                {
                                    data.Promosi_File1 = data.Promosi_File1;
                                }
                                else
                                {
                                    data.Promosi_File1 = null;
                                }
                                                                
                                if (Request.Form["Promosi_File2"].Contains(checkbase64))
                                {
                                    string base64 = Request.Form["Promosi_File2"];
                                    string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                    string b = DashboardController.RandomString("B2bGoldeneye");
                                    string c = "promoImages1" + a + b + ".png";
                                    data.Promosi_File2 = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                    DashboardController.UploadToAzure(base64, c);
                                }
                                else if (Request.Form["Promosi_File2"] != "")
                                {
                                    data.Promosi_File2 = data.Promosi_File2;
                                }
                                else
                                {
                                    data.Promosi_File2 = null;
                                }

                                data.Promosi_Type = 1;
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                            }
                        }
                        else if (data.Radios == 2)
                        {
                            try
                            {
                                DashboardController.UploadToAzure(Request.Files["Promosi_File1"]);
                                data.Promosi_File1 = "https://foodiegost17.blob.core.windows.net/vidio/" + Request.Files["Promosi_File1"].FileName;
                                DashboardController.UploadToAzure(Request.Files["Promosi_File2"]);
                                data.Promosi_File2 = "https://foodiegost17.blob.core.windows.net/vidio/" + Request.Files["Promosi_File2"].FileName;
                                data.Promosi_Type = 2;
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                            }
                        }
                        else if (data.Radios == 3)
                        {
                            data.Promosi_Type = 3;
                        }

                        try
                        {
                            data.Promosi_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Promosi_InsertBy = Convert.ToString(Session["Username"]);
                            try
                            {
                                if (PromoDB.Cek(data) != 0)
                                {
                                    return Json("has", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    PromoDB.AddUpd(data);
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.Message.ToString();
                                return Json("failed", JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (data.Radios == 1)
                            {
                                try
                                {
                                    string checkbase64 = "base64";
                                    if (Request.Form["Promosi_File1"].Contains(checkbase64))
                                    {
                                        string base64 = Request.Form["Promosi_File1"];
                                        string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                        string b = DashboardController.RandomString("B2bGoldeneye");
                                        string c = "promoImages1" + a + b + ".png";
                                        data.Promosi_File1 = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                        DashboardController.UploadToAzure(base64, c);
                                    }
                                    else if (Request.Form["Promosi_File1"] != "")
                                    {
                                        data.Promosi_File1 = data.Promosi_File1;
                                    }
                                    else
                                    {
                                        data.Promosi_File1 = null;
                                    }

                                    if (Request.Form["Promosi_File2"].Contains(checkbase64))
                                    {
                                        string base64 = Request.Form["Promosi_File2"];
                                        string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                                        string b = DashboardController.RandomString("B2bGoldeneye");
                                        string c = "promoImages1" + a + b + ".png";
                                        data.Promosi_File2 = "https://foodiegost17.blob.core.windows.net/images/" + c;
                                        DashboardController.UploadToAzure(base64, c);
                                    }
                                    else if (Request.Form["Promosi_File2"] != "")
                                    {
                                        data.Promosi_File2 = data.Promosi_File2;
                                    }
                                    else
                                    {
                                        data.Promosi_File2 = null;
                                    }

                                    data.Promosi_Type = 1;
                                }
                                catch (Exception ex)
                                {
                                    ex.Message.ToString();
                                }
                            }
                            else if (data.Radios == 2)
                            {
                                try
                                {
                                    if (Request.Form["Promosi_File1"] != "" || Request.Form["Promosi_File2"] != "")
                                    {
                                        for (int i = 0; i < Request.Files.Count; i++)
                                        {
                                            HttpPostedFileBase postedFile = Request.Files[i];
                                            string fileName = postedFile.FileName;
                                            string filePath = string.Empty;
                                            if (postedFile != null)
                                            {
                                                string path = Server.MapPath("~/Uploads/");
                                                if (!Directory.Exists(path))
                                                {
                                                    Directory.CreateDirectory(path);
                                                }

                                                filePath = path + Path.GetFileName(postedFile.FileName);
                                                string extension = Path.GetExtension(postedFile.FileName);
                                                postedFile.SaveAs(filePath);

                                                DashboardController.UploadToAzure(Request.Files[i]);
                                                if (data.Promosi_File1 != null && data.Promosi_File1.Contains(fileName))
                                                {
                                                    data.Promosi_File1 = "https://foodiegost17.blob.core.windows.net/images/" + fileName;                                                    
                                                }
                                                else if(data.Promosi_File2 != null && data.Promosi_File2.Contains(fileName))
                                                {
                                                    data.Promosi_File2 = "https://foodiegost17.blob.core.windows.net/images/" + fileName;
                                                }
                                            }
                                        }
                                    }

                                    if(data.Promosi_File1 == null && data.Promosi_File11 != "")
                                    {
                                        data.Promosi_File1 = data.Promosi_File11;                                        
                                    }else if(data.Promosi_File2 == null && data.Promosi_File22 != "")
                                    {
                                        data.Promosi_File2 = data.Promosi_File22;
                                    }
                                    
                                    data.Promosi_Type = 2;
                                }
                                catch (Exception ex)
                                {
                                    ex.Message.ToString();
                                }
                            }
                            else if (data.Radios == 3)
                            {
                                data.Promosi_Type = 3;
                            }
                                                        
                            data.Promosi_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                            data.Promosi_UpdateBy = Convert.ToString(Session["Username"]);

                            if (PromoDB.AddUpd(data) >= 0)
                            {
                                return Json("update", JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }                    
                }
                return RedirectToAction("Index", "Promo");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(Promo data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.Promosi_UpdateBy = Convert.ToString(Session["Username"]);
                    PromoDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}