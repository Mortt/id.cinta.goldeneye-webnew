﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web.Security;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace b2bWeb.Controllers
{
    public class StockReport2Controller : Controller
    {
        StockReportDB StockReportDB = new StockReportDB();

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                string Month = DateTime.Now.Month.ToString();
                string Year = DateTime.Now.Year.ToString();
                string all = Month + "/1/" + Year;

                ViewBag.Title = "Report / Daily Opname";

                DateTime a = Convert.ToDateTime(all);
                DateTime b = DateTime.Now;
                DataTable A = new DataTable();
                A = StockReportDB.StockReport2(a, b);

                //if (Convert.ToString(StartDate) == null && Convert.ToString(EndDate) == null)
                //{
                //     A = StockReportDB.StockReport2(a, b);
                //    //ViewBag.data = A;
                //    //ViewData["StartDate"] = a;                                                
                //    //ViewData["EndDate"] = b;
                //}
                //else
                //{
                //     A = StockReportDB.StockReport2(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate));
                //    //ViewData["StartDate"] = StartDate;
                //    //ViewData["EndDate"] = EndDate;
                //}

                return View(A);
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult ExportToExcel(DateTime StartDate, DateTime EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                var g = Convert.ToDateTime(StartDate.ToString("MM/dd/yyyy"));
                var c = Convert.ToDateTime(EndDate.ToString("MM/dd/yyyy"));

               DataTable A = StockReportDB.StockReport2(g, c);

                A.AcceptChanges();

                XLWorkbook wbook = new XLWorkbook();
                var wr = wbook.Worksheets.Add(A, "Sheet1");
                wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                // Prepare the response
                HttpResponseBase httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Provide you file name here
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"Daily Opname( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xlsx\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wbook.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
                return View("");
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}