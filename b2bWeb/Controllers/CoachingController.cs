﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class CoachingController : Controller
    {
        CoachingDB CoachingDB = new CoachingDB();
        DateTime Now = DateTime.Now;
        string B;
        string T;

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Coaching";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataCoaching()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = CoachingDB.GetDataCoaching(param) ?? new List<JsonCoaching>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonCoaching Baru = new JsonCoaching();
                Baru.total = 0;
                Baru.rows = new List<rowsCoaching>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var W = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Coaching(" + W.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    var Coachings = CoachingDB.ListExport(W, c);

                    var json_Coachings = JsonConvert.SerializeObject(Coachings);
                    DataTable dt_json_Coachings = (DataTable)JsonConvert.DeserializeObject(json_Coachings, (typeof(DataTable)));

                    if (dt_json_Coachings.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Coaching");
                    }

                    dt_json_Coachings.Columns.Remove("Coaching_Isdelete");
                    dt_json_Coachings.Columns.Remove("Coaching_UpdateBy");
                    dt_json_Coachings.Columns.Remove("Coaching_UpdateOn");
                    dt_json_Coachings.Columns.Remove("Coaching_CompanyID");
                    dt_json_Coachings.Columns.Remove("Coaching_ReasonID");
                    dt_json_Coachings.Columns.Remove("Coaching_ReffID");
                    dt_json_Coachings.Columns.Remove("Coaching_With");
                    dt_json_Coachings.Columns.Remove("Coaching_ID");

                    dt_json_Coachings.Columns.Add("Coaching_By", typeof(string));
                    dt_json_Coachings.Columns.Add("Coaching_With", typeof(string));
                    dt_json_Coachings.Columns.Add("Start", typeof(DateTime));
                    dt_json_Coachings.Columns.Add("End", typeof(DateTime));
                    dt_json_Coachings.Columns.Add("Coaching_Insert_On", typeof(string));

                    for (int g = 0; g < dt_json_Coachings.Rows.Count; g++)
                    {
                        dt_json_Coachings.Rows[g]["Coaching_By"] = dt_json_Coachings.Rows[g]["CoachingInsertBy"];
                        dt_json_Coachings.Rows[g]["Coaching_With"] = dt_json_Coachings.Rows[g]["CoachingWith"];
                        dt_json_Coachings.Rows[g]["Start"] = Convert.ToDateTime(dt_json_Coachings.Rows[g]["Coaching_Start"]).ToString("HH:mm:ss");
                        dt_json_Coachings.Rows[g]["End"] = Convert.ToDateTime(dt_json_Coachings.Rows[g]["Coaching_End"]).ToString("HH:mm:ss");
                        dt_json_Coachings.Rows[g]["Coaching_Insert_On"] = Convert.ToDateTime(dt_json_Coachings.Rows[g]["Coaching_InsertOn"]).ToString("dd/MM/yyyy");
                    }

                    dt_json_Coachings.Columns.Remove("CoachingInsertBy");
                    dt_json_Coachings.Columns.Remove("CoachingWith");
                    dt_json_Coachings.Columns.Remove("Coaching_Start");
                    dt_json_Coachings.Columns.Remove("Coaching_End");
                    dt_json_Coachings.Columns.Remove("Coaching_InsertOn");

                    dt_json_Coachings.Columns["Coaching_By"].SetOrdinal(0);
                    dt_json_Coachings.Columns["Coaching_With"].SetOrdinal(1);
                    dt_json_Coachings.Columns["Coaching_Desc"].SetOrdinal(2);
                    dt_json_Coachings.Columns["Start"].SetOrdinal(3);
                    dt_json_Coachings.Columns["End"].SetOrdinal(4);
                    dt_json_Coachings.Columns["Coaching_Insert_On"].SetOrdinal(5);

                    dt_json_Coachings.AcceptChanges();

                    XLWorkbook wbook = new XLWorkbook();
                    var wr = wbook.Worksheets.Add(dt_json_Coachings, "Sheet1");
                    wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                    wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                    // Prepare the response
                    HttpResponseBase httpResponse = Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Provide you file name here
                    httpResponse.AddHeader("content-disposition", "attachment;filename=\""+name+".xlsx\"");

                    // Flush the workbook to the Response.OutputStream
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        wbook.SaveAs(memoryStream);
                        memoryStream.WriteTo(httpResponse.OutputStream);
                        memoryStream.Close();
                    }
                    httpResponse.End();
                    return View("");
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

        //public ActionResult ExportToExcel(string Coaching_name, string date)
        //{
        //    if (Session["Username"] != null && Session["CompanyID"] != null)
        //    {
        //        try
        //        {
        //            if (date == null)
        //            {
        //                B = Convert.ToString(Now.Month);
        //                T = Convert.ToString(Now.Year);
        //                ViewBag.Tgl = Now.ToString("MMMM yyyy");
        //            }
        //            else
        //            {
        //                B = Convert.ToDateTime(date).ToString("MM");
        //                T = Convert.ToDateTime(date).ToString("yyyy");
        //                ViewBag.Tgl = date;
        //            }

        //            var Coachings = CoachingDB.ListAll(B, T);

        //            if (Coaching_name == null)
        //            {
        //                Coachings = Coachings.ToList();
        //            }
        //            else
        //            {
        //                Coachings = Coachings.Where(p => p.CoachingInsertBy != null && p.CoachingInsertBy.ToLower().Contains(Coaching_name.ToLower()) || p.CoachingInsertBy != null && p.CoachingWith.ToLower().Contains(Coaching_name.ToLower())).ToList();
        //            }

        //            var json_Coachings = JsonConvert.SerializeObject(Coachings);
        //            DataTable dt_json_Coachings = (DataTable)JsonConvert.DeserializeObject(json_Coachings, (typeof(DataTable)));

        //            if (dt_json_Coachings.Rows.Count == 0)
        //            {
        //                TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
        //                return RedirectToAction("index", "Coaching");
        //            }

        //            dt_json_Coachings.Columns.Remove("Coaching_Isdelete");
        //            dt_json_Coachings.Columns.Remove("Coaching_UpdateBy");
        //            dt_json_Coachings.Columns.Remove("Coaching_UpdateOn");
        //            dt_json_Coachings.Columns.Remove("Coaching_CompanyID");
        //            dt_json_Coachings.Columns.Remove("Coaching_ReasonID");
        //            dt_json_Coachings.Columns.Remove("Coaching_ReffID");
        //            dt_json_Coachings.Columns.Remove("Coaching_With");
        //            dt_json_Coachings.Columns.Remove("Coaching_ID");

        //            dt_json_Coachings.Columns.Add("Coaching_By", typeof(string));
        //            dt_json_Coachings.Columns.Add("Coaching_With", typeof(string));
        //            dt_json_Coachings.Columns.Add("Start", typeof(DateTime));
        //            dt_json_Coachings.Columns.Add("End", typeof(DateTime));
        //            dt_json_Coachings.Columns.Add("Coaching_Insert_On", typeof(string));

        //            for(int g = 0; g < dt_json_Coachings.Rows.Count; g++)
        //            {
        //                dt_json_Coachings.Rows[g]["Coaching_By"] = dt_json_Coachings.Rows[g]["CoachingInsertBy"];
        //                dt_json_Coachings.Rows[g]["Coaching_With"] = dt_json_Coachings.Rows[g]["CoachingWith"];
        //                dt_json_Coachings.Rows[g]["Start"] = Convert.ToDateTime(dt_json_Coachings.Rows[g]["Coaching_Start"]).ToString("HH:mm:ss");
        //                dt_json_Coachings.Rows[g]["End"] = Convert.ToDateTime(dt_json_Coachings.Rows[g]["Coaching_End"]).ToString("HH:mm:ss");
        //                dt_json_Coachings.Rows[g]["Coaching_Insert_On"] = Convert.ToDateTime(dt_json_Coachings.Rows[g]["Coaching_InsertOn"]).ToString("dd/MM/yyyy");                        
        //            }

        //            dt_json_Coachings.Columns.Remove("CoachingInsertBy");
        //            dt_json_Coachings.Columns.Remove("CoachingWith");
        //            dt_json_Coachings.Columns.Remove("Coaching_Start");
        //            dt_json_Coachings.Columns.Remove("Coaching_End");
        //            dt_json_Coachings.Columns.Remove("Coaching_InsertOn");

        //            dt_json_Coachings.Columns["Coaching_By"].SetOrdinal(0);
        //            dt_json_Coachings.Columns["Coaching_With"].SetOrdinal(1);
        //            dt_json_Coachings.Columns["Coaching_Desc"].SetOrdinal(2);
        //            dt_json_Coachings.Columns["Start"].SetOrdinal(3);
        //            dt_json_Coachings.Columns["End"].SetOrdinal(4);
        //            dt_json_Coachings.Columns["Coaching_Insert_On"].SetOrdinal(5);

        //            dt_json_Coachings.AcceptChanges();

        //            XLWorkbook wbook = new XLWorkbook();
        //            var wr = wbook.Worksheets.Add(dt_json_Coachings, "Sheet1");
        //            wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
        //            wr.Tables.FirstOrDefault().ShowAutoFilter = false;

        //            // Prepare the response
        //            HttpResponseBase httpResponse = Response;
        //            httpResponse.Clear();
        //            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //            //Provide you file name here
        //            httpResponse.AddHeader("content-disposition", "attachment;filename=\"Coaching.xlsx\"");

        //            // Flush the workbook to the Response.OutputStream
        //            using (MemoryStream memoryStream = new MemoryStream())
        //            {
        //                wbook.SaveAs(memoryStream);
        //                memoryStream.WriteTo(httpResponse.OutputStream);
        //                memoryStream.Close();
        //            }
        //            httpResponse.End();
        //            return View("");
        //        }
        //        catch (Exception ex)
        //        {
        //            ex.Message.ToString();
        //            return RedirectToAction("Error", "Dashboard");
        //        }
        //    }
        //    else
        //    {
        //        return Redirect("/");
        //    }
        //}

    }
}