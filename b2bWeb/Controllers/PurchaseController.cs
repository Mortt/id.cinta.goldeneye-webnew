﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class PurchaseController : Controller
    {
        OrderDB OrdDB = new OrderDB();
       
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }
                                
                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Purchase";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataOrder()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;
            
            var Result = OrdDB.GetDataOrder(param) ?? new List<JsonOrder>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonOrder Baru = new JsonOrder();
                Baru.total = 0;
                Baru.rows = new List<rowsOrder>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetbyID(int ID)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                IList<OrderDetails> listOrderID = new List<OrderDetails>();

                OrderDB dt = new OrderDB();
                listOrderID = dt.ListID(Convert.ToInt32(ID));                
                return Json(listOrderID, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    var Order = OrdDB.ListExport(g, c);

                    var json_Order = JsonConvert.SerializeObject(Order);
                    DataTable dt_Order = (DataTable)JsonConvert.DeserializeObject(json_Order, (typeof(DataTable)));

                    if (dt_Order.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Purchase");
                    }

                    dt_Order.Columns.Add("Order_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_Order.Rows)
                    {
                        objCon["Order_InsertOn1"] = Convert.ToDateTime(objCon["Order_InsertOn"]).ToString("dd/MM/yyyy");
                    }

                    dt_Order.Columns.Remove("TransID");
                    dt_Order.Columns.Remove("Order_Item_ID");
                    dt_Order.Columns.Remove("Order_InsertBy");
                    dt_Order.Columns.Remove("Order_InsertOn");

                    dt_Order.Columns.Add("Order_InsertOn", typeof(string));
                    foreach (DataRow objCon in dt_Order.Rows)
                    {
                        objCon["Order_InsertOn"] = Convert.ToString(objCon["Order_InsertOn1"]);
                    }

                    dt_Order.AcceptChanges();

                    dt_Order.Columns["Employee_Name"].SetOrdinal(0);
                    dt_Order.Columns["Order_InsertOn"].SetOrdinal(1);
                    dt_Order.Columns["TransCode"].SetOrdinal(2);                    
                    dt_Order.Columns["Order_No"].SetOrdinal(3);
                    dt_Order.Columns["Item_Name"].SetOrdinal(4);
                    dt_Order.Columns["Item_Image1"].SetOrdinal(5);
                    dt_Order.Columns["Order_Qty"].SetOrdinal(6);
                    dt_Order.Columns["Order_Price"].SetOrdinal(7);
                    dt_Order.Columns["Order_LineTotal"].SetOrdinal(8);
                    dt_Order.Columns["Order_Total_Qty"].SetOrdinal(9);
                    dt_Order.Columns["Order_Total_Amount"].SetOrdinal(10);

                    dt_Order.Columns.Remove("Order_InsertOn1");

                    dt_Order.AcceptChanges();

                    XLWorkbook wbook = new XLWorkbook();
                    var wr = wbook.Worksheets.Add(dt_Order, "Sheet1");
                    wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                    wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                    // Prepare the response
                    HttpResponseBase httpResponse = Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Provide you file name here
                    httpResponse.AddHeader("content-disposition", "attachment;filename=\"Order( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xlsx\"");

                    // Flush the workbook to the Response.OutputStream
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        wbook.SaveAs(memoryStream);
                        memoryStream.WriteTo(httpResponse.OutputStream);
                        memoryStream.Close();
                    }
                    httpResponse.End();
                    return View("");
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }

    }
}