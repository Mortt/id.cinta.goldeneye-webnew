﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;
using System.Data;
using Newtonsoft.Json;

namespace b2bWeb.Controllers
{
    public class FeedbackController : Controller
    {
        FeedbackDB feedbackDB = new FeedbackDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Feedback";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataFeedback()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = feedbackDB.GetDataFeedback(param) ?? new List<JsonFeedback>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonFeedback Baru = new JsonFeedback();
                Baru.total = 0;
                Baru.rows = new List<rowsFeedback>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate, bool image)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    string name = "Feedback(" + g.ToString("dd/MM/yyyy") + " — " + c.ToString("dd/MM/yyyy") + ")";

                    var Feddback = feedbackDB.ListExport(g, c);

                    var json_feedbackDB = JsonConvert.SerializeObject(Feddback);
                    DataTable dt_feedbackDB = (DataTable)JsonConvert.DeserializeObject(json_feedbackDB, (typeof(DataTable)));

                    if (dt_feedbackDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Feedback");
                    }

                    dt_feedbackDB.Columns.Add("Feedback_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_feedbackDB.Rows)
                    {
                        objCon["Feedback_InsertOn1"] = Convert.ToString(objCon["Feedback_InsertOn"]);
                    }

                    dt_feedbackDB.Columns.Remove("Feedback_OutletCode");
                    //dt_feedbackDB.Columns.Remove("Feedback_Image");
                    dt_feedbackDB.Columns.Remove("Feedback_RespondID");
                    dt_feedbackDB.Columns.Remove("Feedback_UpdateBy");
                    dt_feedbackDB.Columns.Remove("Feedback_UpdateOn");
                    dt_feedbackDB.Columns.Remove("Feedback_InsertOn");

                    dt_feedbackDB.Columns.Add("Feedback_InsertOn", typeof(string));

                    foreach (DataRow objCon in dt_feedbackDB.Rows)
                    {
                        objCon["Feedback_InsertOn"] = Convert.ToString(objCon["Feedback_InsertOn1"]);
                    }

                    dt_feedbackDB.Columns.Remove("Feedback_InsertOn1");

                    dt_feedbackDB.AcceptChanges();


                    dt_feedbackDB.Columns["Feedback_Employee_Name"].SetOrdinal(0);
                    dt_feedbackDB.Columns["Feedback_ProductCode"].SetOrdinal(1);
                    dt_feedbackDB.Columns["Feedback_Product_Name"].SetOrdinal(2);
                    dt_feedbackDB.Columns["Feedback_Desc"].SetOrdinal(3);
                    dt_feedbackDB.Columns["Feedback_Image"].SetOrdinal(4);
                    dt_feedbackDB.Columns["Feedback_Respond_Name"].SetOrdinal(5);
                    dt_feedbackDB.Columns["Feedback_InsertOn"].SetOrdinal(6);
                    dt_feedbackDB.Columns["Feedback_Isdelete"].SetOrdinal(7);

                    dt_feedbackDB.AcceptChanges();

                    dt_feedbackDB.Columns.Remove("Feedback_ID");
                    dt_feedbackDB.Columns.Remove("Feedback_InsertBy");

                    if (image == false)
                    {
                        TempData["DT_JSON"] = dt_feedbackDB;
                        return RedirectToAction("ToExcel", "Dashboard", new { name });
                    }
                    else
                    {
                        dt_feedbackDB.AcceptChanges();

                        for (int a = 0; a < dt_feedbackDB.Rows.Count; a++)
                        {
                            string url = Convert.ToString(dt_feedbackDB.Rows[a]["Feedback_Image"]);
                            string fileName = String.Join(string.Empty, url.Substring(url.LastIndexOf('/') + 1).Split('-'));
                            dt_feedbackDB.Rows[a]["Feedback_Image"] = fileName;
                        }

                        TempData["DT_JSON"] = dt_feedbackDB;

                        int row = 1;
                        int column = 5;
                        string TableImageName = "Feedback_Image";
                        return RedirectToAction("ToExcelImage", "Dashboard", new { name, TableImageName, row, column });
                    }
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
        
    }
}