﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b2bWeb.Models;
using MvcPaging;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace b2bWeb.Controllers
{
    public class SellingController : Controller
    {
        SalesDB salesDB = new SalesDB();        

        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Transaction / Selling";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataSelling()
        {
            Parameter param = new Parameter();
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];
            string filter = Request.QueryString["filter"];

            if (filter != null && filter != "")
            {
                string Value = Convert.ToString(filter);
                string[] ValueOke = Value.Split('-');
                param.startDate = ValueOke[0];
                param.endDate = ValueOke[1];
            }

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = salesDB.GetDataSelling(param) ?? new List<JsonSales>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonSales Baru = new JsonSales();
                Baru.total = 0;
                Baru.rows = new List<rowsSales>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetbyID(int data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                SalesDetailDB dt = new SalesDetailDB();
                List<SalesDetail>  listSalesID = dt.ListAll(data);
                return Json(listSalesID, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult ExportToExcel(DateTime startDate, DateTime endDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    var g = Convert.ToDateTime(startDate.ToString("MM/dd/yyyy"));
                    var c = Convert.ToDateTime(endDate.ToString("MM/dd/yyyy"));

                    var Selling = salesDB.ListExport(g, c);

                    var json_salesDB = JsonConvert.SerializeObject(Selling);
                    DataTable dt_salesDB = (DataTable)JsonConvert.DeserializeObject(json_salesDB, (typeof(DataTable)));

                    if (dt_salesDB.Rows.Count == 0)
                    {
                        TempData["Error"] = "tidak ditemukan data dengan periode yang anda cari";
                        return RedirectToAction("index", "Selling");
                    }

                    dt_salesDB.Columns.Add("Sales_InsertOn1", typeof(string));

                    foreach (DataRow objCon in dt_salesDB.Rows)
                    {
                        objCon["Sales_InsertOn1"] = Convert.ToString(objCon["Sales_InsertOn"]);
                    }

                    //dt_salesDB.Columns.Remove("Sales_Transdate");
                    dt_salesDB.Columns.Remove("Sales_Merchant_ID");
                    dt_salesDB.Columns.Remove("Sales_Customer_ID");
                    dt_salesDB.Columns.Remove("Sales_Type_ID");
                    dt_salesDB.Columns.Remove("Sales_Address");
                    dt_salesDB.Columns.Remove("Sales_Contact_ID");
                    dt_salesDB.Columns.Remove("Sales_Delivery_ID");
                    dt_salesDB.Columns.Remove("Sales_Delivery_Contact");
                    dt_salesDB.Columns.Remove("Sales_Delivery_Name");
                    dt_salesDB.Columns.Remove("Sales_Delivery_InsertOn");
                    dt_salesDB.Columns.Remove("Sales_Promo_ID");
                    dt_salesDB.Columns.Remove("Sales_Table_No");
                    dt_salesDB.Columns.Remove("Sales_Promotion_Amout");
                    dt_salesDB.Columns.Remove("Sales_Gross_Amount");
                    dt_salesDB.Columns.Remove("Sales_PPN_Amount");
                    dt_salesDB.Columns.Remove("Sales_Nett_amount");
                    dt_salesDB.Columns.Remove("Sales_isServed");
                    dt_salesDB.Columns.Remove("Sales_ServedOn");
                    dt_salesDB.Columns.Remove("Sales_ServedBy");
                    dt_salesDB.Columns.Remove("Sales_isPaid");
                    dt_salesDB.Columns.Remove("Sales_InsertOn");
                    dt_salesDB.Columns.Remove("Sales_UpdateOn");
                    dt_salesDB.Columns.Remove("Sales_UpdateBy");
                    dt_salesDB.Columns.Add("Sales_InsertOn", typeof(string));
                    dt_salesDB.Columns.Add("Sales_Outlet", typeof(string));
                    dt_salesDB.Columns.Add("Sales_Outlet_Code", typeof(string));

                    foreach (DataRow objCon in dt_salesDB.Rows)
                    {
                        if (Convert.ToString(objCon["Selling_Outlet_Code"]) == "0")
                        {
                            objCon["Sales_Outlet_Code"] = "-";
                            objCon["Sales_Outlet"] = "-";
                        }
                        else
                        {
                            objCon["Sales_Outlet_Code"] = objCon["Selling_Outlet_Code"];
                            objCon["Sales_Outlet"] = objCon["Selling_Outlet"];
                        }

                        objCon["Sales_InsertOn"] = Convert.ToString(objCon["Sales_InsertOn1"]);
                    }

                    dt_salesDB.Columns.Remove("Selling_Outlet_Code");
                    dt_salesDB.Columns.Remove("Selling_Outlet");
                    dt_salesDB.Columns.Remove("Sales_InsertOn1");
                    dt_salesDB.Columns.Remove("TransID");
                    dt_salesDB.Columns.Remove("Sales_Desc");
                    dt_salesDB.Columns.Remove("Sales_CompanyID");
                    dt_salesDB.Columns.Remove("Sales_InsertBy");
                    dt_salesDB.Columns.Remove("Sales_Item_ID");

                    dt_salesDB.AcceptChanges();

                    dt_salesDB.Columns["Sales_Insert_Name"].SetOrdinal(0);
                    dt_salesDB.Columns["TransCode"].SetOrdinal(1);
                    dt_salesDB.Columns["Sales_Transdate"].SetOrdinal(2);
                    dt_salesDB.Columns["Sales_Total_Qty"].SetOrdinal(3);
                    dt_salesDB.Columns["Sales_Total_Amount"].SetOrdinal(4);
                    dt_salesDB.Columns["Sales_Product_Name"].SetOrdinal(5);
                    dt_salesDB.Columns["Sales_Product_Image"].SetOrdinal(6);
                    dt_salesDB.Columns["Sales_Price"].SetOrdinal(7);
                    dt_salesDB.Columns["Sales_Qty"].SetOrdinal(8);
                    dt_salesDB.Columns["Sales_Outlet_Code"].SetOrdinal(9);
                    dt_salesDB.Columns["Sales_Outlet"].SetOrdinal(10);
                    dt_salesDB.Columns["Sales_LineTotal"].SetOrdinal(11);
                    dt_salesDB.Columns["Sales_InsertOn"].SetOrdinal(12);
                    dt_salesDB.Columns["Sales_IsDelete"].SetOrdinal(13);

                    dt_salesDB.AcceptChanges();

                    XLWorkbook wbook = new XLWorkbook();
                    var wr = wbook.Worksheets.Add(dt_salesDB, "Sheet1");
                    wr.Tables.FirstOrDefault().Theme = XLTableTheme.None;
                    wr.Tables.FirstOrDefault().ShowAutoFilter = false;

                    // Prepare the response
                    HttpResponseBase httpResponse = Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Provide you file name here
                    httpResponse.AddHeader("content-disposition", "attachment;filename=\"Selling( " + g.ToString("MM/dd/yyyy") + " — " + c.ToString("MM/dd/yyyy") + " ).xlsx\"");

                    // Flush the workbook to the Response.OutputStream
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        wbook.SaveAs(memoryStream);
                        memoryStream.WriteTo(httpResponse.OutputStream);
                        memoryStream.Close();
                    }
                    httpResponse.End();
                    return View("");
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return RedirectToAction("Error", "Dashboard");
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}