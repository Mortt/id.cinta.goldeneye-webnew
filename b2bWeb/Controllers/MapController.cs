﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using b2bWeb.Models;
using Newtonsoft.Json;

namespace b2bWeb.Controllers
{
    public class MapController : Controller
    {
        EmployeeDB employeeDB = new EmployeeDB();
        private IList<Employee> allEmployee = new List<Employee>();
        // GET: Map

        List<Map> map = new List<Map>();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                allEmployee = employeeDB.ListAll();

                var orderEmployee = from A in allEmployee
                                    orderby A.Employee_Name
                                    select A;

                string Month = DateTime.Now.Month.ToString();
                string Year = DateTime.Now.Year.ToString();
                string all = Month + "/1/" + Year;

                DateTime a = Convert.ToDateTime(all);
                DateTime b = DateTime.Now;

                ViewBag.StartDate = a.Date.ToString("MM/dd/yyyy");
                ViewBag.EndDate = b.Date.ToString("MM/dd/yyyy");

                string markers = "[";
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                SqlCommand cmd = new SqlCommand("SP_GetMap_UserDate");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("username", "");
                cmd.Parameters.AddWithValue("Search", "|");
                cmd.Parameters.AddWithValue("StartDate", a);
                cmd.Parameters.AddWithValue("EndDate", b);
                
                cmd.Parameters.AddWithValue("Company_ID", Session["CompanyID"]);

                DataTable dt = new DataTable();
                using (SqlConnection conn = new SqlConnection(cs))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    dt.Load(cmd.ExecuteReader());
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            markers += "{";
                            markers += string.Format("'title': '{0}',", reader["Area_Name"]);
                            markers += string.Format("'lat': '{0}',", Convert.ToDecimal(reader["Visit_Latitude"]));
                            markers += string.Format("'lng': '{0}',", Convert.ToDecimal(reader["Visit_Longitude"]));
                            markers += string.Format("'Employee_Name': '{0}',", reader["Employee_Name"]);
                            markers += string.Format("'Date': '{0}',", Convert.ToDateTime(reader["Visit_InsertOn"]).ToString("MM/dd/yyyy"));
                            markers += string.Format("'Visit_Checkin': '{0}',", reader["Visit_Checkin"]);
                            markers += string.Format("'Visit_Checkout': '{0}',", reader["Visit_Checkout"]);
                            markers += string.Format("'Outlet_Name': '{0}',", reader["Outlet_Name"]);
                            markers += string.Format("'number': '{0}',", reader["number"]);
                            markers += string.Format("'Visit_Image': '{0}',", reader["Visit_Image"]);
                            markers += string.Format("'Visit_Nik': '{0}',", reader["Visit_Nik"]);
                            markers += "},";
                        }
                    }
                    conn.Close();
                }
                markers += "];";

                var x = (from r in dt.AsEnumerable()
                         select r["Visit_Nik"]).Distinct().ToList();

                for (int G = 0; G < x.Count; G++)
                {
                    List<_LatLng> LatLngA = new List<_LatLng>();

                    for (int Q = 0; Q < dt.Rows.Count; Q++)
                    {
                        if(Convert.ToString(x[G]) == Convert.ToString(dt.Rows[Q]["Visit_Nik"]))
                        {
                            _LatLng itm = new _LatLng()
                            {
                                lat = Convert.ToString(dt.Rows[Q]["Visit_Latitude"]),
                                lng = Convert.ToString(dt.Rows[Q]["Visit_Longitude"]),
                                Employee_Name = Convert.ToString(dt.Rows[Q]["Employee_Name"]),
                                Date = Convert.ToDateTime(dt.Rows[Q]["Visit_InsertOn"]).ToString("MM/dd/yyyy"),
                                Outlet_Name = Convert.ToString(dt.Rows[Q]["Outlet_Name"]),
                                Visit_Checkin = Convert.ToString(dt.Rows[Q]["Visit_Checkin"]),
                                Visit_Checkout = Convert.ToString(dt.Rows[Q]["Visit_Checkout"]),
                                number = Convert.ToString(dt.Rows[Q]["number"]),
                                Visit_Image = Convert.ToString(dt.Rows[Q]["Visit_Image"])
                            };
                            LatLngA.Add(itm);
                        }                        
                    }

                    for (int Q = 0; Q < dt.Rows.Count; Q++)
                    {
                        if (Convert.ToString(x[G]) == Convert.ToString(dt.Rows[Q]["Visit_Nik"]))
                        {
                            Map pam = new Map()
                            {
                                title = Convert.ToString(dt.Rows[Q]["Area_Name"]),
                                lat = Convert.ToString(dt.Rows[Q]["Visit_Latitude"]),
                                lng = Convert.ToString(dt.Rows[Q]["Visit_Longitude"]),
                                Employee_Name = Convert.ToString(dt.Rows[Q]["Employee_Name"]),
                                Date = Convert.ToDateTime(dt.Rows[Q]["Visit_InsertOn"]).ToString("MM/dd/yyyy"),
                                Visit_Checkin = Convert.ToString(dt.Rows[Q]["Visit_Checkin"]),
                                Visit_Checkout = Convert.ToString(dt.Rows[Q]["Visit_Checkout"]),
                                Outlet_Name = Convert.ToString(dt.Rows[Q]["Outlet_Name"]),
                                number = Convert.ToString(dt.Rows[Q]["number"]),
                                Visit_Image = Convert.ToString(dt.Rows[Q]["Visit_Image"]),
                                Visit_Nik = Convert.ToString(dt.Rows[Q]["Visit_Nik"]),
                                LatLng = LatLngA
                            };

                            map.Add(pam);
                            break;
                        }                        
                    }                    
                }

                if (markers.Count() == 3)
                {
                    ViewBag.Yes = "no";
                }

                ViewBag.Employee = orderEmployee;
                ViewBag.DisNo = JsonConvert.SerializeObject(x);
                ViewBag.Markers = markers;
                ViewBag.MapJos = JsonConvert.SerializeObject(map);
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public ActionResult Index(string name, DateTime StartDate, DateTime EndDate)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                allEmployee = employeeDB.ListAll();

                var orderEmployee = from A in allEmployee
                                    orderby A.Employee_Name
                                    select A;

                string Month = DateTime.Now.Month.ToString();
                string Year = DateTime.Now.Year.ToString();
                string all = Month + "/1/" + Year;

                DateTime a, b;
                string cari, username;

                if (Convert.ToString(StartDate) != "" && Convert.ToString(EndDate) != "")
                {
                     a = StartDate;
                     b = EndDate;
                }
                else
                {
                     a = Convert.ToDateTime(all);
                     b = DateTime.Now;
                }

                if (name == "")
                {
                    username = "";
                    cari = "|";
                }
                else
                {
                    username = name;
                    cari = "";
                }

                ViewBag.StartDate = a.Date.ToString("MM/dd/yyyy");
                ViewBag.EndDate = b.Date.ToString("MM/dd/yyyy");
                ViewData["name"] = username;

                string markers = "[";  
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                SqlCommand cmd = new SqlCommand("SP_GetMap_UserDate");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("username", username);
                cmd.Parameters.AddWithValue("Search", cari);
                cmd.Parameters.AddWithValue("StartDate", a);
                cmd.Parameters.AddWithValue("EndDate", b);

                cmd.Parameters.AddWithValue("Company_ID", Session["CompanyID"]);

                DataTable dt = new DataTable();
                using (SqlConnection conn = new SqlConnection(cs))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    dt.Load(cmd.ExecuteReader());
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            markers += "{";
                            markers += string.Format("'title': '{0}',", reader["Area_Name"]);
                            markers += string.Format("'lat': '{0}',", Convert.ToDecimal(reader["Visit_Latitude"]));
                            markers += string.Format("'lng': '{0}',", Convert.ToDecimal(reader["Visit_Longitude"]));
                            markers += string.Format("'Employee_Name': '{0}',", reader["Employee_Name"]);
                            markers += string.Format("'Date': '{0}',", Convert.ToDateTime(reader["Visit_InsertOn"]).ToString("MM/dd/yyyy"));
                            markers += string.Format("'Visit_Checkin': '{0}',", reader["Visit_Checkin"]);
                            markers += string.Format("'Visit_Checkout': '{0}',", reader["Visit_Checkout"]);
                            markers += string.Format("'Outlet_Name': '{0}',", reader["Outlet_Name"]);
                            markers += string.Format("'number': '{0}',", reader["number"]);
                            markers += string.Format("'Visit_Image': '{0}',", reader["Visit_Image"]);
                            markers += string.Format("'Visit_Nik': '{0}',", reader["Visit_Nik"]);
                            markers += "},";
                        }
                    }
                    conn.Close();
                }              

                markers += "];";

                var x = (from r in dt.AsEnumerable()
                         select r["Visit_Nik"]).Distinct().ToList();

                for (int G = 0; G < x.Count; G++)
                {
                    List<_LatLng> LatLngA = new List<_LatLng>();

                    for (int Q = 0; Q < dt.Rows.Count; Q++)
                    {
                        if (Convert.ToString(x[G]) == Convert.ToString(dt.Rows[Q]["Visit_Nik"]))
                        {
                            _LatLng itm = new _LatLng()
                            {
                                lat = Convert.ToString(dt.Rows[Q]["Visit_Latitude"]),
                                lng = Convert.ToString(dt.Rows[Q]["Visit_Longitude"]),
                                Employee_Name = Convert.ToString(dt.Rows[Q]["Employee_Name"]),
                                Date = Convert.ToDateTime(dt.Rows[Q]["Visit_InsertOn"]).ToString("MM/dd/yyyy"),
                                Outlet_Name = Convert.ToString(dt.Rows[Q]["Outlet_Name"]),
                                Visit_Checkin = Convert.ToString(dt.Rows[Q]["Visit_Checkin"]),
                                Visit_Checkout = Convert.ToString(dt.Rows[Q]["Visit_Checkout"]),
                                number = Convert.ToString(dt.Rows[Q]["number"]),
                                Visit_Image = Convert.ToString(dt.Rows[Q]["Visit_Image"])
                            };
                            LatLngA.Add(itm);
                        }
                    }

                    for (int Q = 0; Q < dt.Rows.Count; Q++)
                    {
                        if (Convert.ToString(x[G]) == Convert.ToString(dt.Rows[Q]["Visit_Nik"]))
                        {
                            Map pam = new Map()
                            {
                                title = Convert.ToString(dt.Rows[Q]["Area_Name"]),
                                lat = Convert.ToString(dt.Rows[Q]["Visit_Latitude"]),
                                lng = Convert.ToString(dt.Rows[Q]["Visit_Longitude"]),
                                Employee_Name = Convert.ToString(dt.Rows[Q]["Employee_Name"]),
                                Date = Convert.ToDateTime(dt.Rows[Q]["Visit_InsertOn"]).ToString("MM/dd/yyyy"),
                                Visit_Checkin = Convert.ToString(dt.Rows[Q]["Visit_Checkin"]),
                                Visit_Checkout = Convert.ToString(dt.Rows[Q]["Visit_Checkout"]),
                                Outlet_Name = Convert.ToString(dt.Rows[Q]["Outlet_Name"]),
                                number = Convert.ToString(dt.Rows[Q]["number"]),
                                Visit_Image = Convert.ToString(dt.Rows[Q]["Visit_Image"]),
                                Visit_Nik = Convert.ToString(dt.Rows[Q]["Visit_Nik"]),
                                LatLng = LatLngA
                            };

                            map.Add(pam);
                            break;
                        }
                    }
                }

                if (markers.Count() == 3)
                {
                    ViewBag.Yes = "no";
                }

                ViewBag.Employee = orderEmployee;
                ViewBag.DisNo = JsonConvert.SerializeObject(x);
                ViewBag.Markers = markers;
                ViewBag.MapJos = JsonConvert.SerializeObject(map);
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return Redirect("/");
        }


    }
}