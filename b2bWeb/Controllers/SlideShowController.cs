﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using b2bWeb.Models;

namespace b2bWeb.Controllers
{
    public class SlideShowController : Controller
    {
        SlideShowDB SlideShowDB = new SlideShowDB();
        public ActionResult Index()
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                string actionName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (DashboardController.Paket(actionName) == true)
                {

                }
                else
                {
                    return RedirectToAction("Error", "Dashboard");
                }

                ViewBag.HMenu = DashboardController.HeaderMenu();
                ViewBag.Title = "Master / SlideShow";
                return View();
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult GetDataSlideShow()
        {
            string PageNumber = Request.QueryString["pageNumber"];
            string pageSize = Request.QueryString["pageSize"];
            string searchText = Request.QueryString["searchText"];

            Parameter param = new Parameter();

            param.Company_ID = Convert.ToInt32(Session["CompanyID"]);
            param.PageNumber = Convert.ToInt32(PageNumber);
            param.RowspPage = Convert.ToInt32(pageSize);
            param.Search = searchText;

            var Result = SlideShowDB.GetDataSildeShow(param) ?? new List<JsonSlideShow>();

            if (Result.Count != 0)
            {
                return Json(Result[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonSlideShow Baru = new JsonSlideShow();
                Baru.total = 0;
                Baru.rows = new List<rowsSlideShow>();

                return Json(Baru, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddSlideShow(SlideShow data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                if (data.SDS_IDX == null)
                {
                    string checkbase64 = "base64";
                    if (Request.Form["SDS_URL"].Contains(checkbase64))
                    {
                        string base64 = Request.Form["SDS_URL"];
                        string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                        string b = DashboardController.RandomString("B2bGoldeneye");
                        string c = "slideShowImages" + a + b + ".png";
                        data.SDS_URL = "https://foodiegost17.blob.core.windows.net/images/" + c;
                        data.SDS_WEBSITE = "https://foodiegost17.blob.core.windows.net/images/" + c;

                        DashboardController.UploadToAzure(base64, c);
                    }
                    else if (Request.Form["SDS_URL"] != "")
                    {
                        data.SDS_URL = data.SDS_URL;
                        data.SDS_WEBSITE = data.SDS_WEBSITE;
                    }
                    else
                    {
                        data.SDS_URL = null;
                        data.SDS_WEBSITE = null;
                    }

                    try
                    {
                        if(Convert.ToInt16(Session["CompanyID"]) == 1)
                        {
                            data.SDS_Application = "APA";
                        }
                        else
                        {
                            data.SDS_Application = "GE";
                        }

                        data.SDS_CompanyID = Convert.ToInt16(Session["CompanyID"]);
                        data.SDS_INSERTBY = Convert.ToString(Session["Username"]);
                        try
                        {
                            SlideShowDB.AddUpdate(data);
                            return Json("success", JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                            return Json("failed", JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                        return Json("failed", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    try
                    {
                        string checkbase64 = "base64";
                        if (Request.Form["SDS_URL"].Contains(checkbase64))
                        {
                            string base64 = Request.Form["SDS_URL"];
                            string a = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy-");
                            string b = DashboardController.RandomString("B2bGoldeneye");
                            string c = "slideShowImages" + a + b + ".png";
                            //data.Category_Image = "https://foodiegost17.blob.core.windows.net/images/" + c;
                            data.SDS_URL = "https://foodiegost17.blob.core.windows.net/images/" + c;
                            data.SDS_WEBSITE = "https://foodiegost17.blob.core.windows.net/images/" + c;

                            DashboardController.UploadToAzure(base64, c);
                        }
                        else if (Request.Form["SDS_URL"] != "")
                        {
                            data.SDS_URL = data.SDS_URL;
                            data.SDS_WEBSITE = data.SDS_URL;
                        }
                        else
                        {
                            data.SDS_URL = null;
                            data.SDS_WEBSITE = null;
                        }

                        if (Convert.ToInt16(Session["CompanyID"]) == 1)
                        {
                            data.SDS_Application = "APA";
                        }
                        else
                        {
                            data.SDS_Application = "GE";
                        }

                        data.SDS_UPDATEBY = Convert.ToString(Session["Username"]);

                        if (SlideShowDB.AddUpdate(data) >= 0)
                        {
                            return Json("update", JsonRequestBehavior.AllowGet);
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                        return Json("failed", JsonRequestBehavior.AllowGet);
                    }
                }
                return RedirectToAction("Index", "SlideShow");
            }
            else
            {
                return Redirect("/");
            }
        }

        public ActionResult Delete(SlideShow data)
        {
            if (Session["Username"] != null && Session["CompanyID"] != null)
            {
                try
                {
                    data.SDS_UPDATEBY = Convert.ToString(Session["Username"]);
                    SlideShowDB.Delete(data);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}